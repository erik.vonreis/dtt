add_compile_options(
        -fPIC
)

add_compile_definitions(
        DYNAMIC_LOAD=0
        _DYNAMIC_LOAD=0
)

add_link_options()

if(DEFINED EXTRA_LINK_DIRS)
    MESSAGE(STATUS "Extra Link Dirs: ${EXTRA_LINK_DIRS}")
    link_directories(BEFORE ${EXTRA_LINK_DIRS})
endif()


if(NOT DEFINED INCLUDE_INSTALL_DIR)
    MESSAGE(STATUS "INCLUDE_INSTALL_DIR not defined. Setting to 'include'")
    SET(INCLUDE_INSTALL_DIR "include")
else()
    MESSAGE(STATUS "INCLUDE_INSTALL_DIR set to ${INCLUDE_INSTALL_DIR}")
endif()

SET(ABS_INCLUDE_DIR "${CMAKE_INSTALL_PREFIX}/${INCLUDE_INSTALL_DIR}")
SET(CMAKE_REQUIRED_INCLUDES ${CMAKE_REQUIRED_INCLUDES} ${ABS_INCLUDE_DIR})

#OS specific settings
if(APPLE)
    # force to ".so"
    # this is compatible with ROOT, should we ever use it on the Mac
    # it's also hard-coded throughout cmake and some dynamic library loads
    set(CMAKE_SHARED_LIBRARY_SUFFIX .so)
    
    SET(ONLY_FOTON TRUE)
	# /opt/local is the install folder for macports on OSX Mojave ... maybe there's a way to look this up
        SET(CMAKE_REQUIRED_INCLUDES ${CMAKE_REQUIRED_INCLUDES} /opt/local/include)
	# include_directories(BEFORE /opt/local/include)
	add_compile_definitions(
		HAVE_ISFINITE
		AVOID_SIGNALS
	)
	#include_directories(os/osx/include)
	#link_directories(/opt/local/lib)
elseif(UNIX)

endif()


FIND_PACKAGE(PkgConfig)

#setup gds
find_path(GDS_INCLUDE_DIR gds_memory.hh
	HINTS /usr/ / ${ABS_INCLUDE_DIR}
    PATH_SUFFIXES gds include/gds
    CMAKE_FIND_ROOT_PATH_BOTH)

MESSAGE(STATUS "GDS_INCLUDE_DIR=${GDS_INCLUDE_DIR}")

include_directories(BEFORE SYSTEM
        ${GDS_INCLUDE_DIR}
        ${GDS_INCLUDE_DIR}/calibration
        ${GDS_INCLUDE_DIR}/dfm
        ${GDS_INCLUDE_DIR}/fantom
        ${GDS_INCLUDE_DIR}/framefast
        ${GDS_INCLUDE_DIR}/fSeries
        ${GDS_INCLUDE_DIR}/lmsg
        ${GDS_INCLUDE_DIR}/memmap
        ${GDS_INCLUDE_DIR}/refcount
        ${GDS_INCLUDE_DIR}/xml
        #${GDS_INCLUDE_DIR}/xsil
        )


#find nds2client

if(NOT ONLY_FOTON)
    FIND_PACKAGE(readline)
    FIND_PROGRAM(ROOTCINT rootcint)
    MESSAGE(STATUS "rootcint: ${ROOTCINT}")
    #setup root
    set(ROOT_FIND_COMPONENTS Gui)
    FIND_PACKAGE(ROOT)
    link_directories(BEFORE ${ROOT_LIBRARY_DIR})
    include_directories(BEFORE SYSTEM ${ROOT_INCLUDE_DIR})


    find_path(NDS2CLIENT_INCLUDE_DIR nds.hh
            PATH_SUFFIXES "nds2-client")

    if(NOT NDS2CLIENT_INCLUDE_DIR)
        MESSAGE(FATAL_ERROR "Could not find nds2-client headers")
    endif()

    MESSAGE(STATUS "NDS2CLIENT_INCLUDE_DIR=${NDS2CLIENT_INCLUDE_DIR}")

    include_directories(BEFORE SYSTEM
            ${NDS2CLIENT_INCLUDE_DIR})

    # error out if no ROOT
    if(NOT(${ROOT_FOUND}))
        MESSAGE(FATAL_ERROR "Root needs to be installed")
    else()
        MESSAGE(STATUS "Root found version ${ROOT_VERSION}")
    endif()

    # get C++ standard from ROOT.  Ours must match ROOT's.
    EXECUTE_PROCESS(COMMAND ${ROOT_CONFIG_EXECUTABLE} --cflags OUTPUT_VARIABLE ROOT_CPP_FLAGS)
    MESSAGE(STATUS "ROOT_CPP_FLAGS = ${ROOT_CPP_FLAGS}")
    IF(${ROOT_CPP_FLAGS} MATCHES " -std=c\\+\\+(..)")
        SET(ROOT_CPP_STD ${CMAKE_MATCH_1})
        MESSAGE(STATUS "ROOT_CPP_STD = ${ROOT_CPP_STD}")
    else()
        MESSAGE(FATAL_ERROR "Could not read c++ standard from root-config")
    endif()
    SET(CMAKE_CXX_STANDARD ${ROOT_CPP_STD})
else()
    SET(CMAKE_CXX_STANDARD 14)
endif()


#trim the last '.' from a string and everything after it
#used to help create symlinks for shared libraries
MACRO(TRIM_LAST_DOT name value)
    string(FIND ${value} "\." LAST_DOT REVERSE)
    string(SUBSTRING ${value} 0 ${LAST_DOT} ${name})
ENDMACRO()

if(NOT DEFINED LIB_INSTALL_DIR)
    MESSAGE(STATUS "LIB_INSTALL_DIR not defined.  Setting to 'lib'")
    SET(LIB_INSTALL_DIR "lib")
    SET(ABS_LIB_PATH "${CMAKE_INSTALL_PREFIX}/${LIB_INSTALL_DIR}")
else()
    if(IS_ABSOLUTE ${LIB_INSTALL_DIR})
		SET(ABS_LIB_PATH ${LIB_INSTALL_DIR})
	else()
		SET(ABS_LIB_PATH "${CMAKE_INSTALL_PREFIX}/${LIB_INSTALL_DIR}")
    endif()
    MESSAGE(STATUS "LIB_INSTALL_DIR set to ${LIB_INSTALL_DIR}")
endif()
MESSAGE(STATUS "ABS_LIB_PATH=${ABS_LIB_PATH}")

MACRO(INSTALL_HEADERS target)
    set(extra_args ${ARGN})

    list(LENGTH extra_args num_extra_args)

    set(header_dir include/gds/dtt)
    if(${num_extra_args} GREATER 0)
        list(GET extra_args 0 header_dir)
    endif()

    install(TARGETS ${target}
            PUBLIC_HEADER
              DESTINATION ${header_dir}
              COMPONENT Development
            PRIVATE_HEADER
              DESTINATION ${header_dir}
              COMPONENT Development
            )
ENDMACRO()

# pass a header directory name, otherwise, gds/dtt is used
MACRO(INSTALL_LIB libname)

    set(extra_args ${ARGN})

    list(LENGTH extra_args num_extra_args)

    set(header_dir include/gds/dtt)
    if(${num_extra_args} GREATER 0)
        list(GET extra_args 0 header_dir)
    endif()

    #get the version number for later breakdown
    get_target_property(libversion ${libname} VERSION)

    TRIM_LAST_DOT(libver2 ${libversion})
    TRIM_LAST_DOT(libver1 ${libver2})

    #MESSAGE(STATUS "libversions ${libversion}, ${libver2}, ${libver}")

    install(TARGETS ${libname}
            LIBRARY
                DESTINATION ${LIB_INSTALL_DIR}
                COMPONENT Libraries
                NAMELINK_SKIP
            PUBLIC_HEADER
                DESTINATION ${header_dir}
                COMPONENT Development
            PRIVATE_HEADER
                DESTINATION ${header_dir}
                COMPONENT Development
            )
    if(APPLE)
        install(FILES
                ${CMAKE_CURRENT_BINARY_DIR}/lib${libname}.so
                DESTINATION ${LIB_INSTALL_DIR}
                COMPONENT libraries
                )
    elseif(UNIX)
        #build up symlinks
        execute_process(
                COMMAND ${CMAKE_COMMAND} -E create_symlink lib${libname}.so.${libversion} lib${libname}.so.${libver2}
                COMMAND ${CMAKE_COMMAND} -E create_symlink lib${libname}.so.${libversion} lib${libname}.so.${libver1}
                COMMAND ${CMAKE_COMMAND} -E create_symlink lib${libname}.so.${libversion} lib${libname}.so
        )
        install(FILES
                ${CMAKE_BINARY_DIR}/lib${libname}.so.${libver2}
                ${CMAKE_BINARY_DIR}/lib${libname}.so.${libver1}
                ${CMAKE_BINARY_DIR}/lib${libname}.so
                DESTINATION ${LIB_INSTALL_DIR}
                COMPONENT libraries
                )
    endif()
ENDMACRO()

#join a list with a particular character
#list is in argn
#output goes into variable named outname
function(join_list out_name char )
    SET(_buildstring "")
    MESSAGE(STATUS ${ARGN})
    SET(_first 1)    
    
    foreach(word ${ARGN})
        MESSAGE(STATUS ${word})
        if(_first)
            SET(_first 0)
        else()
            string(APPEND _buildstring ${char})
        endif()
        string(APPEND _buildstring ${word})
    endforeach()
    SET(${out_name} ${_buildstring} PARENT_SCOPE)
endfunction()

#improves CHECK_INCLUDE_FILE by returning 0 in flag if 
#include file does not exist
function(check_include inc_file flag)
    #unset(${flag} CACHE) #force looking each time cmake is called
    #MESSAGE(STATUS "inc:${inc_file}:    flag:${flag}:")
    CHECK_INCLUDE_FILE(${inc_file} ${flag})
    #MESSAGE(STATUS "exists:${_h_exists}:")
    if( "${${flag}}" STREQUAL "")
        SET(${flag} 0 PARENT_SCOPE)
        MESSAGE(WARNING "${inc_file} not found")
    else()
        SET(${flag} 1 PARENT_SCOPE)
    endif()    
endfunction()

#same as check_include() but also sets a compiler define
#with name def and value 1 or 0 depending on existence of 
#include file.  If def is not given, flag is used as name
#of the compiler define
function(define_include inc_file flag)
    #unset(${flag} CACHE)  #force looking each time cmake is called
    check_include(${inc_file} ${flag})
    #MESSAGE(STATUS "flag:${${flag}}")   
    SET(${flag} ${${flag}} PARENT_SCOPE)
    if(${ARGC} GREATER 2)
        SET(_def ${ARGV3})
    else()
        SET(_def ${flag})
    endif()         
    add_compile_definitions(
        ${_def}=${${flag}}
    )       
endfunction()

set_property(GLOBAL PROPERTY CINT_LIBS "")

SET(CINT_DICT_DIR ${CMAKE_BINARY_DIR}/cint_output )
FILE(MAKE_DIRECTORY ${CINT_DICT_DIR} )

IF(ROOT_VERSION VERSION_GREATER_EQUAL "6.20")
    SET(CINT_CREATE_PCM ON)
    MESSAGE(STATUS "Installing PCM files for ROOT dictionaries.")
    SET(ROOT_FLAGS -f -inlineInputHeader)
elseif(ROOT_VERSION VERSION_GREATER_EQUAL "6.0")
    SET(CINT_CREATE_PCM ON)
    MESSAGE(STATUS "Installing PCM files for ROOT dictionaries.")
    SET(ROOT_FLAGS -f)
else()
    SET(CINT_CREATE_PCM OFF)
    MESSAGE(STATUS "Not installing PCM files for ROOT dictionaries")
    SET(ROOT_FLAGS -f)
endif()

#create a target for a cint dictionary output file.
#after the output file name, pass first a list of include directories, 
#like -I/path/to/some/dir
#then a list of all header files needed
#to generate the dictionary
#these should be absolute paths since they are copied directly into the generated file.
SET(cint_link_dir ${CMAKE_BINARY_DIR}/_links)
function(create_cint_dict output_name lib_name version)
    SET(out_fname ${CINT_DICT_DIR}/${output_name}_dict.cc)
    SET(out_header ${CINT_DICT_DIR}/${output_name}_dict.h)

    if(CINT_CREATE_PCM)
        SET(pcm_fname "${CINT_DICT_DIR}/${output_name}_dict_rdict.pcm")
    else()
        UNSET(pcm_fname)
    endif()

    SET(lib_base_name R${lib_name})
    SET(lib_full_name lib${lib_base_name})
    SET(obj_lib_name ${lib_base_name})
    SET(symlink "${cint_link_dir}/lib${obj_lib_name}.so")

    SET(rootcmd "${ROOTCINT} ${ROOT_FLAGS} ${out_fname} ${ARGN}")
    MESSAGE(STATUS "root cmd: ${rootcmd}")
    add_custom_command(OUTPUT ${out_fname} ${out_header} ${pcm_fname}
	    COMMAND ${ROOTCINT} ${ROOT_FLAGS} ${out_fname} ${ARGN}
        COMMAND ${CMAKE_COMMAND} -E make_directory ${cint_link_dir}
        COMMAND ${CMAKE_COMMAND} -E create_symlink ../lib${obj_lib_name}.so ${symlink}
    )

    MESSAGE(STATUS "creating CINT object library target: ${obj_lib_name}")
    add_library(${obj_lib_name} SHARED
        ${out_fname}
    )
    target_include_directories(${obj_lib_name} PUBLIC
	    ${EXTERNAL_INCLUDE_DIRECTORIES}
        ${CMAKE_CURRENT_SOURCE_DIR}
        )

    target_link_libraries(${obj_lib_name} PUBLIC
            )

    set_target_properties( ${obj_lib_name}
            PROPERTIES
            VERSION ${version}
            )

    INSTALL_LIB(${obj_lib_name})

    if(CINT_CREATE_PCM)
        INSTALL(
                FILES ${pcm_fname}
                DESTINATION ${LIB_INSTALL_DIR}
                COMPONENT Libraries
                )
    endif()

#    execute_process(
#            COMMAND ${CMAKE_COMMAND} -E make_directory _links
#            COMMAND ${CMAKE_COMMAND} -E create_symlink ../lib${obj_lib_name}.so _links/lib${obj_lib_name}.so
#    )

    #install the symbolic links from the root directory
    install(FILES
            ${symlink}
            DESTINATION ${LIB_INSTALL_DIR}/root
            COMPONENT libraries
            )

    get_property(tmp GLOBAL PROPERTY CINT_LIBS)
    set_property(GLOBAL PROPERTY CINT_LIBS ${tmp} ${obj_lib_name})

    SET(${output_name}_path ${out_fname} PARENT_SCOPE)
endfunction()

#check for FFT library
check_include("sfftw.h" SFFTW)
check_include("fftw3.h" FFTW3)
define_include("sys/types.h" HAVE_SYS_TYPES_H)
define_include("sys/stat.h" HAVE_SYS_STAT_H)
define_include("stdlib.h" HAVE_STLIB_H)
define_include("string.h" HAVE_STRING_H)
define_include("memory.h" HAVE_MEMORY_H)
define_include("strings.h" HAVE_STRINGS_H)
define_include("inttypes.h" HAVE_INTTYPES_H)
define_include("stdint.h" HAVE_STDINT_H)
define_include("unistd.h" HAVE_UNISTD_H)
define_include("dlfcn.h" HAVE_DLFCN_H)
define_include("memory.h" HAVE_MEMORY_H)
define_include("sys/inotify.h" HAVE_SYS_INOTIFY_H)



if( NOT (${FFTW3} OR ${SFFTW}))
    message(FATAL_ERROR "Either fftw version 3 or sfftw is required")
endif()


if(${FFTW3})
    MESSAGE(STATUS "Found FFTW3")
    add_compile_definitions(
        FFT_DOUBLE=1
        FFTW3=1
    )
    SET(FFTW_LIBS fftw3 fftw3f)
else()
    #MESSAGE(STATUS "Did not find FFTW3")
endif()

#setup python
# on MAC, we avoid finding any Framework python in favor of CONDA
SET(Python_FIND_FRAMEWORK LAST)
SET(Python_FIND_STRATEGY LOCATION)

SET(Python2_FIND_FRAMEWORK LAST)
SET(Python3_FIND_FRAMEWORK LAST)
SET(Python2_FIND_STRATEGY LOCATION)
SET(Python3_FIND_STRATEGY LOCATION)

if(${ENABLE_PYTHON3})
    find_package(Python COMPONENTS Interpreter Development)
    find_package(Python3 COMPONENTS Interpreter Development)
endif()
if(${ENABLE_PYTHON2})
    find_package(Python2 COMPONENTS Interpreter Development)
endif()






if( ENABLE_PYTHON2 AND NOT DEFINED Python2_SITELIB )
    MESSAGE(FATAL_ERROR "Python 2 library installation directory was not found.")
endif()
if( ENABLE_PYTHON3 AND NOT DEFINED Python3_SITELIB)
    MESSAGE(FATAL_ERROR "Python 3 library installation directory was not found.")
endif()

if(${ENABLE_PYTHON2})
    get_filename_component(Python2_PKG_DIR ${Python2_SITELIB} NAME)
    get_filename_component(Python2_BASE_DIR ${Python2_SITELIB} DIRECTORY)
    get_filename_component(Python2_PYTHON_DIR ${Python2_BASE_DIR} NAME)
    SET(Python2_INSTALL_DIR ${LIB_INSTALL_DIR}/${Python2_PYTHON_DIR}/${Python2_PKG_DIR})
    
    MESSAGE(STATUS "Python2 version: ${Python2_VERSION}")
    MESSAGE(STATUS "Python2 lib install dir: ${Python2_SITELIB}")
    MESSAGE(STATUS "Python2 lib install dir: ${Python2_INSTALL_DIR}")
    MESSAGE(STATUS "Python2 include dirs: ${Python2_INCLUDE_DIRS}")
    MESSAGE(STATUS "Python2 libraries: ${Python2_LIBRARIES}")
endif()

if(${ENABLE_PYTHON3})

    # get install directory from SITELIB



    get_filename_component(Python3_LIB_PKG_DIR ${Python3_SITELIB} NAME)
    get_filename_component(Python3_LIB_BASE_DIR ${Python3_SITELIB} DIRECTORY)
    get_filename_component(Python3_LIB_PYTHON_DIR ${Python3_LIB_BASE_DIR} NAME)
    SET(Python3_LIB_INSTALL_DIR ${LIB_INSTALL_DIR}/${Python3_LIB_PYTHON_DIR}/${Python3_LIB_PKG_DIR})

    get_filename_component(Python3_ARCH_PKG_DIR ${Python3_SITEARCH} NAME)
    get_filename_component(Python3_ARCH_BASE_DIR ${Python3_SITEARCH} DIRECTORY)
    get_filename_component(Python3_ARCH_PYTHON_DIR ${Python3_ARCH_BASE_DIR} NAME)
    SET(Python3_ARCH_INSTALL_DIR ${LIB_INSTALL_DIR}/${Python3_ARCH_PYTHON_DIR}/${Python3_ARCH_PKG_DIR})


    MESSAGE(STATUS "Python3 version: ${Python3_VERSION}")
    MESSAGE(STATUS "Python3 site lib: ${Python3_SITELIB}")
    MESSAGE(STATUS "Python3 lib install dir: ${Python3_LIB_INSTALL_DIR}")
    MESSAGE(STATUS "Python3 arch install dir: ${Python3_ARCH_INSTALL_DIR}")
    MESSAGE(STATUS "Python3 include dirs: ${Python3_INCLUDE_DIRS}")
    MESSAGE(STATUS "Python3 libraries: ${Python3_LIBRARIES}")
    MESSAGE(STATUS "Python3 library dirs: ${Python3_LIBRARY_DIRS}")
    MESSAGE(STATUS "Python3 sitearch: ${Python3_SITEARCH}")
    MESSAGE(STATUS "Python3 development found: ${Python3_Development_FOUND}")
endif()

#global defines
SET(max_chnname_size "-DMAX_CHNNAME_SIZE=60")
add_definitions(${max_chnname_size})

#parent directories of different GDS modules we are dependent upon
SET(GDS_DIRS
                )

#generate include directory list for GDS dependencies
SET(EXTERNAL_INCLUDE_DIRECTORIES ${ROOT_INCLUDE_DIRECTORIES} ${CMAKE_CURRENT_SOURCE_DIR}/config)
MACRO(GETSUBDIRS parentdir)
    FILE(GLOB children RELATIVE ${CMAKE_CURRENT_SOURCE_DIR}/${parentdir} ${parentdir}/*)
    FOREACH(child ${children})
        SET(child_path ${CMAKE_CURRENT_SOURCE_DIR}/${parentdir}/${child})
        #MESSAGE(STATUS "child: ${child_path}")
        IF(IS_DIRECTORY ${child_path})
            #MESSAGE(STATUS "! - A directory")
            SET(EXTERNAL_INCLUDE_DIRECTORIES ${EXTERNAL_INCLUDE_DIRECTORIES} ${child_path})
        ELSE()
            #MESSAGE(STATUS "X - Not a directory")
        ENDIF()
    ENDFOREACH()
ENDMACRO()

FOREACH(dep ${GDS_DIRS})
    GETSUBDIRS(${dep})
ENDFOREACH()

#MESSAGE(STATUS "includes: ${EXTERNAL_INCLUDE_DIRECTORIES}")

#build include inserts for rootcint calls
SET(ROOTCINT_INCLUDE_ARGS "")
FOREACH(inc ${EXTERNAL_INCLUDE_DIRECTORIES})
    SET(ROOTCINT_INCLUDE_ARGS ${ROOTCINT_INCLUDE_ARGS} "-I${inc}")
ENDFOREACH()






#MESSAGE(STATUS gdsincludes: ${GDS_INCLUDE_DIRECTORIES})
#message(FATAL_ERROR "debug stop")
#build GDS dependencies sources
foreach(dep ${GDS_DIRS})
        add_subdirectory(${dep})
endforeach()

#build and install dtt
add_subdirectory(dtt)

#configure and install python files
add_subdirectory(python)

