#ifndef _LIGO_VIRTUALPLOTPAD_H
#define _LIGO_VIRTUALPLOTPAD_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: VirtualPlotPad						*/
/*                                                         		*/
/* Module Description: Virtual plot pad					*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 29Oct99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: TLGPlotSet.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

   class BasicPlotDescriptor;


/** A virtual plot pad. Implements dummy update methods which are use
    by the plot set to update registered pads. Must be overwritten by 
    real pads.
   
    @brief Virtual Pad
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class VirtualPlotPad  {
   public:
      /// Constructor
      VirtualPlotPad () {
      }
      /// Destructor
      virtual ~VirtualPlotPad() {
      }
      /// Updates the plot. If force is true regenerates the data
      virtual void UpdatePlot (const BasicPlotDescriptor* plotd = 0,
                        bool force = true) {
      }
      /// Updates option panel and plot
      virtual void Update (bool force = false) {
      }
   };

/** A virtual plot window. Implements dummy destructor to delete
    all registerd windows.
   
    @brief Virtual Pad
    @author Written November 1999 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class VirtualPlotWindow  {
   public:
      /// Constructor
      VirtualPlotWindow () {
      }
      /// Destructor
      virtual ~VirtualPlotWindow() {
      }
   };

#endif // _LIGO_VIRTUALPLOTPAD_H

