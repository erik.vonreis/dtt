#ifndef CIRCBUF_HH
#define CIRCBUF_HH
//
//    Circular buffer pointer
//
class circbuf {
public:
  circbuf(void)  {setN(0);}
  circbuf(int N) {setN(N);}
  ~circbuf() {}
  void setN(int N) {
      mFirst = -1;
      mPtr   = 0;
      mMax   = N;
  }
  int length(int start=-1) {
      if (mFirst < 0) return 0;
      if (start < 0)  start = mFirst;
      int l = mPtr - start;
      if (l <= 0) l += mMax;
      return l;
  }
  int add(int N=1) {
      int x = mPtr;
      mPtr += N;
      if (mFirst < 0) {
	 mFirst = x;
      } else if (mFirst >= x && mFirst <= mPtr) {
	 mFirst = mPtr;
	 if (mFirst >= mMax) mFirst -= mMax;
      }
      if (mPtr >= mMax) mPtr -= mMax;
      return x;
  }
  int first() {
      return mFirst;}

  int next(int x) {
      if (x < 0) return x;
      x++;
      if (x >= mMax) x = 0;
      if (x == mPtr) return -1;
      return x;
  }

  int prev(int x) {
      if (x < 0) return x;
      if (x == mFirst) return -1;
      x--;
      if (x < 0) x += mMax;
      return x;
  }

private:
  int mFirst; // pointer to first full element mFirst==-1 -> no data
  int mPtr;   // pointer to next element to be filled (1 after last)
  int mMax;   // number of elements in history list.
};
#endif  //  !def(CIRCBUF_HH)
