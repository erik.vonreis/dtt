/* Version $Id: multiawgstream.c 1177 2011-05-16 21:31:54Z james.batch@LIGO.ORG $ */
/*=============================================================================
multiawgstream.c

Modification history:

June 19, 2002 - Modified Peter Shawhan's awgstream client to read a configuration
                file containing multiple waveforms to be injected at different
                times.  Time offsets are sorted in this version. -- Isabel Leonor
Modified 11 Feb 2003 to print out start time, etc. (unless '-q' is specified)
Modified 12 Feb 2003 to inject calibration lines with S2 freqs and amplitudes
Modified 9 Apr 2003 with revised calibration lines for DARM_CTRL_EXC, and to
   read waveform data from binary files (with either byte ordering)

To compile:
  cc multiawgstream.c SIStr.o -I$GDS_DIR/src/util -I$GDS_DIR/src/awg \
            -L$GDS_DIR/lib -lawg -ltestpoint -lrt -o multiawgstream
where GDS_DIR on red      = /opt/CDS/d/gds/diag
              on london   = /opt/LLO/c/gds/diag

To run: $GDS_DIR/lib must be in your LD_LIBRARY_PATH

=============================================================================*/

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <math.h>
#include <unistd.h>
#include "SIStr.h"

typedef int (*compfn)(const void*, const void*);

#undef SKIP_SISTR
#define CLIENT      "multiawgstream"
#define ONEDAY        86400.0
#define WFNUMMAX        200
#define CONFIGCHARMAX   209
#define NAMECHARMAX     101
#define WFFILETAG   "wffile"


/*===========================================================================*/
void PrintUsage( void )
{
  printf( 
  "Usage: multiawgstream <channel> <rate> <configfile> <scale> <gpstime> [-q] [-c] [-d]\n" );
  printf( 
  "\n");
  printf( 
  "<channel>    is case-sensitive and must be a real excitation channel.\n" );
  printf( 
  "<rate>       is in Hz and must match the excitation channel's true rate.\n" );
  printf( 
  "<configfile> is a configuration file.  Each waveform to be injected\n" );
  printf( 
  "             should have a corresponding line in this file specifying\n" );
  printf( 
  "             the file containing the waveform data preceded by the\n" );
  printf( 
  "             keyword \"%s\", and a unique alias for this waveform.\n", WFFILETAG );
  printf( 
  "             After all the waveform files and corresponding aliases have\n" );
  printf( 
  "             been specified, the remaining lines of the configuration\n" );
  printf( 
  "             file should each specify the alias of the signal to be\n" );
  printf( 
  "             injected, the scale factor to be multiplied to the overall\n" );
  printf( 
  "             scale factor specified at the command line, and the time\n" );
  printf( 
  "             offset relative to the GPS time specified at the command\n" );
  printf( 
  "             line, in that order.  A \"#\" at the beginning of a line\n" );
  printf(
  "             can be used to indicate that the rest of the line should\n" );
  printf( 
  "             be treated as a comment.  For example, the contents of a\n" );
  printf( 
  "             configuration file should be similar to this:\n" );
  printf( 
  "\n");
  printf( 
  "                  # Specify waveform files and aliases\n" );
  printf( 
  "                  # in the following lines:\n" );
  printf( 
  "                  wffile sinewave.dat sine\n" );
  printf( 
  "                  wffile zwergermueller1.dat zm1\n" );
  printf( 
  "                  wffile zwergermueller2.dat zm2\n" );
  printf( 
  "\n");
  printf( 
  "                  # Specify aliases, scale factors, and\n" );
  printf( 
  "                  # time offsets (in that order) of signals\n" );
  printf(
  "                  # to be injected:\n" );
  printf( 
  "                  zm1 0.1 0.0\n" );
  printf( 
  "                  zm1 1.0 15.0\n" );
  printf( 
  "                  sine 10.0 30.0\n" );
  printf( 
  "                  zm2 100.0 45.0\n" );
  printf( 
  "\n");
  printf( 
  "             The file containing the waveform data can be either formatted\n" );
printf( 
  "             ASCII or unformatted binary.  If ASCII, the values must be\n" );
printf( 
  "             separated by newlines and/or spaces.  If binary, the file must\n" );
printf( 
  "             contain four-byte real values; the first value in the file must\n" );
printf( 
  "             be 1234.5, which is used to determine the byte ordering and is\n" );
printf( 
  "             followed by the waveform data.  Any conversion error causes the\n" );
  printf(
  "             rest of the file to be skipped.  If <configfile> is '-'\n" );
  printf(
  "             (i.e. a dash), data is read from standard input.  Waveform\n" );
  printf( 
  "             file names and aliases are limited to %d characters, and\n",\
                NAMECHARMAX-1 );
  printf( 
  "             each line in the configuration file is limited to %d\n",\
                CONFIGCHARMAX-1);
  printf( 
  "             characters.\n" );
  printf( 
  "<scale>      is a REQUIRED overall scale factor to apply to all the\n" );
  printf( 
  "             waveforms.  This overall scale factor is multiplied by\n" );
  printf(
  "             the scale factor given in the configuration file to\n" );
  printf(
  "             obtain the total scale factor applied to a waveform.\n" );
  printf( 
  "<gpstime>    is a reference time for injecting the waveforms.  It can\n" );
  printf( 
  "             take any real value, not just an integer.  The resulting\n" ); 
  printf( 
  "             time from the sum of the reference time and the time offset\n" );
  printf( 
  "             given in the configuration file is the signal injection\n" );
  printf( 
  "             start time, and must occur during the 24-hour period\n" );
  printf( 
  "             following the execution of the multiawgstream command.\n" );
  printf(
  "Parameters (channel, config file, scale factor, and start time) are\n" );
  printf(
  "    echoed to stdout UNLESS the '-q' option is specified.\n" );
  printf( 
  "The '-c' option causes calibration lines to be added to the data.\n" );
  printf(
  "      The frequencies and amplitudes are hard-coded values from 9 Apr 2003.\n" );
  printf( 
  "The '-d' option causes debugging information to be printed to the screen.\n" );
  printf( 
  "             Specify '-d' twice for extra information.\n" );
  printf( "\nmultiawgstream version %s\n$Id: multiawgstream.c 1177 2011-05-16 21:31:54Z james.batch@LIGO.ORG $\n", VERSION) ;
  return;
}

void PrintError(void)
{
  printf("Error from %s.  Aborting...\n", CLIENT);
  return;
}


struct wfname {
  char name[NAMECHARMAX];
  char alias[NAMECHARMAX];
};

struct wfinject {
  char alias[NAMECHARMAX];
  double scale;
  double timeoffset;
};

int cmp_wfalias(const struct wfname *, const struct wfname *);
int cmp_wfnames(const struct wfname *, const struct wfname *);
int cmp_wftimes(const struct wfinject *, const struct wfinject *);

/*===========================================================================*/
int main( int argc, char **argv )
{
  char *arg;
  int iarg;
  int nposarg = 0;
  char *channel = NULL;
  int samprate = 0;
  float scale = 1.0;
  double starttime = 0.0;
  int quiet = 0;
  int callines = 0;
  FILE *file;
  int binaryfile;
  int swapbytes = 0;
  union {
    float real;
    char byte[4];
  } swapper;
  char tempbyte;

  SIStream sis;
  int status;
  float val, outval;
  double elapsed=0.0, dt=0.0, phi=0.0, twopi=0.0, cal=0.0;
  /*-- Calibration line stuff --*/
  int isample = 0;
  double mtrxx, mtrxy;
  double camp1=0.0, cfreq1=0.0, camp2=0.0, cfreq2=0.0, camp3=0.0, cfreq3=0.0;

  char              configLine[CONFIGCHARMAX];
  int               dupsFlag;
  int               endFilesFlag;
  char              firstString[NAMECHARMAX];
  int               i;
  int               injectErrorFlag;
  char              injectInfo[256];
  int               lineCtr;
  char             *newLine;
  int               openStreamFlag;
  double            previousWfEndTime;
  double            quietTimeInterval;
  int               sigInjectCtr;
  int               signalErrorFlag;
  char              wfAlias[NAMECHARMAX];
  int               wfDataCtr = 0;
  int               wfFileCtr;
  struct wfname    *wfAliasFound = NULL;
  struct wfinject   wfInject[WFNUMMAX];
  int               wfInjectCtr;
  struct wfname     wfName[WFNUMMAX];
  double            wfScale;
  struct wfname     wfAliasSearch;
  double            wfStartTime;
  double            wfTimeOffset;
  double            wfTotalScale;
  FILE             *configfile = NULL;
  char             *configfilename = NULL;
  

  /*------ Beginning of code ------*/

  if ( argc <= 1 ) {
    PrintUsage(); return 0;
  }

  /*-- Parse command-line arguments --*/
  for ( iarg=1; iarg<argc; iarg++ ) {
    arg = argv[iarg];
    if ( strlen(arg) == 0 ) { continue; }

    /*-- See whether this introduces an option --*/
    if ( arg[0] == '-' && arg[1] != '\0' && (arg[1]<'0' || arg[1]>'9') ) {
      /*-- The only valid options are "-q", "-c", and "-d" --*/
      if (( strcmp(arg,"-q") == 0 ) || (strcmp(arg,"--quiet") == 0)) {
	quiet = 1;
      } else if ( strcmp(arg,"-c") == 0 ) {
	callines = 1;
      } else if (( strcmp(arg,"-d") == 0 ) || (strcmp(arg, "--debug") == 0)) {
	SIStr_debug++;
      } else if ((strcmp(arg, "--help") == 0) || (strcmp(arg, "-h") == 0) || (strcmp(arg, "-?") == 0)) {
	 PrintUsage(); return 1; 
      } else {
	fprintf(stderr,"Error: Invalid option %s\n", arg );
	PrintUsage(); return 1;
      }

    } else {
      /*-- This is a positional argument --*/

      nposarg++;
      switch (nposarg) {
      case 1:
	channel = arg;
	break;
      case 2:
	samprate = atol( arg );
	if ( samprate < 1 || samprate > 65536 ) {
	  fprintf(stderr,"Error: Invalid sampling rate\n" );
	  PrintUsage(); return 1;
	}
	break;
      case 3:
	configfilename = arg;
	break;
      case 4:
	scale = atof( arg );
	/*
	if ( scale == 0.0 || scale > 600000000.0 ) {
	  fprintf(stderr,"Error: Invalid scale factor\n" );
	  PrintUsage(); return 1;
	}
	*/
	break;
      case 5:
	starttime = atof( arg );
	if ( starttime < 600000000.0 || starttime > 1800000000.0 ) {
	  fprintf(stderr,"Error: Invalid start time\n" );
	  PrintUsage(); return 1;
	}
	break;
      default:
	fprintf(stderr,"Error: Too many arguments\n" );
	PrintUsage(); return 1;
      }	

    }

  }  /* End loop over command-line arguments */

  /*-- Make sure the required arguments were present --*/
  if ( channel == NULL ) {
    fprintf(stderr,"Error: Channel was not specified\n" );
  }
  if ( samprate == 0 ) {
    fprintf(stderr,"Error: Sampling rate was not specified\n" );
  }
  if ( configfilename == NULL ) {
    fprintf(stderr,"Error: File was not specified\n" );
  }
  if ( channel == NULL || samprate == 0 || configfilename == NULL ) {
    PrintUsage(); return 1;
  }

  /* User is REQUIRED to specify the start time */
  if ( starttime < 600000000.0 || starttime > 1800000000.0 ) {
    PrintUsage(); return 1;
  }

  if ( quiet == 0 ) {
    /* Print out argument values */
    printf( "Channel = %s\n", channel );
    printf( "Config  = %s\n", configfilename );
    printf( "Scale   = %17.6f\n", scale );
    printf( "Start   = %17.6f\n", starttime );
  }

  dt = 1.0 / (double) samprate;

  /*-- Initialize calibration-line stuff --*/
  if ( callines ) {
    /*-- Get appropriate values for this interferometer --*/
    /*-- **** NOTE ****
      All calibration line frequencies must be multiples of 0.05 Hz ! --*/
    if ( channel[0] == 'L' ) {           /* L1 */
      mtrxx = 2.50 ; mtrxy = -2.50 ;
      camp1 = 0.02   ; cfreq1 = 52.30 ;
      camp2 = 0.003  ; cfreq2 = 166.7 ;
      camp3 = 4.0    ; cfreq3 = 927.7 ;
    } else if ( channel[1] == '1' ) {    /* H1 */
      mtrxx = 1.04 ; mtrxy = -0.90 ;
      camp1 = 0.0025 ; cfreq1 = 41.25 ;
      camp2 = 0.0028 ; cfreq2 = 151.3 ;
      camp3 = 0.21   ; cfreq3 = 973.3 ;
    } else {                             /* H2 */
      mtrxx = 0.53 ; mtrxy = -0.50 ;
      camp1 = 0.01   ; cfreq1 = 41.75 ;
      camp2 = 0.0033 ; cfreq2 = 151.8 ;
      camp3 = 0.08   ; cfreq3 = 973.8 ;
    }

    /*-- If this channel is not ETMX_EXC, modify amplitudes accordingly --*/
    if ( strcmp(channel+3,"LSC-ETMX_EXC") == 0 ) {
      /*-- Keep amplitudes as above --*/
    } else if ( strcmp(channel+3,"LSC-ETMY_EXC") == 0 ) {
      camp1 = camp1 * mtrxy / mtrxx ;
      camp2 = camp2 * mtrxy / mtrxx ;
      camp3 = camp3 * mtrxy / mtrxx ;
    } else if ( strcmp(channel+3,"LSC-DARM_CTRL_EXC") == 0 ) {
      camp1 = 0.5 * camp1 / mtrxx ;
      camp2 = 0.5 * camp2 / mtrxx ;
      camp3 = 0.5 * camp3 / mtrxx ;
    } else {
      /*-- This is not a channel we can put calib lines on --*/
      fprintf(stderr,"Error: Cannot put calibration lines on %s\n", channel );
      return 1;
    }

    isample = 0;
    twopi = 8.0 * atan(1.0);
  }

  if ( strcmp(configfilename,"-") != 0 )
  {
    /*-----------------------------------*/
    /* Open configuration file */
    if ((configfile = fopen(configfilename, "r")) == NULL)
    {
      fprintf(stderr,"Error while opening %s for reading.\n", configfilename );
      PrintError();
      return -2;
    }

    /*-----------------------------------*/
    /* Initialize arrays */
    for (i = 0; i < WFNUMMAX; i++)
    {
      strcpy(wfName[i].name,"");
      strcpy(wfName[i].alias,"");
      strcpy(wfInject[i].alias,"");
      wfInject[i].scale       = 0.0;
      wfInject[i].timeoffset  = 0.0;
    }
   
    /*-----------------------------------*/
    /* Read configuration file, loop over waveforms */
    lineCtr          = 0;
    wfFileCtr        = 0;
    wfInjectCtr      = 0;
    endFilesFlag     = 0;
    strcpy(configLine,"");
    fseek(configfile,0,0);
    while (fgets(configLine,CONFIGCHARMAX,configfile) != NULL)
    {
      lineCtr++;
      if (strchr(configLine,'\n') == NULL)
      {
        fprintf(stderr,"\nLine %d of %s too long.  Limit is %d characters.\n",\
                          lineCtr, configfilename, CONFIGCHARMAX-1);
        PrintError();
        return 1;
      }
      if (configLine[0] == '#' || configLine[0] == ' ' || configLine[0] == '\n')
      {
        continue;
      }
      if (sscanf(configLine,"%s", firstString) == 1)
      {
        if (strcmp(firstString,WFFILETAG) == 0)
        {
          if (endFilesFlag == 1)
          {
            fprintf(stderr,"\nLine %d of %s not formatted correctly.\n",\
                              lineCtr, configfilename);
            fprintf(stderr,"Type multiawgstream without arguments to see usage.\n");
            PrintError();
            return 1;
          }
          if (wfFileCtr == WFNUMMAX)
          {
            fprintf(stderr,"\nToo many waveform files specified in %s.  Limit is %d.\n",\
                                                         configfilename, WFNUMMAX);
            PrintError();
            return 1;
          }
          newLine = NULL;
          newLine = configLine + strspn(configLine,WFFILETAG);
          if (sscanf(newLine,"%s %s", wfName[wfFileCtr].name,\
                                      wfName[wfFileCtr].alias) == 2)
          {
            if (strlen(wfName[wfFileCtr].name)  >= NAMECHARMAX || 
                strlen(wfName[wfFileCtr].alias) >= NAMECHARMAX)
            {
              fprintf(stderr, 
              "\nFile name or alias in line %d of %s too long.  Limit is %d characters.\n",\
                                            lineCtr, configfilename, NAMECHARMAX-1);
              PrintError();
              return 1;
            }
            wfFileCtr++;
          }
          else
          {
            fprintf(stderr,"\nLine %d of %s not formatted correctly.\n",\
                              lineCtr, configfilename);
            fprintf(stderr,"Type multiawgstream without arguments to see usage.\n");
            PrintError();
            return 1;
          }
        }
        else
        {
          if (wfFileCtr == 0)
          {
            fprintf(stderr,"\nLine %d of %s not formatted correctly.\n",\
                              lineCtr, configfilename);
            fprintf(stderr,"Type multiawgstream without arguments to see usage.\n");
            PrintError();
            return 1;
          }
          if (endFilesFlag == 0)
          {
            if (wfFileCtr > 1)
            {
              qsort(wfName, wfFileCtr, sizeof(struct wfname),
		    (compfn) cmp_wfnames);
              dupsFlag = 0;
              for (i = 1; i < wfFileCtr; i++)
              {
                if (strcmp(wfName[i].name,wfName[i-1].name) == 0)
                {
                  fprintf(stderr,"\nDuplication of waveform filename %s detected.\n",\
                                                                     wfName[i].name);
                  dupsFlag = 1;
                }
              }

              qsort(wfName, wfFileCtr, sizeof(struct wfname),
		    (compfn) cmp_wfalias);
              for (i = 1; i < wfFileCtr; i++)
              {
                if (strcmp(wfName[i].alias,wfName[i-1].alias) == 0)
                {
                  fprintf(stderr,"\nDuplication of alias %s detected.\n", wfName[i].alias);
                  dupsFlag = 1;
                }
              }
              if (dupsFlag == 1) 
              {
                PrintError();
                return 1;
              }
            }
            endFilesFlag = 1;
          }
          if (sscanf(configLine,"%s %lf %lf", wfAlias, &wfScale, &wfTimeOffset) == 3)
          {
            if (wfInjectCtr == WFNUMMAX)
            {
              fprintf(stderr,"\nToo many signals to be injected.  Limit is %d.\n", WFNUMMAX);
              PrintError();
              return 1;
            }
            if (strlen(wfAlias) >= NAMECHARMAX)
            {
              fprintf(stderr,"\nAlias in line %d of %s too long.  Limit is %d characters.\n", \
                                      lineCtr, configfilename, NAMECHARMAX-1);
              PrintError();
              return 1;
            }
            strcpy(wfAliasSearch.alias,wfAlias);
            wfAliasFound = NULL;
            if ((wfAliasFound = bsearch(&wfAliasSearch, wfName, wfFileCtr,
                                        sizeof(struct wfname),
					(compfn) cmp_wfalias)) == NULL)
            {
              fprintf(stderr,"\nAlias %s in line %d of %s does not correspond to a waveform.\n",
                                wfAlias, lineCtr, configfilename);
              PrintError();
              return 1;
            }
            else
            {
              if (wfTimeOffset < 0.0 || wfTimeOffset > ONEDAY)
              {
                fprintf(stderr,"\nInvalid time offset %f in line %d of %s.\n",\
                                                      wfTimeOffset, lineCtr, configfilename);
                fprintf(stderr,"Type multiawgstream without arguments to see usage.\n");
                PrintError();
	        return 1;
              }
              if (wfScale == 0.0) continue;

              wfTotalScale    = scale*wfScale;
              wfStartTime     = starttime + wfTimeOffset;
              if (wfStartTime != 0.0 && \
                  (wfStartTime < 600000000.0 || wfStartTime > 1800000000.0) )
              {
                fprintf(stderr,"\nInvalid start time %f for %s in line %d of %s.\n",\
                                  wfStartTime, wfAliasFound->alias, lineCtr, configfilename);
                fprintf(stderr,"Type multiawgstream without arguments to see usage.\n");
                PrintError();
                return 1;
              }

              strcpy(wfInject[wfInjectCtr].alias,wfAliasFound->alias);
              wfInject[wfInjectCtr].scale      = wfScale;
              wfInject[wfInjectCtr].timeoffset = wfTimeOffset;
              wfInjectCtr++;

            } /* bsearch for alias successful */
          }
          else
          {
            fprintf(stderr,"\nLine %d of %s not formatted correctly.\n", lineCtr, configfilename);
            fprintf(stderr,"Type multiawgstream without arguments to see usage.\n");
            PrintError();
            return 1;
          }
        } /* if first string is WFFILETAG */
      } /* if first string of line is converted */
    } /* loop over configfile contents */

    if (wfInjectCtr > 1)
    {
      qsort(wfInject, wfInjectCtr, sizeof(struct wfinject),
	    (compfn) cmp_wftimes);
      dupsFlag = 0;
      for (i = 1; i < wfInjectCtr; i++)
      {
        if (wfInject[i].timeoffset == wfInject[i-1].timeoffset)
        {
          fprintf(stderr,"\nDuplication of time offset %f detected in %s.\n",\
                            wfInject[i].timeoffset, configfilename);
          fprintf(stderr,"Overlapping waveforms are not permitted (yet).\n");
          dupsFlag = 1;
        }
      }
      if (dupsFlag == 1)
      {
        PrintError();
        return 1;
      }
    }

    /*-----------------------------------*/
    /* Loop through waveforms and send them to SIStr */
    signalErrorFlag   = 0;
    openStreamFlag    = 0;
    injectErrorFlag   = 0;
    sigInjectCtr      = 0;
    previousWfEndTime = 0.0;
    quietTimeInterval = 0.0;
    for (i = 0; i < wfInjectCtr; i++)
    {
      strcpy(wfAliasSearch.alias,wfInject[i].alias);
      wfAliasFound = NULL;
      if ((wfAliasFound = bsearch(&wfAliasSearch, wfName, wfFileCtr,
				  sizeof(struct wfname), 
				  (compfn) cmp_wfalias)) == NULL)
      {
        fprintf(stderr,"\nAlias %s in %s does not correspond to a waveform.\n",
                          wfAlias, configfilename);
        PrintError();
        signalErrorFlag = 1;
        break;
      }
      else
      {
        if (i > 0 && wfInject[i].timeoffset < previousWfEndTime)
        {
          fprintf(stderr,"\nOverlapping waveforms %s and %s found in %s.\n",\
                            wfInject[i].alias, wfInject[i-1].alias, configfilename);
          PrintError();
          signalErrorFlag = 1;
          break;
        }

        /*-----------------------------------*/
        /* Open the waveform file */
        if ((file = fopen(wfAliasFound->name, "r")) == NULL)
        {
          fprintf(stderr,"\nError while opening waveform file %s for reading.\n",\
                                                              wfAliasFound->name);
          PrintError();
          signalErrorFlag = 1;
          break;
        }

	/*-- Figure out whether this is a binary or an ASCII file.  If binary,
	  figure out whether we need to swap bytes --*/
	fread( (void*)&val, 4, 1, file );
	if ( val == 1234.5 ) {
	  binaryfile = 1;
	  swapbytes = 0;
	} else {
	  /*-- See if this is a binary file with opposite byte ordering --*/
	  swapper.real = val;
	  tempbyte = swapper.byte[0];
	  swapper.byte[0] = swapper.byte[3];
	  swapper.byte[3] = tempbyte;
	  tempbyte = swapper.byte[1];
	  swapper.byte[1] = swapper.byte[2];
	  swapper.byte[2] = tempbyte;
	  if ( swapper.real == 1234.5 ) {
	    binaryfile = 1;
	    swapbytes = 1;
	  } else {
	    binaryfile = 0;
	    /*-- This is an ASCII file --*/
	    /*-- Rewind the file to the beginning --*/
	    fseek( file, 0, SEEK_SET );
	  }
	}

        wfTotalScale = scale*wfInject[i].scale;
        wfStartTime  = starttime + wfInject[i].timeoffset;


        /*-----------------------------------*/
        /* Open the Signal Injection Stream */
        if (i == 0)
        {
          /*-----------------------------------*/
          /* Report some information about this application and waveform */
          sprintf(injectInfo,"%s %s %.6g %s",\
                              CLIENT, wfAliasFound->name, wfTotalScale, configfilename);
          fprintf(stderr,"%s\n", injectInfo);
#ifndef SKIP_SISTR
          SIStrAppInfo(injectInfo);
#endif

          fprintf(stderr,"\nOpening signal injection stream...\n");
#ifndef SKIP_SISTR
          status = SIStrOpen( &sis, channel, samprate, starttime);
#else
	  status = SIStr_OK;
#endif

          if ( SIStr_debug ) { printf( "SIStrOpen returned %d\n", status ); }
          if ( status != SIStr_OK ) {
            fprintf(stderr,"Error while opening SIStream: %s\n", SIStrErrorMsg(status));
            return 2;
          }
          openStreamFlag = 1;
        }

        quietTimeInterval = wfInject[i].timeoffset - previousWfEndTime;

	/*-- Make sure this is an integer number of samples --*/
	quietTimeInterval = (floor) (quietTimeInterval*(double)samprate+0.5)
	  / (double) samprate;

        if (quietTimeInterval > 0.0 && quietTimeInterval < ONEDAY)
        {
          fprintf(stderr,"\nSending %f seconds of zeroes to signal injection stream...\n",\
                           quietTimeInterval);

	  for ( elapsed=0.0; elapsed < quietTimeInterval; elapsed += dt ) {

	    /*-- Send calibration lines, if appropriate --*/
	    if ( callines ) {
	      isample++;
	      if ( isample == 20*samprate ) { isample = 0; }
	      phi = dt * (double) isample * twopi;
	      cal = camp1*sin(phi*cfreq1) + camp2*sin(phi*cfreq2)
		+ camp3*sin(phi*cfreq3);
	    } else {
	      cal = 0.0;
	    }

	    outval = (float) cal;

#ifndef SKIP_SISTR
	    status = SIStrAppend( &sis, &outval, 1, 1.0 );
#else
	    printf( "%f\n", outval );
	    status = SIStr_OK;
#endif
	    if ( SIStr_debug >= 2 ) { printf( "SIStrAppend returned %d\n", status ); }
	    if ( status != SIStr_OK ) {
	      fprintf(stderr,"Error while adding data to stream: %s\n", SIStrErrorMsg(status));
	      injectErrorFlag = 1;
	      break;
	    }
	  }
	  if ( injectErrorFlag ) { break; }

          fprintf(stderr,"...zeroes sent\n");
        }
        else
        {
          if (quietTimeInterval != 0.0)
          {
            fprintf(stderr,"\nInvalid quiet time interval %f calculated.\n", quietTimeInterval);
            PrintError();
            signalErrorFlag = 1;
          }
        }

        fprintf(stderr,"\n");
        fprintf(stderr,"Sending waveform signal %d to SIStr...\n", sigInjectCtr+1);
        fprintf(stderr,"------------------\n");
        fprintf(stderr,"signal alias     :  %s\n", wfInject[i].alias);
        fprintf(stderr,"waveform file    :  %s\n", wfAliasFound->name);
        fprintf(stderr,"channel          :  %s\n", channel);
        fprintf(stderr,"sampling rate    :  %d samples/sec\n", samprate);
        fprintf(stderr,"total scale      :  %.6f\n", wfTotalScale);
        fprintf(stderr,"GPS time         :  %.6f seconds\n", starttime);
        fprintf(stderr,"time offset      :  %.6f seconds\n", wfInject[i].timeoffset);
        fprintf(stderr,"config file      :  %s\n", configfilename);
        fprintf(stderr,"------------------\n");

        /*-----------------------------------*/
        /* Read data from input file and send it */
        wfDataCtr = 0;

	while ( 1 ) {

	  /*-- Read from the (binary or ASCII) file --*/
	  if ( binaryfile ) {
	    if ( fread(&val,4,1,file) <= 0 ) { break; }
	    if ( swapbytes ) {
	      swapper.real = val;
	      tempbyte = swapper.byte[0];
	      swapper.byte[0] = swapper.byte[3];
	      swapper.byte[3] = tempbyte;
	      tempbyte = swapper.byte[1];
	      swapper.byte[1] = swapper.byte[2];
	      swapper.byte[2] = tempbyte;
	      val = swapper.real;
	    }
	  } else {
	    if ( fscanf(file,"%f",&val) <= 0 ) { break; }
	  }
          wfDataCtr++;

	  /*-- Add in calibration lines, if appropriate --*/
	  if ( callines ) {
	    isample++;
	    if ( isample == 20*samprate ) { isample = 0; }
	    phi = dt * (double) isample * twopi;
	    cal = camp1*sin(phi*cfreq1) + camp2*sin(phi*cfreq2)
	      + camp3*sin(phi*cfreq3);
	  } else {
	    cal = 0.0;
	  }

	  outval = (float) wfTotalScale * val + (float) cal;

#ifndef SKIP_SISTR
          status = SIStrAppend( &sis, &outval, 1, 1.0 );
#else
	  printf( "%f\n", outval );
	  status = SIStr_OK;
#endif
          if ( SIStr_debug >= 2 ) { printf( "SIStrAppend returned %d\n", status ); }
          if ( status != SIStr_OK ) {
            fprintf(stderr,"Error while adding data to stream: %s\n", SIStrErrorMsg(status));
            injectErrorFlag = 1;
            break;
          }
        }


        fclose(file);
        if (injectErrorFlag == 1) 
        {
          fprintf(stderr,"\nError sending waveform signal %s given in %s.\n",\
                                          wfInject[i].alias, configfilename);
          break;
        }
        if (wfDataCtr == 0)
        {
          break;
        }
        previousWfEndTime = wfInject[i].timeoffset + ((double)wfDataCtr)/((double)samprate);
        fprintf(stderr,"...waveform sent to SIStr\n");
        sigInjectCtr++;
      } /* if bsearch for alias successful */
    } /* loop through waveforms to be injected */

    /*-----------------------------------*/
    /* Close the stream */
    if (openStreamFlag == 1)
    {
      fprintf(stderr,"\nClosing signal injection stream...\n");
#ifndef SKIP_SISTR
      status = SIStrClose( &sis );
#else
      status = SIStr_OK;
#endif
      if ( SIStr_debug ) { printf( "SIStrClose returned %d\n", status ); }
      if ( status != SIStr_OK ) {
        fprintf(stderr,"Error while closing SIStream: %s\n", SIStrErrorMsg(status));
        return 2;
      }
    }

    if (signalErrorFlag == 1) return 1;
    if (injectErrorFlag == 1)
    {
      printf("Error appending to signal injection stream.  Closed stream and aborting...\n");
      return 2;
    }
    if (wfDataCtr == 0 && wfInjectCtr > 0)
    {
      fprintf(stderr,"\nError reading waveform file %s.\n", wfAliasFound->name);
      PrintError();
      return -2;
    }

    if (wfInjectCtr > 0)
    {
      fprintf(stderr,"\nSignal injection summary :  %d signals successfully injected\n",\
                        sigInjectCtr);
    }
    else
    {
      fprintf(stderr,"\nNo signals specified for injection.\n");
    }
    printf("End of signal injection.\n");

  } /* if not stdin */ 
  else
  {
    file = stdin;

    /*-----------------------------------*/
    /* Open the Signal Injection Stream */
#ifndef SKIP_SISTR
    status = SIStrOpen( &sis, channel, samprate, starttime );
#else
    status = SIStr_OK;
#endif
    if ( SIStr_debug ) { printf( "SIStrOpen returned %d\n", status ); }
    if ( status != SIStr_OK ) {
      fprintf(stderr,"Error while opening SIStream: %s\n", SIStrErrorMsg(status));
      return 2;
    }

    /*-----------------------------------*/
    /* Read data from input file and send it */
    while ( fscanf(file,"%f",&val) == 1 ) {

      /*-- Add in calibration lines, if appropriate --*/
      if ( callines ) {
	isample++;
	if ( isample == 20*samprate ) { isample = 0; }
	phi = dt * (double) isample * twopi;
	cal = camp1*sin(phi*cfreq1) + camp2*sin(phi*cfreq2)
	  + camp3*sin(phi*cfreq3);
      } else {
	cal = 0.0;
      }

      outval = scale * val + (float) cal;

#ifndef SKIP_SISTR
      status = SIStrAppend( &sis, &outval, 1, 1.0 );
#else
      printf( "%f\n", outval );
      status = SIStr_OK;
#endif
      if ( SIStr_debug >= 2 ) { printf( "SIStrAppend returned %d\n", status ); }
      if ( status != SIStr_OK ) {
        fprintf(stderr,"Error while adding data to stream: %s\n", SIStrErrorMsg(status));
        break;
      }
    }

    /*-----------------------------------*/
    /* Close the stream */
#ifndef SKIP_SISTR
    status = SIStrClose( &sis );
#else
    status = SIStr_OK;
#endif
    if ( SIStr_debug ) { printf( "SIStrClose returned %d\n", status ); }
    if ( status != SIStr_OK ) {
      fprintf(stderr,"Error while closing SIStream: %s\n", SIStrErrorMsg(status));
      return 2;
    }

  }

  return 0;
}

int cmp_wfalias(const struct wfname *p1, const struct wfname *p2)
{
  return strcmp(p1->alias,p2->alias);
}

int cmp_wfnames(const struct wfname *p1, const struct wfname *p2)
{
  return strcmp(p1->name,p2->name);
}

int cmp_wftimes(const struct wfinject *p1, const struct wfinject *p2)
{
  double timesDiff = p1->timeoffset - p2->timeoffset;
  if (timesDiff > 0)
  {
    return 1;
  }
  else
  {
    if (timesDiff < 0)
    {
      return -1;
    }
    else
    {
      return 0;
    }
  }
}

