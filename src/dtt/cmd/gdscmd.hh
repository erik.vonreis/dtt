/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: gdscmd							*/
/*                                                         		*/
/* Module Description: API for executing gds command 			*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 11Jan99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: gdscmd.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8336  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C 4.2				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			TBD			*/
/*			  POSIX			TBD			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS 20B-145				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_CMD_H
#define _GDS_CMD_H

#ifdef __cplusplus
extern "C" {
#endif


/* Header File List: */

/** @name Diagnostics Command API
    This API implements four functions to initialize and terminate a 
    diagnostics kernel, and to send and recieve messages to and 
    from it, respectively. This API is linked directly to the 
    diagnostics kernel.

    @memo API for diagnostics command message
    @author Written January 1999 by Daniel Sigg
    @version 0.1
************************************************************************/

/*@{*/

#ifndef __CMD_FLAGS
#define __CMD_FLAGS

/** Flag representing the capability for performing diagnostics tests.

    @author DS, January 99
************************************************************************/
#define CMD_TEST		0x01


/** Flag representing the capability for controling test points.

    @author DS, January 99
************************************************************************/
#define CMD_TESTPOINT		0x02


/** Flag representing the capability for controling the arbitrary 
    waveform generator.

    @author DS, January 99
************************************************************************/
#define CMD_AWG			0x04
#endif


/** Command notification function. This function prototype is used by 
    the diagnostics kernel to pass back a notification message.

    @param msg name of message
    @param prm parameter string
    @param pLen length of parameter string
    @param res pointer to reply string
    @param rLen returned length of reply string
    @return 0 if successful, <0 otherwise
    @author DS, January 99
************************************************************************/
   typedef int (*gdsCmdNotification) (const char* msg, const char* prm, 
                     int pLen, char** res, int* rLen);


/** Initializes a diagnostics kernel. The flag argument specifies the
    required capabilities. Currently supported are CMD_TEST (for 
    performing diagnostics tests), CMD_TESTPOINT (for controling
    testpoints) and CMD_AWG (for controling the arbitarry waveform
    generator. The returned value is the bit encoded flag of the 
    capabilities of th ecommand line interface. If all requested
    capabilities are supported by the system, this return value will
    be indentical to the supplied flag value.

    @param flag specifies required services
    @param conf configuration string
    @return capabilities if successful, <0 otherwise
    @author DS, January 99
************************************************************************/
   int gdsCmdInit (int flag, const char* conf);

/** Terminates a diagnostics kernel.

    @param void
    @return 0 if successful, <0 otherwise
    @author DS, January 99
************************************************************************/
   int gdsCmdFini ();

/** Sends a command message. The command message is send directly to
    the diagnostics kernel.

    @param msg name of message
    @param prm parameter string
    @param pLen length of parameter string
    @param res pointer to reply string
    @param rLen returned length of reply string
    @return 0 if successful, <0 otherwise
    @author DS, January 99
************************************************************************/
   int gdsCmd (const char* msg, const char* prm, int pLen,
              char** res, int* rLen);

/** Transfer data. The data command retrieves and stores data from and
    to the diagnostics kernel, respectively. The data format is floats. 
    The data type is either: 0 - asis, 1 - complex, 2 - real part, and 
    3 - imaginary part. Additionally, the data length and the offset
    into the data object have to be specified in number of points
    (i.e. 1 complex number point = 2 floating point numbers).
    Depending whether data is retrieved or stored the arguments 'data' 
    and 'data length' are either return or input arguments, respectively.
    When retrieving a data array the caller is reponsible to free the 
    returned data array.

    @param name name of data object
    @param toKernel transfer direction (boolean)
    @param datatype Type of data
    @param len Number of data points
    @param ofs Offset into data object (in number of data points)
    @param data pointer to data array
    @param datalen Number of float values in data array
    @return 0 if successful, <0 otherwise
    @author DS, January 99
************************************************************************/
   int gdsCmdData (const char* name, int toKernel, int datatype,
                  int len, int ofs, float** data, int* datalength);

/** Installs a message handler for notfication messages.

    @param callback callback routine
    @return 0 if successful, <0 otherwise
    @author DS, January 99
************************************************************************/
   int gdsCmdNotifyHandler (gdsCmdNotification callback);

/** Sends a command notification message. The command notification 
    message is send back from the diagnostics kernel.

    @param msg name of message
    @param prm parameter string
    @param pLen length of parameter string
    @param res pointer to reply string
    @param rLen returned length of reply string
    @return 0 if successful, <0 otherwise
    @author DS, January 99
************************************************************************/
   int cmdNotification (const char* msg, const char* prm, 
                     int pLen, char** res, int* rLen);

/*@}*/


#ifdef __cplusplus
}
#endif


#endif /*_GDS_CMD_H */
