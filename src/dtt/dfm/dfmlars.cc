#include "dfmlars.hh"
#include "larsio.hh"
#include <cstdlib>

namespace dfm {
   using namespace fantom;
   using namespace std;


   // Default Lars port
   static const int kLARSPORT = 8089;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   // static string trim (const char* p)
   // {
      // while (isspace (*p)) p++;
      // string s = p;
      // while ((s.size() > 0) && isspace (s[s.size()-1])) {
         // s.erase (s.size() - 1);
      // }
      // return s;
   // }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dfmlars							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   dfmlars::dfmlars () : fPort (0)
   {
      fChnPreselFull = true;
   }

//______________________________________________________________________________
   bool dfmlars::open (const std::string& addr, bool read)
   {
      fAddr = addr;
      string::size_type pos = fAddr.find (":");
      if (pos != string::npos) {
         fServer = fAddr.substr (0, pos);
         fPort = atoi (fAddr.c_str() + pos + 1);
      }
      else {
         fServer = fAddr;
         fPort = kLARSPORT;
      }
      for (string::iterator i = fServer.begin(); 
          i != fServer.end(); ++i) {
         *i = tolower (*i);
      }
      while (!fServer.empty() && isspace (fServer[0])) {
         fServer.erase (0, 1);
      }
      while (!fServer.empty() && 
            isspace (fServer[fServer.size()-1])) {
         fServer.erase (fServer.size() - 1);
      }
      return true;
   }

//______________________________________________________________________________
   void dfmlars::close()
   {
      fAddr = "";
   }

//______________________________________________________________________________
   bool dfmlars::login (const UDN& udn,
                     const char* uname, const char* pword)
   {
      return lars_support::login (udn, uname, pword);
   }

//______________________________________________________________________________
   bool dfmlars::requestUDNs (UDNList& udn)
   {
      if (!fServer.empty()) {
         return false;
      }
      //udn.clear();
      lars_support::udnlist udns;
      if (!lars_support::getUDNList (udns)) {
         return false;
      }
      for (lars_support::udniter i = udns.begin(); 
          i != udns.end(); ++i) {
         udn.insert (UDNList::value_type (UDN (i->c_str()), UDNInfo()));
      }
      return true;
   }

//______________________________________________________________________________
   bool dfmlars::requestUDNInfo (const UDN& udn, UDNInfo& info)
   {
      if (!fServer.empty()) {
         return false;
      }
      frametype ftype;
      channellist chns; 
      UDNInfo::dataseglist dsegs;
      if (!lars_support::getInfo ((const char*)udn, ftype, chns, dsegs)) {
         return false;
      }
      info.setType (ftype);
      info.setChannels (chns);
      info.setDataSegs (dsegs);
      return true;
   }


}
