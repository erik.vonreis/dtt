/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: dfmlars							*/
/*                                                         		*/
/* Module Description: DFM low level interface (LARS)			*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 7Mar01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dfmsm.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_FDFMLARS_H
#define _LIGO_FDFMLARS_H


#include "dfmapi.hh"


namespace dfm {


/** @name DFM low level API (LARS)
    This header defines support methods for accessing data from the
    archive.
   
    @memo DFM low level API (LARS)
    @author Written March 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** Basic API to input from the archive
    
    @memo Basic archive input class.
 ************************************************************************/
   class dfmlars : public dfmapi {
   public:
      /// Default constructor
      dfmlars ();
   
      /// Open connection
      virtual bool open (const std::string& addr, bool read = true);
      /// Close connection
      virtual void close();
      /// Checks username/password; uses last/default login if no arguments
      virtual bool login (const UDN& udn,
                        const char* uname = 0, const char* pword = 0);
      /// Request a list of avaialble UDNs
      virtual bool requestUDNs (UDNList& udn);
      /// Request information asscociated with a UDN
      virtual bool requestUDNInfo (const UDN& udn, UDNInfo& info);
   
   protected:
      /// Server name
      std::string	fServer;
      /// Port number
      int		fPort;
   };

//@}

}

#endif // _LIGO_FDFMLARS_H
