#include "dfmtape.hh"
#include <stdio.h>
#include <sys/types.h>
#include <dirent.h>
#include <cstdlib>
#include <cstring>

namespace dfm {
   using namespace std;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dfmtape							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   dfmtape::dfmtape () 
   {
   }

//______________________________________________________________________________
   bool dfmtape::open (const std::string& addr, bool read)
   {
      fAddr = addr;
      return true;
   }

//______________________________________________________________________________
   void dfmtape::close()
   {
      fAddr = "";
   }

//______________________________________________________________________________
   bool dfmtape::requestUDNs (UDNList& udn)
   {
      //udn.clear();
      // check rmt devices
      DIR* dir = ::opendir ("/dev/rmt");
      if (dir) {
//         dirent* entry = (dirent*) 
//            new char [sizeof (dirent) + 1024 + 1];
         dirent* res;
         for (res = readdir (dir); res; res = readdir (dir)) {
//         while ((::readdir_r (dir, entry, &res) == 0) && res) {
            if (!*res->d_name || !isdigit (res->d_name[0])) {
               continue;
            }
            char* stop = 0;
            int num = strtol (res->d_name, &stop, 10);
            if (strcmp (stop, "n") == 0) {
               char buf[64];
               sprintf (buf, "tape:///dev/rmt/%in", num);
               udn.insert (UDNList::value_type (UDN (buf), UDNInfo()));
            }
         }
//         delete [] (char*)entry;
         ::closedir (dir);
      }
      return true;
   }

//______________________________________________________________________________
   bool dfmtape::requestUDNInfo (const UDN& udn, UDNInfo& info)
   {
      return true;
   }


}
