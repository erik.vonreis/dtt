//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmServer							        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

#include "TLGDfmServer.hh"
#include "TLGEntry.hh"
#include <TGMsgBox.h>
#include <TVirtualX.h>


namespace dfm {
   using namespace std;
   using namespace dttgui;
   using namespace fantom;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static string trim (const char* p)
   {
      while (isspace (*p)) p++;
      string s = p;
      while ((s.size() > 0) && isspace (s[s.size()-1])) {
         s.erase (s.size() - 1);
      }
      return s;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGDfmServerDlg						        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGDfmServerDlg::TLGDfmServerDlg (const TGWindow *p, 
                     const TGWindow *main, dataaccess& dacc,
                     Bool_t sourcesel, selserverlist& list, 
                     TString& server, Bool_t& ret)
   : TLGTransientFrame (p, main, 10, 10), fSourceSel (sourcesel),
   fDacc (&dacc), fCurServers (list), fServers (&list), 
   fServer (&server), fOk (&ret), fId (0) 
   {
   
      fL[0] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 0, 2, 2);
      fL[1] = new TGLayoutHints (kLHintsTop | kLHintsLeft, 0, 0, 0, 0);
      fL[2] = new TGLayoutHints (kLHintsTop | kLHintsLeft,
                           400, 2, 2, 2);
      fL[3] = new TGLayoutHints (kLHintsTop | kLHintsRight,
                           30, 5, 12, 2);
      fL[4] = new TGLayoutHints (kLHintsTop | kLHintsExpandY, 5, 5, 5, 5);
      fL[5] = new TGLayoutHints (kLHintsTop | kLHintsExpandX, 5, 5, 5, 5);
      fL[6] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 12, 2, -2);
      fL[7] = new TGLayoutHints (kLHintsTop | kLHintsLeft, 0, 0, 5, 0);
      fL[8] = new TGLayoutHints (kLHintsTop | kLHintsLeft, 5, 5, 5, 5);
      fL[9] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 0, 2, -2);
   
      fF[0] = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fF[0], fL[1]);
      // available group
      fAvailGroup = new TGGroupFrame (fF[0], "Available");
      fF[0]->AddFrame (fAvailGroup, fL[8]);
      fAvail = new TGListBox (fAvailGroup, kDfmSrvAvail);
      fAvail->Resize (300, 200);
      fAvail->Associate (this);
      fAvailGroup->AddFrame (fAvail, fL[7]);
      // select buttons
      fF[1] = new TGVerticalFrame (fF[0], 10, 10);
      fF[0]->AddFrame (fF[1], fL[4]);
      fAddButton = new TGTextButton (fF[1], "      >>      ", kDfmSrvAdd);
      fAddButton->Associate (this);
      fF[1]->AddFrame (fAddButton, fL[0]);
      fRemButton = new TGTextButton (fF[1], "      <<      ", kDfmSrvRemove);
      fRemButton->Associate (this);
      fF[1]->AddFrame (fRemButton, fL[0]);
      // selected group
      fSelGroup = new TGGroupFrame (fF[0], "Selected");
      fF[0]->AddFrame (fSelGroup, fL[8]);
      fSel = new TGListBox (fSelGroup, kDfmSrvAvail);
      fSel->Resize (300, 200);
      fSel->Associate (this);
      fSelGroup->AddFrame (fSel, fL[7]);
      // new group
      if (fSourceSel) {
         fNewGroup = new TGGroupFrame (this, "New");
         AddFrame (fNewGroup, fL[5]);
         fF[2] = new TGHorizontalFrame (fNewGroup, 10, 10);
         fNewGroup->AddFrame (fF[2], fL[5]);
         fLabel[0] = new TGLabel (fF[2], "Type:");
         fF[2]->AddFrame (fLabel[0], fL[6]);
         fNewType = new TGComboBox (fF[2], kDfmSrvNewType);
         fNewType->Resize (100, 23);
         fNewType->Associate (this);
         fF[2]->AddFrame (fNewType, fL[6]);
         fLabel[1] = new TGLabel (fF[2], "  Addr:");
         fF[2]->AddFrame (fLabel[1], fL[6]);
         fNewAddr = new TLGTextEntry (fF[2], "", kDfmSrvNewAddr);
         fNewAddr->Resize (320, fNewType->GetDefaultHeight());
         fNewAddr->Associate (this);
         fF[2]->AddFrame (fNewAddr, fL[6]);
         fLabel[2] = new TGLabel (fF[2], "  Port:");
         fF[2]->AddFrame (fLabel[2], fL[6]);
         fNewPort = new TLGNumericControlBox (fF[2], 0, 6, kDfmSrvNewPort, 
                              kNESInteger, kNEANonNegative, 
                              kNELLimitMax, 0, 65535);
         fNewPort->Associate (this);
         fF[2]->AddFrame (fNewPort, fL[6]);
         fNewButton = new TGTextButton (fF[2], 
                              new TGHotString ("   &Add   "), kDfmSrvNewAdd);
         fNewButton->Associate (this);
         fF[2]->AddFrame (fNewButton, fL[9]);
      }
      else {
         fNewGroup = 0;
         fF[2] = 0;
         fLabel[0] = 0;
         fNewType = 0;
         fLabel[1] = 0;
         fNewAddr = 0;
         fLabel[2] = 0;
         fNewPort = 0;
         fNewButton = 0;
      }
      // buttons
      fBtnFrame = new TGHorizontalFrame (this, 100, 20);
      AddFrame (fBtnFrame, fL[5]);
      fCancelButton = new TGTextButton (fBtnFrame,
                           new TGHotString ("     &Cancel     "), 0);
      fCancelButton->Associate (this);
      fBtnFrame->AddFrame (fCancelButton, fL[3]);
      fOkButton = new TGTextButton (fBtnFrame, 
                           new TGHotString ("         &Ok         "), 1);
      fOkButton->Associate (this);
      fBtnFrame->AddFrame (fOkButton, fL[3]);
   
      // set initial values
      for (selserveriter i = fCurServers.begin(); 
          i != fCurServers.end(); ++i) {
         i->setID (fId++);
      }
      Build (kTRUE, kTRUE);
      Build (kFALSE, kTRUE);
      if (fSourceSel) {
         // if (fDacc->isSupp (st_LARS)) {
            // fNewType->AddEntry ("LARS", 0);
            // fNewType->Select (0);
         // }
         if (fDacc->isSupp (st_NDS)) {
            fNewType->AddEntry ("NDS", 1);
            fNewType->Select (1);
         }
      }
      fAddDirty = false;
      // set dialog box title
      TString name = fSourceSel ? "Server Selection" : "Client Selection";
      SetWindowName (name);
      SetIconName (name);
      SetClassHints ("AddServerDlg", "AddServerDlg");
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
   
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
   
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGDfmServerDlg::~TLGDfmServerDlg ()
   {
      delete fSel;
      delete fAvail;
      delete fAddButton;
      delete fRemButton;
      delete fNewType;
      delete fNewAddr;
      delete fNewPort;
      delete fNewButton;
      delete fOkButton;
      delete fCancelButton;
      for (int i = 0; i < 3; i++) {
         delete fLabel[i];
      }
      for (int i = 0; i < 3; i++) {
         delete fF[i];
      }
      delete fSelGroup;
      delete fAvailGroup;
      delete fNewGroup;
      delete fBtnFrame;
      for (int i = 0; i < 10; i++) {
         delete fL[i];
      }
   }

//______________________________________________________________________________
   void TLGDfmServerDlg::CloseWindow()
   {
      if (fOk) *fOk = kFALSE;
      DeleteWindow();
   }

//______________________________________________________________________________
   void TLGDfmServerDlg::Build (Bool_t sel, Bool_t init)
   {
      if (sel) {
         if (!init) {
            fSel->RemoveEntries (0, 10000);
         }
         int id = 0;
         for (selserveriter i = fCurServers.begin(); 
             i != fCurServers.end(); ++i) {
            char buf[1024];
            sprintf (buf, "%s (%i)", (const char*)i->getName(), 
                    i->getID());
            fSel->AddEntry (buf, id++);
         }
         if (!init) {
            fSel->MapSubwindows();
            fSel->Layout();
         }
      }
      else {
         if (!init) {
            fAvail->RemoveEntries (0, 10000);
         }
         int id = 0;
         for (serveriter i = fDacc->begin(); i != fDacc->end(); ++i) {
            if ((fSourceSel && !i->second.supportInput()) ||
               (!fSourceSel && !i->second.supportOutput())) {
               continue;
            }
            fAvail->AddEntry ((const char*)i->first, id++);
         }
         if (!init) {
            fAvail->MapSubwindows();
            fAvail->Layout();
         }
      }
   }

//______________________________________________________________________________
   Bool_t TLGDfmServerDlg::checkLARS (const char* serv, int port) const 
   {
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGDfmServerDlg::checkNDS (const char* serv, int port) const 
   {
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGDfmServerDlg::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // Cancel
            case 0:
               {
                  if (fOk) *fOk = kFALSE;
                  DeleteWindow();
                  break;
               }
            // Ok
            case 1:
               {
                  // Check if add server is dirty first
                  if (fSourceSel) {
                     string addr = trim (fNewAddr->GetText());
                     if (fSourceSel && !addr.empty() && fAddDirty) {
                        Int_t retval;
                        new TGMsgBox (gClient->GetRoot(), this, "Warning", 
                                     "New server not yet added. Add it now?", 
                                     kMBIconQuestion, kMBYes | kMBNo, &retval);
                        if (retval == kMBYes) {
                           ProcessMessage (msg, kDfmSrvNewAdd, 0);
                        }
                     }
                  }
                  // get values
                  *fServers = fCurServers;
                  if (fCurServers.size() == 1) {
                     *fServer = fCurServers.front().getName();
                  }
                  else {
                     *fServer = "";
                  }
                  if (fOk) *fOk = kTRUE;
                  DeleteWindow();
                  break;
               }
            // Add server
            case kDfmSrvAdd:
               {
                  TGTextLBEntry* e = 
                     (TGTextLBEntry*) fAvail->GetSelectedEntry();
                  if (e) {
                     TString name = e->GetText()->GetString();
                     selserverentry e (dataservername ((const char*)name));
                     e.setID (fId++);
                     fCurServers.push_back (e);
                     Build ();
                  }
                  break;
               }
            // Remove server
            case kDfmSrvRemove:
               {
                  Int_t id = fSel->GetSelected();
                  if ((id >= 0) && (id < (int)fCurServers.size())) {
                     selserveriter pos = fCurServers.begin();
                     advance (pos, id);
                     fCurServers.erase (pos);
                     Build();
                  }
                  break;
               }
            // New server
            case kDfmSrvNewAdd:
               {
                  if (!fSourceSel) {
                     break;
                  }
                  Int_t id = fNewType->GetSelected();
                  if ((id >= 0) && (id < 2)) {
                     // get type, addr and port
                     dataservicetype type = id == 0 ?
                        st_LARS : st_NDS;
                     string addr = trim (fNewAddr->GetText());
                     int port = fNewPort->GetIntNumber();
                     if (port == 0) {
                        port = (type == st_LARS) ? kLARSPORT : kNDSPORT;
                     }
                     if ((type == st_NDS) && addr.empty()) {
                        addr = "0";
                     }
                     // check if valid address
                     if (((type == st_NDS) && 
                         !checkNDS (addr.c_str(), port)) ||
                        ((type == st_LARS) && 
                        !checkLARS (addr.c_str(), port))) {
                        Int_t retval;
                        new TGMsgBox(gClient->GetRoot(), this, "Error", 
                                    "Invalid or inaccessible data server.", 
                                    kMBIconStop, kMBOk, &retval);
                     }
                     // now add server
                     else {
                        char buf[1024];
                        if ((type == st_LARS) && addr.empty()) {
                           strcpy (buf, "");
                        }
                        else {
                           sprintf (buf, "%s:%i", addr.c_str(), port);
                        }
                        dataservername dname (type, string (buf));
                        dataserver ds (type, buf);
                        if (fDacc->insert (dname.get(), ds)) {
                           Build (kFALSE);
                        }
                     }
                  }
                  fAddDirty = false;
                  break;
               }
         }
      }
      // Text entry
      if ((GET_MSG (msg) == kC_TEXTENTRY) &&
         (GET_SUBMSG (msg) == kTE_TEXTCHANGED)) {
         switch (parm1) {
            // server address and port
            case kDfmSrvNewAddr:
            case kDfmSrvNewPort:
               {
                  fAddDirty = true;
                  break;
               }
         }
      }
      return kTRUE;
   }


}
