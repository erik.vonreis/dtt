#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;

// #pragma link C++ struct dfm::MonitorProgram;
// #pragma link C++ class dfm::TLGDfmMonitorDlg;
// #pragma link C++ class dfm::TLGDfmChnSelDlg;
// #pragma link C++ class dfm::TLGDfmServerDlg;
// #pragma link C++ class dfm::TLGDfmTimeSelDlg;
// #pragma link C++ class dfm::TLGDfmUDNDirDlg;
// #pragma link C++ class dfm::TLGDfmUDNTapeDlg;
// #pragma link C++ class dfm::TLGDfmUDNSmDlg;
// #pragma link C++ class dfm::TLGDfmUDNDlg;
// #pragma link C++ class dfm;

#endif

