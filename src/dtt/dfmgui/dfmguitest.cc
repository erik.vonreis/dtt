
#include "dfmgui.hh"
#include "dataacc.hh"
#include <TApplication.h>
#include <TEnv.h>
#include <TGButton.h>
#include <TGFrame.h>
#include <TROOT.h>
#include <TSystem.h>
#include <TVirtualX.h>
//#include <iostream>

   using namespace dfm;
   using namespace std;


   dataaccess dacc (dataaccess::kSuppAll);


   class MainMainFrame : public TGMainFrame {
   private:
      TGLayoutHints*     fL1;
      TGButton*		 fDfmButton[4];
      TGButton*		 fExitButton;
   
   public:
      MainMainFrame (const TGWindow *p, UInt_t w, UInt_t h);
      virtual ~MainMainFrame();
      virtual void CloseWindow();
      virtual Bool_t ProcessMessage (Long_t msg, Long_t parm1, Long_t);
   };

   MainMainFrame::MainMainFrame (const TGWindow *p, UInt_t w, UInt_t h)
   : TGMainFrame (p, w, h, kMainFrame | kVerticalFrame)
   {
      FontStruct_t labelfont;
      labelfont = gClient->GetFontByName 
         (gEnv->GetValue
         ("Gui.NormalFont",
         "-adobe-helvetica-medium-r-*-*-12-*-*-*-*-*-iso8859-1"));
   
   /* Define new graphics context. Only the fields specified in
      fMask will be used. */
   
      GCValues_t   gval;
      gval.fMask = kGCForeground | kGCFont;
      gval.fFont = gVirtualX->GetFontHandle(labelfont);
   
      fL1 = new TGLayoutHints (kLHintsTop | kLHintsLeft, 6, 6, 6, 6);
   
      // add buttons
      for (int i = 0; i < 4; i++) {
         fDfmButton[i] = new TGTextButton (this, " DFM Test... ", i+2);
         fDfmButton[i]->Associate (this);
         AddFrame (fDfmButton[i], fL1);
      }
      fExitButton = new TGTextButton (this, " Exit ", 1);
      fExitButton->Associate (this);
      AddFrame (fExitButton, fL1);
   
      SetWindowName ("DFM Test");
      SetIconName ("DFM Test");
      SetClassHints ("DFMTest", "DFMTest");
      SetWMPosition (0,0);
      MapSubwindows ();
   
      // we need to use GetDefault...() to initialize the layout algorithm...
      Resize (GetDefaultSize());
      MapWindow ();
   }


   MainMainFrame::~MainMainFrame()
   {
      for (int i = 0; i < 4; i++) {
         delete fDfmButton[i];
      }
      delete fExitButton;
      delete fL1;
   }



   Bool_t MainMainFrame::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      switch (GET_MSG (msg)) {
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCM_BUTTON:
                     {
                        switch (parm1) {
                           // exit button
                           case 1:
                              {
                                 CloseWindow();
                                 break;
                              }
                           // Dfm button
                           case 2:
                              {
                                 dfmDlg (gClient->GetRoot(), this, dacc);
                                 break;
                              }
                           // Dfm button
                           case 3:
                              {
                                 dfmDlg (gClient->GetRoot(), this, dacc, true,
                                        true, false);
                                 break;
                              }
                           // Dfm button
                           case 4:
                              {
                                 dfmDlg (gClient->GetRoot(), this, dacc, false, 
                                        false, false);
                                 break;
                              }
                           // Dfm button
                           case 5:
                              {
                                 dfmDlg (gClient->GetRoot(), this, dacc, false, 
                                        false, true);
                                 break;
                              }
                        }
                        break;
                     }
               }
            }
            break;
      }
      return kTRUE;
   }


   void MainMainFrame::CloseWindow()
   {
      TGMainFrame::CloseWindow();
      gApplication->Terminate(0);
   }


   TROOT root("GUI", "DFM Test");

   int main(int argc, char **argv)
   {
      TApplication theApp ("DFM Test", &argc, argv);
      MainMainFrame mainWindow (gClient->GetRoot(), 120, 120);
      theApp.Run();
      return 0;
   }
