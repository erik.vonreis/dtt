/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: ldxtype							*/
/*                                                         		*/
/* Module Description: Types used by lidax				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dfmxml.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_MONTYPE_H
#define _LIGO_MONTYPE_H

#include <string>
#include <vector>


namespace dfm {


/** @name DFM Types 
    This header defines some common types used by lidax.
   
    @memo DFM Types
    @author Written April 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** Monitor program description.
    @memo Monitor program description.
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   struct MonitorProgram {
      /// Program name
      std::string 	fName;
      /// Aruments
      std::string	fArgs;
      /// Shared memory partition
      std::string	fUDN;
   };
   /// List of monitor programs
   typedef std::vector<MonitorProgram> MonitorList;
   typedef MonitorList::iterator monitor_iter;
   typedef MonitorList::const_iterator const_monitor_iter;

//@}

}

#endif // _LIGO_MONTYPE_H
