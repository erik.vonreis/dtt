/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: repeat							*/
/*                                                         		*/
/* Procedure Description:Repeat test iterator				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/


#include <time.h>
#include "repeat.hh"
#include "diagdatum.hh"
#include "diagtest.hh"

namespace diag {
   using namespace std;

   bool repeatiterator::begin (std::ostringstream& errmsg)
   {
      if ((storage->Sync == 0) ||
         (!diagSync::self().getParam (*storage->Sync, 
                           stSyncRepeat, numsteps))) {
         numsteps = 0;
      }
      return (numsteps >= 1);
   }

   bool repeatiterator::evaluate (std::ostringstream& errmsg, 
                     bool& anotherone, int testrindex,
                     int& rindex, bool& note) 
   {
      anotherone = (step() < numOfSteps());
      note = (numsteps > 1);
      return true;
   }

   int repeatiterator::numOfSteps () const 
   {
      return numsteps;
   }

   testiterator* repeatiterator::self () const 
   {
      return new (nothrow) repeatiterator();
   }

}
