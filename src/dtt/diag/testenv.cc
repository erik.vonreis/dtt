/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: testenv							*/
/*                                                         		*/
/* Module Description: test environment abstract class			*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

/* Header File List: */

#include <time.h>
#include <iostream> // JCB
#include "testenv.hh"
#include "diagnames.h"
#include "diagdatum.hh"


namespace diag {
   using namespace std;
   using namespace thread;

static const int my_debug = 0 ;

   // JCB - initialize class instance count.
   int testenvironment::instance_count = 0 ;

   // JCB - move contructor to source file from header file.
   testenvironment::testenvironment()
   {
      myinstance = instance_count ; // JCB
      instance_count++; // JCB
      { // JCB
//	 char str[128] ;
//	 sprintf(str, "testenvironment instance %d created\n", myinstance) ;
//	 cerr << str ;
      }
   }


   testenvironment::~testenvironment () 
   {
      stopEnvironment (_ONESEC);
      { // JCB
//	 char str[128] ;
//	 sprintf(str, "testenvironment instance %d destroyed\n", myinstance) ;
//	 cerr << str ;
      }
   }


   bool testenvironment::subscribeEnvironment (ostringstream& errmsg)
   {
      semlock		lockit (envmux);
      bool 		err = false;
   
      if (my_debug) cerr << "testenvironment::subscribeEnvironment()" << endl ;
      if (envexc == 0) {
         errmsg << "No excitation manager for environment" << endl;
         return false;
      }
   
      for (environmentlist::const_iterator iter = envlist.begin();
          iter != envlist.end(); iter++) {
         if (iter->active) {
            ostringstream	cmd (iter->waveform);
            /* add waveform points to arb command */
            if (iter->waveform.find ("arb") != string::npos) {
               for (vector<float>::const_iterator 
                   iter2 = iter->points.begin();
                   iter2 != iter->points.end(); iter2++) {
                  cmd << " " << *iter2;
               }
            }
         
            if (!envexc->add (iter->channel, cmd.str(), iter->wait)) {
               errmsg << "Invalid environment channel '" << iter->channel <<
                  "'" << endl;
               err = true;
            }
         }
      }
      if (my_debug) cerr << "testenvironment::subscribeEnvironment() return" << endl ;
      return !err;
   }


   double testenvironment::waitEnvironment () const 
   {
      semlock		lockit (envmux);
   
      if (envexc != 0) {
         return envexc->dwellTime();
      }
      else {
         return -1;
      }
   }


   bool testenvironment::startEnvironment (tainsec_t starttime, 
                     tainsec_t timeout) 
   {
      semlock		lockit (envmux);
      bool retval ;
   
      if (my_debug) cerr << "testenvironment::startEnvironment(starttime = " << (starttime / _ONESEC) << '.' << (starttime % _ONESEC) << ", timeout = " << (timeout / _ONESEC) << '.' << (timeout % _ONESEC) << ')' << endl ;
      if (envexc != 0) {
         retval = envexc->start (starttime, timeout);
      }
      else {
         retval = false;
      }
      if (my_debug) cerr << "testenvironment::startEnvironment() return " << (retval ? "true" : "false") << endl ;
      return (retval) ;
   }


   bool testenvironment::stopEnvironment (tainsec_t timeout) 
   {
      semlock		lockit (envmux);
      bool retval ;
   
      if (my_debug) cerr << "testenvironment::stopEnvironment(timeout = " << (timeout / _ONESEC) << '.' << (timeout % _ONESEC) << ')' << endl ;
      if (envexc != 0) {
         retval = envexc->stop (timeout);
      }
      else {
         retval = false;
      }
      if (my_debug) cerr << "testenvironment::stopEnvironment() return "<< (retval ? "true" : "false") << endl ;
      return (retval) ;
   }


   bool testenvironment::clearEnvironment (tainsec_t timeout) 
   {
      semlock		lockit (envmux);
      bool retval ;
   
      if (my_debug) cerr << "testenvironment::clearEnvironment(timeout = " << (timeout / _ONESEC) << '.' << (timeout % _ONESEC) << ')' << endl ;
   
      if (envexc != 0) {
         retval = envexc->del ();
      }
      else {
         retval = false;
      }
      if (my_debug) cerr << "testenvironment::clearEnvironment() return "<< (retval ? "true" : "false") << endl ;
      return (retval) ;
   }


}
