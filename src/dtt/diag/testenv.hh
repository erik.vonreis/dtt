/* version $Id: testenv.hh 7692 2016-08-17 19:41:40Z ed.maros@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: testenv.h						*/
/*                                                         		*/
/* Module Description: test environment class				*/
/*									*/
/*                                                         		*/
/* Module Arguments: none				   		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 17Apr99  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: testenv.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-2178  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/* Code Compilation and Runtime Specifications:				*/
/*	Code Compiled on: Ultra-Enterprise, Solaris 5.6			*/
/*	Compiler Used: sun workshop C++ 5.0				*/
/*	Runtime environment: sparc/solaris				*/
/*                                                         		*/
/* Code Standards Conformance:						*/
/*	Code Conforms to: LIGO standards.	OK			*/
/*			  Lint.			TBD			*/
/*			  ANSI			OK			*/
/*			  POSIX			OK			*/
/*									*/
/* Known Bugs, Limitations, Caveats:					*/
/*								 	*/
/*									*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1996.			*/
/*                                                         		*/
/*                                                         		*/
/* California Institute of Technology			   		*/
/* LIGO Project MS 51-33				   		*/
/* Pasadena CA 91125					   		*/
/*                                                         		*/
/* Massachusetts Institute of Technology		   		*/
/* LIGO Project MS NW17-161				   		*/
/* Cambridge MA 01239					   		*/
/*                                                         		*/
/* LIGO Hanford Observatory				   		*/
/* P.O. Box 1970 S9-02					   		*/
/* Richland WA 99352					   		*/
/*                                                         		*/
/* LIGO Livingston Observatory		   				*/
/* 19100 LIGO Lane Rd.					   		*/
/* Livingston, LA 70754					   		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _GDS_TESTENV_H
#define _GDS_TESTENV_H

/* Header File List: */
#include <string>
#include <vector>
#include "gmutex.hh"
#include "gdsstring.h"
#include "excitation.hh"

namespace diag {


/** Test Environment
    This object implements a test enviroment.
    It is an abstract object which implements most of the control 
    and management functions, except reading the settings.
   
    @memo Object for implementing a test environment
    @author Written April 1999 by Daniel Sigg
    @version 0.1
 ************************************************************************/
   class testenvironment {
   public:
   
      /** Environment settings
          This object implements one evironment channel setting.
   
          @memo Type representing an environment channel
       ******************************************************************/
      class environment {
      public:
         /// array index of Env[]
         int			num;
         /// active flag
         bool			active;
         /// channel name
         std::string		channel;
         /// string describing waveform
         std::string			waveform;
         /// points of arbitrary waveform
         std::vector<float>	points;
         /// settling time
         double 		wait;
      
         /// Default constructor
         environment () {
         }
         /// Copy constructor
         environment (const environment& env) {
            *this = env;
         }
      };
   
      /// type representing a list of environment channels
      typedef std::vector<environment> environmentlist;
   
      /** Constructs a testenvironment object.
          @memo Default constructor.
       ******************************************************************/
      testenvironment () ;
   
      /** Destructss a testenvironment object. Calls stopEnvironment to
          make sure environment channels are off.
          @memo Default constructor.
       ******************************************************************/
      virtual ~testenvironment ();
   
      /** Sets the excitation manager for the environment.
          @memo Set excitation manager.
          @return true if successful
       ******************************************************************/
      virtual void setEnvironmentExcitationManager (
                        excitationManager* excmgr) {
         envexc = excmgr;
      }
   
      /** A function which reads the enviroment from the storage object
          into the environment list.
          @memo Read environment.
          @return true if successful
       ******************************************************************/
      virtual bool readEnvironment () = 0;
   
      /** A function which subscribes enviroment channels to the 
          excitation manager.
          @memo Subscribe environment channels.
          @param errmsg error message stream
          @return true if successful
       ******************************************************************/
      virtual bool subscribeEnvironment (std::ostringstream& errmsg);
   
      /** A function which returns the dwell time of all environment
          channels.
          @memo Wait time to settle environment.
          @return dwell time
       ******************************************************************/
      virtual double waitEnvironment () const;
   
      /** Switches excitation signals of the environment on. A zero start
          time will use the current time, a negative one will be ignored.
   	  @param start time start time of excitation signals
   	  @param timeout time to wait for completion
          @memo Switch environment signals on.
          @return true if successful
       ******************************************************************/
      virtual bool startEnvironment (tainsec_t starttime = 0, 
                        tainsec_t timeout = -1);
   
      /** Switches excitation signals of the environment off.
   	  @param timeout time to wait for completion
          @memo Switch environment signals off.
          @return true if successful
       ******************************************************************/
      virtual bool stopEnvironment (tainsec_t timeout = -1);
   
      /** Switches excitation signals of the environment off and 
          clears the list of environment channels.
   	  @param timeout time to wait for completion
          @memo Clean up environment.
          @return true if successful
       ******************************************************************/
      virtual bool clearEnvironment (tainsec_t timeout = -1);
   
   protected:
      /// mutex to protect environment
      mutable thread::recursivemutex	envmux;
      /// excitation manager
      excitationManager*	envexc;
      /// list of environment channels
      environmentlist		envlist;

   // JCB
   private:
      // Keep track of instance of this class.
      static int instance_count ;
      int	myinstance ;
   
   };


}
#endif /* _GDS_TESTENV_H */
