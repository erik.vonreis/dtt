#include <string.h>
#include <strings.h>
#include <iostream>
#include <TVirtualX.h>
#include "TLGMonitor.hh"
#include "TLGEntry.hh"
#include "PlotSet.hh"
 
#ifndef WIDE_LISTBOXES
#define WIDE_LISTBOXES 1
#endif

namespace dttgui {
   using namespace std;
   using namespace xml;
   using namespace monapi;


   const int kMonitorSel = 100;
   const int kObjectSel = 101;
   const int kObjectCur = 102;
   const int kButtonAdd = 103;
   const int kButtonDel = 104;
   const int kButtonUpdate = 105;
   const int kObjectOpt = 106;
   const int kUpdateNever = 107;
   const int kUpdateTime = 112;

   const int kMonDObjSelH = 405;
   const int kMonitorSelW = 200;
#ifdef WIDE_LISTBOXES
   const int kDObjectSelW = 400;
   const int kSelectdedH = 250;
   const int kSelectdedW = 440;
#else
   const int kDObjectSelW = 240;
   const int kSelectdedH = 250;
   const int kSelectdedW = 300;
#endif

   Cursor_t TLGMonitorSelection::fWaitCursor = (Cursor_t)-1;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGMonitorSelection                                                  //
//                                                                      //
// Monitor selection frame			                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGMonitorSelection::TLGMonitorSelection (const TGWindow* parent, 
                     TLGMonitorMgr& mgr, TLGMonitorDatumList& list,
                     Int_t id)
   : TGHorizontalFrame (parent, 10, 10), TGWidget (id),
   fMgr (&mgr), fDobjs (&list)
   {
      if (fWaitCursor == (Cursor_t)-1) {
         fWaitCursor = gVirtualX->CreateCursor (kWatch);
      }
      // layout hints
      fL[0] = new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                           kLHintsTop, 4, 4, 4, 4);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                           kLHintsTop, 0, 0, 0, 0);
      fL[2] = new TGLayoutHints (kLHintsLeft | 
                           kLHintsTop, 4, 4, 4, 4);
      fL[3] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 4, 4, 4, 4);
      fL[4] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsCenterY, 4, 4, 4, 4);
      fL[5] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsTop | kLHintsExpandY, 4, 4, 4, 4);
      fL[6] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop, 0, 0, 0, 0);
   
   
      fL[7] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop | kLHintsExpandY, 4, 4, 4, 4);
      fL[9] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 0, 2, -6);
      fL[6] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 0, 0, 0);
      fL[8] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 4, 2, 2);
      // group frames
      fAvail = new TGGroupFrame (this, "Available");
      AddFrame (fAvail, fL[0]);
      fButton = new TGVerticalFrame (this, 10, 10);
      AddFrame (fButton, fL[0]);
      fActive = new TGGroupFrame (this, "Selected");
      AddFrame (fActive, fL[0]);
      // helper frames
      fF[0] = new TGHorizontalFrame (fAvail, 10, 10);
      fAvail->AddFrame (fF[0], fL[1]);
      fF[1] = new TGVerticalFrame (fF[0], 10, 10);
      fF[0]->AddFrame (fF[1], fL[2]);
      fF[2] = new TGVerticalFrame (fF[0], 10, 10);
      fF[0]->AddFrame (fF[2], fL[2]);
      // Avail
      fLabel[0] = new TGLabel (fF[1], " Monitors:");
      fF[1]->AddFrame (fLabel[0], fL[3]);
      fMonitors = new TGListBox (fF[1], kMonitorSel);
      fMonitors->Resize (kMonitorSelW, kMonDObjSelH);
      fMonitors->Associate (this);
      fF[1]->AddFrame (fMonitors, fL[3]);
      fLabel[1] = new TGLabel (fF[2], " Data Objects:");
      fF[2]->AddFrame (fLabel[1], fL[3]);
      fDObjects = new TGListBox (fF[2], kObjectSel);
      fDObjects->Resize (kDObjectSelW, kMonDObjSelH);
      fDObjects->Associate (this);
      fF[2]->AddFrame (fDObjects, fL[3]);
      // Buttons
      for (int i = 0; i < 3; i++) {
         static const char* const btxt[3] = 
            {"    >>    ", "    <<    ", "  Refresh  "};
         fAction[i] = new TGTextButton (fButton, btxt[i], kButtonAdd + i);
         fAction[i]->Associate (this);
         fButton->AddFrame (fAction[i], fL[4]);
      }
      // Selection
      fF[3] = new TGVerticalFrame (fActive, 300, 250);
      fActive->AddFrame (fF[3], fL[1]);
      fSel = new TGListBox (fF[3], kObjectCur);
      fSel->Resize (kSelectdedW, kSelectdedH);
      fSel->Associate (this);
      fF[3]->AddFrame (fSel, fL[3]);
      fOptions = new TGTab (fF[3], 25, 25);
      fOptions->Associate (this);
      fTabUpdate = fOptions->AddTab (" Update ");
      fTabType = fOptions->AddTab (" Type ");
      fTabCal = fOptions->AddTab (" Calibration ");
      fF[3]->AddFrame (fOptions, fL[5]);
      for (int i = 0; i < 4; i++) {
         static const char* const utxt[4] = 
            {"Never", "Upon Request", "When changed", "Every"};
         fF[4+i] = new TGHorizontalFrame (fTabUpdate, 10, 10);
         fTabUpdate->AddFrame (fF[4+i], fL[6]);
         fUpdateSel[i] = new TGRadioButton (fF[4+i], utxt[i], 
                              kUpdateNever + i);
         fUpdateSel[i]->Associate (this);
         fF[4+i]->AddFrame (fUpdateSel[i], fL[3]);
      }
      fUpdateSel[2]->SetState (kButtonDisabled);
      fUpdateTime = new TLGNumericControlBox (fF[7], 10, 8, kUpdateTime,
                           kNESMinSec, kNEAPositive);
      fUpdateTime->Associate (this);
      fF[7]->AddFrame (fUpdateTime, fL[3]);
      fLabel[2] = new TGLabel (fF[7], " mm:ss");
      fF[7]->AddFrame (fLabel[2], fL[3]);
      // Initialize values
      if (fMgr->List().empty()) {
         fMgr->UpdateServices();
      }
      BuildAvailList (-1);
      BuildActiveList (-1);
   }

//______________________________________________________________________________
   TLGMonitorSelection::~TLGMonitorSelection ()
   {
      for (int i = 0; i < 3; i++) {
         delete fAction[i];
      }
      delete fUpdateTime;
      for (int i = 0; i < 4; i++) {
         delete fUpdateSel[i];
      }
      delete fOptions;
      delete fSel;
      delete fDObjects;
      delete fMonitors;
      for (int i = 0; i < 3; i++) {
         delete fLabel[i];
      }
      for (int i = 0; i < 8; i++) {
         delete fF[i];
      }
      delete fAvail;
      delete fActive;
      delete fButton;
      for (int i = 0; i < 10; i++) {
         delete fL[i];
      }
   }

//______________________________________________________________________________
   Bool_t TLGMonitorSelection::BuildAvailList (Int_t level)
   {
      // build monitor selection list
      if (level <= 0) {
         if (level == 0) {
            fMonitors->RemoveEntries (0, 10000);
         }
         int selid = -1;
         int id = -1;
         for (TLGMonitorMgr::servicelist::const_iterator 
             i = fMgr->List().begin(); i != fMgr->List().end(); i++) {
            fMonitors->AddEntry (i->first.c_str(), ++id);
            if (i->first == fCurService) selid = id;
         }
         if (selid >= 0) {
            fMonitors->Select (selid);
         }
         else if (id == -1) {
            fCurService = "";
         }
         else {
            fCurService = "";
         }
         if (level == 0) {
            fMonitors->MapSubwindows();
            fMonitors->Layout();
         }
      }
      // build data objects selection list
      if (level <= 1) {
         if (level >= 0) {
            fDObjects->RemoveEntries (0, 10000);
            fDObjects->Layout();
         }
         if (fCurService.empty()) {
            fCurDObject = "";
         }
         else {
            int selid = -1;
            int id = -1;
            TLGMonitorMgr::servicelist::const_iterator el = 
               fMgr->List().find (fCurService);
            if (el != fMgr->List().end()) {
               for (TLGMonitorMgr::dobjlist::const_iterator 
                   i = el->second.begin(); i !=  el->second.end(); i++) {
                  string e = i->first + " (" + 
                     kSeriesTypenames[i->second.fType]+ ")";
                  fDObjects->AddEntry (e.c_str(), ++id);
                  if (i->first == fCurDObject) selid = id;
               }
            }
            if (selid >= 0) {
               fDObjects->Select (selid);
            }
            else if (id == -1) {
               fCurDObject = "";
            }
            else {
               fCurDObject = el->second.begin()->first;
               fDObjects->Select (0);
            }
         }
         if (level >= 0) {
            fDObjects->MapSubwindows();
            fDObjects->Layout();
         }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGMonitorSelection::BuildActiveList (Int_t level)
   {
      // build active list
      if (level <= 0) {
         if (level >= 0) {
            fSel->RemoveEntries (0, 10000);
         }
         int selid = -1;
         int id = -1;
         for (TLGMonitorDatumList::iterator i = fDobjs->begin();
             i != fDobjs->end(); i++) {
            string entry = i->first.second + " (" +i->first.first + ")";
            fSel->AddEntry (entry.c_str(), ++id);
            if (i->first == fActiveDObject) selid = id;
         }
         if (selid >= 0) {
            fSel->Select (selid);
         }
         else if (id == -1) {
            fActiveDObject = TLGMonitorDatum::name_t ("", "");
         }
         else {
            fActiveDObject = fDobjs->begin()->first;
            fSel->Select (0);
         }
         if (level >= 0) {
            fSel->MapSubwindows();
            fSel->Layout();
         }
      }
      // update tab options
      if (level <= 1) {
         TransferOpt (kTRUE);
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGMonitorSelection::TransferOpt (Bool_t toGUI)
   {
      // write
      if (toGUI) {
         TLGMonitorDatumList::iterator a = fDobjs->find (fActiveDObject);
         if (a != fDobjs->end()) {
            for (int i = 0; i < 4; i++) {
               fUpdateSel[i]->SetState 
                  (i == (int) (a->second->GetUpdateOpt()) ? 
                  kButtonDown : kButtonUp);
            }
            fUpdateSel[2]->SetState (kButtonDisabled);
            fUpdateTime->SetNumber (a->second->GetUpdateInterval());
         }
      }
      // read
      else {
         TLGMonitorDatumList::iterator a = fDobjs->find (fActiveDObject);
         if (a != fDobjs->end()) {
            for (int i = 0; i < 4; i++) {
               if (fUpdateSel[i]->GetState() == kButtonDown) {
                  a->second->SetUpdateOpt 
                     ((TLGMonitorDatum::MonitorUpdate_t)i);
               }
            }
            a->second->SetUpdateInterval (fUpdateTime->GetNumber());
         }
      }
      return kTRUE;
   }

//______________________________________________________________________________
   Bool_t TLGMonitorSelection::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      // action buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
         switch (parm1) {
            // add >>
            case kButtonAdd:
               {
                  // get monitor and data object names
                  TGTextLBEntry* el1 = 
                     (TGTextLBEntry*)fMonitors->GetSelectedEntry();
                  TGTextLBEntry* el2 = 
                     (TGTextLBEntry*)fDObjects->GetSelectedEntry();
                  if ((el1 == 0) || (el2 == 0)) {
                     break;
                  }
                  string newmon = el1->GetText()->GetString();
                  string newobj = el2->GetText()->GetString();
                  string::size_type pos = newobj.rfind ('(');
                  if (pos != string::npos) {
                     newobj.erase (pos - 1);
                  }
                  TLGMonitorDatum::name_t name (newmon, newobj);
                  // check if already in active list
                  if (fDobjs->find (name) != fDobjs->end()) {
                     break;
                  }
                  // get monitor/data object pointer
                  TLGMonitorMgr::servicelist::const_iterator m = 
                     fMgr->List().find (newmon);
                  if (m == fMgr->List().end()) {
                     break;
                  }
                  TLGMonitorMgr::dobjlist::const_iterator d = 
                     m->second.find (newobj);
                  if (d == m->second.end()) {
                     break;
                  }
                  // add to active list
                  fDobjs->add (new TLGMonitorDatum 
                              (newmon.c_str(), newobj.c_str(), 
                              d->second.fType, d->second.fOption.c_str()));
                  // add to list box
                  fActiveDObject = name;
                  BuildActiveList (0);
                  // select the next entry in the data object list
                  Int_t id = fDObjects->GetSelected();
                  fDObjects->Select (id + 1);
                  if (fDObjects->GetSelectedEntry() == 0) {
                     fDObjects->Select (id);
                  }
                  break;
               }
            // delete <<
            case kButtonDel:
               {
                  // get monitor and data object names
                  TGTextLBEntry* el1 = 
                     (TGTextLBEntry*)fSel->GetSelectedEntry();
                  if (el1 == 0) {
                     break;
                  }
                  string n = el1->GetText()->GetString();
                  string::size_type pos1 = n.rfind ('(');
                  string::size_type pos2 = n.rfind (')');
                  if ((pos1 == string::npos) || (pos2 == string::npos)) {
                     break;
                  }
                  string newmon = n.substr (pos1 + 1, pos2 - pos1 - 1);
                  string newobj = n.substr (0, pos1 - 1);
                  TLGMonitorDatum::name_t name (newmon, newobj);
                  // delete from active list and list box
                  fDobjs->remove (name);
                  fActiveDObject = TLGMonitorDatum::name_t ("", "");
                  BuildActiveList (0);
                  break;
               }
            // update
            case kButtonUpdate:
               {
                  gVirtualX->SetCursor (fId, fWaitCursor);
                  gVirtualX->Update();
                  fMgr->UpdateServices();
                  BuildAvailList (0);
                  fCurService = "";
                  gVirtualX->SetCursor (fId, kNone);
                  ProcessMessage (MK_MSG (kC_COMMAND, kCM_LISTBOX), 
                                 kMonitorSel, 0);
                  break;
               }
         }
      }
      // radio buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_RADIOBUTTON)) {
         if ((parm1 >= kUpdateNever) && (parm1 < kUpdateNever + 4)) {
            for (int i = 0; i < 4; i++) {
               fUpdateSel[i]->SetState ((i == parm1 - kUpdateNever) ? 
                                    kButtonDown : kButtonUp);
            }
            fUpdateSel[2]->SetState (kButtonDisabled);
            TransferOpt (kFALSE);
         }
      }
      // text entries
      if ((GET_MSG (msg) == kC_TEXTENTRY) &&
         (GET_SUBMSG (msg) == kTE_TEXTUPDATED)) {
         if (parm1 == kUpdateTime) {
            TransferOpt (kFALSE);
         }
      }
      // list boxes
      else if ((GET_MSG (msg) == kC_COMMAND) &&
              (GET_SUBMSG (msg) == kCM_LISTBOX)) {
         switch (parm1) {
            // monitor selection
            case kMonitorSel:
               {
                  // update data object list
                  string newmon = fMonitors->GetSelectedEntry() ? 
                     ((TGTextLBEntry*)fMonitors->GetSelectedEntry())->
                     GetText()->GetString() : "";
                  if (newmon != fCurService) {
                     fCurService = newmon;
                     if (!fMgr->Initialized (fCurService.c_str())) {
                        gVirtualX->SetCursor (fId, fWaitCursor);
                        gVirtualX->Update();
                        fMgr->UpdateDObjects (fCurService.c_str());
                        BuildAvailList (1);
                        gVirtualX->SetCursor (fId, kNone);
                     }
                     else {
                        BuildAvailList (1);
                     }
                  }
                  break;
               }
            // object selection
            case kObjectSel:
               {
                  // nothing to do
                  break;
               }
            // current object selection
            case kObjectCur:
               {
                  // update choices in tab
                  TransferOpt (kFALSE);
                  // get new active selection
                  TGTextLBEntry* el1 = 
                     (TGTextLBEntry*)fSel->GetSelectedEntry();
                  if (el1 == 0) {
                     break;
                  }
                  string n = el1->GetText()->GetString();
                  string::size_type pos1 = n.rfind ('(');
                  string::size_type pos2 = n.rfind (')');
                  if ((pos1 == string::npos) || (pos2 == string::npos)) {
                     break;
                  }
                  string newmon = n.substr (pos1 + 1, pos2 - pos1 - 1);
                  string newobj = n.substr (0, pos1 - 1);
                  fActiveDObject = 
                     TLGMonitorDatum::name_t (newmon, newobj);
                  BuildActiveList (1);
                  break;
               }
         }
      }
      return kTRUE;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGMonitorSelectionDialog                                            //
//                                                                      //
// Monitor selection dialog box			                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGMonitorSelectionDialog::TLGMonitorSelectionDialog 
   (const TGWindow *p, const TGWindow *main, TLGMonitorMgr& mgr,
   TLGMonitorDatumList& list)
   : TLGTransientFrame (p, main, 10, 10)
   {
      // add widgets
      fL[0] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 4, 4, 4, 4);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsCenterY, 4, 4, 4, 4);
      fMon = new TLGMonitorSelection (this, mgr, list, 2);
      fMon->Associate (this);
      AddFrame (fMon, fL[0]);
      fOkButton = new TGTextButton (fMon->fButton, "    Ok    ", 1);
      fOkButton->Associate (this);
      fMon->fButton->AddFrame (fOkButton, fL[1]);
   
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
   
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates 
            (main->GetId(), GetParent()->GetId(),
            (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
            (((TGFrame*)main)->GetHeight() - fHeight) >> 1, ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
   
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
   
      // set dialog box title
      TString name = "Monitors";
      SetWindowName (name);
      SetIconName (name);
      SetClassHints ("MonSelDlg", "MonSelDlg");
   
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGMonitorSelectionDialog::~TLGMonitorSelectionDialog ()
   {
      delete fOkButton;
      delete fMon;
      delete fL[1];
      delete fL[0];
   }

//______________________________________________________________________________
   void TLGMonitorSelectionDialog::CloseWindow()
   {
      DeleteWindow();
   }

//______________________________________________________________________________
   Bool_t TLGMonitorSelectionDialog::ProcessMessage (Long_t msg, 
                     Long_t parm1, Long_t parm2) 
   {
      // Ok Button
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON) &&
         (parm1 == 1)) {
         DeleteWindow();
      }
      return kTRUE;
   }


}
