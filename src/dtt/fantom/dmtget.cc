#include <time.h>
#include "goption.hh"
#include "framefast/frameio.hh"
#include "dmtio.hh"
#include <stdio.h>
#include <iostream>
#include <fstream>

   using namespace fantom;
   using namespace gdsbase;
   using namespace std;

   int main (int argc, char** argv)
   {
      // option parsing
      option_string opts (argc, argv, "n:p:");
      if (opts.help() || (opts.argbegin() == opts.argend())) {
         cout << "usage: dmtget [-n] [-p partition] filename" << endl;
         cout << "       use # to represent an auto-increment number" << endl;
         exit (1);
      }
      int num = 1;
      string s;
      if (opts.getOpt ('n', s)) {
         num = atoi (s.c_str());
      }
      string part;
      if (!opts.getOpt ('p', part)) {
         part = "LHO_Online";
      }
      string filename = *opts.argbegin();
      if (num <= 0) {
         exit (0);
      }
      // create DMT access
      dmt_support dmt (false,  part.c_str());
      // loop over number of buffers
      for (int i = 0; i < num; i++) {
         // assemble file name
         string fname = filename;
         string::size_type pos = fname.find ('#');
         if (pos != string::npos) {
            fname.erase (pos, 1);
            char buf[16];
            sprintf (buf, "%i", num);
            fname.insert (pos, buf);
         }
         // get frame buffer
         framefast::frame_storage_ptr f (dmt.readFrame());
         // char* data = 0;
         // int size = dmt.readFrame (data, 0);
         if (f.data() == 0) {
            cerr << "Unable to get data from " << part << endl;
            exit (1);
         }
         // save data
         if (f.size() > 0) {
            ofstream out (fname.c_str());
            if (out) {
               out.write (f.data(), f.size());
            }
            cout << "Write a frame buffer to " << fname << endl;
         }
      }
   }
