/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: dmtio							*/
/*                                                         		*/
/* Module Description: DMT shared memory support for smartio		*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 13Dec00  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dmtio.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_DMTIO_H
#define _LIGO_DMTIO_H

#include "gmutex.hh"
#include "iosupport.hh"
#include <string>

   // shared memory producer
   class LSMP_PROD;
   // shared memory consumer
   class LSMP_CON;

namespace fantom {

   // Default shared memory size
   const int kSMDEFSIZE = 0x100000;
   // Default shared memory buffer number
   const int kSMDEFNUM = 2;

/** @name DMT support
    This header defines support methods for reading from and writing to
    shared memory partitions.
   
    @memo DMT support
    @author Written November 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** DMT IO class. This is a support class for the smart DMT input
    and smart DMT output device classes.
    \begin{verbatim}
    dmt://'part_name' [-l 'length'] [-n 'num']
    The following options are supported:
    -l 'length' : length of buffers in bytes; default 1MB
    -n 'num' : number of buffers in partition; default 2
    -o : off-line support; all clients must have read the buffer 
         before it is discarded
    \end{verbatim}
    
    @memo DMT IO support class.
 ************************************************************************/
   class dmt_support : public iosupport {
      friend class dmt_frame_storage;
      friend class dmt_frameout;
   
   public:
       /** Create support for DMT shared memory partitions.
           @param prod DMT support acts as a producer
           @param pname Partition name
           @param conf Option argument
         */
      explicit dmt_support (bool prod = true, const char* pname = 0, 
                        const char* conf = 0, bool open = true) 
      : fOut (prod), fBufLen (kSMDEFSIZE), fBufNum(kSMDEFNUM),
      fOffline (false), fProducer (0), fConsumer (0) {
         setPname (pname, conf, open); }
      /// Destructor
      virtual ~dmt_support();
   
      /// Set partition name
      bool setPname (const char* pname = 0, const char* conf = 0,
                    bool open = true);
      /// Get partition name
      const char* getPname () const {
         return (fPname.empty() ? 0 : fPname.c_str()); }
      /// Get buffer length
      int getBufLen () const {
         return fBufLen; }
      /// Get buffer number
      int getBufNum () const {
         return fBufNum; }
      /// Offline support?
      bool offline() const {
         return fOffline; }
   
      /// read frame from next buffer
      int readBuffer (char*& data, int max);
   
      /// Read next frame into buffer
      virtual framefast::basic_frame_storage* readFrame () {
         return readFrame (false); }
      /// Read next frame into buffer
      virtual framefast::basic_frame_storage* readFrame (bool nowait);
      /// get dmt writer
      virtual framefast::basic_frameout* getWriter (const char* fname);
      /// End of file?
      virtual bool eof() const {
         return (!fConsumer && !fProducer); }
   
   protected:
      /// Mutex
      thread::mutex	fMux;
      /// Is producer? (frame output)
      bool		fOut;
      /// Partition name (as specified)
      std::string	fPname;
      /// Buffer length
      int		fBufLen;
      /// Number of buffers
      int		fBufNum;
      /// Offline support
      bool		fOffline;
      /// DMT producer
      LSMP_PROD*	fProducer;
      /// DMT consumer
      LSMP_CON*		fConsumer;
   };


//@}

}

#endif // _LIGO_DMTIO_H
