#include <time.h>
#include <stdio.h>
#include <pthread.h>
#include <signal.h>
#include <unistd.h>
#include <fcntl.h>
#include <string>
#include <iostream>
#include <fstream>
#include "goption.hh"
#include "fantom.hh"
#include "framefast/framefast.hh"
#include <cstdlib>

   using namespace std;
   using namespace gdsbase;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// constants				                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////

   const string		help_text =
   "Usage: fantom : start interactive mode\n"
   "       fantom -c 'file': start batch mode reading from file\n"
   "       fantom -i -c 'file': start interactive mode initializing from file\n"
   "       fantom -e [commands] : start batch mode reading the arguments\n"
   "       fantom -i -e [commands] : start interactive mode with arguments\n"
   "       fantom -h : this help\n"
   "The list of commands has to be separated by semi-colon; typically\n"
   "surrounded by quotes to prevent the shell from interpreting them.\n"
   "In batch mode a go/quit command is automatically executed last.\n"
   "\nVersion $Id$\n" ;

   const string	help_in = 
   "Input commands:\n"
   "  open ['UDN']    : open an input channel (load UDN if specified)\n"
   "  close           : close input\n"
   "  flush           : flush input\n"
   "  add 'UDN'       : add an UDN to input\n"
   "  channels 'list' : set an input channel list";

   const string	help_out = 
   "Output commands:\n"
   "  open ['UDN']    : open an output channel (set UDN if specified)\n"
   "  close           : close output\n"
   "  flush           : flush output\n"
   "  add 'UDN'       : add an UDN to output\n"
   "  type 'format'   : set the output frame format\n"
   "                    FF16N1C0 - 16s frames, 1 per file, no compression\n"
   "  channels 'list' : set an output channel list";

   const string	help_set = 
   "Variables:\n"
   "  clock           : processing clock";

   const string	help_udn_file = 
   "file://\n"
   "Files can be specified by any valid UNIX format including wildcards.\n"
   "Example 1: 'file:///home/sigg/mydata/H-658085674-1.gwf'\n"
   "           represents a single file\n"
   "Example 2: 'file:///export/raid2/E2/00-11-12_17:10:19.0/*.gwf'\n"
   "           represents all frame files in the specified directory";

   const string	help_udn_dir = 
   "dir://\n"
   "A directory name can specify a numbering scheme. The format is:\n"
   "  'dirname'[@startdir[.startfile]:stop[.stopfile]][#filenum]\n"
   "If any of the options are appended to the directory, auto-increment\n"
   "support is enabled. The start argument describes the first directory\n"
   "number and optionally the first file number. Similarly, the stop\n"
   "argument describes the last directory number and optionally the last\n"
   "file number. The file number argument is used to determine how many\n"
   "files should be created per directory.\n"
   "Example 1: 'dir://test.@3:5'\n"
   "           represents the files 'test.3/*', 'test.4/*' and 'test.5/*'\n"
   "Example 2: 'dir://test.@4#3600'\n"
   "           represents the directories 'test.4/', 'test.5/', etc.,\n"
   "           assuming no more than 3600 files per directory";

   const string	help_udn_lars = 
   "lars://\n"
   "Requires a valid UDN into the archive\n"
   "(see www.ldas-sw.caltech.edu/ligotools/dataflow/UDN_List.html)\n"
   "Example: 'lars:////ligo/raw/lho/e5\n"
   "         represents raw frames from the 5th engineering run";

   const string	help_udn_tape = 
   "tape://\n"
   "If the device is a magnetic tape it should be of the format\n"
   "'/dev/rmt/0n'. If the device is a tar archive on disk, the name\n"
   "represents a file name. The following options are supported:\n"
   "-p 'filepos' : file position to start (read only)\n"
   "-a 'archnum' : archives per tape (write only)\n"
   "-n 'filenum' : number of files to read (read only)\n"
   "-d 'dir'     : directory name to use; may contain [#filenum] (write only)\n"
   "-f 'files'   : file name or wildcard (read only)\n"
   "-r 'robot'   : tape robot diver specification";

   const string	help_udn_http = 
   "http://\n"
   "The name must contain a valid http server followed by a slash and\n"
   "a valid file name. The format is: 'host[:port]/file'\n"
   "Example: 'http://ldas.caltech.edu:80/lho/H-658085674-1.gwf'\n"
   "         represents a single file in the directory lho\n"
   "The option '-p proxy:port' can be used to specifed a proxy server";

   const string	help_udn_nds = 
   "nds://\n"
   "Must point to a valid network data server. The default port is 8088.\n"
   "Example: 'nds://red.ligo-wa.caltech.edu/frames'\n"
   "         represents full frames form the NDS";

   const string	help_udn_dmt = 
   "dmt://\n"
   "Describes a shared memory partition. Supported options are:\n"
   "-l 'length' : length of partition in bytes\n"
   "-n 'num'    : number of partitions\n"
   "-o          : offline support; all clients must read buffer\n"
   "Exampe: 'dmt:///LHO_Online' to read LHO online data";

   const string	help_udn = 
   "The full format of a UDN is 'utype'://'name' ['options']\n"
   "Supported types\n"
   "  file            : frame file (supports wildcards)\n"
   "  dir             : directory (set) with frame files\n"
   "  lars            : LIGO archive server\n"
   "  tape            : magnetic tape\n"
   "  http            : web address\n"
   "  nds             : network data server\n"
   "  dmt             : DMT shared memory partition\n"
   "  eof             : end of file mark";

   const string	help_all = 
   "Commands:\n"
   "  in  'num' 'cmd' : setup input 'num' (see 'help in' for commands) \n"
   "  out 'num' 'cmd' : setup input 'num' (see 'help out' for commands) \n"
   "  get 'var'       : get a variable (see 'help get' for a list)\n"
   "  set 'var'       : set a variable (see 'help set' for a list)\n"
   "  go  ['num']     : process n seconds of data\n"
   "  read 'file'     : read script file\n"
   "  help 'cmd'      : help about commands\n"
   "  help UDN        : help about universal data set names\n"
   "  help 'utype'    : help about UDN of specified type\n"
   "  help            : this help"
   "  exit            : quit";

   const char* const prompt = "fantom> ";

   const int MAX_SMARTINPUT = 1000000;
   const int MAX_SMARTOUTPUT = 1000000;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// forward declarations			                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
extern "C" char* readline (const char* prompt);
extern "C" void add_history (const char* line);
extern "C" int rl_bind_key (int key, int (*)(void));
extern "C" int rl_insert (void);
extern "C" int rl_initialize (void);

namespace fantom {


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static string trim (const char* p)
   {
      while (isspace (*p)) p++;
      string s = p;
      while ((s.size() > 0) && isspace (s[s.size()-1])) {
         s.erase (s.size() - 1);
      }
      return s;
   }

//______________________________________________________________________________
   static string trimEq (const char* p)
   {
      while (isspace (*p) || (*p == '=')) p++;
      string s = p;
      while ((s.size() > 0) && isspace (s[s.size()-1])) {
         s.erase (s.size() - 1);
      }
      return s;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// fantom				                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   fantom::fantom ()
   : fError (false), fInteractive (true),  fVerbose (false), fHelp (false),
   fCtrlC (false), fFinished (false), fMux (fInputs, fOutputs, &fCtrlC)
   {
      // give me back my keys!
      rl_initialize();
      rl_bind_key ('@', rl_insert);
      rl_bind_key ('#', rl_insert);
   }

//______________________________________________________________________________
   fantom::fantom (int argc, char* argv[])
   : fError (false), fInteractive (true), fVerbose (false), fHelp (false),
   fCtrlC (false), fFinished (false), fMux (fInputs, fOutputs, &fCtrlC)
   {
      // give me back my keys!
      rl_initialize();
      rl_bind_key ('@', rl_insert);
      rl_bind_key ('#', rl_insert);

      // hack to find if -h was given as option.
      // option_string can't differentiate between -h and an error.
      bool has_help = false;

      for(int i=0; i < argc; ++i)
      {
        if(argv[i][0] == '-' && argv[i][1] == 'h')
        {
          has_help = true;
        }
      }

      // parse command line arguments
      option_string opts (argc, argv, "hic:e:v");
      if (opts.getOpt ('c', fFilename)) {
         fInteractive = false;
      }
      if (opts.getOpt ('e', fCommands)) {
         fInteractive = false;
      }
      if (opts.opt ('i')) {
         fInteractive = true;
      }
      if (opts.opt ('v')) {
         fVerbose = true;
      }
      if (has_help) {
        fHelp = true;
      }
      else if(opts.help()) {
        fError = true;
        fFinished = true;
      }
   }

//______________________________________________________________________________
   fantom::~fantom ()
   {
   }

//______________________________________________________________________________
   bool fantom::init ()
   {
      // read commands from file
      if (!fFilename.empty()) {
         if (!read (fFilename.c_str())) {
            fMsg = "Error: Illegal filename: ";
            fMsg += fFilename;
            fError = true;
         }
      }
   
      // read commands from command line
      if (!fCommands.empty()) {
         // make copy
         char* cmd = new (nothrow) char [fCommands.size() + 10];
         if (!cmd) {
            fMsg = "Error: insufficient memory";
            fError = true;
            return false;
         }
         strcpy (cmd, fCommands.c_str());
         // separate and parse
         char* last;
         char* p = strtok_r (cmd, ";", &last);
         while (p && !fError) {
            while (isspace (*p)) p++;
            if (!parse (p)) {
               fError = true;
            }
            p = strtok_r (0, ";", &last);
         }
         delete [] cmd;
      }
   
      return !fError;
   }

//______________________________________________________________________________
   bool fantom::process (int sec) 
   {
      const double epsilon = 1E-7;
   
      // process frames until done
      double duration = 0;
      double delta;
      do {
         delta = fMux.process();
         if (delta > 0) duration += delta;
         if (fCtrlC) {
            fCtrlC = false;
            return false;
         }
      } while ((delta > 0) && ((sec <= 0) || (duration + epsilon < sec)));
      if ((sec <= 0) && (delta == 0)) {
         fMux.flushOutput ();
         if (fCtrlC) {
            fCtrlC = false;
            return false;
         }
      }
      return true;
   }

//______________________________________________________________________________
   bool fantom::read (const char* filename)
   {
      cerr << "read file " << filename << endl;
      ifstream		inp (filename);
      string		line;
   
      // check if file exists
      if (!inp) {
         return false;
      }
   
      // read in file and call interpreter
      getline (inp, line, '\n');
      while (inp) {
      	 // remove blanks
         while (!line.empty() && 
               ((line[0] == ' ') || (line[0] == '\t'))) {
            line.erase (0, 1);
         }
         if ((!line.empty()) && (line[0] != '#')) {
            cerr << "read: " << line << endl;
            parse (line.c_str());
         }
         getline (inp, line, '\n');
      }
      return true;
   }

//______________________________________________________________________________
   bool fantom::parse (const char* line)
   {
      // Quit/Exit
      if ((strncasecmp (line, "quit", 4) == 0) ||
         (strncasecmp (line, "exit", 4) == 0)) {
         fFinished = true;
         return false;
      }
      
      // help
      else if (strncasecmp (line, "help", 4) == 0) {
         string arg = trim (line + 4);
         if (strncasecmp (arg.c_str(), "in", 2) == 0) {
            cout << help_in << endl;
         }
         else if (strncasecmp (arg.c_str(), "out", 3) == 0) {
            cout << help_out << endl;
         }
         else if ((strncasecmp (arg.c_str(), "get", 3) == 0) ||
                 (strncasecmp (arg.c_str(), "set", 3) == 0)) {
            cout << help_set << endl;
         }
         else if (strncasecmp (arg.c_str(), "udn", 3) == 0) {
            cout << help_udn << endl;
         }
         else if (strncasecmp (arg.c_str(), "file", 4) == 0) {
            cout << help_udn_file << endl;
         }
         else if (strncasecmp (arg.c_str(), "dir", 3) == 0) {
            cout << help_udn_dir << endl;
         }
         else if (strncasecmp (arg.c_str(), "lars", 4) == 0) {
            cout << help_udn_lars << endl;
         }
         else if (strncasecmp (arg.c_str(), "tape", 4) == 0) {
            cout << help_udn_tape << endl;
         }
         else if (strncasecmp (arg.c_str(), "http", 4) == 0) {
            cout << help_udn_http << endl;
         }
         else if (strncasecmp (arg.c_str(), "nds", 3) == 0) {
            cout << help_udn_nds << endl;
         }
         else if (strncasecmp (arg.c_str(), "dmt", 3) == 0) {
            cout << help_udn_dmt << endl;
         }
         else {
            cout << help_all << endl;
         }
         fMsg = "Help";
         return true;
      }
      
      // get value
      else if (strncasecmp (line, "get", 3) == 0) {
         string arg = trim (line + 3);
         if (strncasecmp (arg.c_str(), "clock", 5) == 0) {
            char buf [256];
            sprintf (buf, "clock = %lu", fMux.clock().getS());
            fMsg = string (buf);
            return true;
         }
         else {
            fMsg = "Error: Unknown parameter.";
            return false;
         }
      }
      // set value
      else if (strncasecmp (line, "set", 3) == 0) {
         string arg = trim (line + 3);
         if (strncasecmp (arg.c_str(), "clock", 5) == 0) {
            string val = trimEq (arg.c_str() + 5);
            char* stop;
            unsigned int n = strtoul (val.c_str(), &stop, 10);
            fMux.setClock (Time (n));
            fMsg = "Clock set";
            return true;
         }
         else {
            fMsg = "Error: Unknown parameter.";
            return false;
         }
      }
      
      // Define input channel
      else if (strncasecmp (line, "input", 2) == 0) {
         // skip input definition
         const char* p1 = line;
         const char* p2 = "input";
         while (*p1 && *p2 && (tolower (*p1) == tolower (*p2))) {
            p1++; p2++; 
         }
         // extract input number
         char* stop;
         int n = strtol (p1, &stop, 10);
         if ((n < 0) || (n > MAX_SMARTINPUT)) {
            fMsg = "Error: Input number out of range.";
            return false;
         }
         string s = trim (stop);
      
         // test if input exists
         smart_input* chn = fInputs.Get(n);
         if ((strncasecmp (s.c_str(), "open", 4) != 0) &&
            (chn == 0)) {
            fMsg = "Error: Input not defined.";
            return false;
         }
      
         // open
         if (strncasecmp (s.c_str(), "open", 4) == 0) {
            if (chn != 0) {
               fMsg = "Error: Input already in use.";
               return false;
            }
            s = trim (s.c_str() + 4);
            bool ret = fInputs.Add (n, s.c_str());
            fMsg = fInputs.Message();
            return ret;
         }
         // close
         else if (strncasecmp (s.c_str(), "close", 5) == 0) {
            fInputs.Delete (n);
            fMsg = "Input closed.";
            return true;
         }
         // flush
         else if (strncasecmp (s.c_str(), "flush", 5) == 0) {
            fMsg = "Input flushed.";
            return true;
         }
         // add
         else if (strncasecmp (s.c_str(), "add", 3) == 0) {
            s = trim (s.c_str() + 3);
            chn->addName (s.c_str());
            fMsg = "Input set.";
            return true;
         }
         // type
         else if (strncasecmp (s.c_str(), "type", 4) == 0) {
            fMsg = "Channel type ignored.";
            return true;
         }
         // channels
         else if (strncasecmp (s.c_str(), "channels", 8) == 0) {
            fMsg = "Channel list ignored.";
            return true;
         }
         // unknowwn
         else {
            fMsg = "Error: Unknown input command";
            return false;
         }
      }
      
      // Output channel
      else if (strncasecmp (line, "output", 3) == 0) {
         // skip output definition
         const char* p1 = line;
         const char* p2 = "output";
         while (*p1 && *p2 && (tolower (*p1) == tolower (*p2))) {
            p1++; p2++; 
         }
         // extract output number
         char* stop;
         int n = strtol (p1, &stop, 10);
         if ((n < 0) || (n > MAX_SMARTOUTPUT)) {
            fMsg = "Error: Output number out of range.";
            return false;
         }
         string s = trim (stop);
      
         // test if output exists
         smart_output* chn = fOutputs.Get(n);
         if ((strncasecmp (s.c_str(), "open", 4) != 0) &&
            (chn == 0)) {
            fMsg = "Error: Output not defined.";
            return false;
         }
      
         // open
         if (strncasecmp (s.c_str(), "open", 4) == 0) {
            if (chn != 0) {
               fMsg = "Error: Output already in use.";
               return false;
            }
            s = trim (s.c_str() + 4);
            bool ret = fOutputs.Add (n, s.c_str());
            fMsg = fOutputs.Message();
            return ret;
         }
         // close
         else if (strncasecmp (s.c_str(), "close", 5) == 0) {
            fOutputs.Delete (n);
            fMsg = "Output closed.";
            return true;
         }
         // flush
         else if (strncasecmp (s.c_str(), "flush", 5) == 0) {
	    bool ret = fMux.flushOutput (Time(n));
            if (!ret) fMsg = fMux.Message(); 
            else fMsg = "Output flushed.";
            return ret;
         }
         // add
         else if (strncasecmp (s.c_str(), "add", 3) == 0) {
            s = trim (s.c_str() + 3);
            chn->addName (s.c_str());
            fMsg = "Output set.";
            return true;
         }
         // type
         else if (strncasecmp (s.c_str(), "type", 4) == 0) {
            s = trim (s.c_str() + 4);
            if (!chn->setType (s.c_str())) {
               fMsg = chn->Message();
               return false;   
            }
            fMsg = "Channel type defined.";
            return true;
         }
         // channels
         else if (strncasecmp (s.c_str(), "channels", 8) == 0) {
            s = trim (s.c_str() + 8);
            if (!chn->setChannelList (s.c_str())) {
               fMsg = "Error: Invalid channel list.";
               return false;   
            }
            fMsg = "New channel list defined.";
            return true;
         }
         // unknowwn
         else {
            fMsg = "Error: Unknown output command";
            return false;
         }
      }
      
      // read configuration file
      else if (strncasecmp (line, "read", 4) == 0) {
         char buf [1024];
         strncpy (buf, line + 4, 1024); buf[1023] = 0;
         char* p = buf;
         while (isspace (*p)) p++;
         int pos = strlen (p) - 1;
         while ((pos >= 0) && isspace (p[pos])) p[pos] = 0;
         if (!read (p)) {
            fMsg = "Error: Illegal filename.";
            return false;
         }
         else {
            fMsg = "Configuration read.";
            return true;
         }
      }
      
      // go
      else if (strncasecmp (line, "go", 2) == 0) {
         char* stop;
         int n = strtol (line + 2, &stop, 10);
         bool ret = process (n);
         if (!ret) {
            fMsg = "Transfer aborted.";
         }
         return ret;
      }
      
      // no action for empty strings
      else if (strlen (line) == 0) {
         return true;
      }
      
      // unrecognized strings
      else {
         // smart_output* chn = fOutputs.Get(1);
         // if (chn != 0) {
            // const channelquery* q = 
               // findChannelMatch (chn->getChannelList(), line);
            // if (q) {
               // cerr << "Match " << q->GetPattern() << " @ " << 
                  // q->GetRate() << endl;
            // }
            // else {
               // cerr << "No match" << endl;
            // }
            // return true;
         // }
         fMsg = "Error: Unrecognized command";
         return false;
      }
   
      return true;
   }

//______________________________________________________________________________
   bool fantom::operator() ()
   {
      // main input loop
      if (fInteractive) {
         // read line from std input
         char* newline = readline (prompt);
         if (newline == 0) {
            return false;
         }
         // remove leading blanks
         char* startline = newline;
         while (*startline == ' ') {
            startline++;
         }
         // add to history
         if ((startline != 0) && (strlen (startline) > 0) && 
            (fLastline != startline)) {
            add_history (startline);
         }
         // parse
         fLastline = string (startline);
         bool ret = parse (startline);
         free (newline);
         return ret;
      }
      else {
         return false;
      }
   }

//______________________________________________________________________________
   void fantom::help ()
   {
      cerr << help_text;
   }

//______________________________________________________________________________
   void fantom::errorMessage ()
   {
      if (!fMsg.empty()) {
         cerr << fMsg << endl;
         fMsg = "";
      }
   }

//______________________________________________________________________________
   void fantom::interrupt (int signal)
   {
      fCtrlC = true;
      if (signal == SIGTERM) {
         fFinished = true;
         cerr << "Termination signal received" << endl;
      }
      else if (signal == SIGINT) {
         cerr << "Control-C received" << endl;
      }
   }

//______________________________________________________________________________
extern "C"
   void* connect_control_C (void* f) 
   {
   //    // mask ^C signal
      // sigset_t set;
      // if ((sigemptyset (&set) != 0) ||
         // (sigaddset (&set, SIGINT) != 0) ||
         // (pthread_sigmask (SIG_BLOCK, &set, 0) != 0)) {
         // cerr << "Unable to connect Ctrl-C" << endl;
         // return 0;
      // }
   
      // set signal mask to include Ctrl-C */
      sigset_t set;
      if ((sigemptyset (&set) != 0) ||
         (sigaddset (&set, SIGTERM) != 0) ||
         (sigaddset (&set, SIGINT) != 0)) {
         cerr << "Unable to connect Ctrl-C" << endl;
         return 0;
      }
      while (1) {
         // wait for ctrl-C signal
         int sig;
         sigwait (&set, &sig);
         ((fantom*)f)->interrupt (sig);
      }
      return 0;
   }

//______________________________________________________________________________
   int fantom::main (int argc, char *argv[])
   {
      fantom f (argc, argv);
   
      // on error print help
      if (!f) {
         f.help ();
         return 1;
      }
      // on help print help
      if (f.fHelp)
      {
        f.help();
        return 0;
      }
      // disable std err if not verbose
      if (!f.verbose()) {
         int i = ::open ("/dev/null", 2);
         //(void) dup2 (i, 1); // std out
         (void) dup2 (i, 2); // std err  
      }
   
      // initialization
      if (!f.init()) {
         f.errorMessage ();
      }
   
      // mask Ctrl-C signal
      sigset_t set;
      if ((sigemptyset (&set) != 0) ||
         (sigaddset (&set, SIGINT) != 0) ||
         (sigaddset (&set, SIGTERM) != 0) ||
         (pthread_sigmask (SIG_BLOCK, &set, 0) != 0)) {
         cerr << "Unable to connect Ctrl-C" << endl;
      }
      // create thread which handles Ctrl-C
      else {
         pthread_attr_t		tattr;
         int			status;
         if (pthread_attr_init (&tattr) == 0) {
            pthread_attr_setdetachstate (&tattr, PTHREAD_CREATE_DETACHED);
            pthread_attr_setscope (&tattr, PTHREAD_SCOPE_PROCESS);
            pthread_t tid;
            status = pthread_create (&tid, &tattr, connect_control_C, &f);
            pthread_attr_destroy (&tattr);
         }
         else {
            cerr << "Unable to connect Ctrl-C" << endl;
         }
      }
      // interactive processing
      if (f.interactive()) { 
      
         // main processing loop
         while (!f.finished()) {
            if (!f()) {
               f.errorMessage ();
            }
            else {
               f.errorMessage ();
            }
         }
         return 0;
      }
      // batch mode
      else {
         // go/quit
         if (!f.parse ("go")) {
            f.errorMessage ();
         }
         // timespec wait = {5, 0};
         // nanosleep (&wait, 0);
         return !f ? 1 : 0;
      }
   }

}



