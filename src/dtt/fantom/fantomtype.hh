/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: fantomtype						*/
/*                                                         		*/
/* Module Description: Basic data types for framefast and fantom	*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 26Sep00  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: fftype.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_FANTOMTYPE_H
#define _LIGO_FANTOMTYPE_H

#include <framefast/fftype.hh>

namespace fantom {

/** @name Atomic fantom data types
    Data types used by fantom.
   
    @memo Atomic fantom data types
    @author Written October 2000 by Daniel Sigg
    @version 1.0
 ************************************************************************/
//@{

/** Device type. Devices supported by fantom.
    
    @memo Device type.
 ************************************************************************/
   enum devicetype {
   /// Invalid
   dev_invalid = -1,
   /// File
   dev_file = 0,
   /// Directory
   dev_dir = 1,
   /// Tape/tar
   dev_tape = 2,
   /// DMT shared memory partition
   dev_dmt = 3,
   /// LARS/dfm server
   dev_lars = 4,
   /// HTTP address
   dev_http = 5,
   /// FTP address
   dev_ftp = 6,
   /// Network data server
   dev_nds = 7,
   /// NDS2 server
   dev_sends = 8,
   /// Function callback 
   dev_func = 9,
   /// End of file marker
   dev_eof = 10
   };


   /// Frame type
   typedef framefast::frametype frametype;
   using framefast::NONE;
   using framefast::FF;
   using framefast::STF;
   using framefast::MTF;

}
//@}

#endif // _LIGO_FANTOMTYPE_H
