#include "fmsgq.hh"


namespace fantom {
   using namespace std;
   using namespace thread;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// fmsgqueue::fmsg							//
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   fmsgqueue::fmsg::fmsg (const char* msg, double p1, double p2, double p3, 
                     double p4) 
   : fMsg (msg ? msg : ""), fTime (Now()) 
   {
      fParam[0] = p1; 
      fParam[1] = p2; 
      fParam[2] = p3; 
      fParam[3] = p4; 
   }

//______________________________________________________________________________
   double fmsgqueue::fmsg::param (int i) const
   {
      return ((i >= 0) && (i < 4)) ? fParam[i] : 0.0;
   }

//______________________________________________________________________________
   void fmsgqueue::fmsg::setparam (int i, double p)
   {
      if ((i >= 0) && (i < 4)) {
         fParam[i] = p;
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// fmsgqueue								//
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   fmsgqueue& fmsgqueue::operator= (const fmsgqueue& mq)
   {
      if (this != &mq) {
         semlock lock (fMux);
         semlock lock2 (mq.fMux);
         fMsg = mq.fMsg;
         fMax = mq.fMax;
      }
      return *this;
   }

//______________________________________________________________________________
   bool fmsgqueue::push (const fmsg& msg)
   {
      semlock lock (fMux);
      if ((int)fMsg.size() >= fMax) {
         return false;
      }
      else {
         fMsg.push_back (msg);
         return true;
      }
   }

//______________________________________________________________________________
   bool fmsgqueue::pop (fmsg& msg)
   {
      semlock lock (fMux);
      if (fMsg.empty()) {
         return false;
      }
      else {
         msg = fMsg.front();
         fMsg.pop_front();
         return true;
      }
   }

//______________________________________________________________________________
   fmsgqueue::fmsg fmsgqueue::front () const
   {
      semlock lock (fMux);
      fmsg msg ("");
      if (!fMsg.empty()) {
         msg = fMsg.front();
      }
      return msg;
   }

//______________________________________________________________________________
   void fmsgqueue::clear()
   {
      semlock lock (fMux);
      fMsg.clear();
   }

//______________________________________________________________________________
   bool fmsgqueue::empty() const
   {
      semlock lock (fMux);
      return fMsg.empty();
   }

//______________________________________________________________________________
   int fmsgqueue::size() const
   {
      semlock lock (fMux);
      return fMsg.size();
   }

//______________________________________________________________________________
   void fmsgqueue::setmax (int max) 
   {
      semlock lock (fMux);
      fMax = max; 
   }

//______________________________________________________________________________
   int fmsgqueue::max() const 
   {
      semlock lock (fMux);
      return fMax; 
   }

}
