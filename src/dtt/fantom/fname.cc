#include <time.h>
#include "framefast/fftype.hh"
#include "fname.hh"
#include "goption.hh"
#include "framedir.hh"
#include <stdio.h>
#include <iostream>
#include <algorithm>
#include <cstring>

namespace fantom {
   using namespace std;
   using namespace gdsbase;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static string trim (const char* p)
   {
      while (isspace (*p)) p++;
      string s = p;
      while ((s.size() > 0) && isspace (s[s.size()-1])) {
         s.erase (s.size() - 1);
      }
      return s;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dev_from_name / name_from_dev	                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   devicetype dev_from_name (const char* dname)
   {
      if (!dname) {
         return dev_invalid;
      }
      else if (strncasecmp (dname, "file://", 7) == 0) {
         return dev_file;
      }
      else if (strncasecmp (dname, "dir://", 6) == 0) {
         return dev_dir;
      }
      else if (strncasecmp (dname, "tape://", 7) == 0) {
         return dev_tape;
      }
      else if (strncasecmp (dname, "dmt://", 6) == 0) {
         return dev_dmt;
      }
      else if (strncasecmp (dname, "lars://", 7) == 0) {
         return dev_lars;
      }
      else if (strncasecmp (dname, "http://", 7) == 0) {
         return dev_http;
      }
      else if (strncasecmp (dname, "ftp://", 6) == 0) {
         return dev_ftp;
      }
      else if (strncasecmp (dname, "nds://", 6) == 0) {
         return dev_nds;
      }
      else if (strncasecmp (dname, "nds2://", 7) == 0) {
         return dev_sends;
      }
      else if (strncasecmp (dname, "func://", 7) == 0) {
         return dev_func;
      }
      else if (strncasecmp (dname, "eof://", 6) == 0) {
         return dev_eof;
      }
      else {
         return dev_invalid;
      }
   }

//______________________________________________________________________________
   std::string name_from_dev (devicetype dtype)
   {
      switch (dtype) {
         case dev_file:
            return "file://";
         case dev_dir:
            return "dir://";
         case dev_tape:
            return "tape://";
         case dev_dmt:
            return "dmt://";
         case dev_lars:
            return "lars://";
         case dev_http:
            return "http://";
         case dev_ftp:
            return "ftp://";
         case dev_nds:
            return "nds://";
         case dev_sends:
            return "nds2://";
         case dev_func:
            return "func://";
         case dev_eof:
            return "eof://";
         case dev_invalid:
         default:
            return "";
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// dev_from_name / name_from_dev	                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   std::string fformat_to_string (frametype ftype,
                     int len, int num, int compr,int vers)
   {
      string s;
      switch (ftype) {
         default:
         case FF:
            {
               s = "FF";
               break;
            }
         case STF:
            {
               s = "STF";
               break;
            }
         case MTF:
            {
               s = "MTF";
               break;
            }
      }
      if (len <= 0) len = 1;
      if (num <= 0) num = 1;
      if (compr < 0) compr = 0;
      if (vers < 4) vers = framefast::kDefaultFrameVersion;
      char buf[256];
      sprintf (buf, "%s%iN%iC%iV%i", s.c_str(), len, num, compr, vers);
      s = buf;
      return s;
   }

//______________________________________________________________________________
   bool string_to_fformat (const char* format, frametype& ftype, 
                     int& len, int& fnum, int& compr, int& vers)
   {
      // Frame type
      string s = trim (format ? format : "");
      if (strncasecmp (s.c_str(), "FF", 2) == 0) {
         ftype = FF;
         len = 1;
         fnum = 1;
         s.erase (0, 2);
      }
      else if (strncasecmp (s.c_str(), "STF", 3) == 0) {
         ftype = STF;
         len = 60*60;
         fnum = 1;
         s.erase (0, 3);
      }
      else if (strncasecmp (s.c_str(), "MTF", 3) == 0) {
         ftype = MTF;
         len = 24*60;
         fnum = 1;
         s.erase (0, 3);
      }
      else {
         return false;
      }
      compr = 0;
      vers = framefast::kDefaultFrameVersion;
      // Frame length
      s = trim (s.c_str());
      if (!s.empty() && isdigit (s[0])) {
         char* stop;
         int num = strtol (s.c_str(), &stop, 10);
         s = trim (stop);
         if (ftype == FF) {
            if (num <= 0) { // || !ispoweroftwo (num)) {
               len = 1;
            }
            else {
               len = num;
            }
         }
         else if (ftype == STF) {
            if ((num <= 0) || (num % 60 != 0)) {
               len = 60;
            }
            else {
               len = num;
            }
         }
         else if (ftype == MTF) {
            if ((num <= 0) || (num % 60 != 0)) {
               len = 60;
            }
            else {
               len = num;
            }
         }
      }
      // Number of frames
      if (strncasecmp (s.c_str(), "N", 1) == 0) {
         s.erase (0, 1);
         char* stop;
         int num = strtol (s.c_str(), &stop, 10);
         s = trim (stop);
         if (num <= 0) {
            fnum = 1;
         }
         else {
            fnum = num;
         }
      }
      // compression ?
      if (strncasecmp (s.c_str(), "C", 1) == 0) {
         s.erase (0, 1);
         char* stop;
         int num = strtol (s.c_str(), &stop, 10);
         s = trim (stop);
         if (num < 0) {
            compr = 0;
         }
         else {
            compr = num;
         }
      }
      // version ?
      if (strncasecmp (s.c_str(), "V", 1) == 0) {
         s.erase (0, 1);
         char* stop;
         int num = strtol (s.c_str(), &stop, 10);
         s = trim (stop);
         if (num < 4) {
            vers = 4;
         }
         else {
            vers = num;
         }
      }
      return true;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// namerecord     			                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   void namerecord::setName (const std::string& name) 
   {
      fFullname = name;
      string::size_type pos = fFullname.find ("://");
      if (pos == string::npos) {
         fName = fFullname;
      }
      else {
         fName = trim (fFullname.c_str() + pos + 3);
      }
      fDevType = dev_from_name (fFullname.c_str()); 
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// filenamerecord     			                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   bool filenamerecord::isOutside (const Time& t0, 
                     const Interval& dt) const
   {
     Interval eps(1e-8);
      // check upper limit
     if (fT0 + double(fNCont + 1) * fDt < t0 + eps) {
         return true;
      }
      // check lower limit
     if ((dt > Interval(0.)) && (fT0 > t0 + dt + eps)) {
         return true;
      }
      return false;
   }

//______________________________________________________________________________
   bool filenamerecord::setConf (const char* conf)
   {
      fNCont = 0;
      if (!conf || (fName.find_first_of ("[*?") != string::npos)) {
         return false;
      }
      option_string opts ("", conf);
      opts.parse ("c:");
      int n;
      if (opts.getOpt ('c', n)) {
         fNCont = n; // continuation
         char fconf[256];
         sprintf (fconf, "-c %i", n);
         namerecord::setConf (fconf);
      }
      return true;
   }

//______________________________________________________________________________
   bool filenamerecord::getNextName (std::string& fname)
   {
      return getNextName (fname, fT0, double(fNCont + 1) * fDt);
   }

//______________________________________________________________________________
   bool filenamerecord::getNextName (std::string& fname, 
                     const Time& t0, const Interval& dt)
   {
      if (fN > fNCont) {
         return false;
      }
      if (fNCont == 0) {
         ++fN;
         fname = fName;
         return true;
      }
      if (fN == 0) {
         FrameDir::gps_t time;
         FrameDir::gps_t tlen;
         char prefix[16*1048];
         char suffix[16*1048];
         if (FrameDir::parseName (fName.c_str(), time, tlen, 
                              prefix, suffix) && (tlen > 0)) {
	    fData = ffData (prefix, suffix, Time(time), tlen);
         }
         else {
            fN = fNCont + 1;
            fname = fName;
            return true;
         }
         if (t0 > fT0) {
            fN = (unsigned int)(double(t0 - fT0) / fDt.GetS());
            if (fN > fNCont) {
               return false;
            }
         }
         fNMax = fNCont + 1;
         if (t0 + dt < fT0 + double(fNCont + 1) * fDt) {
            unsigned int delta = (unsigned int)
               (double(fT0 + double(fNCont + 1) * fDt - (t0 + dt)) / fDt.GetS());
            fNMax = (delta > fNMax) ? 0 : fNMax - delta;
         }
      }
      if (fN >= fNMax) {
         return false;
      }
      fname = fData.getFile (fN++);
      return true;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// namelist			                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   namelist::~namelist ()
   {
      clear();
   }

//______________________________________________________________________________
   bool namelist::parseName (devicetype dtype, const char* arg)
   {
      // options
      bool error = false;
      option_string opts ("", arg);
   
      switch (dtype) {
         // file
         case dev_file:
            {
               opts.parse ("");
               opts.parse ("c:");
               string conf;
               int n = 0;
               if (opts.getOpt ('c', n) && (n < 0)) n = 0;
               int fileadd = 0;
               for (option_string::arg_list::const_iterator i = 
                   opts.argbegin(); i != opts.argend(); ++i) {
                  // if no wildcards, just add it as is
                  if (i->find_first_of ("[*?") == string::npos) {
                     if (addFileName (i->c_str(), n)) {
                        ++fileadd;
                     }
                  }
                  // if wildcard assume these are frames
                  else {
                     fileadd += addFiles (i->c_str());
                  }
               }
               if (!fileadd) {
                  fMsg = "Unable to add a file.";
                  error = true;
               }
               break;
            }
         
         // directory
         case dev_dir:
            {
               opts.parse ("");   
               int diradd = 0;
               for (option_string::arg_list::const_iterator i = 
                   opts.argbegin(); i != opts.argend(); ++i, ++diradd) {
                  string dir  = "dir://" + *i;
                  addName (dir.c_str());
               }
               if (!diradd) {
                  fMsg = "Unable to add a directory.";
                  error = true;
               }
               break;
            }
         
         // tape/tar
         case dev_tape:
            {
               opts.parse ("p:a:n:d:f:r:");
               string conf;
               string s;
               if (opts.getOpt ('p', s)) conf += " -p " + s; // filepos
               if (opts.getOpt ('a', s)) conf += " -a " + s; // archnum
               if (opts.getOpt ('n', s)) conf += " -n " + s; // filenum
               if (opts.getOpt ('d', s)) conf += " -d " + s; // dir name
               if (opts.getOpt ('f', s)) conf += " -f " + s; // file(s)
               if (opts.getOpt ('r', s)) conf += " -r " + s; // robot
               int tapeadd = 0;
               for (option_string::arg_list::const_iterator i = 
                   opts.argbegin(); i != opts.argend(); ++i, ++tapeadd) {
                  string dev  = "tape://" + *i;
                  addName (dev.c_str(), conf.c_str());
               }
               if (!tapeadd) {
                  fMsg = "Unable to add tape device.";
                  error = true;
               }
               break;
            }
         
         // dmt
         case dev_dmt:
            {
               opts.parse ("l:n:o");
               string conf;
               string s;
               if (opts.getOpt ('l', s)) conf += " -l " + s; // buffer length
               if (opts.getOpt ('n', s)) conf += " -n " + s; // buffer number
               if (opts.opt ('o'))       conf += " -o";      // off-line use
               int dmtadd = 0;
               for (option_string::arg_list::const_iterator i = 
                   opts.argbegin(); i != opts.argend(); ++i, ++dmtadd) {
                  string dev  = "dmt://" + *i;
                  addName (dev.c_str(), conf.c_str());
               }
               if (!dmtadd) {
                  fMsg = "Unable to add DMT device.";
                  error = true;
               }
               break;
            }
         
         // lars (archive) server
         case dev_lars:
            {
               opts.parse ("");   
               int larsadd = 0;
               for (option_string::arg_list::const_iterator i = 
                   opts.argbegin(); i != opts.argend(); ++i, ++larsadd) {
                  string lars  = "lars://" + *i;
                  addName (lars.c_str());
               }
               if (!larsadd) {
                  fMsg = "Unable to add a lars.";
                  error = true;
               }
               break;
            }
         
         // web
         case dev_http:
            {
               opts.parse ("");   
               int httpadd = 0;
               for (option_string::arg_list::const_iterator i = 
                   opts.argbegin(); i != opts.argend(); ++i, ++httpadd) {
                  string web  = "http://" + *i;
                  addName (web.c_str());
               }
               if (!httpadd) {
                  fMsg = "Unable to add a http address.";
                  error = true;
               }
               break;
            }
         
         // ftp
         case dev_ftp:
            {
               break;
            }
         
         // nds
         case dev_nds:
         case dev_sends:
            {
               opts.parse ("");
               string conf;
               string s;
               int ndsadd = 0;
               for (option_string::arg_list::const_iterator i = 
                   opts.argbegin(); i != opts.argend(); ++i, ++ndsadd) {
		 string dev  = name_from_dev(dtype) + *i;
                  addName (dev.c_str(), conf.c_str());
                  //cerr << "add " << dev << " with " << conf << endl;
               }
               if (!ndsadd) {
                  fMsg = "Unable to add NDS device.";
                  error = true;
               }
               break;
            }
         
         // function callback
         case dev_func:
            {
               opts.parse ("d:");
               string conf;
               string s;
               if (opts.getOpt ('d', s)) conf += " -d " + s; // ptr to priv. data
               int funcadd = 0;
               for (option_string::arg_list::const_iterator i = 
                   opts.argbegin(); i != opts.argend(); ++i, ++funcadd) {
                  string dev  = "func://" + *i;
                  addName (dev.c_str(), conf.c_str());
               }
               if (!funcadd) {
                  fMsg = "Unable to add function callback device.";
                  error = true;
               }
               break;
            }
         
         // unknown/invalid
         case dev_invalid:
         default:
            {
               fMsg = "Unknown device type.";
               error = true;
               break;
            }
      }
   
      return !error;
   }

//______________________________________________________________________________
   bool namelist::parseName (const char* conf) 
   {
      string c = trim (conf);
      devicetype device;
      string arg;
   
      // determin device
      string::size_type pos = c.find ("://");
      if ((pos == string::npos) || (pos == 0)) {
         return false;
      }
      device = dev_from_name (c.substr (0, pos + 3).c_str());
      // reverse arguments suited for "command line" parsing
      c = trim (c.c_str() + pos + 3);
      pos = c.find (" -");
      if (pos != string::npos) {
         arg = c.substr (pos + 1);
         if (pos > 0) {
            arg += " ";
            arg += c.substr (0, pos);
         }
      }
      else {
         arg = c;
      }
   
      return parseName (device, arg.c_str());
   }

//______________________________________________________________________________
   bool namelist::addName (namerecord* name, bool back) 
   {
      if (!name) {
         return false;
      }
      if (back) {
         push_back (name);
      }
      else {
         push_front (name);
      }
      return true;
   }

//______________________________________________________________________________
   bool namelist::addName (const char* name, bool back) 
   {
      return addName (name, 0, back);
   }

//______________________________________________________________________________
   bool namelist::addName (const char* name, const char* conf,
                     bool back) 
   {
      string n = trim (name);
      namerecord* nrec = 0;
      if (!n.empty() && (n.find ("://") != string::npos)) {
         nrec = new (nothrow) namerecord (n.c_str(), conf);
         if (nrec) {
            if (back) {
               push_back (nrec);
            }
            else {
               push_front (nrec);
            }
         }
      }
      return (nrec != 0);
   }

//______________________________________________________________________________
   bool namelist::addFileName (const char* name, unsigned int cont,
                     bool back) 
   {
      string n = trim (name);
      // empty?
      if (n.empty()) {
         return true;
      }
      // accurately defined?
      if (n.find ("://") == string::npos) {
         n.insert (0, "file://");
      }
      namerecord* nrec = 0;
      FrameDir::gps_t t0;
      FrameDir::gps_t dt;
      if ((cont == 0) || !FrameDir::parseName (name, t0, dt)) {
         nrec = new (nothrow) namerecord (n.c_str());
      }
      else {
         char conf[256];
         strcpy (conf, "");
         if (cont) {
            sprintf (conf, "-c %u", cont);
         }
         nrec = new (nothrow) filenamerecord (n.c_str(), conf, 
                              Time (t0, 0), Interval (dt, 0));
      }
      if (nrec) {
         if (back) {
            push_back (nrec);
         }
         else {
            push_front (nrec);
         }
      }
      return (nrec != 0);
   }

//______________________________________________________________________________
   int namelist::addFiles (const char* name, bool back) 
   {
      if (!name) {
         return 0;
      }
      string n = trim (name);
      // empty?
      if (n.empty()) {
         return 0;
      }
      // assume list of wildcard'ed file/dir names
      FrameDir fdir;
      char* files = new (nothrow) char [strlen (n.c_str()) + 10];
      if (files) {
         strcpy (files, n.c_str());
         char* last;
         char* p = strtok_r (files, " \f\n\r\t\v", &last);
         while (p) {
            fdir.add (p, true);
            p = strtok_r (0, " \f\n\r\t\v", &last);
         }
         delete [] files;
      }
      // add files to name list
      int num = 0;
      namelist::iterator ins = back ? end() : begin();
      for (FrameDir::series_iterator i = fdir.beginSeries(); 
          i != fdir.endSeries(); i++) {
         string filename ("file://");
         filename += i->second.getFile();
         char conf[256];
         strcpy (conf, "");
         if (i->second.getNFiles() > 1) {
            sprintf (conf, "-c %lu", i->second.getNFiles() - 1);
         }
         namerecord* nrec = new (nothrow) filenamerecord (filename.c_str(),
                              conf, i->second.getStartTime(), 
                              i->second.getDt());
         if (nrec) { 
            ins = insert (ins, nrec);
            ++ins;
            num++;
         }
      }
      return num; 
   }

//______________________________________________________________________________
   bool namelist::addNameList (const char* mem, int len, bool back)
   {
      const long buflen = 4*1024;
   
      // test for non-printable characters
      int testlen = (len > 10 * 1024) ? 10 * 1024 : len;
      for (int i = 0; i < testlen; ++i) {
         if (!isgraph (mem[i]) && !isspace (mem[i])) {
            return false;
         }
      }
      namelist::iterator ins = back ? end() : begin();
      int errnum = 0;
      const char* p = mem;
      const char* e;
      char* line = new char[buflen];
      char* pre = new char[buflen];
      char* post = new char[buflen];
      char* buf = new char[buflen];
      while ((p < mem + len) && (errnum < 10)) {
         while ((p < mem + len) && isspace (*p)) ++p;
         e = p;
         while ((e < mem + len) && (*e != '\n')) ++e;
         int sz = min (buflen - 10, long(e - p));
         strncpy (line, p, sz);
         line[sz] = 0;
         if (line[0] != '#') {
            for (int i = sz - 1; (i >= 0) && isspace (line[i]); --i) line[i] = 0;
            if (strstr (line, "://") != 0) {
               // conf string?
               const char* conf = 0;
               for (char* br = line; *br; ++br) {
                  if (isspace (*br)) {
                     *br = 0;
                     conf = br + 1;
                     break;
                  }
               }
               // make new namerecord
               namerecord* nrec = 0;
               if ((strncmp (line, "file://", 7) == 0) &&
                  (strpbrk (line, "[*?") == 0))  {
                  int n = 0;
                  const char* cont = conf ? strstr (conf, "-c") : 0;
                  if (cont) { 
                     cont += 2;
                     n = strtol (cont, (char**)&cont, 10);
                  }
                  char fconf[256];
                  strcpy (fconf, "");
                  if (n > 0) {
                     sprintf (fconf, "-c %i", n);
                  }
                  FrameDir::gps_t time;
                  FrameDir::gps_t tlen;
                  if (FrameDir::parseName (line + 7, time, tlen) && 
                     (tlen > 0)) {
                     nrec = new (nothrow) 
		        filenamerecord (line, fconf, Time(time), tlen);
                     // for (int i = 0; i < n; ++i) {
                        // time += tlen;
                        // sprintf (buf, "file://%s%i-%i%s", 
                                // pre, time, tlen, post);
                        // if (nrec) {
                           // ins = insert (ins, nrec);
                           // ++ins;
                           // nrec = 0;
                        // }
                        // nrec = new (nothrow) filenamerecord (buf, time, tlen);
                     // }
                  // }
                  // FrameDir fdir;
                  // fdir.addFile (line + 7);
                  // if ((fdir.size() == 1) &&
                     // ((double)fdir.begin()->second.getDt() > 0)) {
                     // nrec = new (nothrow) filenamerecord (line,
                                          // fdir.begin()->second.getStartTime(), 
                                          // fdir.begin()->second.getDt());
                  }
                  else {
                     nrec = new (nothrow) namerecord (line);
                  }
               }
               else {
                  nrec = new (nothrow) namerecord (line, conf);
               }
               if (nrec) {
                  ins = insert (ins, nrec);
                  ++ins;
               }
            }
            else {
               ++errnum;
            }
         }
         p = e;
      }
      delete [] line;
      delete [] pre;
      delete [] post;
      delete [] buf;
      return errnum < 10;
   }

//______________________________________________________________________________
   namerecord* namelist::removeName (bool back) 
   {
      if (empty()) {
         return 0;
      }
      else {
         namerecord* s = 0;
         if (back) {
            s = namerecordlist::back();
            pop_back();
         }
         else {
            s = namerecordlist::front();
            pop_front();
         }
         return s;
      }
   }

//______________________________________________________________________________
   void namelist::erase (iterator pos) 
   {
      if (*pos) {
         delete *pos; 
         *pos = 0;
      }
      namerecordlist::erase (pos); 
   }

//______________________________________________________________________________
   void namelist::clear () 
   {
      for (namelist::iterator i = namerecordlist::begin(); 
          i != namerecordlist::end(); ++i) {
         delete *i; *i = 0;
      }
      namerecordlist::clear();
   }





}
