/* -*- mode: c++; c-basic-offset: 3; -*- */
#ifndef _DEFAULT_SOURCE
#define _DEFAULT_SOURCE
#endif
#include "PConfig.h"
#include "goption.hh"
#include "inetio.hh"
#include "framefast/frametype.hh"
#include "framefast/framefast.hh"
#include "sockutil.h"
#include <cerrno>
#include <cstring>
#include <sys/types.h>
#include <sys/time.h>
#include <sys/stat.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <unistd.h>
#include <fcntl.h>
#include <sys/file.h>
#include <cstdio>
#include <cstdlib>
#include <iostream>
#include <algorithm>


namespace fantom {
   using namespace std;
   using namespace framefast;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// constants		                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   const int kHttpPort = 80;
   const char* const kHttpRequest = 
   "GET %s HTTP/1.0\r\n"
   "Host: %s\r\n"
   "User-Agent: Fantom\r\n"
   "\r\n";
   const char* const kContLen = "Content-Length:";
   const char* const kPROXYSERVER = "HTTPPROXY";

   const int _TIMEOUT = 30; // 30s


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// utility functions                                                    //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   static string trim (const char* p)
   {
      while (isspace (*p)) p++;
      string s = p;
      while ((s.size() > 0) && isspace (s[s.size()-1])) {
         s.erase (s.size() - 1);
      }
      return s;
   }

//______________________________________________________________________________
#if 0
   static string escapeurl (const string& s) 
   {
      string r = s;
      string::size_type pos = 0;
      while (pos < r.size()) {
         if (strchr (" ><%#{}|\\^~[]`;/?:@=&$", r[pos]) != 0) {
            char buf[16];
            sprintf (buf, "%%%02X", (int)r[pos]);
            r.erase (pos, 1);
            r.insert (pos, buf);
         }
         ++pos;
      }
      return r;
   }
#endif

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// http_support		                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   http_support::http_support (const char* url, const char* conf) 
   : fPort (kHttpPort), fOpen (false), fSock (-1)
   {
      setUrl (url);
      setConf (conf);
   }

//______________________________________________________________________________
   http_support::~http_support()
   {
      close();
   }

//______________________________________________________________________________
   bool http_support::setUrl (const char* url)
   {
      fUrl = trim (url);
      fAddr = "";
      fPort = kHttpPort;
      fFilename = "";
      // remove http
      string s = fUrl;
      if (s.find ("http://") == 0) {
         s.erase (0, 7);
      }
      string::size_type colon = s.find (':');
      string::size_type slash = s.find ('/');
      if (slash == string::npos) {
         return false;
      }
      if (colon == string::npos) colon = s.size();
      string::size_type len = min (colon, slash);
      if (len == s.size()) {
         return false;
      }
      // set address
      fAddr = fUrl.substr (0, len);
      // set port
      if (colon < slash) {
         fPort = atoi (fUrl.c_str() + colon + 1);
      }
      // set filename
      fFilename = fUrl;
      fFilename.erase (0, slash);
      //fFilename = escapeurl (fFilename);
      // cerr << "URL = " << fUrl << endl;
      // cerr << "Addr = " << fAddr << endl;
      // cerr << "Port = " << fPort << endl;
      // cerr << "Filename = " << fFilename << endl;
      return true;
   }

//______________________________________________________________________________
   void http_support::setConf (const char* conf)
   {
      // proxy
      fProxyAddr = "";
      fProxyPort = kHttpPort;
      if (conf) {
         gdsbase::option_string opts ("http", conf, "p:");
         string s;
         if (opts.getOpt ('p', s)) {
            fProxyAddr = s;
         } 
      }
      // check environment variable
      if (fProxyAddr.empty()) {
         const char* env = getenv (kPROXYSERVER);
         if (env) fProxyAddr = env;
      }
      if (!fProxyAddr.empty()) {
         string::size_type pos = fProxyAddr.find (":");
         if (pos != string::npos) {
            fProxyPort = atoi (fProxyAddr.c_str() + pos + 1);
            fProxyAddr.erase (pos);
         }
      }
      // cerr << "Proxy address = " << fProxyAddr << endl;
      // cerr << "Proxy port = " << fProxyPort << endl;
   }

//______________________________________________________________________________
   framefast::basic_frame_storage* http_support::readFrame ()
   {
      // can't be open!
      if (fOpen) {
         return NULL;
      }
      // open connection
      if (!open()) {
         close();
         cerr << "Unable to open connection" << endl;
         return NULL;
      }
      // send request
      if (!request()) {
         close();
         cerr << "Unable to send request" << endl;
         return NULL;
      }
      // download file
      char* data = 0;
      int len;
      if (!download (data, len)) {
         close();
         cerr << "Unable to download file" << endl;
         return NULL;
      }
      // close connection
      close();
      return new memory_frame_storage (data, len, true);
   }

//______________________________________________________________________________
   bool http_support::open ()
   {
      if (fOpen) {
         return true;
      }
      if (fAddr.empty()) {
         return false;
      }
   
      // create the socket
      fSock = ::socket (PF_INET, SOCK_STREAM, 0);
      if (fSock == -1) {
         return false;
      }
      // determine address & port
      string server;
      int port = 80;
      if (fProxyAddr.empty()) {
         server = fAddr;
         port = fPort;
      }
      else {
         server = fProxyAddr;
         port = fProxyPort;
      }
      // fill destination address
      struct sockaddr_in name;          /* socket name */
      name.sin_family = AF_INET;
      name.sin_port = htons (port);
      if (nslookup (server.c_str(), &name.sin_addr) < 0) {
         ::close (fSock);
         return false;
      }
      // connect to the web server
      //timeval timeout = {_TIMEOUT, 0};
      wait_time timeout = _TIMEOUT;
      if (connectWithTimeout (fSock, (struct sockaddr *) &name, 
                           sizeof (name), timeout) < 0) {
         ::close (fSock);
         return false;
      }
      fOpen = true;
      return true;
   }

//______________________________________________________________________________
   void http_support::close()
   {
      if (!fOpen) {
         return;
      }
      ::close (fSock);
      fOpen = false;
   }

//______________________________________________________________________________
   bool http_support::request()
   {
      //cerr << "Filename to download " << fFilename << endl;
      if (!fOpen || fFilename.empty()) {
         return false;
      }
      char buf[2048];
      string server;
      // deal with proxy
      if (!fProxyAddr.empty()) {
         server = "http://" + fAddr;
         if (fPort != kHttpPort) {
            sprintf (buf, ":%i", fPort);
            server += buf;
         }
      }
      // assemble http request
      server += fFilename;
      sprintf (buf, kHttpRequest, server.c_str(), fAddr.c_str());
      //cerr << "HTTP request" << endl;
      //printf (kHttpRequest, server.c_str(), fAddr.c_str());
      //cerr << "End of HTTP request" << endl;
      if (send (fSock, buf, strlen (buf), 0) <= 0) {
         close();
         return false;
      }
      return true;
   }

//______________________________________________________________________________
   bool http_support::download (char*& data, int& len)
   {
      if (!fOpen) {
         return false;
      }
      // read server reponse
      len = 0;
      char buf[16*1024];
      memset (buf, 0, sizeof (buf));
      int max = sizeof (buf) - 1;
      int sofar = 0;
      char* p = buf;
      bool emptyline = false;
      bool firstline = true;
      do {
         int nB = recv (fSock, buf + sofar, max - sofar, 0);
         if (nB <= 0) {
            close();
            cerr << "Socket error " << nB << " " << errno << endl;
            return false;
         }
         sofar += nB;
         // analyze return string
         string::size_type pos;
         do {
            string line = p;
            pos = line.find ("\r\n");
            if (pos != string::npos) {
               p += pos + 2;
            }
            else if ((pos = line.find ("\n")) != string::npos) {
               p += pos + 1;
            }
            if (pos != string::npos) {
               line.erase (pos);
               //cerr << "Response line = " << line << endl;
               string::size_type pos2;
               if (firstline) {
                  if ((strncasecmp (line.c_str(), "HTTP/1.0 200 OK", 12) != 0) &&
                     (strncasecmp (line.c_str(), "HTTP/1.1 200 OK", 12) != 0)) {
                     close();
                     return false;
                  }
                  firstline = false;
               }
               else if (line.empty()) {
                  emptyline = true;
                  //cerr << "EMPTY LINE FOUND" << endl;
                  break;
               }
               else if ((pos2 = line.find (kContLen)) != string::npos) {
                  len = atoi (line.c_str() + pos2 + strlen (kContLen));
               }
            } 
         } while (pos != string::npos);
      } while ((sofar < max) && !emptyline);
      // cerr << "Data length = " << len << endl;
      if (!emptyline) {
         close();
         cerr << "HTTP error" << endl;
         return false;
      }
      // allocate date
      if (len <= 0) {
         close();
         return false;
      }
      data = new (nothrow) char [len];
      if (!data) {
         close();
         return false;
      }
      // copy already read data
      int m = sofar - (int)(p - buf);
      if (m > len) m = len;
      // cerr << "sofar = " << sofar << endl;
      // cerr << "p - buf = " << (int)(p - buf) << endl;
      // cerr << "m = " << m << endl;
      memcpy (data, p, m);
      // cerr << "m = " << m;
      // read remaining
      while (m < len) {
         int nB = recv (fSock, data + m, len - m, 0);
         if (nB <= 0) {
            close();
            return false;
         }
         m += nB;
         // cerr << " " << m;
      }
      // cerr << endl;
      // cerr << "READ IT ALL -------------------------------" << endl;
      // cerr << string (data, 4) << endl;
      return true;
   }


}
