/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: inetio							*/
/*                                                         		*/
/* Module Description: internet support for smartio			*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 13Mar01  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: inetio.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_INETIO_H
#define _LIGO_INETIO_H

#include "iosupport.hh"
#include <string>


namespace fantom {


/** @name Internet support
    This header defines support methods for reading from the internet
    (http).
   
    @memo Internet support
    @author Written April 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** http IO class. This is a support class for the smart http input
    class.
    
    @memo Http IO support class.
 ************************************************************************/
   class http_support : public iosupport {
   public:
      /// Create http support
      explicit http_support (const char* url = 0, const char* conf = 0);
      /// Destructor
      virtual ~http_support();
   
      /// Set Url
      bool setUrl (const char* url);
      /// Get Url
      const char* getUrl() const {
         return fUrl.c_str(); }
      /// Get IP address
      const char* getAddr() const {
         return fAddr.c_str(); }
      /// Get port number
      int getPort() const {
         return fPort; }
      /// Get the proxy address
      const char* getProxyAddr() const {
         return fProxyAddr.empty() ? 0 : fProxyAddr.c_str(); }
      /// Get the proxy port
      int getProxyPort() const {
         return fProxyPort; }
      /// Get filename
      const char* getFilename() const {
         return fFilename.c_str(); }
      /// Set configuration
      void setConf (const char* conf);
   
      /// Get the file from the URL
      virtual framefast::basic_frame_storage* readFrame ();
      /// Get frame writer (output not supported!)
      virtual framefast::basic_frameout* getWriter (const char* fname) {
         return 0; }
      /// End of file
      virtual bool eof() const {
         return !fOpen; }
   
   protected:
      /// Url
      std::string	fUrl;
      /// IP address
      std::string	fAddr;
      /// Port number
      int		fPort;
      /// Filename
      std::string	fFilename;
      /// Proxy address
      std::string       fProxyAddr;
      /// Proxy port
      int 		fProxyPort;
   
      /// Open connection
      bool open ();
      /// Close connection
      void close();
      /// request data
      bool request ();
      /// download data
      bool download (char*& data, int& len);
   
   private:
      /// Internet connection open?
      bool		fOpen;
      /// Socket handle
      int		fSock;
   };


//@}

}

#endif // _LIGO_INETIO_H
