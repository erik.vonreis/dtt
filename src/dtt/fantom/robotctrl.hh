/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: robotctrl						*/
/*                                                         		*/
/* Module Description: Tape robot support class				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 23Apr01  D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dfmtape.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_ROBOTCTRL_H
#define _LIGO_ROBOTCTRL_H


#include <string>


namespace fantom {


/** @name DFM low level API (tape robot)
    This header defines helper methods for conbtrolling a tape
    robot. It is primarily used by dfmtape.
   
    @memo DFM low level API (tape robot)
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** API to control a tape robot.
    the configuration script is of the following format:
    \begin{verbatim}
    devname[@startslot:stopsloty:[first]][#tapes]
    with:
    devname   - the device name
    startslot - the first slot to be used (default: 1)
    stopslot  - the last slot to be used (default: 5)
    first     - the first tape to be read (default: startslot)
    tapes     - number of tapes to read (default: stopslot-startslot+1)
    \end{verbatim}
    The start and stop slot represent the range of slot numbers used
    by to read tapes from. If the number of tapes to be read is larger
    than this range, the script cycles back to the beginning.
    
    Example:
    \begin{verbatim}
    CY0@1:10:5#15
    \end{verbatim}
    assumes that you have setup a Cybernetics TL-8 library with tapes
    in slot 5 through 10. The program will load tape 5 first, the move
    thru tapes 6, 7, 8, 9, 10. Then it will start again at slot 1 and
    read tapes 1 to 10. At this point it has read 15 tapes and it will
    stop.

    Understanding device names: The device name is divided into a 
    name and a number. The name corresponds to a script which has to
    be found by the program in the current path. The number represents
    the device number if there are more than one tape drives/robots 
    attached to the system. For example, "CY0" requires a shell script 
    with name CY.robot which understand the following commands:
    Example:
    \begin{verbatim}
    CY.robot -d 0 -u 4 -l 5 -f /dev/rmt/0
    with:
    d - the device number option
    u - the unload command with the slot to unload to
    l - the load command with the slot to load from
    f - device path corresponding to device d
    \end{verbatim}
    The option -d will always be specified (default 0). Load and unload 
    options may be specified together. However, at least one of them
    can be expected. The script of the cybernetics TL-8 robot uses the
    the device number to both specifiy the tape drive and the tape
    robot. CY0 corresponds to the robot at /dev/cychs0 and drive 0;
    CY1 corresponds to robot at /dev/cychs0 and drive 1; CY10 would
    correspond to robot at /dev/cychs1 and drive 0, and so on.
    The script must return an error code of zero if successful.

    A special manual "robot" is invoked by MAN.robot which prompts 
    the user to change the tape by hand. This is useful for stand-alone
    drives without tape changers.
    
    @memo Tape robot control.
 ************************************************************************/
   class robot_ctrl {
   public:
      /// Default constructor
      robot_ctrl (const char* mtpath, const char* conf);
      /// Destructor
      virtual ~robot_ctrl ();
   
      /// Set the configuration
      virtual bool setConf (const char* mtpath, const char* conf);
      /// Get the configuration
      const char* getConf () const {
         return fConf.c_str(); }
      /// Get the device path of the magnetic tape
      const char* getMtPath () const {
         return fMtPath.c_str(); }
   
      /** Move to next tape.
          If next returns true, a new tape is loaded.
          If next returns false, either the index has moved beyond
          the last tape or the the tape scipt aborted with an error.
          A correct use of next is as follows:
          \begin{verbatim}
          robot_ctrl robot ("CY0@1:10:5#15");
          while (robot.next()) {
            // read data from tape
          }
          if (robot.eof()) {
             // success: all done
          }
          else {
             // error
          }
          \end{verbatim}
        */
      virtual bool next();
      /// Last tape done?
      bool eof() {
         return fDone; }
      /// First slot
      int firstSlot() const {
         return fSlotFirst; }
      /// Last slot
      int lastSlot() const {
         return fSlotLast; }
      /// Current slot
      int currentSlot() const {
         return fSlotCur; }
      /// Total number of tapes
      int tapeTotal() const {
         return fTapesTotal; }
      /// Current tape index
      int tapeIndex() const {
         return fTapesIndex; }
      /// Tape control script
      const char* getScript () const {
         return fScript.c_str(); }
   
   protected:
      /// Device path to magnetic tape
      std::string	fMtPath;
      /// Configuration
      std::string	fConf;
      /// Done?
      bool		fDone;
      /// first slot
      int		fSlotFirst;
      /// last slot
      int		fSlotLast;
      /// current slot
      int		fSlotCur;
      /// Total number of tapes
      int		fTapesTotal;
      /// Current tape index (1 = first tape)
      int		fTapesIndex;
      /// Tape script
      std::string	fScript;
      /// Device number
      int		fDevNum;
   };

//@}

}

#endif // _LIGO_ROBOTCTRL_H
