/* version $Id: FilterModule.hh 7815 2017-01-05 23:51:57Z james.batch@LIGO.ORG $
 */
#ifndef _LIGO_FILTERMODULE_H
#define _LIGO_FILTERMODULE_H
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: FilterModule						*/
/*                                                         		*/
/* Module Description: Filter module as defined by online system	*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 1.0	 4Aug02   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: FilterModule.html					*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "FilterDesign.hh"
#include "FilterSection.hh"
#include "FilterMessage.hh"
#include <string>
#include <vector>
#include <list> // JCB - test list instead of vector for FilterModuleList

namespace filterwiz
{

    /// Maximum number of supported filter sections by a filter module.
    const int kMaxFilterSections = 10;

    /// Maximum number of supported SOS by a filter section.
    const int kMaxFilterSOS = 10;

    /// String for filter module defintion
    const char* const kModuleDef = "# MODULES";

    /// String for filter module sampling rate defintion
    const char* const kModuleSamplingDef = "# SAMPLING";

    /// String for filter section design defintion
    const char* const kSectionDesignDef = "# DESIGN";

    class SectCopy; // JCB

    /** Filter Module Class. Represents a single filter module. Filter
        sections are automatically allocated up to kMaxFilterSections.

        @memo Filter Module
        @author Written August 2002 by Daniel Sigg
        @version 1.0
     ************************************************************************/
    class FilterModule
    {
    public:
        /// Default constructor
        FilterModule( );

        /// Constructor
        FilterModule( const char* name, double fsample );

        ~FilterModule( );

        /// Check design of filter sections
        void checkDesign( std::vector< message_t >* message_vec );

        /// Get name
        const char*
        getName( ) const
        {
            return fName.c_str( );
        }

        /// Set name
        void setName( const char* p );

        /// Get sampling rate
        double
        getFSample( ) const
        {
            return fSample;
        }

        /// Set sampling rate
        void setFSample( double sample );

        /// Set default sampling rate
        // This constitutes an error, it can't really be guessed.
        void defaultFSample( );

        /// Access to filter section
        FilterSection&
        operator[]( int i )
        {
            return fSect[ i ];
        }

        /// Access to filter section
        const FilterSection&
        operator[]( int i ) const
        {
            return fSect[ i ];
        }

        /// Comparison operator for sorting a list by name
        bool operator<( const FilterModule& b ) const;

        /// Comparison operator for sorting a list by name
        bool operator==( const FilterModule& b ) const;

        /// Save a copy of the filter sections to restore after copy/paste.
        /// This is called before a paste operation.
        void SaveSections( );

        /// Restore the previous filter section designs after pasting in
        /// case a mistake was made.
        bool RestoreSections( );

        /// Check to see if there is anything in the undo set.
        bool RestoreSectionsEmpty( );

        /// Change sample rate of filter module by replacing the fFilter in
        /// each section of the module with a new one constructed from the
        /// design string with the new sample rate.
        void changeSampleRate( double                    newSample,
                               std::vector< message_t >* message_vec );

    protected:
        /// Name
        std::string fName;

        /// Sampling rate
        /// The sampling rate is either read from the filter file or
        /// set from the sample rate menu in the design window.  If editing
        /// filter files, this value is set when the file is read, and can't
        /// be changed.  If editing a module from diaggui, this value must be
        /// set by the user from the design window.
        double fSample;

        /// List of filter sections
        FilterSectionList fSect;

        // Copy of existing filter section design information for undo.
        // Definition in the FilterModule means that each module in the file
        // has it's own undo.
        std::vector< SectCopy > fRevertSect;
    };

    /// List of filter modules
    // JCB - Change to a list instead of a vector to allow sort on names.
    typedef std::list< FilterModule > FilterModuleList;
    //   typedef std::vector<FilterModule> FilterModuleList;

} // namespace filterwiz

#endif // _LIGO_FILTERMODULE_H
