/* -*- mode: c++; c-basic-offset: 3; -*- */
/* version $Id: TLGFilterDlg.cc 7980 2018-01-05 01:26:54Z john.zweizig@LIGO.ORG
 * $ */
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <cmath>
#include "TLGFilterDlg.hh"
#include "FilterParse.hh"
#include "FilterDesign.hh"
#include "TLGFilterWizard.hh" // JCB
#include "iirutil.hh"
#include "TLGEntry.hh"
#include "constant.hh"
#include <TGMsgBox.h>
#include <TGTableLayout.h>
#include <TPad.h>
#include <TCanvas.h>
#include <TGFSComboBox.h> // JCB
#include <TGListView.h> // JCB
#include <TGFSContainer.h> // JCB
#include <TSystem.h> //
#include <TVirtualX.h>
#include <iostream>
#include <fstream> // JCB
#include <algorithm>
#include <strings.h>

using namespace std;
using namespace dttgui;

namespace filterwiz
{

    const int kOkId = 1;
    const int kCancelId = 0;
    const int kPathId = 11;
    const int kDirUpId = 12;
    const int kFileId = 13;

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // Utility function			                                //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    static const double dBconst = log( 10.0 ) / 20.0;

    //===================================  Convert from DB to gain, i.e
    //                                     cvtDB(x) = 10^(x/20)
    inline double
    cvtDB( double x )
    {
        return exp( dBconst * x );
    }

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // Parser				                                //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    // This class is used to parse a design string that has been highlighted
    // in the design dialog of foton, so that the parameters of the
    // filter design can populate the proper dialog box allowing the user to
    // make modifications to an existing filter design.
    class FilterDlgParser : public FilterParse
    {
    public:
        // Constructor
        FilterDlgParser( TLGGainDialog*  gdlg = 0,
                         TLGZpkDialog*   zpkdlg = 0,
                         TLGNotchDialog* ndlg = 0,
                         TLGEllipDialog* edlg = 0,
                         TLGSosDialog*   sosdlg = 0,
                         TLGPolyDialog*  polydlg = 0 )
            : fFirst( true ), fGainDlg( gdlg ), fZpkDlg( zpkdlg ),
              fPolyDlg( polydlg ), fNotchDlg( ndlg ), fEllipDlg( edlg ),
              fSosDlg( sosdlg )
        {
        }
        // Gain
        bool gain( double g, const char* format = "scalar" );
        // Pole
        bool pole( double f, double gain, const char* plane = "s" );
        bool
        pole( double f, const char* plane = "s" )
        {
            return pole( f, 1., plane );
        }
        // Zero
        bool zero( double f, double gain, const char* plane = "s" );
        bool
        zero( double f, const char* plane = "s" )
        {
            return zero( f, 1., plane );
        }
        // Pole2
        bool pole2( double f, double Q, double gain, const char* plane = "s" );
        bool
        pole2( double f, double Q, const char* plane = "s" )
        {
            return pole2( f, Q, 1., plane );
        }
        // Zero2
        bool zero2( double f, double Q, double gain, const char* plane = "s" );
        bool
        zero2( double f, double Q, const char* plane = "s" )
        {
            return zero2( f, Q, 1., plane );
        }
        // ZPK
        bool zpk( int             nzeros,
                  const dComplex* zero,
                  int             npoles,
                  const dComplex* pole,
                  double          gain,
                  const char*     plane = "s" );
        // Polynomial form
        bool rpoly( int           nnumer,
                    const double* numer,
                    int           ndenom,
                    const double* denom,
                    double        gain = 1.0 );
        // SOS
        bool sos( int nba, const double* ba, const char* format = "s" );
        // z roots
        bool zroots( int             nzeros,
                     const dComplex* zero,
                     int             npoles,
                     const dComplex* pole,
                     double          gain = 1.0 );
        // Direct form
        bool direct( int nb, const double* b, int na, const double* a );
        // Elliptic
        bool ellip( Filter_Type type,
                    int         order,
                    double      rp,
                    double      as,
                    double      f1,
                    double      f2 = 0.0 );
        // Chebyshev type 1
        bool cheby1( Filter_Type type,
                     int         order,
                     double      rp,
                     double      f1,
                     double      f2 = 0.0 );
        // Chebyshev type 2
        bool cheby2( Filter_Type type,
                     int         order,
                     double      as,
                     double      f1,
                     double      f2 = 0.0 );
        // Butterworth
        bool butter( Filter_Type type, int order, double f1, double f2 = 0.0 );
        // notch
        bool notch( double f0, double Q, double depth = 0.0 );
        // resgain
        bool resgain( double f0, double Q, double height = 30.0 );
        // comb
        bool comb( double f0, double Q, double amp = 0.0, int N = 0 );

    private:
        bool            fFirst;
        TLGGainDialog*  fGainDlg;
        TLGZpkDialog*   fZpkDlg;
        TLGPolyDialog*  fPolyDlg;
        TLGNotchDialog* fNotchDlg;
        TLGEllipDialog* fEllipDlg;
        TLGSosDialog*   fSosDlg;
    };

    //______________________________________________________________________________
    bool
    FilterDlgParser::gain( double g, const char* format )
    {
        bool dB = format && ( strcasecmp( format, "dB" ) == 0 );
        if ( fGainDlg )
        {
            if ( fFirst )
            {
                fGainDlg->fGain->SetNumber( g );
            }
            else
            {
                double x = fGainDlg->fGain->GetNumber( );
                if ( fGainDlg->fFormat[ 1 ]->GetState( ) == kButtonDown )
                {
                    if ( dB )
                    {
                        x += g;
                    }
                    else
                    {
                        x = cvtDB( x ) * g;
                        dB = false;
                    }
                }
                else
                {
                    if ( dB )
                    {
                        x *= cvtDB( g );
                        dB = false;
                    }
                    else
                    {
                        x *= g;
                    }
                }
                fGainDlg->fGain->SetNumber( x );
            }
            fGainDlg->fFormat[ 0 ]->SetState( dB ? kButtonUp : kButtonDown );
            fGainDlg->fFormat[ 1 ]->SetState( dB ? kButtonDown : kButtonUp );
        }
        else if ( fZpkDlg )
        {
            if ( dB )
                g = cvtDB( g );
        }
        else
        {
            return false;
        }
        fFirst = false;
        return true;
    }

    //______________________________________________________________________________
    bool
    FilterDlgParser::pole( double f0, double gain, const char* plane )
    {
        if ( fZpkDlg )
        {
            dComplex pole( plane && ( plane[ 0 ] == 'n' ) ? f0 : -f0, 0 );
            return zpk( 0, 0, 1, &pole, gain, plane );
        }
        return false;
    }

    //______________________________________________________________________________
    bool
    FilterDlgParser::zero( double f0, double gain, const char* plane )
    {
        if ( fZpkDlg )
        {
            dComplex zero( plane && ( plane[ 0 ] == 'n' ) ? f0 : -f0, 0 );
            return zpk( 1, &zero, 0, 0, gain, plane );
        }
        return false;
    }

    //______________________________________________________________________________
    bool
    FilterDlgParser::pole2( double      f0,
                            double      Q,
                            double      gain,
                            const char* plane )
    {
        if ( fZpkDlg )
        {
            dComplex pole[ 2 ];
            double   twoQ = 2. * fabs( Q );
            if ( twoQ <= 1 )
            {
                pole[ 0 ] =
                    dComplex( plane && ( plane[ 0 ] == 'n' ) ? f0 : -f0, 0 );
            }
            else
            {
                pole[ 0 ] = dComplex(
                    plane && ( plane[ 0 ] == 'n' ) ? f0 / twoQ : -f0 / twoQ,
                    f0 * sqrt( 1 - 1. / ( twoQ * twoQ ) ) );
            }
            pole[ 1 ] = ~pole[ 0 ];
            return zpk( 0, 0, 2, pole, gain, plane );
        }
        return false;
    }

    //______________________________________________________________________________
    bool
    FilterDlgParser::zero2( double      f0,
                            double      Q,
                            double      gain,
                            const char* plane )
    {
        if ( fZpkDlg )
        {
            dComplex zero[ 2 ];
            double   twoQ = 2. * fabs( Q );
            if ( twoQ <= 1 )
            {
                zero[ 0 ] =
                    dComplex( plane && ( plane[ 0 ] == 'n' ) ? f0 : -f0, 0 );
            }
            else
            {
                zero[ 0 ] = dComplex(
                    plane && ( plane[ 0 ] == 'n' ) ? f0 / twoQ : -f0 / twoQ,
                    f0 * sqrt( 1 - 1. / ( twoQ * twoQ ) ) );
            }
            zero[ 1 ] = ~zero[ 0 ];
            return zpk( 2, zero, 0, 0, gain, plane );
        }
        return false;
    }

    //______________________________________________________________________________
    bool
    FilterDlgParser::zpk( int             nzeros,
                          const dComplex* zero,
                          int             npoles,
                          const dComplex* pole,
                          double          gain,
                          const char*     plane )
    {
        if ( fZpkDlg && fZpkDlg->fSPlaneSpec )
        {
            // check if multiple zpks are parsed
            if ( !fZpkDlg->fPZ[ 0 ].empty( ) || !fZpkDlg->fPZ[ 1 ].empty( ) )
            {
                return false;
            }
            fZpkDlg->fPZ[ 0 ].clear( );
            for ( int i = 0; i < npoles; ++i )
            {
                fZpkDlg->fPZ[ 0 ].push_back( pole[ i ] );
            }
            fZpkDlg->fPZ[ 1 ].clear( );
            for ( int i = 0; i < nzeros; ++i )
            {
                fZpkDlg->fPZ[ 1 ].push_back( zero[ i ] );
            }
            fZpkDlg->fGain->SetNumber( gain );
            fZpkDlg->fGainFormat[ 0 ]->SetState( kButtonDown );
            fZpkDlg->fGainFormat[ 1 ]->SetState( kButtonUp );
            int j;
            if ( plane && ( plane[ 0 ] == 'n' ) )
            {
                j = 2;
                fZpkDlg->fPlane = "n";
            }
            else if ( plane && ( plane[ 0 ] == 'f' ) )
            {
                j = 1;
                fZpkDlg->fPlane = "f";
            }
            else
            {
                j = 0;
                fZpkDlg->fPlane = "s";
            }
            for ( int i = 0; i < 3; ++i )
            {
                fZpkDlg->fRootFormat[ i ]->SetState( i == j ? kButtonDown
                                                            : kButtonUp );
            }
            return true;
        }
        return false;
    }

    //______________________________________________________________________________
    bool
    FilterDlgParser::zroots( int             nzeros,
                             const dComplex* zero,
                             int             npoles,
                             const dComplex* pole,
                             double          gain )
    {
        if ( fZpkDlg && !fZpkDlg->fSPlaneSpec )
        {
            fZpkDlg->fPZ[ 0 ].clear( );
            for ( int i = 0; i < npoles; ++i )
            {
                fZpkDlg->fPZ[ 0 ].push_back( pole[ i ] );
            }
            fZpkDlg->fPZ[ 1 ].clear( );
            for ( int i = 0; i < nzeros; ++i )
            {
                fZpkDlg->fPZ[ 1 ].push_back( zero[ i ] );
            }
            fZpkDlg->fGain->SetNumber( gain );
            fZpkDlg->fGainFormat[ 0 ]->SetState( kButtonDown );
            fZpkDlg->fGainFormat[ 1 ]->SetState( kButtonUp );
            return true;
        }
        return false;
    }

    //______________________________________________________________________________
    bool
    FilterDlgParser::rpoly( int           nnumer,
                            const double* numer,
                            int           ndenom,
                            const double* denom,
                            double        gain )
    {
        if ( fPolyDlg && fPolyDlg->fSPlaneSpec && ( nnumer <= kMaxPolyCoeff ) &&
             ( ndenom <= kMaxPolyCoeff ) )
        {
            for ( int i = 0; i < nnumer; ++i )
            {
                fPolyDlg->fPolyCoeff[ 0 ][ nnumer - i - 1 ]->SetNumber(
                    numer[ i ] );
            }
            for ( int i = nnumer; i < kMaxPolyCoeff; ++i )
            {
                fPolyDlg->fPolyCoeff[ 0 ][ i ]->SetNumber( 0 );
            }
            for ( int i = 0; i < ndenom; ++i )
            {
                fPolyDlg->fPolyCoeff[ 1 ][ ndenom - i - 1 ]->SetNumber(
                    denom[ i ] );
            }
            for ( int i = ndenom; i < kMaxPolyCoeff; ++i )
            {
                fPolyDlg->fPolyCoeff[ 1 ][ i ]->SetNumber( 0 );
            }
            fPolyDlg->fGain->SetNumber( gain );
            return true;
        }
        return false;
    }

    //______________________________________________________________________________
    bool
    FilterDlgParser::direct( int nb, const double* b, int na, const double* a )
    {
        if ( fPolyDlg && !fPolyDlg->fSPlaneSpec && ( nb < kMaxPolyCoeff ) &&
             ( na <= kMaxPolyCoeff ) )
        {
            for ( int i = 0; i <= nb; ++i )
            {
                fPolyDlg->fPolyCoeff[ 0 ][ i ]->SetNumber( b[ i ] );
            }
            for ( int i = nb + 1; i < kMaxPolyCoeff; ++i )
            {
                fPolyDlg->fPolyCoeff[ 0 ][ i ]->SetNumber( 0 );
            }
            for ( int i = 0; i < na; ++i )
            {
                fPolyDlg->fPolyCoeff[ 1 ][ i ]->SetNumber( a[ i ] );
            }
            for ( int i = na; i < kMaxPolyCoeff; ++i )
            {
                fPolyDlg->fPolyCoeff[ 1 ][ i ]->SetNumber( 0 );
            }
            return true;
        }
        return false;
    }

    //______________________________________________________________________________
    bool
    FilterDlgParser::sos( int nba, const double* ba, const char* format )
    {
        if ( fSosDlg )
        {
            fSosDlg->fGain->SetNumber( ba[ 0 ] );
            for ( int i = 0; i < ( nba - 1 ) / 4; ++i )
            {
                double b1 = ba[ 1 + 4 * i + 0 ];
                double b2 = ba[ 1 + 4 * i + 1 ];
                double a1 = ba[ 1 + 4 * i + 2 ];
                double a2 = ba[ 1 + 4 * i + 3 ];
                if ( format && ( format[ 0 ] == 'o' ) )
                {
                    swap( b1, a1 );
                    swap( b2, a2 );
                }
                char buf[ 1024 ];
                sprintf( buf, "%g %g %g %g", b1, b2, a1, a2 );
                fSosDlg->fCoeffSel->AddEntry( buf, ++fSosDlg->fId );
            }
            fSosDlg->fCoeffSel->MapSubwindows( );
            fSosDlg->fCoeffSel->Layout( );
            return true;
        }
        return false;
    }

    //______________________________________________________________________________
    bool
    FilterDlgParser::ellip( Filter_Type type,
                            int         order,
                            double      rp,
                            double      as,
                            double      f1,
                            double      f2 )
    {
        if ( fEllipDlg )
        {
            fEllipDlg->fFilterType->Select( type );
            fEllipDlg->fOrder->SetIntNumber( order );
            fEllipDlg->fFreq1->SetNumber( f1 );
            if ( fEllipDlg->fRipple )
                fEllipDlg->fRipple->SetNumber( rp );
            if ( fEllipDlg->fAtten )
                fEllipDlg->fAtten->SetNumber( as );
            if ( ( type == kLowPass ) || ( type == kHighPass ) )
            {
                fEllipDlg->fFreq2->SetState( kFALSE );
            }
            else
            {
                fEllipDlg->fFreq2->SetNumber( f2 );
                fEllipDlg->fFreq2->SetState( kTRUE );
            }
            return true;
        }
        return false;
    }

    //______________________________________________________________________________
    bool
    FilterDlgParser::cheby1(
        Filter_Type type, int order, double rp, double f1, double f2 )
    {
        if ( fEllipDlg )
        {
            fEllipDlg->fFilterType->Select( type );
            fEllipDlg->fOrder->SetIntNumber( order );
            fEllipDlg->fFreq1->SetNumber( f1 );
            if ( fEllipDlg->fRipple )
                fEllipDlg->fRipple->SetNumber( rp );
            if ( ( type == kLowPass ) || ( type == kHighPass ) )
            {
                fEllipDlg->fFreq2->SetState( kFALSE );
            }
            else
            {
                fEllipDlg->fFreq2->SetNumber( f2 );
                fEllipDlg->fFreq2->SetState( kTRUE );
            }
            return true;
        }
        return false;
    }

    //______________________________________________________________________________
    bool
    FilterDlgParser::cheby2(
        Filter_Type type, int order, double as, double f1, double f2 )
    {
        if ( fEllipDlg )
        {
            fEllipDlg->fFilterType->Select( type );
            fEllipDlg->fOrder->SetIntNumber( order );
            fEllipDlg->fFreq1->SetNumber( f1 );
            if ( fEllipDlg->fAtten )
                fEllipDlg->fAtten->SetNumber( as );
            if ( ( type == kLowPass ) || ( type == kHighPass ) )
            {
                fEllipDlg->fFreq2->SetState( kFALSE );
            }
            else
            {
                fEllipDlg->fFreq2->SetNumber( f2 );
                fEllipDlg->fFreq2->SetState( kTRUE );
            }
            return true;
        }
        return false;
    }

    //______________________________________________________________________________
    bool
    FilterDlgParser::butter( Filter_Type type, int order, double f1, double f2 )
    {
        if ( fEllipDlg )
        {
            fEllipDlg->fFilterType->Select( type );
            fEllipDlg->fOrder->SetIntNumber( order );
            fEllipDlg->fFreq1->SetNumber( f1 );
            if ( ( type == kLowPass ) || ( type == kHighPass ) )
            {
                fEllipDlg->fFreq2->SetState( kFALSE );
            }
            else
            {
                fEllipDlg->fFreq2->SetNumber( f2 );
                fEllipDlg->fFreq2->SetState( kTRUE );
            }
            return true;
        }
        return false;
    }

    //______________________________________________________________________________
    bool
    FilterDlgParser::notch( double f0, double Q, double depth )
    {
        if ( fNotchDlg )
        {
            fNotchDlg->fFreq->SetNumber( f0 );
            fNotchDlg->fQ->SetNumber( Q );
            fNotchDlg->fDepth->SetNumber( depth );
            return true;
        }
        else
        {
            return false;
        }
    }

    //______________________________________________________________________________
    bool
    FilterDlgParser::resgain( double f0, double Q, double height )
    {
        if ( fNotchDlg )
        {
            fNotchDlg->fFreq->SetNumber( f0 );
            fNotchDlg->fQ->SetNumber( Q );
            fNotchDlg->fDepth->SetNumber( height );
            return true;
        }
        else
        {
            return false;
        }
    }

    //______________________________________________________________________________
    bool
    FilterDlgParser::comb( double f0, double Q, double amp, int N )
    {
        if ( fNotchDlg )
        {
            fNotchDlg->fFreq->SetNumber( f0 );
            fNotchDlg->fQ->SetNumber( Q );
            fNotchDlg->fDepth->SetNumber( amp );
            if ( fNotchDlg->fHarmonics )
            {
                fNotchDlg->fHarmonics->SetIntNumber( N );
            }
            return true;
        }
        else
        {
            return false;
        }
    }

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // TLGGainDialog			                                //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    TLGGainDialog::TLGGainDialog( const TGWindow* p,
                                  const TGWindow* main,
                                  TString&        result )
        : TLGTransientFrame( p, main, 10, 10 ), fRet( &result )
    {
        // layout hints
        fL[ 0 ] = new TGLayoutHints(
            kLHintsLeft | kLHintsExpandX | kLHintsTop, 2, 2, 2, 2 );
        fL[ 1 ] = new TGLayoutHints( kLHintsRight | kLHintsTop, 6, 6, 12, 4 );
        fL[ 2 ] = new TGTableLayoutHints(
            0, 1, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 25, 2 );
        fL[ 3 ] =
            new TGTableLayoutHints( 1,
                                    3,
                                    0,
                                    1,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    25,
                                    2 );
        fL[ 4 ] = new TGTableLayoutHints(
            0, 1, 1, 2, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 0 );
        fL[ 5 ] = new TGTableLayoutHints(
            1, 2, 1, 2, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 0 );
        fL[ 6 ] = new TGTableLayoutHints(
            2, 3, 1, 2, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 0 );
        // gain selection
        fF[ 0 ] = new TGGroupFrame( this, "Gain Selection" );
        AddFrame( fF[ 0 ], fL[ 0 ] );
        fF[ 0 ]->SetLayoutManager( new TGTableLayout( fF[ 0 ], 2, 3 ) );
        fLabel[ 0 ] = new TGLabel( fF[ 0 ], "Gain:   " );
        fF[ 0 ]->AddFrame( fLabel[ 0 ], fL[ 2 ] );
        fLabel[ 1 ] = new TGLabel( fF[ 0 ], "Format:   " );
        fF[ 0 ]->AddFrame( fLabel[ 1 ], fL[ 4 ] );
        fGain = new TLGNumericControlBox( fF[ 0 ], 1.0, 6, 10, kNESReal );
        fGain->Associate( this );
        fF[ 0 ]->AddFrame( fGain, fL[ 3 ] );
        fFormat[ 0 ] = new TGRadioButton( fF[ 0 ], "Scalar   ", 11 );
        fFormat[ 0 ]->Associate( this );
        fF[ 0 ]->AddFrame( fFormat[ 0 ], fL[ 5 ] );
        fFormat[ 1 ] = new TGRadioButton( fF[ 0 ], "dB", 12 );
        fFormat[ 1 ]->Associate( this );
        fF[ 0 ]->AddFrame( fFormat[ 1 ], fL[ 6 ] );
        fFormat[ 0 ]->SetState( kButtonDown );
        fF[ 0 ]->Resize( fF[ 0 ]->GetDefaultWidth( ),
                         fF[ 0 ]->GetDefaultHeight( ) );
        // buttons
        fFButton = new TGHorizontalFrame( this, 10, 10 );
        AddFrame( fFButton, fL[ 0 ] );
        fButton[ 1 ] = new TGTextButton(
            fFButton, new TGHotString( "     &Cancel     " ), kCancelId );
        fButton[ 1 ]->Associate( this );
        fFButton->AddFrame( fButton[ 1 ], fL[ 1 ] );
        fButton[ 0 ] = new TGTextButton(
            fFButton, new TGHotString( "        &Ok        " ), kOkId );
        fButton[ 0 ]->Associate( this );
        fFButton->AddFrame( fButton[ 0 ], fL[ 1 ] );
        // setup
        Setup( result );

        // resize & move to center
        MapSubwindows( );
        UInt_t width = GetDefaultWidth( );
        UInt_t height = GetDefaultHeight( );
        Resize( width, height );
        Int_t ax;
        Int_t ay;
        if ( main )
        {
            Window_t wdum;
            gVirtualX->TranslateCoordinates(
                main->GetId( ),
                GetParent( )->GetId( ),
                ( ( (TGFrame*)main )->GetWidth( ) - fWidth ) >> 1,
                ( ( (TGFrame*)main )->GetHeight( ) - fHeight ) >> 1,
                ax,
                ay,
                wdum );
        }
        else
        {
            UInt_t root_w, root_h;
            gVirtualX->GetWindowSize(
                fClient->GetRoot( )->GetId( ), ax, ay, root_w, root_h );
            ax = ( root_w - fWidth ) >> 1;
            ay = ( root_h - fHeight ) >> 1;
        }
        Move( ax, ay );
        SetWMPosition( ax, ay );

        // make the message box non-resizable
        SetWMSize( width, height );
        SetWMSizeHints( width, height, width, height, 0, 0 );

        // set dialog box title
        SetWindowName( "Gain Factor" );
        SetIconName( "Gain" );
        SetClassHints( "GainFactorDlg", "GainFactorDlg" );
        SetMWMHints( kMWMDecorAll | kMWMDecorResizeH | kMWMDecorMaximize |
                         kMWMDecorMinimize | kMWMDecorMenu,
                     kMWMFuncAll | kMWMFuncResize | kMWMFuncMaximize |
                         kMWMFuncMinimize,
                     kMWMInputModeless );

        MapWindow( );
        fClient->WaitFor( this );
    }

    //______________________________________________________________________________
    TLGGainDialog::~TLGGainDialog( )
    {
        delete fGain;
        delete fFormat[ 0 ];
        delete fFormat[ 1 ];
        delete fLabel[ 0 ];
        delete fLabel[ 1 ];
        delete fButton[ 0 ];
        delete fButton[ 1 ];
        delete fFButton;
        for ( int i = 0; i < 1; ++i )
            delete fF[ i ];
        for ( int i = 0; i < 7; ++i )
            delete fL[ i ];
    }

    //______________________________________________________________________________
    void
    TLGGainDialog::CloseWindow( )
    {
        if ( fRet )
            *fRet = "";
        DeleteWindow( );
    }

    //______________________________________________________________________________
    Bool_t
    TLGGainDialog::Setup( const char* cmd )
    {
        if ( !cmd || !*cmd )
        {
            return kTRUE;
        }
        FilterDlgParser parser( this );
        return parser.filter( cmd );
    }

    //______________________________________________________________________________
    Bool_t
    TLGGainDialog::ProcessMessage( Long_t msg, Long_t parm1, Long_t parm2 )
    {
        // Buttons
        if ( ( GET_MSG( msg ) == kC_COMMAND ) &&
             ( GET_SUBMSG( msg ) == kCM_BUTTON ) )
        {
            switch ( parm1 )
            {
            // ok
            case kOkId: {
                double g = fGain->GetNumber( );
                bool   scalar = fFormat[ 0 ]->GetState( ) == kButtonDown;
                char   buf[ 256 ];
                if ( scalar )
                {
                    sprintf( buf, "gain(%g)", g );
                }
                else
                {
                    sprintf( buf, "gain(%g,\"dB\")", g );
                }
                if ( fRet )
                    *fRet = buf;
                cout << "GAIN = " << buf << endl;
                DeleteWindow( );
                break;
            }
            // cancel
            case kCancelId: {
                if ( fRet )
                    *fRet = "";
                DeleteWindow( );
                break;
            }
            }
        }
        // Radio buttons
        if ( ( GET_MSG( msg ) == kC_COMMAND ) &&
             ( GET_SUBMSG( msg ) == kCM_RADIOBUTTON ) )
        {
            switch ( parm1 )
            {
            case 11:
            case 12: {
                for ( int i = 0; i < 2; ++i )
                {
                    fFormat[ i ]->SetState( i == parm1 - 11 ? kButtonDown
                                                            : kButtonUp );
                }
                break;
            }
            }
        }
        return kTRUE;
    }

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // TLGZpkDialog //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    TLGZpkDialog::TLGZpkDialog( const TGWindow* p,
                                const TGWindow* main,
                                double          fsample,
                                TString&        result,
                                Bool_t          splane )
        : TLGTransientFrame( p, main, 10, 10 ), fSample( fsample ),
          fRet( &result ), fSPlaneSpec( splane )
    {
        // layout hints
        fL[ 0 ] = new TGLayoutHints(
            kLHintsLeft | kLHintsExpandX | kLHintsTop, 2, 2, 2, 2 );
        fL[ 1 ] = new TGLayoutHints( kLHintsRight | kLHintsTop, 6, 6, 12, 4 );
        fL[ 2 ] = new TGLayoutHints( kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        fL[ 3 ] = new TGTableLayoutHints(
            0, 1, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2 );
        fL[ 4 ] = new TGTableLayoutHints(
            1, 2, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2 );
        fL[ 5 ] = new TGTableLayoutHints(
            2, 3, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2 );
        fL[ 6 ] = new TGTableLayoutHints(
            3, 4, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2 );
        fL[ 7 ] = new TGTableLayoutHints(
            0, 1, 1, 2, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2 );
        fL[ 8 ] = new TGTableLayoutHints(
            1, 2, 1, 2, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2 );
        fL[ 9 ] = new TGTableLayoutHints(
            2, 3, 1, 2, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2 );
        fL[ 10 ] = new TGTableLayoutHints(
            3, 4, 1, 2, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2 );
        fL[ 11 ] = new TGTableLayoutHints(
            0, 1, 2, 3, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2 );
        fL[ 12 ] = new TGTableLayoutHints(
            1, 2, 2, 3, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2 );
        fL[ 13 ] = new TGTableLayoutHints(
            2, 3, 2, 3, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2 );
        fL[ 14 ] = new TGTableLayoutHints(
            3, 4, 2, 3, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2 );
        fL[ 15 ] = new TGTableLayoutHints(
            0, 1, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 12, 2 );
        fL[ 16 ] = new TGTableLayoutHints(
            1, 2, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 12, 2 );
        fL[ 17 ] = new TGTableLayoutHints(
            0, 1, 1, 6, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2 );
        fL[ 18 ] = new TGTableLayoutHints(
            1, 2, 1, 6, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 2 );
        fL[ 19 ] =
            new TGTableLayoutHints( 2,
                                    3,
                                    1,
                                    2,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    12,
                                    2,
                                    2,
                                    2 );
        fL[ 20 ] =
            new TGTableLayoutHints( 2,
                                    3,
                                    2,
                                    3,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    12,
                                    2,
                                    2,
                                    2 );
        fL[ 21 ] =
            new TGTableLayoutHints( 2,
                                    3,
                                    3,
                                    4,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    12,
                                    2,
                                    2,
                                    2 );
        fL[ 22 ] =
            new TGTableLayoutHints( 2,
                                    3,
                                    4,
                                    5,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    12,
                                    2,
                                    2,
                                    2 );
        fL[ 23 ] =
            new TGTableLayoutHints( 2,
                                    3,
                                    5,
                                    6,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    12,
                                    2,
                                    2,
                                    2 );

        // gain selection
        fF[ 0 ] = new TGGroupFrame( this, "Gain Selection" );
        AddFrame( fF[ 0 ], fL[ 0 ] );
        fF[ 1 ] = new TGHorizontalFrame( fF[ 0 ], 10, 10 );
        fF[ 0 ]->AddFrame( fF[ 1 ], fL[ 0 ] );
        fLabel[ 0 ] = new TGLabel( fF[ 1 ], "Gain:   " );
        fF[ 1 ]->AddFrame( fLabel[ 0 ], fL[ 2 ] );
        fGain = new TLGNumericControlBox( fF[ 1 ], 1.0, 6, 10, kNESReal );
        fGain->Associate( this );
        fF[ 1 ]->AddFrame( fGain, fL[ 2 ] );
        fLabel[ 1 ] = new TGLabel( fF[ 1 ], "         Format:   " );
        fF[ 1 ]->AddFrame( fLabel[ 1 ], fL[ 2 ] );
        fGainFormat[ 0 ] = new TGRadioButton( fF[ 1 ], "Scalar   ", 11 );
        fGainFormat[ 0 ]->Associate( this );
        fF[ 1 ]->AddFrame( fGainFormat[ 0 ], fL[ 2 ] );
        fGainFormat[ 1 ] = new TGRadioButton( fF[ 1 ], "dB", 12 );
        fGainFormat[ 1 ]->Associate( this );
        fF[ 1 ]->AddFrame( fGainFormat[ 1 ], fL[ 2 ] );
        // Format
        fF[ 2 ] = new TGGroupFrame( this, "Number Format" );
        AddFrame( fF[ 2 ], fL[ 0 ] );
        fF[ 2 ]->SetLayoutManager(
            new TGTableLayout( fF[ 2 ], splane ? 3 : 2, 4 ) );
        fLabel[ 2 ] = new TGLabel( fF[ 2 ], "Complex Format:            " );
        fF[ 2 ]->AddFrame( fLabel[ 2 ], fL[ 3 ] );
        fCmplxFormat[ 0 ] = new TGRadioButton( fF[ 2 ], "Re/Im   ", 13 );
        fCmplxFormat[ 0 ]->Associate( this );
        fF[ 2 ]->AddFrame( fCmplxFormat[ 0 ], fL[ 4 ] );
        fCmplxFormat[ 1 ] = new TGRadioButton( fF[ 2 ], "Mag/Phase   ", 14 );
        fCmplxFormat[ 1 ]->Associate( this );
        fF[ 2 ]->AddFrame( fCmplxFormat[ 1 ], fL[ 5 ] );
        fCmplxFormat[ 2 ] = new TGRadioButton( fF[ 2 ], "Mag/Q", 15 );
        fCmplxFormat[ 2 ]->Associate( this );
        fF[ 2 ]->AddFrame( fCmplxFormat[ 2 ], fL[ 6 ] );
        fLabel[ 3 ] = new TGLabel( fF[ 2 ], "Phase Angle:   " );
        fF[ 2 ]->AddFrame( fLabel[ 3 ], fL[ 7 ] );
        fPhaseFormat[ 0 ] = new TGRadioButton( fF[ 2 ], "degree   ", 16 );
        fPhaseFormat[ 0 ]->Associate( this );
        fF[ 2 ]->AddFrame( fPhaseFormat[ 0 ], fL[ 8 ] );
        fPhaseFormat[ 1 ] = new TGRadioButton( fF[ 2 ], "rad   ", 17 );
        fPhaseFormat[ 1 ]->Associate( this );
        fF[ 2 ]->AddFrame( fPhaseFormat[ 1 ], fL[ 9 ] );
        if ( splane )
        {
            fLabel[ 4 ] = new TGLabel( fF[ 2 ], "Root Location:   " );
            fF[ 2 ]->AddFrame( fLabel[ 4 ], fL[ 11 ] );
            fRootFormat[ 0 ] = new TGRadioButton( fF[ 2 ], "s-Plane   ", 18 );
            fRootFormat[ 0 ]->Associate( this );
            fF[ 2 ]->AddFrame( fRootFormat[ 0 ], fL[ 12 ] );
            fRootFormat[ 1 ] = new TGRadioButton( fF[ 2 ], "Frequency   ", 19 );
            fRootFormat[ 1 ]->Associate( this );
            fF[ 2 ]->AddFrame( fRootFormat[ 1 ], fL[ 13 ] );
            fRootFormat[ 2 ] = new TGRadioButton( fF[ 2 ], "Normalized", 20 );
            fRootFormat[ 2 ]->Associate( this );
            fF[ 2 ]->AddFrame( fRootFormat[ 2 ], fL[ 14 ] );
        }
        else
        {
            fLabel[ 4 ] = 0;
            fRootFormat[ 0 ] = 0;
            fRootFormat[ 1 ] = 0;
            fRootFormat[ 2 ] = 0;
        }
        fF[ 2 ]->Resize( fF[ 2 ]->GetDefaultWidth( ),
                         fF[ 2 ]->GetDefaultHeight( ) );

        // Root spec
        fF[ 3 ] = new TGGroupFrame( this, "Root Location" );
        AddFrame( fF[ 3 ], fL[ 0 ] );
        fF[ 4 ] = new TGHorizontalFrame( fF[ 3 ], 10, 10 );
        fF[ 3 ]->AddFrame( fF[ 4 ], fL[ 0 ] );
        fLabel[ 5 ] = new TGLabel( fF[ 4 ], "Mag: " );
        fF[ 4 ]->AddFrame( fLabel[ 5 ], fL[ 2 ] );
        fNum[ 0 ] = new TLGNumericControlBox( fF[ 4 ], 1.0, 12, 21, kNESReal );
        fNum[ 0 ]->Associate( this );
        fF[ 4 ]->AddFrame( fNum[ 0 ], fL[ 2 ] );
        fLabel[ 6 ] = new TGLabel( fF[ 4 ], "Hz         " );
        fF[ 4 ]->AddFrame( fLabel[ 6 ], fL[ 2 ] );
        fLabel[ 7 ] = new TGLabel( fF[ 4 ], "Phase: " );
        fF[ 4 ]->AddFrame( fLabel[ 7 ], fL[ 2 ] );
        fNum[ 1 ] = new TLGNumericControlBox( fF[ 4 ], 0.0, 12, 22, kNESReal );
        fNum[ 1 ]->Associate( this );
        fF[ 4 ]->AddFrame( fNum[ 1 ], fL[ 2 ] );
        fLabel[ 8 ] = new TGLabel( fF[ 4 ], "rad/s         " );
        fF[ 4 ]->AddFrame( fLabel[ 8 ], fL[ 2 ] );
        fRealCmplx[ 0 ] = new TGRadioButton( fF[ 4 ], "Real   ", 23 );
        fRealCmplx[ 0 ]->Associate( this );
        fF[ 4 ]->AddFrame( fRealCmplx[ 0 ], fL[ 12 ] );
        fRealCmplx[ 1 ] = new TGRadioButton( fF[ 4 ], "Complex", 24 );
        fRealCmplx[ 1 ]->Associate( this );
        fF[ 4 ]->AddFrame( fRealCmplx[ 1 ], fL[ 13 ] );

        // List of poles and zeros
        fF[ 5 ] = new TGGroupFrame( this, "List of Roots" );
        AddFrame( fF[ 5 ], fL[ 0 ] );
        fF[ 5 ]->SetLayoutManager( new TGTableLayout( fF[ 5 ], 6, 3 ) );
        fPoleZero[ 0 ] = new TGRadioButton( fF[ 5 ], "Poles", 25 );
        fPoleZero[ 0 ]->Associate( this );
        fF[ 5 ]->AddFrame( fPoleZero[ 0 ], fL[ 15 ] );
        fPoleZero[ 1 ] = new TGRadioButton( fF[ 5 ], "Zeros", 26 );
        fPoleZero[ 1 ]->Associate( this );
        fF[ 5 ]->AddFrame( fPoleZero[ 1 ], fL[ 16 ] );
        fPZList[ 0 ] = new TGListBox( fF[ 5 ], 27 );
        fPZList[ 0 ]->Associate( this );
        fPZList[ 0 ]->Resize( 250, 200 );
        fF[ 5 ]->AddFrame( fPZList[ 0 ], fL[ 17 ] );
        fPZList[ 1 ] = new TGListBox( fF[ 5 ], 28 );
        fPZList[ 1 ]->Associate( this );
        fPZList[ 1 ]->Resize( 250, 200 );
        fF[ 5 ]->AddFrame( fPZList[ 1 ], fL[ 18 ] );
        fPZBtn[ 0 ] = new TGTextButton(
            fF[ 5 ], new TGHotString( "     &Add     " ), 29 );
        fPZBtn[ 0 ]->Associate( this );
        fF[ 5 ]->AddFrame( fPZBtn[ 0 ], fL[ 19 ] );
        fPZBtn[ 1 ] = new TGTextButton(
            fF[ 5 ], new TGHotString( "    &Remove    " ), 30 );
        fPZBtn[ 1 ]->Associate( this );
        fF[ 5 ]->AddFrame( fPZBtn[ 1 ], fL[ 20 ] );
        fPZBtn[ 2 ] = new TGTextButton(
            fF[ 5 ], new TGHotString( "    &Modify    " ), 31 );
        fPZBtn[ 2 ]->Associate( this );
        fF[ 5 ]->AddFrame( fPZBtn[ 2 ], fL[ 21 ] );
        fPZBtn[ 3 ] = new TGTextButton(
            fF[ 5 ], new TGHotString( "    C&lear     " ), 32 );
        fPZBtn[ 3 ]->Associate( this );
        fF[ 5 ]->AddFrame( fPZBtn[ 3 ], fL[ 22 ] );
        fPZBtn[ 4 ] = new TGTextButton(
            fF[ 5 ], new TGHotString( "     &Sort     " ), 33 );
        fPZBtn[ 4 ]->Associate( this );
        fF[ 5 ]->AddFrame( fPZBtn[ 4 ], fL[ 23 ] );
        fF[ 5 ]->Resize( fF[ 5 ]->GetDefaultWidth( ),
                         fF[ 5 ]->GetDefaultHeight( ) );

        // buttons
        fFButton = new TGHorizontalFrame( this, 10, 10 );
        AddFrame( fFButton, fL[ 0 ] );
        fButton[ 1 ] = new TGTextButton(
            fFButton, new TGHotString( "     &Cancel     " ), kCancelId );
        fButton[ 1 ]->Associate( this );
        fFButton->AddFrame( fButton[ 1 ], fL[ 1 ] );
        fButton[ 0 ] = new TGTextButton(
            fFButton, new TGHotString( "        &Ok        " ), kOkId );
        fButton[ 0 ]->Associate( this );
        fFButton->AddFrame( fButton[ 0 ], fL[ 1 ] );
        // setup
        fGainFormat[ 0 ]->SetState( kButtonDown );
        fCmplxFormat[ 0 ]->SetState( kButtonDown );
        fPhaseFormat[ 0 ]->SetState( kButtonDown );
        if ( fSPlaneSpec )
            fRootFormat[ 2 ]->SetState( kButtonDown );
        fPlane = "n";
        fRealCmplx[ 0 ]->SetState( kButtonDown );
        fNum[ 1 ]->SetState( kFALSE );
        fPoleZero[ 0 ]->SetState( kButtonDown );
        Setup( result );
        Build( 0 );

        // resize & move to center
        MapSubwindows( );
        UInt_t width = GetDefaultWidth( );
        UInt_t height = GetDefaultHeight( );
        Resize( width, height );
        Int_t ax;
        Int_t ay;
        if ( main )
        {
            Window_t wdum;
            gVirtualX->TranslateCoordinates(
                main->GetId( ),
                GetParent( )->GetId( ),
                ( ( (TGFrame*)main )->GetWidth( ) - fWidth ) >> 1,
                ( ( (TGFrame*)main )->GetHeight( ) - fHeight ) >> 1,
                ax,
                ay,
                wdum );
        }
        else
        {
            UInt_t root_w, root_h;
            gVirtualX->GetWindowSize(
                fClient->GetRoot( )->GetId( ), ax, ay, root_w, root_h );
            ax = ( root_w - fWidth ) >> 1;
            ay = ( root_h - fHeight ) >> 1;
        }
        Move( ax, ay );
        SetWMPosition( ax, ay );

        // make the message box non-resizable
        SetWMSize( width, height );
        SetWMSizeHints( width, height, width, height, 0, 0 );

        // set dialog box title
        SetWindowName( "Zero-Pole-Gain" );
        SetIconName( "ZPK" );
        SetClassHints( "ZpkFilterDlg", "ZpkFilterDlg" );
        SetMWMHints( kMWMDecorAll | kMWMDecorResizeH | kMWMDecorMaximize |
                         kMWMDecorMinimize | kMWMDecorMenu,
                     kMWMFuncAll | kMWMFuncResize | kMWMFuncMaximize |
                         kMWMFuncMinimize,
                     kMWMInputModeless );

        MapWindow( );
        fClient->WaitFor( this );
    }

    //______________________________________________________________________________
    TLGZpkDialog::~TLGZpkDialog( )
    {
        delete fGain;
        for ( int i = 0; i < 2; ++i )
            delete fGainFormat[ i ];
        for ( int i = 0; i < 3; ++i )
            delete fCmplxFormat[ i ];
        for ( int i = 0; i < 2; ++i )
            delete fPhaseFormat[ i ];
        for ( int i = 0; i < 3; ++i )
            delete fRootFormat[ i ];
        for ( int i = 0; i < 2; ++i )
            delete fNum[ i ];
        for ( int i = 0; i < 2; ++i )
            delete fRealCmplx[ i ];
        for ( int i = 0; i < 2; ++i )
            delete fPoleZero[ i ];
        for ( int i = 0; i < 2; ++i )
            delete fPZList[ i ];
        for ( int i = 0; i < 5; ++i )
            delete fPZBtn[ i ];
        delete fButton[ 0 ];
        delete fButton[ 1 ];
        delete fFButton;
        for ( int i = 0; i < 9; ++i )
            delete fLabel[ i ];
        for ( int i = 0; i < 6; ++i )
            delete fF[ i ];
        for ( int i = 0; i < 24; ++i )
            delete fL[ i ];
    }

    //______________________________________________________________________________
    void
    TLGZpkDialog::CloseWindow( )
    {
        if ( fRet )
            *fRet = "";
        DeleteWindow( );
    }

    //______________________________________________________________________________
    Bool_t
    TLGZpkDialog::Setup( const char* cmd )
    {
        if ( !cmd || !*cmd )
        {
            return kTRUE;
        }
        fPZ[ 0 ].clear( );
        fPZ[ 1 ].clear( );
        FilterDlgParser parser( 0, this );
        if ( parser.filter( cmd ) )
        {
            return kTRUE;
        }
        fPZ[ 0 ].clear( );
        fPZ[ 1 ].clear( );
        FilterDesign ds( fSample );
        string       zpk;
        if ( fSPlaneSpec )
        {
            if ( !ds.filter( cmd ) || !iir2zpk( ds.get( ), zpk, "s" ) )
            {
                return kFALSE;
            }
        }
        else
        {
            if ( !ds.filter( cmd ) || !iir2z( ds.get( ), zpk ) )
            {
                return kFALSE;
            }
        }
        return parser.filter( zpk.c_str( ) );
    }

    //______________________________________________________________________________
    Bool_t
    TLGZpkDialog::SetPlane( const char* plane )
    {
        if ( !fSPlaneSpec )
        {
            return kFALSE;
        }
        string newplane;
        if ( plane && ( plane[ 0 ] == 'n' ) )
        {
            newplane = "n";
        }
        else if ( plane && ( plane[ 0 ] == 'f' ) )
        {
            newplane = "f";
        }
        else
        {
            newplane = "s";
        }
        if ( newplane == fPlane )
        {
            return kTRUE;
        }
        double g = 1;
        for ( int i = 0; i < 2; ++i )
        {
            for ( RootList::iterator j = fPZ[ i ].begin( );
                  j != fPZ[ i ].end( );
                  ++j )
            {
                // convert to s-plane
                if ( fPlane[ 0 ] == 'n' )
                {
                    double f0 = j->Mag( );
                    *j = -~*j * 2. * pi;
                    double dg = ( fabs( f0 ) > 1E-10 ) ? 2 * pi * f0 : 2 * pi;
                    if ( i == 0 )
                        g *= dg;
                    else
                        g /= dg;
                }
                else if ( fPlane[ 0 ] == 'f' )
                {
                    *j *= 2 * pi;
                }
                // convert from s-plane
                if ( newplane[ 0 ] == 'n' )
                {
                    *j = -~*j / ( 2. * pi );
                    double f0 = j->Mag( );
                    double dg = ( fabs( f0 ) > 1E-10 ) ? 2 * pi * f0 : 2 * pi;
                    if ( i == 0 )
                        g /= dg;
                    else
                        g *= dg;
                }
                else if ( newplane[ 0 ] == 'f' )
                {
                    *j /= 2 * pi;
                }
            }
        }
        if ( g != 1 )
        {
            if ( fGainFormat[ 0 ]->GetState( ) == kButtonDown )
            {
                fGain->SetNumber( g * fGain->GetNumber( ) );
            }
            else if ( g < 0 )
            {
                fGain->SetNumber( g * cvtDB( fGain->GetNumber( ) ) );
                fGainFormat[ 0 ]->SetState( kButtonDown );
                fGainFormat[ 1 ]->SetState( kButtonUp );
            }
            else
            {
                fGain->SetNumber( g + 20 * log10( fGain->GetNumber( ) ) );
            }
        }
        fPlane = newplane;
        return kTRUE;
    }

    //______________________________________________________________________________
    Bool_t
    TLGZpkDialog::AddRoot( int l, int* index )
    {
        // insert position
        RootList::iterator pos = fPZ[ l ].end( );
        if ( index && ( *index >= 0 ) && ( *index < (int)fPZ[ l ].size( ) ) )
        {
            pos = fPZ[ l ].begin( ) + *index;
        }
        // get format
        ComplexFormat fmt = kReIm;
        if ( fCmplxFormat[ 1 ]->GetState( ) == kButtonDown )
            fmt = kMagPhase;
        if ( fCmplxFormat[ 2 ]->GetState( ) == kButtonDown )
            fmt = kMagQ;
        bool degrees = ( fPhaseFormat[ 0 ]->GetState( ) == kButtonDown );
        // get numbers
        double x = fNum[ 0 ]->GetNumber( );
        double y = fNum[ 1 ]->GetNumber( );
        // add roots to list
        if ( fRealCmplx[ 0 ]->GetState( ) == kButtonDown )
        {
            pos = fPZ[ l ].insert( pos, x );
        }
        else if ( fmt == kReIm )
        {
            dComplex r( x, fabs( y ) );
            pos = fPZ[ l ].insert( pos, ~r );
            pos = fPZ[ l ].insert( pos, r );
        }
        else if ( fmt == kMagPhase )
        {
            dComplex r;
            if ( degrees )
                y /= 180. / pi;
            r.setMArg( x, y );
            pos = fPZ[ l ].insert( pos, ~r );
            pos = fPZ[ l ].insert( pos, r );
        }
        else
        {
            dComplex r;
            double   twoQ = 2. * fabs( y );
            if ( twoQ <= 1 )
            {
                r = dComplex( x, 0 );
            }
            else
            {
                r = dComplex( x / twoQ, x * sqrt( 1 - 1. / ( twoQ * twoQ ) ) );
            }
            pos = fPZ[ l ].insert( pos, ~r );
            pos = fPZ[ l ].insert( pos, r );
        }
        if ( index )
            *index = pos - fPZ[ l ].begin( );
        return kTRUE;
    }

    //______________________________________________________________________________
    Bool_t
    TLGZpkDialog::RemoveRoot( int l, int* index )
    {
        // position of entry to remove
        int idx = ( index == 0 ) ? fPZList[ l ]->GetSelected( ) : *index;
        if ( ( idx < 0 ) || ( idx >= (int)fPZ[ l ].size( ) ) )
        {
            if ( index )
                *index = fPZ[ l ].size( );
            return kFALSE;
        }
        RootList::iterator pos = fPZ[ l ].begin( ) + idx;
        // remove entry
        dComplex z = fPZ[ l ][ idx ];
        pos = fPZ[ l ].erase( pos );
        if ( z.Imag( ) != 0 )
        {
            if ( ( idx < (int)fPZ[ l ].size( ) ) &&
                 ( ( fPZ[ l ][ idx ] - ~z ).Mag( ) < 1E-6 ) )
            {
                pos = fPZ[ l ].erase( pos );
            }
            else if ( ( idx - 1 >= 0 ) &&
                      ( ( fPZ[ l ][ idx - 1 ] - ~z ).Mag( ) < 1E-6 ) )
            {
                --idx;
                pos = fPZ[ l ].erase( --pos );
            }
        }
        if ( index )
            *index = idx;
        return kTRUE;
    }

    //______________________________________________________________________________
    Bool_t
    TLGZpkDialog::UpdateText( )
    {
        ComplexFormat fmt = kReIm;
        if ( fCmplxFormat[ 1 ]->GetState( ) == kButtonDown )
            fmt = kMagPhase;
        if ( fCmplxFormat[ 2 ]->GetState( ) == kButtonDown )
            fmt = kMagQ;
        bool degrees = ( fPhaseFormat[ 0 ]->GetState( ) == kButtonDown );

        // Update widget's text
        TString angularunits;
        if ( fSPlaneSpec )
        {
            angularunits = fRootFormat[ 0 ]->GetState( ) == kButtonDown
                ? "rad/s      "
                : "Hz         ";
        }
        else
        {
            angularunits = "             ";
        }
        if ( fRealCmplx[ 0 ]->GetState( ) == kButtonDown )
        {
            fLabel[ 5 ]->SetText( "Value:" );
            fLabel[ 6 ]->SetText( angularunits );
            fLabel[ 7 ]->SetText( "         " );
            fLabel[ 8 ]->SetText( "             " );
        }
        else
        {
            switch ( fmt )
            {
            case kReIm: {
                fLabel[ 5 ]->SetText( "Real:" );
                fLabel[ 6 ]->SetText( angularunits );
                fLabel[ 7 ]->SetText( "Imag:" );
                fLabel[ 8 ]->SetText( angularunits );
                break;
            }
            case kMagPhase: {
                fLabel[ 5 ]->SetText( "Mag:" );
                fLabel[ 6 ]->SetText( angularunits );
                fLabel[ 7 ]->SetText( "Phase:" );
                fLabel[ 8 ]->SetText( degrees ? "deg       " : "rad       " );
                break;
            }
            case kMagQ: {
                fLabel[ 5 ]->SetText( "Mag:" );
                fLabel[ 6 ]->SetText( angularunits );
                fLabel[ 7 ]->SetText( "   Q:" );
                fLabel[ 8 ]->SetText( "" );
                break;
            }
            }
        }
        return kTRUE;
    }

    //______________________________________________________________________________
    Bool_t
    TLGZpkDialog::Build( int level, Bool_t plist, Bool_t zlist )
    {
        ComplexFormat fmt = kReIm;
        if ( fCmplxFormat[ 1 ]->GetState( ) == kButtonDown )
            fmt = kMagPhase;
        if ( fCmplxFormat[ 2 ]->GetState( ) == kButtonDown )
            fmt = kMagQ;
        bool degrees = ( fPhaseFormat[ 0 ]->GetState( ) == kButtonDown );

        // Poles
        if ( plist )
        {
            int sel = fPZList[ 0 ]->GetSelected( );
            fPZList[ 0 ]->RemoveEntries( 0, 100000 );
            int n = 0;
            for ( RootList::iterator i = fPZ[ 0 ].begin( );
                  i != fPZ[ 0 ].end( );
                  ++i, ++n )
            {
                string s = cmplx2str( *i, fmt, degrees );
                fPZList[ 0 ]->AddEntry( s.c_str( ), n );
            }
            if ( ( level > 0 ) && ( sel >= 0 ) && ( sel < n ) )
            {
                fPZList[ 0 ]->Select( sel );
            }
            fPZList[ 0 ]->MapSubwindows( );
            fPZList[ 0 ]->Layout( );
        }

        // Zeros
        if ( zlist )
        {
            int sel = fPZList[ 1 ]->GetSelected( );
            fPZList[ 1 ]->RemoveEntries( 0, 100000 );
            int n = 0;
            for ( RootList::iterator i = fPZ[ 1 ].begin( );
                  i != fPZ[ 1 ].end( );
                  ++i, ++n )
            {
                string s = cmplx2str( *i, fmt, degrees );
                fPZList[ 1 ]->AddEntry( s.c_str( ), n );
            }
            if ( ( level > 0 ) && ( sel >= 0 ) && ( sel < n ) )
            {
                fPZList[ 1 ]->Select( sel );
            }
            fPZList[ 1 ]->MapSubwindows( );
            fPZList[ 1 ]->Layout( );
        }

        // Update widget's text
        if ( level == 0 )
            fPZBtn[ 2 ]->SetState( kButtonDisabled );
        UpdateText( );

        return kTRUE;
    }

    //______________________________________________________________________________
    Bool_t
    TLGZpkDialog::ProcessMessage( Long_t msg, Long_t parm1, Long_t parm2 )
    {
        // Buttons
        if ( ( GET_MSG( msg ) == kC_COMMAND ) &&
             ( GET_SUBMSG( msg ) == kCM_BUTTON ) )
        {
            switch ( parm1 )
            {
            // ok
            case kOkId: {
                string s = fSPlaneSpec ? "zpk([" : "zroots([";
                for ( RootList::iterator i = fPZ[ 1 ].begin( );
                      i != fPZ[ 1 ].end( );
                      ++i )
                {
                    if ( i != fPZ[ 1 ].begin( ) )
                        s += ";";
                    s += cmplx2str( *i, kReIm );
                }
                s += "],[";
                for ( RootList::iterator i = fPZ[ 0 ].begin( );
                      i != fPZ[ 0 ].end( );
                      ++i )
                {
                    if ( i != fPZ[ 0 ].begin( ) )
                        s += ";";
                    s += cmplx2str( *i, kReIm );
                }
                s += "],";
                double g = fGain->GetNumber( );
                if ( fGainFormat[ 1 ]->GetState( ) == kButtonDown )
                {
                    g = cvtDB( g );
                }
                char buf[ 128 ];
                sprintf( buf, "%g", g );
                s += buf;
                if ( fSPlaneSpec && ( fPlane != "s" ) )
                {
                    s += ",\"" + fPlane + "\"";
                }
                s += ")";
                if ( fRet )
                    *fRet = s.c_str( );
                DeleteWindow( );
                break;
            }
            // cancel
            case kCancelId: {
                if ( fRet )
                    *fRet = "";
                DeleteWindow( );
                break;
            }
            // add
            case 29: {
                // get list
                bool plist = fPoleZero[ 0 ]->GetState( ) == kButtonDown;
                int  l = plist ? 0 : 1;
                int  index = -1;
                AddRoot( l, &index );
                Build( 0, plist, !plist );
                if ( ( index >= 0 ) && ( index < (int)fPZ[ l ].size( ) ) )
                {
                    fPZList[ l ]->Select( index );
                    fPZBtn[ 2 ]->SetState( kButtonUp );
                }
                break;
            }
            // remove
            case 30: {
                bool plist = fPoleZero[ 0 ]->GetState( ) == kButtonDown;
                int  l = plist ? 0 : 1;
                int  i = fPZList[ l ]->GetSelected( );
                if ( RemoveRoot( l, &i ) )
                {
                    Build( 0, plist, !plist );
                    if ( i >= (int)fPZ[ l ].size( ) )
                        --i;
                    if ( ( i >= 0 ) && ( i < (int)fPZ[ l ].size( ) ) )
                    {
                        fPZList[ l ]->Select( i );
                        fPZBtn[ 2 ]->SetState( kButtonUp );
                    }
                }
                break;
            }
            // modify
            case 31: {
                bool plist = fPoleZero[ 0 ]->GetState( ) == kButtonDown;
                int  l = plist ? 0 : 1;
                int  i = fPZList[ l ]->GetSelected( );
                // first remove old entry, tehn add new one
                if ( RemoveRoot( l, &i ) && AddRoot( l, &i ) )
                {
                    Build( 0, plist, !plist );
                    if ( i >= (int)fPZ[ l ].size( ) )
                        --i;
                    if ( ( i >= 0 ) && ( i < (int)fPZ[ l ].size( ) ) )
                    {
                        fPZList[ l ]->Select( i );
                        fPZBtn[ 2 ]->SetState( kButtonUp );
                    }
                }
                break;
            }
            // clear
            case 32: {
                bool plist = fPoleZero[ 0 ]->GetState( ) == kButtonDown;
                if ( plist )
                {
                    fPZ[ 0 ].clear( );
                    Build( 0, kTRUE, kFALSE );
                }
                else
                {
                    fPZ[ 1 ].clear( );
                    Build( 0, kFALSE, kTRUE );
                }
                break;
            }
            // sort
            case 33: {
                bool      plist = fPoleZero[ 0 ]->GetState( ) == kButtonDown;
                int       l = plist ? 0 : 1;
                dComplex* root = new dComplex[ fPZ[ l ].size( ) + 1 ];
                for ( int i = 0; i < (int)fPZ[ l ].size( ); ++i )
                {
                    root[ i ] = fPZ[ l ][ i ];
                }
                sort_roots( root, fPZ[ l ].size( ), fSPlaneSpec );
                for ( int i = 0; i < (int)fPZ[ l ].size( ); ++i )
                {
                    fPZ[ l ][ i ] = root[ i ];
                }
                delete[] root;
                Build( 0, plist, !plist );
                break;
            }
            }
        }

        // Radio buttons
        else if ( ( GET_MSG( msg ) == kC_COMMAND ) &&
                  ( GET_SUBMSG( msg ) == kCM_RADIOBUTTON ) )
        {
            switch ( parm1 )
            {
            // Gain format
            case 11:
            case 12: {
                for ( int i = 0; i < 2; ++i )
                {
                    fGainFormat[ i ]->SetState( i == parm1 - 11 ? kButtonDown
                                                                : kButtonUp );
                }
                break;
            }
            // Complex format
            case 13:
            case 14:
            case 15: {
                for ( int i = 0; i < 3; ++i )
                {
                    fCmplxFormat[ i ]->SetState( i == parm1 - 13 ? kButtonDown
                                                                 : kButtonUp );
                }
                Build( 1 );
                break;
            }
            // Degree/rad
            case 16:
            case 17: {
                for ( int i = 0; i < 2; ++i )
                {
                    fPhaseFormat[ i ]->SetState( i == parm1 - 16 ? kButtonDown
                                                                 : kButtonUp );
                }
                Build( 1 );
                break;
            }
            // Root location
            case 18:
            case 19:
            case 20: {
                int j = parm1 - 18;
                for ( int i = 0; i < 3; ++i )
                {
                    fRootFormat[ i ]->SetState( i == j ? kButtonDown
                                                       : kButtonUp );
                }
                SetPlane( j == 0 ? "s" : ( j == 1 ? "f" : "n" ) );
                Build( 1 );
                break;
            }
            // Real/Complex
            case 23:
            case 24: {
                for ( int i = 0; i < 2; ++i )
                {
                    fRealCmplx[ i ]->SetState( i == parm1 - 23 ? kButtonDown
                                                               : kButtonUp );
                }
                fNum[ 1 ]->SetState( 1 == parm1 - 23 );
                UpdateText( );
                break;
            }
            // List of poles or zeros
            case 25:
            case 26: {
                int l = parm1 - 25;
                for ( int i = 0; i < 2; ++i )
                {
                    fPoleZero[ i ]->SetState( i == l ? kButtonDown
                                                     : kButtonUp );
                }
                // deselect entry in other list
                l = ( l + 1 ) % 2;
                fPZList[ l ]->Select( -1 );
                break;
            }
            }
        }

        // List box
        else if ( ( GET_MSG( msg ) == kC_COMMAND ) &&
                  ( GET_SUBMSG( msg ) == kCM_LISTBOX ) )
        {
            switch ( parm1 )
            {
            // pole/zero lists
            case 27:
            case 28: {
                int l = parm1 - 27;
                int i = fPZList[ l ]->GetSelected( );
                // set numbers
                if ( ( i >= 0 ) && ( i < (int)fPZ[ l ].size( ) ) )
                {
                    dComplex      r = fPZ[ l ][ i ];
                    ComplexFormat fmt = kReIm;
                    if ( fCmplxFormat[ 1 ]->GetState( ) == kButtonDown )
                        fmt = kMagPhase;
                    if ( fCmplxFormat[ 2 ]->GetState( ) == kButtonDown )
                        fmt = kMagQ;
                    bool degrees =
                        ( fPhaseFormat[ 0 ]->GetState( ) == kButtonDown );
                    double x;
                    double y;
                    fRealCmplx[ 1 ]->SetState( kButtonDown );
                    fRealCmplx[ 0 ]->SetState( kButtonUp );
                    if ( fabs( r.Imag( ) ) < 1E-12 )
                    {
                        fRealCmplx[ 0 ]->SetState( kButtonDown );
                        fRealCmplx[ 1 ]->SetState( kButtonUp );
                        x = r.Real( );
                        y = 0;
                    }
                    else if ( fmt == kReIm )
                    {
                        x = r.Real( );
                        y = r.Imag( );
                    }
                    else if ( fmt == kMagPhase )
                    {
                        x = r.Mag( );
                        y = r.Arg( );
                        if ( degrees )
                            y *= 180. / pi;
                    }
                    else
                    {
                        x = r.Mag( );
                        y = x / fabs( 2. * r.Real( ) );
                    }
                    fNum[ 0 ]->SetNumber( x );
                    fNum[ 1 ]->SetNumber( y );
                    fNum[ 1 ]->SetState( fRealCmplx[ 0 ]->GetState( ) !=
                                         kButtonDown );
                    fPZBtn[ 2 ]->SetState( kButtonUp );
                    UpdateText( );
                }
                // set pole/zero list radio buttons
                for ( int j = 0; j < 2; ++j )
                {
                    fPoleZero[ j ]->SetState( j == l ? kButtonDown
                                                     : kButtonUp );
                }
                // deselect entry in other list
                l = ( l + 1 ) % 2;
                fPZList[ l ]->Select( -1 );
                break;
            }
            }
        }
        return kTRUE;
    }

    //______________________________________________________________________________
    std::string
    TLGZpkDialog::cmplx2str( dComplex x, ComplexFormat format, bool degrees )
    {
        char buf[ 256 ];
        // Real case first
        if ( fabs( x.Imag( ) ) < 1E-12 )
        {
            sprintf( buf, "%g", x.Real( ) );
        }
        // Re/Im case next
        else if ( format == kReIm )
        {
            sprintf( buf,
                     "%g%ci*%g",
                     x.Real( ),
                     x.Imag( ) > 0 ? '+' : '-',
                     fabs( x.Imag( ) ) );
        }
        // Mag/Phase
        else if ( format == kMagPhase )
        {
            double m = x.Mag( );
            double a = x.Arg( );
            if ( degrees )
                a *= 180. / pi;
            sprintf(
                buf, "|z|=%g arg(z)=%g %s", m, a, degrees ? "deg" : "rad" );
        }
        // Mag/Q
        else
        {
            double m = x.Mag( );
            double q = m / fabs( 2. * x.Real( ) );
            sprintf( buf, "|z|=%g Q=%g", m, q );
        }
        return buf;
    }

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // TLGPolyDialog			                                //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    TLGPolyDialog::TLGPolyDialog( const TGWindow* p,
                                  const TGWindow* main,
                                  TString&        result,
                                  double          sample,
                                  Bool_t          splane )
        : TLGTransientFrame( p, main, 10, 10 ), fSample( sample ),
          fSPlaneSpec( splane ), fRet( &result ), fFormulaText( 0 )
    {
        // layout hints
        fL[ 0 ] = new TGLayoutHints(
            kLHintsLeft | kLHintsExpandX | kLHintsTop, 2, 2, 2, 2 );
        fL[ 1 ] = new TGLayoutHints( kLHintsRight | kLHintsTop, 6, 6, 12, 4 );
        fL[ 2 ] = new TGTableLayoutHints(
            0, 1, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 15, -5 );
        fL[ 3 ] =
            new TGTableLayoutHints( 1,
                                    2,
                                    0,
                                    1,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    15,
                                    -5 );
        for ( int k = 0; k < 2; ++k )
        {
            for ( int j = 0; j < kMaxPolyCoeff; ++j )
            {
                int indx = 4 + 2 * j + 2 * k * kMaxPolyCoeff;
                int left = 2 * ( j / ( kMaxPolyCoeff / 2 ) + 2 * k );
                int top = j % ( kMaxPolyCoeff / 2 );
                fL[ indx ] =
                    new TGTableLayoutHints( left,
                                            left + 1,
                                            top,
                                            top + 1,
                                            kLHintsLeft | kLHintsCenterY,
                                            left == 4 ? 120 : 20,
                                            2,
                                            4,
                                            0 );
                fL[ indx + 1 ] =
                    new TGTableLayoutHints( left + 1,
                                            left + 2,
                                            top,
                                            top + 1,
                                            kLHintsLeft | kLHintsCenterY,
                                            2,
                                            2,
                                            4,
                                            0 );
            }
        }
        // gain selection
        if ( fSPlaneSpec )
        {
            fF[ 0 ] = new TGGroupFrame( this, "Gain Selection" );
            AddFrame( fF[ 0 ], fL[ 0 ] );
            fF[ 0 ]->SetLayoutManager( new TGTableLayout( fF[ 0 ], 1, 2 ) );
            fLabel[ 0 ] = new TGLabel( fF[ 0 ], "Gain:   " );
            fF[ 0 ]->AddFrame( fLabel[ 0 ], fL[ 2 ] );
            fGain = new TLGNumericControlBox( fF[ 0 ], 1.0, 12, 10, kNESReal );
            fGain->Associate( this );
            fF[ 0 ]->AddFrame( fGain, fL[ 3 ] );
            fF[ 0 ]->Resize( fF[ 0 ]->GetDefaultWidth( ),
                             fF[ 0 ]->GetDefaultHeight( ) );
        }
        else
        {
            fF[ 0 ] = 0;
            fLabel[ 0 ] = 0;
            fGain = 0;
        }
        // coefficients
        fF[ 1 ] = new TGGroupFrame( this, "Coefficients" );
        AddFrame( fF[ 1 ], fL[ 0 ] );
        fF[ 1 ]->SetLayoutManager(
            new TGTableLayout( fF[ 1 ], kMaxPolyCoeff / 2, 8 ) );
        for ( int k = 0; k < 2; ++k )
        {
            const char* const coeff = ( k == 0 ) ? " b%i:" : " a%i:";
            int               ofs = ( k == 1 ) && !fSPlaneSpec ? 1 : 0;
            char              buf[ 64 ];
            for ( int i = 0; i < kMaxPolyCoeff; ++i )
            {
                int indx = 4 + 2 * i + 2 * k * kMaxPolyCoeff;
                sprintf( buf, coeff, i + ofs );
                fPolyLabel[ k ][ i ] = new TGLabel( fF[ 1 ], buf );
                fF[ 1 ]->AddFrame( fPolyLabel[ k ][ i ], fL[ indx ] );
                fPolyCoeff[ k ][ i ] = new TLGNumericControlBox(
                    fF[ 1 ], 0.0, 15, 100 * ( k + 1 ) + i, kNESReal );
                fPolyCoeff[ k ][ i ]->Associate( this );
                fF[ 1 ]->AddFrame( fPolyCoeff[ k ][ i ], fL[ indx + 1 ] );
            }
        }
        fF[ 1 ]->Resize( fF[ 1 ]->GetDefaultWidth( ),
                         fF[ 1 ]->GetDefaultHeight( ) );
        // formula
        fF[ 2 ] = new TGGroupFrame( this, "Formula" );
        AddFrame( fF[ 2 ], fL[ 0 ] );
        fFormula = new TRootEmbeddedCanvas( "polyformula", fF[ 2 ], 10, 100 );
        fFormula->SetHeight( 100 );
        fF[ 2 ]->AddFrame( fFormula, fL[ 0 ] );
        // fF[2]->Resize (fF[2]->GetDefaultWidth(), 150);

        // buttons
        fFButton = new TGHorizontalFrame( this, 10, 10 );
        AddFrame( fFButton, fL[ 0 ] );
        fButton[ 1 ] = new TGTextButton(
            fFButton, new TGHotString( "     &Cancel     " ), kCancelId );
        fButton[ 1 ]->Associate( this );
        fFButton->AddFrame( fButton[ 1 ], fL[ 1 ] );
        fButton[ 0 ] = new TGTextButton(
            fFButton, new TGHotString( "        &Ok        " ), kOkId );
        fButton[ 0 ]->Associate( this );
        fFButton->AddFrame( fButton[ 0 ], fL[ 1 ] );
        // setup
        Setup( result );

        // resize & move to center
        MapSubwindows( );
        UInt_t width = GetDefaultWidth( );
        UInt_t height = GetDefaultHeight( );
        Resize( width, height );
        Int_t ax;
        Int_t ay;
        if ( main )
        {
            Window_t wdum;
            gVirtualX->TranslateCoordinates(
                main->GetId( ),
                GetParent( )->GetId( ),
                ( ( (TGFrame*)main )->GetWidth( ) - fWidth ) >> 1,
                ( ( (TGFrame*)main )->GetHeight( ) - fHeight ) >> 1,
                ax,
                ay,
                wdum );
        }
        else
        {
            UInt_t root_w, root_h;
            gVirtualX->GetWindowSize(
                fClient->GetRoot( )->GetId( ), ax, ay, root_w, root_h );
            ax = ( root_w - fWidth ) >> 1;
            ay = ( root_h - fHeight ) >> 1;
        }
        Move( ax, ay );
        SetWMPosition( ax, ay );

        // make the message box non-resizable
        SetWMSize( width, height );
        SetWMSizeHints( width, height, width, height, 0, 0 );

        // set dialog box title
        SetWindowName( "Gain Factor" );
        SetIconName( "Gain" );
        SetClassHints( "GainFactorDlg", "GainFactorDlg" );
        SetMWMHints( kMWMDecorAll | kMWMDecorResizeH | kMWMDecorMaximize |
                         kMWMDecorMinimize | kMWMDecorMenu,
                     kMWMFuncAll | kMWMFuncResize | kMWMFuncMaximize |
                         kMWMFuncMinimize,
                     kMWMInputModeless );

        MapWindow( );
        fClient->WaitFor( this );
    }

    //______________________________________________________________________________
    TLGPolyDialog::~TLGPolyDialog( )
    {
        if ( fGain )
            delete fGain;
        if ( fLabel[ 0 ] )
            delete fLabel[ 0 ];
        for ( int k = 0; k < 2; ++k )
        {
            for ( int i = 0; i < kMaxPolyCoeff; ++i )
            {
                delete fPolyLabel[ k ][ i ];
                delete fPolyCoeff[ k ][ i ];
            }
        }
        delete fFormulaText;
        delete fFormula;
        delete fButton[ 0 ];
        delete fButton[ 1 ];
        delete fFButton;
        for ( int i = 0; i < 3; ++i )
            if ( fF[ i ] )
                delete fF[ i ];
        for ( int i = 0; i < 4 + 4 * kMaxPolyCoeff; ++i )
            delete fL[ i ];
    }

    //______________________________________________________________________________
    void
    TLGPolyDialog::CloseWindow( )
    {
        if ( fRet )
            *fRet = "";
        DeleteWindow( );
    }

    //______________________________________________________________________________
    Bool_t
    TLGPolyDialog::Setup( const char* cmd )
    {
        if ( !cmd || !*cmd )
        {
            return kTRUE;
        }
        FilterDlgParser parser( 0, 0, 0, 0, 0, this );
        if ( parser.filter( cmd ) )
        {
            SetFormula( );
            return kTRUE;
        }
        FilterDesign ds( fSample );
        string       poly;
        if ( fSPlaneSpec )
        {
            if ( !ds.filter( cmd ) || !iir2zpk( ds.get( ), poly, "p" ) )
            {
                return kFALSE;
            }
        }
        else
        {
            if ( !ds.filter( cmd ) || !iir2z( ds.get( ), poly, "d" ) )
            {
                return kFALSE;
            }
        }
        if ( !parser.filter( poly.c_str( ) ) )
        {
            return kFALSE;
        }
        SetFormula( );
        return kTRUE;
    }

    //______________________________________________________________________________
    static string
    polyterm( char var, double x, int expo = 1, bool omitplus = true )
    {
        string s;
        char   buf[ 256 ];
        if ( x == 0 )
        {
            return s;
        }
        if ( x < 0 )
        {
            s += "-";
            x = fabs( x );
        }
        else if ( !omitplus )
        {
            s += "+";
        }
        if ( ( fabs( x - 1 ) > 1E-10 ) || ( expo == 0 ) )
        {
            sprintf( buf, "%g", x );
            s += buf;
        }
        if ( expo == 0 )
        {
            // nothing
        }
        else if ( expo == 1 )
        {
            s += ' ';
            s += var;
        }
        else
        {
            sprintf( buf, " %c^{%i}", var, expo );
            s += buf;
        }
        return s;
    }

    //______________________________________________________________________________
    Bool_t
    TLGPolyDialog::SetFormula( )
    {
        // get coefficients
        double* a = new double[ kMaxPolyCoeff ];
        double* b = new double[ kMaxPolyCoeff ];
        int     maxa = 0;
        int     maxb = 0;
        for ( int i = 0; i < kMaxPolyCoeff; ++i )
        {
            b[ i ] = fPolyCoeff[ 0 ][ i ]->GetNumber( );
            a[ i ] = fPolyCoeff[ 1 ][ i ]->GetNumber( );
            if ( b[ i ] != 0 )
                maxb = i + 1;
            if ( a[ i ] != 0 )
                maxa = i + 1;
        }

        // generate latex
        string x = "#frac{";
        int    blen, alen;
        if ( fSPlaneSpec )
        {
            for ( int i = maxb - 1; i >= 0; --i )
            {
                if ( b[ i ] != 0 )
                {
                    x += polyterm( 's', b[ i ], i, i == maxb - 1 );
                }
            }
            if ( maxb <= 0 )
                x += "1";
            blen = x.length( ) - 6;
            x += "}{";
            for ( int i = maxa - 1; i >= 0; --i )
            {
                if ( a[ i ] != 0 )
                {
                    x += polyterm( 's', a[ i ], i, i == maxa - 1 );
                }
            }
            if ( maxa <= 0 )
                x += "1";
        }
        else
        {
            for ( int i = 0; i < maxb; ++i )
            {
                if ( b[ i ] != 0 )
                {
                    x += polyterm( 'z', b[ i ], -i, i == 0 );
                }
            }
            if ( maxb <= 0 )
                x += "1";
            blen = x.length( ) - 6;
            x += "}{1";
            for ( int i = 0; i < maxa; ++i )
            {
                if ( a[ i ] != 0 )
                {
                    x += polyterm( 'z', a[ i ], -i - 1, false );
                }
            }
        }
        x += "}";
        alen = x.length( ) - blen - 9;
        cerr << x << endl;
        delete[] a;
        delete[] b;
        int max = ( blen > alen ) ? blen : alen;

        // draw formula on canvas
        TVirtualPad* padsave = gPad;
        fFormula->GetCanvas( )->cd( );
        if ( fFormulaText )
            delete fFormulaText;
        TLatex txt;
        txt.SetTextAlign( 22 );
        txt.SetTextSize( max >= 100 ? 0.15 : 0.2 );
        fFormulaText = txt.DrawLatex( 0.5, 0.5, x.c_str( ) );
        gPad->Update( );
        gPad = padsave;
        return kTRUE;
    }

    //______________________________________________________________________________
    Bool_t
    TLGPolyDialog::ProcessMessage( Long_t msg, Long_t parm1, Long_t parm2 )
    {
        // Buttons
        if ( ( GET_MSG( msg ) == kC_COMMAND ) &&
             ( GET_SUBMSG( msg ) == kCM_BUTTON ) )
        {
            switch ( parm1 )
            {
            // ok
            case kOkId: {
                double  g = fSPlaneSpec ? fGain->GetNumber( ) : 0;
                double* a = new double[ kMaxPolyCoeff ];
                double* b = new double[ kMaxPolyCoeff ];
                int     maxa = 0;
                int     maxb = 0;
                for ( int i = 0; i < kMaxPolyCoeff; ++i )
                {
                    b[ i ] = fPolyCoeff[ 0 ][ i ]->GetNumber( );
                    a[ i ] = fPolyCoeff[ 1 ][ i ]->GetNumber( );
                    if ( b[ i ] != 0 )
                        maxb = i + 1;
                    if ( a[ i ] != 0 )
                        maxa = i + 1;
                }
                if ( fSPlaneSpec )
                {
                    for ( int i = 0; i < maxb / 2; ++i )
                    {
                        double tmp = b[ i ];
                        b[ i ] = b[ maxb - i - 1 ];
                        b[ maxb - i - 1 ] = tmp;
                    }
                    for ( int i = 0; i < maxa / 2; ++i )
                    {
                        double tmp = a[ i ];
                        a[ i ] = a[ maxa - i - 1 ];
                        a[ maxa - i - 1 ] = tmp;
                    }
                }
                FilterDesign ds( fSample );
                bool         succ = true;
                if ( fSPlaneSpec )
                {
                    succ = ( maxa > 0 ) && ( maxb > 0 ) &&
                        ds.rpoly( maxb, b, maxa, a, g );
                }
                else
                {
                    succ = ( maxb > 0 ) && ds.direct( maxb - 1, b, maxa, a );
                }
                delete[] a;
                delete[] b;
                if ( succ )
                {
                    if ( fRet )
                        *fRet = ds.getFilterSpec( );
                }
                else
                {
                    new TGMsgBox( gClient->GetRoot( ),
                                  this,
                                  "Error",
                                  "Unable to construct filter.\n"
                                  "Probably unstable poles or missing term.",
                                  kMBIconExclamation,
                                  kMBOk,
                                  0 );
                    return kTRUE;
                }
                DeleteWindow( );
                break;
            }
            // cancel
            case kCancelId: {
                if ( fRet )
                    *fRet = "";
                DeleteWindow( );
                break;
            }
            }
        }
        // numeric field updated
        else if ( ( GET_MSG( msg ) == kC_TEXTENTRY ) &&
                  ( GET_SUBMSG( msg ) == kTE_TEXTUPDATED ) )
        {
            SetFormula( );
        }
        return kTRUE;
    }

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // TLGNotchDialog			                                //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    TLGNotchDialog::TLGNotchDialog( const TGWindow* p,
                                    const TGWindow* main,
                                    TString&        result,
                                    subtype_t       subt )
        : TLGTransientFrame( p, main, 10, 10 ), fRet( &result ), fType( subt )
    {
        // layout hints
        fL[ 0 ] = new TGLayoutHints(
            kLHintsLeft | kLHintsExpandX | kLHintsTop, 2, 2, 2, 2 );
        fL[ 1 ] = new TGLayoutHints( kLHintsRight | kLHintsTop, 6, 6, 12, 4 );
        // 1st line
        fL[ 2 ] = new TGTableLayoutHints(
            0, 1, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        fL[ 3 ] =
            new TGTableLayoutHints( 1,
                                    2,
                                    0,
                                    1,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    4,
                                    0 );
        fL[ 4 ] = new TGTableLayoutHints(
            2, 3, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        // 2nd line
        fL[ 5 ] = new TGTableLayoutHints(
            0, 1, 1, 2, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        fL[ 6 ] = new TGTableLayoutHints(
            1, 2, 1, 2, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        fL[ 7 ] = new TGTableLayoutHints(
            2, 3, 1, 2, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        // 3rd line
        fL[ 8 ] = new TGTableLayoutHints(
            0, 1, 2, 3, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        fL[ 9 ] = new TGTableLayoutHints(
            1, 2, 2, 3, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        fL[ 10 ] = new TGTableLayoutHints(
            2, 3, 2, 3, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        // 4th line
        fL[ 11 ] = new TGTableLayoutHints(
            0, 1, 3, 4, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        fL[ 12 ] = new TGTableLayoutHints(
            1, 2, 3, 4, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        fL[ 13 ] = new TGTableLayoutHints(
            2, 3, 3, 4, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );

        // group frame
        fF[ 0 ] = new TGGroupFrame( this, "Parameters" );
        AddFrame( fF[ 0 ], fL[ 0 ] );
        fF[ 0 ]->SetLayoutManager(
            new TGTableLayout( fF[ 0 ], subt == kComb ? 4 : 3, 3 ) );
        // labels
        fLabel[ 0 ] = new TGLabel( fF[ 0 ], "Frequency:   " );
        fF[ 0 ]->AddFrame( fLabel[ 0 ], fL[ 2 ] );
        fLabel[ 1 ] = new TGLabel( fF[ 0 ], "Q:   " );
        fF[ 0 ]->AddFrame( fLabel[ 1 ], fL[ 5 ] );
        switch ( subt )
        {
        case kNotch:
            fLabel[ 2 ] = new TGLabel( fF[ 0 ], "Depth:   " );
            fLabel[ 3 ] = 0;
            break;
        case kResGain:
            fLabel[ 2 ] = new TGLabel( fF[ 0 ], "Height:   " );
            fLabel[ 3 ] = 0;
            break;
        case kComb:
            fLabel[ 2 ] = new TGLabel( fF[ 0 ], "Amplitude:   " );
            fLabel[ 3 ] = new TGLabel( fF[ 0 ], "Harmonics:   " );
            break;
        }
        fF[ 0 ]->AddFrame( fLabel[ 2 ], fL[ 8 ] );
        if ( fLabel[ 3 ] )
            fF[ 0 ]->AddFrame( fLabel[ 3 ], fL[ 11 ] );
        fLabel[ 4 ] = new TGLabel( fF[ 0 ], " Hz" );
        fF[ 0 ]->AddFrame( fLabel[ 4 ], fL[ 4 ] );
        fLabel[ 5 ] = new TGLabel( fF[ 0 ], " dB" );
        fF[ 0 ]->AddFrame( fLabel[ 5 ], fL[ 10 ] );
        // parameters values
        fFreq = new TLGNumericControlBox(
            fF[ 0 ], 1.0, 12, 10, kNESReal, kNEANonNegative );
        fFreq->Associate( this );
        fF[ 0 ]->AddFrame( fFreq, fL[ 3 ] );
        fQ = new TLGNumericControlBox( fF[ 0 ],
                                       10.,
                                       12,
                                       11,
                                       kNESReal,
                                       kNEANonNegative,
                                       kNELLimitMin,
                                       0.5 );
        fQ->Associate( this );
        fF[ 0 ]->AddFrame( fQ, fL[ 6 ] );
        fDepth = new TLGNumericControlBox( fF[ 0 ],
                                           subt == kComb ? -30 : 30,
                                           12,
                                           12,
                                           kNESReal,
                                           subt == kComb ? kNEAAnyNumber
                                                         : kNEANonNegative );
        fDepth->Associate( this );
        fF[ 0 ]->AddFrame( fDepth, fL[ 9 ] );
        if ( subt == kComb )
        {
            fHarmonics = new TLGNumericControlBox(
                fF[ 0 ], 4, 12, 13, kNESInteger, kNEANonNegative );
            fHarmonics->Associate( this );
            fF[ 0 ]->AddFrame( fHarmonics, fL[ 12 ] );
        }
        else
        {
            fHarmonics = 0;
        }
        fF[ 0 ]->Resize( fF[ 0 ]->GetDefaultWidth( ),
                         fF[ 0 ]->GetDefaultHeight( ) );

        // buttons
        fFButton = new TGHorizontalFrame( this, 10, 10 );
        AddFrame( fFButton, fL[ 0 ] );
        fButton[ 1 ] = new TGTextButton(
            fFButton, new TGHotString( "     &Cancel     " ), kCancelId );
        fButton[ 1 ]->Associate( this );
        fFButton->AddFrame( fButton[ 1 ], fL[ 1 ] );
        fButton[ 0 ] = new TGTextButton(
            fFButton, new TGHotString( "        &Ok        " ), kOkId );
        fButton[ 0 ]->Associate( this );
        fFButton->AddFrame( fButton[ 0 ], fL[ 1 ] );
        // setup
        Setup( result );

        // resize & move to center
        MapSubwindows( );
        UInt_t width = GetDefaultWidth( );
        UInt_t height = GetDefaultHeight( );
        Resize( width, height );
        Int_t ax;
        Int_t ay;
        if ( main )
        {
            Window_t wdum;
            gVirtualX->TranslateCoordinates(
                main->GetId( ),
                GetParent( )->GetId( ),
                ( ( (TGFrame*)main )->GetWidth( ) - fWidth ) >> 1,
                ( ( (TGFrame*)main )->GetHeight( ) - fHeight ) >> 1,
                ax,
                ay,
                wdum );
        }
        else
        {
            UInt_t root_w, root_h;
            gVirtualX->GetWindowSize(
                fClient->GetRoot( )->GetId( ), ax, ay, root_w, root_h );
            ax = ( root_w - fWidth ) >> 1;
            ay = ( root_h - fHeight ) >> 1;
        }
        Move( ax, ay );
        SetWMPosition( ax, ay );

        // make the message box non-resizable
        SetWMSize( width, height );
        SetWMSizeHints( width, height, width, height, 0, 0 );

        // set dialog box title
        switch ( fType )
        {
        case kNotch: {
            SetWindowName( "Notch Filter" );
            SetIconName( "Notch" );
            break;
        }
        case kResGain: {
            SetWindowName( "Resonant Gain" );
            SetIconName( "ResGain" );
            break;
        }
        case kComb: {
            SetWindowName( "Comb Filter" );
            SetIconName( "Comb" );
            break;
        }
        }
        SetClassHints( "HighQFilterDlg", "HighQFilterDlg" );
        SetMWMHints( kMWMDecorAll | kMWMDecorResizeH | kMWMDecorMaximize |
                         kMWMDecorMinimize | kMWMDecorMenu,
                     kMWMFuncAll | kMWMFuncResize | kMWMFuncMaximize |
                         kMWMFuncMinimize,
                     kMWMInputModeless );

        MapWindow( );
        fClient->WaitFor( this );
    }

    //______________________________________________________________________________
    TLGNotchDialog::~TLGNotchDialog( )
    {
        delete fFreq;
        delete fQ;
        delete fDepth;
        delete fHarmonics;
        delete fButton[ 0 ];
        delete fButton[ 1 ];
        delete fFButton;
        for ( int i = 0; i < 6; ++i )
            delete fLabel[ i ];
        for ( int i = 0; i < 1; ++i )
            delete fF[ i ];
        for ( int i = 0; i < 14; ++i )
            delete fL[ i ];
    }

    //______________________________________________________________________________
    void
    TLGNotchDialog::CloseWindow( )
    {
        if ( fRet )
            *fRet = "";
        DeleteWindow( );
    }

    //______________________________________________________________________________
    Bool_t
    TLGNotchDialog::Setup( const char* cmd )
    {
        if ( !cmd || !*cmd )
        {
            return kTRUE;
        }
        FilterDlgParser parser( 0, 0, this );
        return parser.filter( cmd );
    }

    //______________________________________________________________________________
    Bool_t
    TLGNotchDialog::ProcessMessage( Long_t msg, Long_t parm1, Long_t parm2 )
    {
        // Buttons
        if ( ( GET_MSG( msg ) == kC_COMMAND ) &&
             ( GET_SUBMSG( msg ) == kCM_BUTTON ) )
        {
            switch ( parm1 )
            {
            // ok
            case kOkId: {
                double f = fFreq->GetNumber( );
                double q = fQ->GetNumber( );
                double a = fDepth->GetNumber( );
                int    n = fHarmonics ? fHarmonics->GetIntNumber( ) : 0;
                char   buf[ 1024 ];
                switch ( fType )
                {
                case kNotch: {
                    sprintf( buf, "notch(%g,%g,%g)", f, q, a );
                    break;
                }
                case kResGain: {
                    sprintf( buf, "resgain(%g,%g,%g)", f, q, a );
                    break;
                }
                case kComb: {
                    sprintf( buf, "comb(%g,%g,%g,%i)", f, q, a, n );
                    break;
                }
                }
                if ( fRet )
                    *fRet = buf;
                DeleteWindow( );
                break;
            }
            // cancel
            case kCancelId: {
                if ( fRet )
                    *fRet = "";
                DeleteWindow( );
                break;
            }
            }
        }
        return kTRUE;
    }

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // TLGEllipDialog			                                //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    TLGEllipDialog::TLGEllipDialog( const TGWindow* p,
                                    const TGWindow* main,
                                    TString&        result,
                                    subtype_t       subt )
        : TLGTransientFrame( p, main, 10, 10 ), fRet( &result ), fType( subt )
    {
        for ( int i = 0; i < 20; ++i )
            fL[ i ] = 0;
        fF = 0;
        for ( int i = 0; i < 10; ++i )
            fLabel[ i ] = 0;
        fFilterType = 0;
        fOrder = 0;
        fFreq1 = 0;
        fFreq2 = 0;
        fRipple = 0;
        fAtten = 0;
        fFButton = 0;
        fButton[ 0 ] = 0;
        fButton[ 1 ] = 0;

        // layout hints
        fL[ 0 ] = new TGLayoutHints(
            kLHintsLeft | kLHintsExpandX | kLHintsTop, 2, 2, 2, 2 );
        fL[ 1 ] = new TGLayoutHints( kLHintsRight | kLHintsTop, 6, 6, 12, 4 );
        // 1st line
        fL[ 2 ] = new TGTableLayoutHints(
            0, 1, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        fL[ 3 ] =
            new TGTableLayoutHints( 1,
                                    2,
                                    0,
                                    1,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    4,
                                    0 );
        fL[ 4 ] = new TGTableLayoutHints(
            2, 3, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        fL[ 5 ] = new TGTableLayoutHints(
            3, 4, 0, 1, kLHintsLeft | kLHintsCenterY, 22, 2, 4, 0 );
        fL[ 6 ] =
            new TGTableLayoutHints( 4,
                                    5,
                                    0,
                                    1,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    4,
                                    0 );
        fL[ 7 ] = new TGTableLayoutHints(
            5, 6, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        // 2nd line
        fL[ 8 ] = new TGTableLayoutHints(
            0, 1, 1, 2, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        fL[ 9 ] = new TGTableLayoutHints(
            1, 2, 1, 2, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        fL[ 10 ] = new TGTableLayoutHints(
            2, 3, 1, 2, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        fL[ 11 ] = new TGTableLayoutHints(
            3, 4, 1, 2, kLHintsLeft | kLHintsCenterY, 22, 2, 4, 0 );
        fL[ 12 ] =
            new TGTableLayoutHints( 4,
                                    5,
                                    1,
                                    2,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    4,
                                    0 );
        fL[ 13 ] = new TGTableLayoutHints(
            5, 6, 1, 2, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        // 3rd line
        fL[ 14 ] = new TGTableLayoutHints(
            0, 1, 2, 3, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        fL[ 15 ] = new TGTableLayoutHints(
            1, 2, 2, 3, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        fL[ 16 ] = new TGTableLayoutHints(
            2, 3, 2, 3, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );
        fL[ 17 ] = new TGTableLayoutHints(
            3, 4, 2, 3, kLHintsLeft | kLHintsCenterY, 22, 2, 4, 0 );
        fL[ 18 ] =
            new TGTableLayoutHints( 4,
                                    5,
                                    2,
                                    3,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    4,
                                    0 );
        fL[ 19 ] = new TGTableLayoutHints(
            5, 6, 2, 3, kLHintsLeft | kLHintsCenterY, 2, 2, 4, 0 );

        // group frame
        fF = new TGGroupFrame( this, "Parameters" );
        AddFrame( fF, fL[ 0 ] );
        fF->SetLayoutManager(
            new TGTableLayout( fF, subt == kButter ? 3 : 4, 6 ) );
        // labels
        fLabel[ 0 ] = new TGLabel( fF, "Type:   " );
        fF->AddFrame( fLabel[ 0 ], fL[ 2 ] );
        fLabel[ 1 ] = new TGLabel( fF, "Order:   " );
        fF->AddFrame( fLabel[ 1 ], fL[ 5 ] );
        fLabel[ 2 ] = new TGLabel( fF, "First Frequency:   " );
        fF->AddFrame( fLabel[ 2 ], fL[ 8 ] );
        fLabel[ 3 ] = new TGLabel( fF, "Second Frequency:   " );
        fF->AddFrame( fLabel[ 3 ], fL[ 11 ] );
        fLabel[ 6 ] = new TGLabel( fF, " Hz" );
        fF->AddFrame( fLabel[ 6 ], fL[ 10 ] );
        fLabel[ 7 ] = new TGLabel( fF, " Hz" );
        fF->AddFrame( fLabel[ 7 ], fL[ 13 ] );
        if ( ( fType == kCheby1 ) || ( fType == kEllip ) )
        {
            fLabel[ 4 ] = new TGLabel( fF, "Passband Ripple:   " );
            fF->AddFrame( fLabel[ 4 ], fL[ 14 ] );
            fLabel[ 8 ] = new TGLabel( fF, " dB" );
            fF->AddFrame( fLabel[ 8 ], fL[ 16 ] );
        }
        else
        {
            fLabel[ 4 ] = 0;
            fLabel[ 8 ] = 0;
        }
        if ( ( fType == kCheby2 ) || ( fType == kEllip ) )
        {
            fLabel[ 5 ] = new TGLabel( fF, "Attenuation:   " );
            fF->AddFrame( fLabel[ 5 ], fL[ 17 ] );
            fLabel[ 9 ] = new TGLabel( fF, " dB" );
            fF->AddFrame( fLabel[ 9 ], fL[ 19 ] );
        }
        else
        {
            fLabel[ 5 ] = 0;
            fLabel[ 9 ] = 0;
        }
        // parameters
        fFilterType = new TGComboBox( fF, 10 );
        fFilterType->Associate( this );
        fFilterType->AddEntry( "Low Pass", kLowPass );
        fFilterType->AddEntry( "High Pass", kHighPass );
        fFilterType->AddEntry( "Band Pass", kBandPass );
        fFilterType->AddEntry( "Band Stop", kBandStop );
        fFilterType->Resize( 10, 24 );
        fFilterType->Select( kLowPass );
        fF->AddFrame( fFilterType, fL[ 3 ] );
        fOrder = new TLGNumericControlBox(
            fF, 4, 10, 11, kNESInteger, kNEAPositive );
        fOrder->Associate( this );
        fF->AddFrame( fOrder, fL[ 6 ] );
        fFreq1 = new TLGNumericControlBox(
            fF, 1.0, 10, 12, kNESReal, kNEANonNegative );
        fFreq1->Associate( this );
        fF->AddFrame( fFreq1, fL[ 9 ] );
        fFreq2 = new TLGNumericControlBox(
            fF, 2.0, 10, 13, kNESReal, kNEANonNegative );
        fFreq2->Associate( this );
        fFreq2->SetState( kFALSE );
        fF->AddFrame( fFreq2, fL[ 12 ] );
        if ( ( fType == kCheby1 ) || ( fType == kEllip ) )
        {
            fRipple = new TLGNumericControlBox(
                fF, 1.0, 10, 14, kNESReal, kNEANonNegative );
            fRipple->Associate( this );
            fF->AddFrame( fRipple, fL[ 15 ] );
        }
        else
        {
            fRipple = 0;
        }
        if ( ( fType == kCheby2 ) || ( fType == kEllip ) )
        {
            fAtten = new TLGNumericControlBox(
                fF, 40, 10, 15, kNESReal, kNEANonNegative );
            fAtten->Associate( this );
            fF->AddFrame( fAtten, fL[ 18 ] );
        }
        else
        {
            fAtten = 0;
        }
        fF->Resize( fF->GetDefaultWidth( ), fF->GetDefaultHeight( ) );

        // buttons
        fFButton = new TGHorizontalFrame( this, 10, 10 );
        AddFrame( fFButton, fL[ 0 ] );
        fButton[ 1 ] = new TGTextButton(
            fFButton, new TGHotString( "     &Cancel     " ), kCancelId );
        fButton[ 1 ]->Associate( this );
        fFButton->AddFrame( fButton[ 1 ], fL[ 1 ] );
        fButton[ 0 ] = new TGTextButton(
            fFButton, new TGHotString( "        &Ok        " ), kOkId );
        fButton[ 0 ]->Associate( this );
        fFButton->AddFrame( fButton[ 0 ], fL[ 1 ] );
        // setup
        Setup( result );

        // resize & move to center
        MapSubwindows( );
        UInt_t width = GetDefaultWidth( );
        UInt_t height = GetDefaultHeight( );
        Resize( width, height );
        Int_t ax;
        Int_t ay;
        if ( main )
        {
            Window_t wdum;
            cerr << "TLGEllipDialog - TranslateCoordinates, line " << __LINE__
                 << endl;
            gVirtualX->TranslateCoordinates(
                main->GetId( ),
                GetParent( )->GetId( ),
                ( ( (TGFrame*)main )->GetWidth( ) - fWidth ) >> 1,
                ( ( (TGFrame*)main )->GetHeight( ) - fHeight ) >> 1,
                ax,
                ay,
                wdum );
        }
        else
        {
            UInt_t root_w, root_h;
            cerr << "TLGEllipDialog - GetWindowSize, line " << __LINE__ << endl;
            gVirtualX->GetWindowSize(
                fClient->GetRoot( )->GetId( ), ax, ay, root_w, root_h );
            ax = ( root_w - fWidth ) >> 1;
            ay = ( root_h - fHeight ) >> 1;
        }
        Move( ax, ay );
        SetWMPosition( ax, ay );

        // make the message box non-resizable
        SetWMSize( width, height );
        SetWMSizeHints( width, height, width, height, 0, 0 );

        // set dialog box title
        switch ( fType )
        {
        case kEllip: {
            SetWindowName( "Elliptic Filter" );
            SetIconName( "Elliptic" );
            break;
        }
        case kButter: {
            SetWindowName( "Butterworth Filter" );
            SetIconName( "Butterworth" );
            break;
        }
        case kCheby1: {
            SetWindowName( "Chebyshev Type 1" );
            SetIconName( "Cheby1" );
            break;
        }
        case kCheby2: {
            SetWindowName( "Chebyshev Type 2" );
            SetIconName( "Cheby2" );
            break;
        }
        }
        SetClassHints( "StdFilterDlg", "StdFilterDlg" );
        SetMWMHints( kMWMDecorAll | kMWMDecorResizeH | kMWMDecorMaximize |
                         kMWMDecorMinimize | kMWMDecorMenu,
                     kMWMFuncAll | kMWMFuncResize | kMWMFuncMaximize |
                         kMWMFuncMinimize,
                     kMWMInputModeless );

        MapWindow( );
        fClient->WaitFor( this );
    }

    //______________________________________________________________________________
    TLGEllipDialog::~TLGEllipDialog( )
    {
        if ( fFilterType )
            delete fFilterType;
        if ( fFreq1 )
            delete fFreq1;
        if ( fFreq2 )
            delete fFreq2;
        if ( fRipple )
            delete fRipple;
        if ( fAtten )
            delete fAtten;
        if ( fButton[ 0 ] )
            delete fButton[ 0 ];
        if ( fButton[ 1 ] )
            delete fButton[ 1 ];
        if ( fFButton )
            delete fFButton;
        for ( int i = 0; i < 10; ++i )
            if ( fLabel[ i ] )
                delete fLabel[ i ];
        if ( fF )
            delete fF;
        for ( int i = 0; i < 20; ++i )
            if ( fL[ i ] )
                delete fL[ i ];
    }

    //______________________________________________________________________________
    void
    TLGEllipDialog::CloseWindow( )
    {
        if ( fRet )
            *fRet = "";
        DeleteWindow( );
    }

    //______________________________________________________________________________
    Bool_t
    TLGEllipDialog::Setup( const char* cmd )
    {
        if ( !cmd || !*cmd )
        {
            return kTRUE;
        }
        FilterDlgParser parser( 0, 0, 0, this );
        return parser.filter( cmd );
    }

    //______________________________________________________________________________
    Bool_t
    TLGEllipDialog::ProcessMessage( Long_t msg, Long_t parm1, Long_t parm2 )
    {
        // Buttons
        if ( ( GET_MSG( msg ) == kC_COMMAND ) &&
             ( GET_SUBMSG( msg ) == kCM_BUTTON ) )
        {
            switch ( parm1 )
            {
            // ok
            case kOkId: {
                Filter_Type ftype = (Filter_Type)fFilterType->GetSelected( );
                int         n = fOrder->GetIntNumber( );
                double      f1 = fFreq1->GetNumber( );
                double      f2 = fFreq2->GetNumber( );
                double      rp = fRipple ? fRipple->GetNumber( ) : 0;
                double      as = fAtten ? fAtten->GetNumber( ) : 0;
                char        typen[ 64 ];
                char        buf[ 1024 ];
                switch ( ftype )
                {
                case kLowPass: {
                    strcpy( typen, "LowPass" );
                    f2 = 0;
                    break;
                }
                case kHighPass: {
                    strcpy( typen, "HighPass" );
                    f2 = 0;
                    break;
                }
                case kBandPass: {
                    strcpy( typen, "BandPass" );
                    break;
                }
                case kBandStop: {
                    strcpy( typen, "BandStop" );
                    break;
                }
                }
                switch ( fType )
                {
                case kButter: {
                    if ( ( ftype == kLowPass ) || ( ftype == kHighPass ) )
                    {
                        sprintf( buf, "butter(\"%s\",%i,%g)", typen, n, f1 );
                    }
                    else
                    {
                        sprintf(
                            buf, "butter(\"%s\",%i,%g,%g)", typen, n, f1, f2 );
                    }
                    break;
                }
                case kCheby1: {
                    if ( ( ftype == kLowPass ) || ( ftype == kHighPass ) )
                    {
                        sprintf(
                            buf, "cheby1(\"%s\",%i,%g,%g)", typen, n, rp, f1 );
                    }
                    else
                    {
                        sprintf( buf,
                                 "cheby1(\"%s\",%i,%g,%g,%g)",
                                 typen,
                                 n,
                                 rp,
                                 f1,
                                 f2 );
                    }
                    break;
                }
                case kCheby2: {
                    if ( ( ftype == kLowPass ) || ( ftype == kHighPass ) )
                    {
                        sprintf(
                            buf, "cheby2(\"%s\",%i,%g,%g)", typen, n, as, f1 );
                    }
                    else
                    {
                        sprintf( buf,
                                 "cheby2(\"%s\",%i,%g,%g,%g)",
                                 typen,
                                 n,
                                 as,
                                 f1,
                                 f2 );
                    }
                    break;
                }
                case kEllip: {
                    if ( ( ftype == kLowPass ) || ( ftype == kHighPass ) )
                    {
                        sprintf( buf,
                                 "ellip(\"%s\",%i,%g,%g,%g)",
                                 typen,
                                 n,
                                 rp,
                                 as,
                                 f1 );
                    }
                    else
                    {
                        sprintf( buf,
                                 "ellip(\"%s\",%i,%g,%g,%g,%g)",
                                 typen,
                                 n,
                                 rp,
                                 as,
                                 f1,
                                 f2 );
                    }
                    break;
                }
                }
                cerr << "TLGEllipDialog::ProcessMessage() " << buf << endl;
                if ( fRet )
                    *fRet = buf;
                DeleteWindow( );
                cerr << "  Window deleted, line " << __LINE__ << " File "
                     << __FILE__ << endl;
                break;
            }
            // cancel
            case kCancelId: {
                CloseWindow( );
                // if (fRet) *fRet = "";
                // DeleteWindow();
                break;
            }
            }
        }

        // Combobox
        else if ( ( GET_MSG( msg ) == kC_COMMAND ) &&
                  ( GET_SUBMSG( msg ) == kCM_COMBOBOX ) )
        {
            switch ( parm1 )
            {
            // filter type
            case 10: {
                if ( ( parm2 == kLowPass ) || ( parm2 == kHighPass ) )
                {
                    fFreq2->SetState( kFALSE );
                }
                else
                {
                    fFreq2->SetState( kTRUE );
                }
                break;
            }
            }
        }
        cerr << "TLGEllipDialog::ProcessMessage() return TRUE" << endl;
        return kTRUE;
    }

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // TLGSosDialog //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    TLGSosDialog::TLGSosDialog( const TGWindow* p,
                                const TGWindow* main,
                                double          sample,
                                TString&        result )
        : TLGTransientFrame( p, main, 10, 10 ), fSample( sample ), fId( 0 ),
          fRet( &result )
    {
        // layout hints
        fL[ 0 ] = new TGLayoutHints(
            kLHintsLeft | kLHintsExpandX | kLHintsTop, 2, 2, 2, 2 );
        fL[ 1 ] = new TGLayoutHints( kLHintsRight | kLHintsTop, 6, 6, 12, 4 );
        fL[ 13 ] =
            new TGLayoutHints( kLHintsLeft | kLHintsCenterY, 2, 2, 8, 0 );
        // 1st line
        fL[ 2 ] = new TGTableLayoutHints(
            0, 1, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 12, 0 );
        fL[ 3 ] =
            new TGTableLayoutHints( 1,
                                    2,
                                    0,
                                    1,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    12,
                                    0 );
        fL[ 4 ] = new TGTableLayoutHints(
            2, 3, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 12, 0 );
        fL[ 5 ] = new TGTableLayoutHints(
            3, 4, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 12, 0 );
        fL[ 6 ] =
            new TGTableLayoutHints( 4,
                                    5,
                                    0,
                                    1,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    12,
                                    0 );
        fL[ 7 ] = new TGTableLayoutHints(
            5, 6, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 12, 0 );
        fL[ 8 ] =
            new TGTableLayoutHints( 6,
                                    7,
                                    0,
                                    1,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    12,
                                    0 );
        fL[ 9 ] = new TGTableLayoutHints(
            7, 8, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 12, 0 );
        // list box
        fL[ 10 ] =
            new TGTableLayoutHints( 0,
                                    8,
                                    1,
                                    4,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    4,
                                    0 );
        // add & remove buttons
        fL[ 11 ] =
            new TGTableLayoutHints( 8,
                                    9,
                                    1,
                                    2,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    12,
                                    2,
                                    4,
                                    0 );
        fL[ 12 ] =
            new TGTableLayoutHints( 8,
                                    9,
                                    2,
                                    3,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    12,
                                    2,
                                    4,
                                    0 );
        fL[ 13 ] =
            new TGTableLayoutHints( 8,
                                    9,
                                    3,
                                    4,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    12,
                                    2,
                                    4,
                                    0 );

        // gain
        fF[ 1 ] = new TGGroupFrame( this, "Gain" );
        AddFrame( fF[ 1 ], fL[ 0 ] );
        fF[ 2 ] = new TGHorizontalFrame( fF[ 1 ], 10, 10 );
        fF[ 1 ]->AddFrame( fF[ 2 ], fL[ 0 ] );
        fLabel[ 0 ] = new TGLabel( fF[ 2 ], "Value:   " );
        fF[ 2 ]->AddFrame( fLabel[ 0 ], fL[ 13 ] );
        fGain = new TLGNumericControlBox( fF[ 2 ], 1.0, 10, 10, kNESReal );
        fGain->Associate( this );
        fF[ 2 ]->AddFrame( fGain, fL[ 13 ] );
        // group frame of SOS
        fF[ 0 ] = new TGGroupFrame( this, "Sections" );
        AddFrame( fF[ 0 ], fL[ 1 ] );
        fF[ 0 ]->SetLayoutManager( new TGTableLayout( fF[ 0 ], 4, 9 ) );
        // parameters
        for ( int i = 0; i < 4; ++i )
        {
            const char* const names[] = { "b1:", " b2:", " a1:", " a2:" };
            fLabel[ 1 + i ] = new TGLabel( fF[ 0 ], names[ i ] );
            fF[ 0 ]->AddFrame( fLabel[ 1 + i ], fL[ 2 + 2 * i ] );
            fCoeff[ i ] =
                new TLGNumericControlBox( fF[ 0 ], 0.0, 10, 11 + i, kNESReal );
            fCoeff[ i ]->Associate( this );
            fF[ 0 ]->AddFrame( fCoeff[ i ], fL[ 2 + 2 * i + 1 ] );
        }
        fCoeffSel = new TGListBox( fF[ 0 ], 15 );
        fCoeffSel->Associate( this );
        fCoeffSel->Resize( 10, 200 );
        fF[ 0 ]->AddFrame( fCoeffSel, fL[ 10 ] );
        fCoeffBtn[ 0 ] = new TGTextButton(
            fF[ 0 ], new TGHotString( "     &Add     " ), 16 );
        fCoeffBtn[ 0 ]->Associate( this );
        fF[ 0 ]->AddFrame( fCoeffBtn[ 0 ], fL[ 11 ] );
        fCoeffBtn[ 1 ] = new TGTextButton(
            fF[ 0 ], new TGHotString( "    &Remove    " ), 17 );
        fCoeffBtn[ 1 ]->Associate( this );
        fF[ 0 ]->AddFrame( fCoeffBtn[ 1 ], fL[ 12 ] );
        fCoeffBtn[ 2 ] = new TGTextButton(
            fF[ 0 ], new TGHotString( "    C&lear    " ), 18 );
        fCoeffBtn[ 2 ]->Associate( this );
        fF[ 0 ]->AddFrame( fCoeffBtn[ 2 ], fL[ 13 ] );
        fF[ 0 ]->Resize( fF[ 0 ]->GetDefaultWidth( ),
                         fF[ 0 ]->GetDefaultHeight( ) );

        // buttons
        fFButton = new TGHorizontalFrame( this, 10, 10 );
        AddFrame( fFButton, fL[ 0 ] );
        fButton[ 1 ] = new TGTextButton(
            fFButton, new TGHotString( "     &Cancel     " ), kCancelId );
        fButton[ 1 ]->Associate( this );
        fFButton->AddFrame( fButton[ 1 ], fL[ 1 ] );
        fButton[ 0 ] = new TGTextButton(
            fFButton, new TGHotString( "        &Ok        " ), kOkId );
        fButton[ 0 ]->Associate( this );
        fFButton->AddFrame( fButton[ 0 ], fL[ 1 ] );
        // setup
        Setup( result );

        // resize & move to center
        MapSubwindows( );
        UInt_t width = GetDefaultWidth( );
        UInt_t height = GetDefaultHeight( );
        Resize( width, height );
        Int_t ax;
        Int_t ay;
        if ( main )
        {
            Window_t wdum;
            gVirtualX->TranslateCoordinates(
                main->GetId( ),
                GetParent( )->GetId( ),
                ( ( (TGFrame*)main )->GetWidth( ) - fWidth ) >> 1,
                ( ( (TGFrame*)main )->GetHeight( ) - fHeight ) >> 1,
                ax,
                ay,
                wdum );
        }
        else
        {
            UInt_t root_w, root_h;
            gVirtualX->GetWindowSize(
                fClient->GetRoot( )->GetId( ), ax, ay, root_w, root_h );
            ax = ( root_w - fWidth ) >> 1;
            ay = ( root_h - fHeight ) >> 1;
        }
        Move( ax, ay );
        SetWMPosition( ax, ay );

        // make the message box non-resizable
        SetWMSize( width, height );
        SetWMSizeHints( width, height, width, height, 0, 0 );

        // set dialog box title
        SetWindowName( "Second Order Sections" );
        SetIconName( "SOS" );
        SetClassHints( "SosFilterDlg", "SosFilterDlg" );
        SetMWMHints( kMWMDecorAll | kMWMDecorResizeH | kMWMDecorMaximize |
                         kMWMDecorMinimize | kMWMDecorMenu,
                     kMWMFuncAll | kMWMFuncResize | kMWMFuncMaximize |
                         kMWMFuncMinimize,
                     kMWMInputModeless );

        MapWindow( );
        fClient->WaitFor( this );
    }

    //______________________________________________________________________________
    TLGSosDialog::~TLGSosDialog( )
    {
        delete fGain;
        for ( int i = 0; i < 4; ++i )
            delete fCoeff[ i ];
        for ( int i = 0; i < 3; ++i )
            delete fCoeffBtn[ i ];
        delete fCoeffSel;
        delete fButton[ 0 ];
        delete fButton[ 1 ];
        delete fFButton;
        for ( int i = 0; i < 5; ++i )
            delete fLabel[ i ];
        for ( int i = 0; i < 3; ++i )
            delete fF[ i ];
        for ( int i = 0; i < 14; ++i )
            delete fL[ i ];
    }

    //______________________________________________________________________________
    void
    TLGSosDialog::CloseWindow( )
    {
        if ( fRet )
            *fRet = "";
        DeleteWindow( );
    }

    //______________________________________________________________________________
    Bool_t
    TLGSosDialog::Setup( const char* cmd )
    {
        if ( !cmd || !*cmd )
        {
            return kTRUE;
        }
        FilterDlgParser parser( 0, 0, 0, 0, this );
        if ( parser.filter( cmd ) )
        {
            return kTRUE;
        }

        // On failure, make sure to clear the edit box again.
        fCoeffSel->RemoveEntries( 0, fId + 1 );

        FilterDesign ds( fSample );
        string       z;
        if ( !ds.filter( cmd ) || !iir2z( ds.get( ), z, "s" ) )
        {
            return kFALSE;
        }
        return parser.filter( z.c_str( ) );
    }

    //______________________________________________________________________________
    Bool_t
    TLGSosDialog::ProcessMessage( Long_t msg, Long_t parm1, Long_t parm2 )
    {
        // Buttons
        if ( ( GET_MSG( msg ) == kC_COMMAND ) &&
             ( GET_SUBMSG( msg ) == kCM_BUTTON ) )
        {
            switch ( parm1 )
            {
            // ok
            case kOkId: {
                double g = fGain->GetNumber( );
                string res = "sos(";
                char   buf[ 1024 ];
                sprintf( buf, "%g", g );
                res += buf + string( ",[" );
                TGTextLBEntry* entry;
                int            order = 0;
                for ( int i = 0; i <= fId; ++i )
                {
                    entry = (TGTextLBEntry*)fCoeffSel->Select( i );
                    if ( !entry )
                    {
                        continue;
                    }
                    TString s = entry->GetText( )->GetString( );
                    double  b1 = 0;
                    double  b2 = 0;
                    double  a1 = 0;
                    double  a2 = 0;
                    sscanf(
                        (const char*)s, "%lf%lf%lf%lf", &b1, &b2, &a1, &a2 );
                    sprintf( buf, "%g;%g;%g;%g", b1, b2, a1, a2 );
                    if ( order > 0 )
                        res += ";";
                    res += buf;
                    ++order;
                }
                res += string( "])" );
                if ( fRet )
                    *fRet = res.c_str( );
                DeleteWindow( );
                break;
            }
            // cancel
            case kCancelId: {
                if ( fRet )
                    *fRet = "";
                DeleteWindow( );
                break;
            }
            // add
            case 16: {
                double b1 = fCoeff[ 0 ]->GetNumber( );
                double b2 = fCoeff[ 1 ]->GetNumber( );
                double a1 = fCoeff[ 2 ]->GetNumber( );
                double a2 = fCoeff[ 3 ]->GetNumber( );
                char   buf[ 1024 ];
                sprintf( buf, "%g %g %g %g", b1, b2, a1, a2 );
                fCoeffSel->AddEntry( buf, ++fId );
                fCoeffSel->MapSubwindows( );
                fCoeffSel->Layout( );
                break;
            }
            // remove
            case 17: {
                fCoeffSel->RemoveEntry( fCoeffSel->GetSelected( ) );
                fCoeffSel->MapSubwindows( );
                fCoeffSel->Layout( );
                break;
            }
            // clear
            case 18: {
                fCoeffSel->RemoveEntries( 0, fId + 1 );
                fCoeffSel->MapSubwindows( );
                fCoeffSel->Layout( );
                break;
            }
            }
        }

        // List box
        else if ( ( GET_MSG( msg ) == kC_COMMAND ) &&
                  ( GET_SUBMSG( msg ) == kCM_LISTBOX ) )
        {
            switch ( parm1 )
            {
            // coefficient list
            case 15: {
                TGLBEntry* entry = fCoeffSel->GetSelectedEntry( );
                if ( entry )
                {
                    TString s =
                        ( (TGTextLBEntry*)entry )->GetText( )->GetString( );
                    double b1 = 0;
                    double b2 = 0;
                    double a1 = 0;
                    double a2 = 0;
                    sscanf(
                        (const char*)s, "%lf%lf%lf%lf", &b1, &b2, &a1, &a2 );
                    fCoeff[ 0 ]->SetNumber( b1 );
                    fCoeff[ 1 ]->SetNumber( b2 );
                    fCoeff[ 2 ]->SetNumber( a1 );
                    fCoeff[ 3 ]->SetNumber( a2 );
                }
                break;
            }
            }
        }
        return kTRUE;
    }

    // JCB start
    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // TLGImportDialog                                                      //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    //   Int_t kImportDlg[] = {   440, 250,             // total width & height
    //   15, 25, 70, 24,       // Path label
    //   15, 185, 70, 24,      // File label
    //   85, 25, 300, 24,      // Path
    //   395, 25, 24, 24,      // Directory up button
    //   85, 55, 335, 121,     // Directory list
    //   85, 185, 335, 24,     // File
    //   147, 215, 65, 24 ,   // OK button
    //   228, 215, 65, 24,    // Cancel button
    //   0 } ;

    TLGImportDialog::TLGImportDialog( const TGWindow* p,
                                      const TGWindow* main,
                                      TString&        result,
                                      const char*     path )
        : TLGTransientFrame( p, main, 10, 10 ), fRet( &result )
    {
        // layout hints
        fL[ 0 ] = new TGTableLayoutHints( 0,
                                          1,
                                          0,
                                          1,
                                          kLHintsLeft | kLHintsCenterY,
                                          2,
                                          2,
                                          20,
                                          2 ); // Path: label
        fL[ 1 ] =
            new TGTableLayoutHints( 1,
                                    2,
                                    0,
                                    1,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    20,
                                    2 ); // Path combo box
        fL[ 2 ] = new TGTableLayoutHints( 2,
                                          3,
                                          0,
                                          1,
                                          kLHintsLeft | kLHintsCenterY,
                                          2,
                                          2,
                                          20,
                                          2 ); // cd up button
        fL[ 3 ] =
            new TGTableLayoutHints( 1,
                                    3,
                                    1,
                                    2,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    2,
                                    0 ); // List Box
        fL[ 4 ] = new TGTableLayoutHints( 0,
                                          1,
                                          2,
                                          3,
                                          kLHintsLeft | kLHintsCenterY,
                                          2,
                                          2,
                                          2,
                                          2 ); // File: label
        fL[ 5 ] =
            new TGTableLayoutHints( 0,
                                    3,
                                    3,
                                    4,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    2,
                                    2 ); // T ext Box (preview)
        fL[ 6 ] =
            new TGTableLayoutHints( 1,
                                    3,
                                    2,
                                    3,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    2,
                                    2 ); // F ile combo box
        fL[ 7 ] = new TGLayoutHints( kLHintsLeft | kLHintsTop | kLHintsCenterX,
                                     2,
                                     2,
                                     2,
                                     2 ); // OK, Cancel button frame.
        fL[ 8 ] = new TGLayoutHints( kLHintsLeft | kLHintsTop | kLHintsExpandX,
                                     2,
                                     2,
                                     2,
                                     2 ); // Group frame layout

        fG = new TGGroupFrame( this, "Import Design String" );
        AddFrame( fG, fL[ 8 ] );
        fG->SetLayoutManager(
            new TGTableLayout( fG, 4, 3 ) ); // Table layout, 4 rows, 3 columns.

        // Path: label
        fLabel[ 0 ] = new TGLabel( fG, "Path:" );
        fLabel[ 0 ]->SetTextJustify( kTextLeft | kTextCenterY );
        fG->AddFrame( fLabel[ 0 ], fL[ 0 ] );
        fLabel[ 0 ]->MoveResize( 15, 25, 70, 24 );

        // Path combo box
        fPath = new TGFSComboBox( fG, kPathId );
        fPath->Associate( this ); // Indicate that events should go to the
                                  // Import Dialog class.
        fG->AddFrame( fPath, fL[ 1 ] );
        fPath->MoveResize( 85, 25, 300, 24 );

        // Change Directory Up button.
        fPcdup = fClient->GetPicture( "tb_uplevel.xpm" );
        if ( !fPcdup )
        {
            Error( "TLGFilterWizWindow", "missing toolbar pixmap(s).n" );
        }
        fCdup = new TGPictureButton( fG, fPcdup, kDirUpId );
        fCdup->SetToolTipText( "Up One Level" );
        fCdup->Associate( this ); // Indicate events should go to Import Dialog.
        fG->AddFrame( fCdup, fL[ 2 ] );
        fCdup->MoveResize( 395, 25, 24, 24 );

        // List box for directory contents.
        fFv = new TGListView( fG, 400, 161 );
        fFc = new TGFileContainer(
            fFv->GetViewPort( ), 10, 10, kHorizontalFrame, fgWhitePixel );
        fFc->Associate( this );
        fFv->GetViewPort( )->SetBackgroundColor( fgWhitePixel );
        fFv->SetContainer( fFc );
        fFv->SetViewMode( kLVList );
        fFc->SetFilter( "YOUneverHAVEaFILElikeTHIS!!!" );
        fFc->Sort( kSortByType );
        if ( !path )
        {
            path = ".";
        }
        fFc->ChangeDirectory( path );
        fPath->Update( fFc->GetDirectory( ) );
        fG->AddFrame( fFv, fL[ 3 ] );
        fFv->MoveResize( 85, 55, 335, 121 );

        // File: label
        fLabel[ 1 ] = new TGLabel( fG, "File:" );
        fLabel[ 1 ]->SetTextJustify( kTextLeft | kTextCenterY );
        fG->AddFrame( fLabel[ 1 ], fL[ 4 ] );
        fLabel[ 1 ]->MoveResize( 15, 185, 70, 24 );

        // File combo box.
        fFile = new TGComboBox( fG, kFileId );
        fFile->Associate( this );
        fG->AddFrame( fFile, fL[ 6 ] );
        // Populate the file box with files from the current directory.
        AddFiles( fFc->GetDirectory( ) );
        fFile->MoveResize( 85, 185, 335, 24 );

        // Text view for file preview.
        fText = new TGTextView( fG, 400, 24 );
        fText->Associate( this );
        fG->AddFrame( fText, fL[ 5 ] );
        fText->MoveResize( 85, 215, 400, 36 );
        fText->Clear( );

        // Horizontal frame to hold the OK and Cancel buttons
        fFButton = new TGHorizontalFrame( this, 10, 10 );
        AddFrame( fFButton, fL[ 7 ] );

        // OK button
        fOk = new TGTextButton( fFButton, "OK", kOkId );
        fOk->Associate( this );
        fFButton->AddFrame( fOk, 0 );
        fOk->MoveResize( 147, 245, 65, 24 );
        fOk->SetState( kButtonDisabled ); // Disable the OK button since
                                          // nothing's been read yet.

        // Cancel button
        fCancel = new TGTextButton( fFButton, "Cancel", kCancelId );
        fCancel->Associate( this );
        fFButton->AddFrame( fCancel, 0 );
        fCancel->MoveResize( 228, 245, 65, 24 );

        fG->Resize( fG->GetDefaultWidth( ), fG->GetDefaultHeight( ) );

        MapSubwindows( );
        UInt_t width = GetDefaultWidth( );
        UInt_t height = GetDefaultHeight( );
        Resize( width, height );

        // make the message box non-resizable
        SetWMSize( width, height );
        SetWMSizeHints( width, height, width, height, 0, 0 );

        // set dialog box title
        SetWindowName( "Import Matlab Filter" );
        SetIconName( "Import" );
        SetClassHints( "ImportDlg`", "ImportDlg`" );
        SetMWMHints( kMWMDecorAll | kMWMDecorResizeH | kMWMDecorMaximize |
                         kMWMDecorMinimize | kMWMDecorMenu,
                     kMWMFuncAll | kMWMFuncResize | kMWMFuncMaximize |
                         kMWMFuncMinimize,
                     kMWMInputModeless );

        MapWindow( );
        fClient->WaitFor( this );
    }

    //_____________________________________________________________________________
    TLGImportDialog::~TLGImportDialog( )
    {
        for ( int i = 0; i < 9; i++ )
            delete fL[ i ];
        delete fLabel[ 0 ];
        delete fLabel[ 1 ];
        delete fPath;
        delete fCdup;
        delete fFv;
        delete fFc;
        delete fFile;
        delete fOk;
        delete fCancel;
        delete fText;
        delete fG;
    }

    //_____________________________________________________________________________
    void
    TLGImportDialog::CloseWindow( )
    {
        DeleteWindow( );
    }

    //_____________________________________________________________________________
    Bool_t
    TLGImportDialog::ProcessMessage( Long_t msg, Long_t parm1, Long_t parm2 )
    {
        // Buttons
        if ( ( GET_MSG( msg ) == kC_COMMAND ) &&
             ( GET_SUBMSG( msg ) == kCM_BUTTON ) )
        {
            switch ( parm1 )
            {
            // ok
            case kOkId: {
                // Process the file.
                // Open the file, read the filter section, and return.  Just
                // return the raw text, put off parsing it until later.

                // Delete the window and return.
                DeleteWindow( );
                break;
            }
            // cancel
            case kCancelId: {
                DeleteWindow( );
                break;
            }
            // Directory up
            case kDirUpId: {
                // Since the directory is changing, disable the OK button until
                // somthing is actually read.
                fOk->SetState( kButtonDisabled );
                // Clear the contents of the text box
                fText->Clear( );

                fFc->ChangeDirectory( ".." );
                // Update the file list box with the contents of the new
                // directory.
                fPath->Update( fFc->GetDirectory( ) );
                // Populate the file combo box with files from the new
                // directory.
                AddFiles( fFc->GetDirectory( ) );
                break;
            }
            }
        }
        else if ( ( GET_MSG( msg ) == kC_COMMAND ) &&
                  ( GET_SUBMSG( msg ) == kCM_COMBOBOX ) )
        {
            switch ( parm1 )
            {
            // Path
            case kPathId: {
                // Since the directory is changing, disable the OK button until
                // something is actually read.
                fOk->SetState( kButtonDisabled );
                // Clear the contents of the text box
                fText->Clear( );

                // Get the selection from the combo box.
                TGTreeLBEntry* e = (TGTreeLBEntry*)fPath->GetSelectedEntry( );

                if ( e )
                {
                    // Change the directory in the List Box.
                    fFc->ChangeDirectory( e->GetPath( )->GetString( ) );
                    // Update the combo box entries with the new directory
                    // contents.
                    fPath->Update( fFc->GetDirectory( ) );
                    // Update the File combo box entries with files from the new
                    // directory
                    AddFiles( fFc->GetDirectory( ) );
                }
                break;
            }
            // File combo box
            case kFileId: {
                cerr << "ProcessCommand - kFileId" << endl;
                // Get the selection from the combo box.
                TGTextLBEntry* e = (TGTextLBEntry*)fFile->GetSelectedEntry( );
                // We need the path as well.
                TGTreeLBEntry* p = (TGTreeLBEntry*)fPath->GetSelectedEntry( );
                // ?? fFc->GetDirectory() might be used instead, this gives a
                // const char *.

                if ( e && p )
                {
                    cerr << "Calling ReadFile(" << p->GetPath( )->GetString( )
                         << "," << e->GetText( )->GetString( ) << ")" << endl;
                    // Read the file to verify the contents.
                    if ( ReadFile( p->GetPath( )->GetString( ),
                                   e->GetText( )->GetString( ) ) )
                    {
                        // If the file is acceptable, allow OK to be used.
                        fOk->SetState( kButtonUp );
                    }
                }
                break;
            }
            }
        }
        else if ( ( GET_MSG( msg ) == kC_CONTAINER ) &&
                  ( GET_SUBMSG( msg ) == kCT_ITEMDBLCLICK ) )
        {
            if ( parm1 == kButton1 ) // Left button clicked.
            {
                if ( fFc->NumSelected( ) == 1 )
                {
                    void* p = 0;

                    // Since the directory is changing, disable the OK button
                    // until somthing is actually read.

                    fOk->SetState( kButtonDisabled );
                    // Clear the contents of the text box
                    fText->Clear( );

                    TGFileItem* f = (TGFileItem*)fFc->GetNextSelected( &p );
                    // Update the file list box with the contents of the new
                    // directory.
                    fFc->ChangeDirectory( f->GetItemName( )->GetString( ) );
                    fPath->Update( fFc->GetDirectory( ) );
                    // Populate the file combo box with files from the new
                    // directory.
                    AddFiles( fFc->GetDirectory( ) );
                }
            }
        }
        return kTRUE;
    }

    //_____________________________________________________________________________
    Bool_t
    TLGImportDialog::AddFiles( const char* dir )
    {
        typedef std::vector< std::string > stringlist;

        // Remove the current entries from the File combo box.
        fFile->RemoveEntries( 0, 10000 );
        // loop over files
        void* dirp;
        if ( ( dirp = gSystem->OpenDirectory( dir ) ) == 0 )
        {
            fFile->SetTopEntry(
                new TGTextLBEntry( fFile, new TGString( "" ), 0 ),
                new TGLayoutHints( kLHintsLeft | kLHintsExpandY |
                                   kLHintsExpandX ) );
            fFile->MapSubwindows( );
            fFile->Layout( );
            return kFALSE;
        }
        const char* file;
        stringlist  nlist;
        while ( ( file = gSystem->GetDirEntry( dirp ) ) != 0 )
        {
            if ( strcmp( file, "." ) && strcmp( file, ".." ) )
            {
                string fullname = string( dir ) + "/" + string( file );
                // Don't list directories in the file list.
                // Do a *nix stat() to figure out what the file is.
                struct stat fstat;
                stat( fullname.c_str( ), &fstat );
                // If the file is a regular file, put it on the list.
                if ( S_ISREG( fstat.st_mode ) )
                {
                    nlist.push_back( file );
                }
            }
        }
        gSystem->FreeDirectory( dirp );
        sort( nlist.begin( ), nlist.end( ) );
        int id = 0;
        // Add the entries to the menu.
        for ( stringlist::iterator i = nlist.begin( ); i != nlist.end( ); ++i )
        {
            fFile->AddEntry( i->c_str( ), id++ );
        }
        // select file
        if ( id > 0 )
        {
            // Select the top item in the list.
            fFile->MapSubwindows( );
            fFile->Select( 0 );
        }
        else
        {
            // The list was empty, so create an empty item for the menu.
            fFile->SetTopEntry(
                new TGTextLBEntry( fFile, new TGString( "" ), 0 ),
                new TGLayoutHints( kLHintsLeft | kLHintsExpandY |
                                   kLHintsExpandX ) );
            fFile->MapSubwindows( );
            fFile->Layout( );
        }
        return kTRUE;
    }

    //_____________________________________________________________________________
    // This function opens the file and reads the first n characters to provide
    // a preview of what's in the file.  The text is displayed in a TGTextView
    // window. Alternately, we could make the text view bigger and just use the
    // LoadFile() function.
    Bool_t
    TLGImportDialog::ReadFile( const char* dir, const char* file )
    {
        if ( dir && file )
        {
            int   length = 0;
            char* buffer = (char*)NULL;

            // Clear the text window.
            fText->Clear( );

            // Create a full file name including the path.
            string fullname = string( dir ) + "/" + string( file );

            // Open the file.
            ifstream inp( fullname.c_str( ) );

            // Get the length of the file.
            // Set the pointer to the end of the file
            inp.seekg( 0, ios::end );
            // Get the position of the pointer.
            length = inp.tellg( );
            // Set the pointer back to the beginning of the file.
            inp.seekg( 0, ios::beg );
            // Read the file into the result string...
            if ( fRet )
            {
                buffer = new char[ length + 1 ];
                inp.read( buffer, length );
                buffer[ length ] = '\0';
                // Can't tell from the documentation if the characters from
                // buffer will be copied to the TString object.  If they aren't,
                // this will fail because buffer will go out of scope and
                // disappear.
                *fRet = buffer;
            }
            // Allocate memory to store the file contents.
            // Note that the preview only holds so much, so only read the first
            // 55 bytes...
            if ( length > 55 )
                length = 55;
            // Make sure this is terminated as a c string.
            if ( buffer )
            {
                buffer[ length ] = '\0';
                // Display the string for the user's approval.
                fText->AddLine( (const char*)buffer );
                fText->Update( );
            }

            // Close the file, we're done with it for now.
            inp.close( );
        }

        return kTRUE;
    }

    // JCB - end

    // JCB start
    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // TLGImportMLDialog                                                    //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    //   Int_t kImportMLDlg[] = {   440, 250,             // total width &
    //   height 15, 25, 70, 24,       // Path label 15, 185, 70, 24,      //
    //   File label 85, 25, 300, 24,      // Path 395, 25, 24, 24,      //
    //   Directory up button 85, 55, 335, 121,     // Directory list 85, 185,
    //   335, 24,     // File 147, 215, 65, 24 ,   // OK button 228, 215, 65,
    //   24,    // Cancel button 0 } ;

    TLGImportMLDialog::TLGImportMLDialog( const TGWindow* p,
                                          const TGWindow* main,
                                          char*           result,
                                          const char*     path )
        : TLGTransientFrame( p, main, 10, 10 ), fRet( result )
    {
        fRet = result;
        // layout hints
        fL[ 0 ] = new TGTableLayoutHints( 0,
                                          1,
                                          0,
                                          1,
                                          kLHintsLeft | kLHintsCenterY,
                                          2,
                                          2,
                                          20,
                                          2 ); // Path: label
        fL[ 1 ] =
            new TGTableLayoutHints( 1,
                                    2,
                                    0,
                                    1,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    20,
                                    2 ); // Path combo box
        fL[ 2 ] = new TGTableLayoutHints( 2,
                                          3,
                                          0,
                                          1,
                                          kLHintsLeft | kLHintsCenterY,
                                          2,
                                          2,
                                          20,
                                          2 ); // cd up button
        fL[ 3 ] =
            new TGTableLayoutHints( 1,
                                    3,
                                    1,
                                    2,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    2,
                                    0 ); // List Box
        fL[ 4 ] = new TGTableLayoutHints( 0,
                                          1,
                                          2,
                                          3,
                                          kLHintsLeft | kLHintsCenterY,
                                          2,
                                          2,
                                          2,
                                          2 ); // File: label
        fL[ 5 ] =
            new TGTableLayoutHints( 0,
                                    3,
                                    3,
                                    4,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    2,
                                    2 ); // T ext Box (preview)
        fL[ 6 ] =
            new TGTableLayoutHints( 1,
                                    3,
                                    2,
                                    3,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    2,
                                    2 ); // F ile combo box
        fL[ 7 ] = new TGLayoutHints( kLHintsLeft | kLHintsTop | kLHintsCenterX,
                                     2,
                                     2,
                                     2,
                                     2 ); // OK, Cancel button frame.
        fL[ 8 ] = new TGLayoutHints( kLHintsLeft | kLHintsTop | kLHintsExpandX,
                                     2,
                                     2,
                                     2,
                                     2 ); // Group frame layout

        fG = new TGGroupFrame( this, "Import Matlab Design" );
        AddFrame( fG, fL[ 8 ] );
        fG->SetLayoutManager(
            new TGTableLayout( fG, 4, 3 ) ); // Table layout, 4 rows, 3 columns.

        // Path: label
        fLabel[ 0 ] = new TGLabel( fG, "Path:" );
        fLabel[ 0 ]->SetTextJustify( kTextLeft | kTextCenterY );
        fG->AddFrame( fLabel[ 0 ], fL[ 0 ] );
        fLabel[ 0 ]->MoveResize( 15, 25, 70, 24 );

        // Path combo box
        fPath = new TGFSComboBox( fG, kPathId );
        fPath->Associate( this ); // Indicate that events should go to the
                                  // Import Dialog class.
        fG->AddFrame( fPath, fL[ 1 ] );
        fPath->MoveResize( 85, 25, 300, 24 );

        // Change Directory Up button.
        fPcdup = fClient->GetPicture( "tb_uplevel.xpm" );
        if ( !fPcdup )
        {
            Error( "TLGFilterWizWindow", "missing toolbar pixmap(s).n" );
        }
        fCdup = new TGPictureButton( fG, fPcdup, kDirUpId );
        fCdup->SetToolTipText( "Up One Level" );
        fCdup->Associate( this ); // Indicate events should go to Import Dialog.
        fG->AddFrame( fCdup, fL[ 2 ] );
        fCdup->MoveResize( 395, 25, 24, 24 );

        // List box for directory contents.
        fFv = new TGListView( fG, 400, 161 );
        fFc = new TGFileContainer(
            fFv->GetViewPort( ), 10, 10, kHorizontalFrame, fgWhitePixel );
        fFc->Associate( this );
        fFv->GetViewPort( )->SetBackgroundColor( fgWhitePixel );
        fFv->SetContainer( fFc );
        fFv->SetViewMode( kLVList );
        fFc->SetFilter( "YOUneverHAVEaFILElikeTHIS!!!" );
        fFc->Sort( kSortByType );
        if ( !path )
        {
            path = ".";
        }
        fFc->ChangeDirectory( path );
        fPath->Update( fFc->GetDirectory( ) );
        fG->AddFrame( fFv, fL[ 3 ] );
        fFv->MoveResize( 85, 55, 335, 121 );

        // File: label
        fLabel[ 1 ] = new TGLabel( fG, "File:" );
        fLabel[ 1 ]->SetTextJustify( kTextLeft | kTextCenterY );
        fG->AddFrame( fLabel[ 1 ], fL[ 4 ] );
        fLabel[ 1 ]->MoveResize( 15, 185, 70, 24 );

        // File combo box.
        fFile = new TGComboBox( fG, kFileId );
        fFile->Associate( this );
        fG->AddFrame( fFile, fL[ 6 ] );
        // Populate the file box with files from the current directory.
        AddFiles( fFc->GetDirectory( ) );
        fFile->MoveResize( 85, 185, 335, 24 );

#if 0
        // Text view for file preview.
        fText = new TGTextView(fG, 400, 24) ;
        fText->Associate(this) ;
        fG->AddFrame(fText, fL[5]) ;
        fText->MoveResize(85, 215, 400, 36) ;
        fText->Clear() ;
#endif

        // Horizontal frame to hold the OK and Cancel buttons
        fFButton = new TGHorizontalFrame( this, 10, 10 );
        AddFrame( fFButton, fL[ 7 ] );

        // OK button
        fOk = new TGTextButton( fFButton, "OK", kOkId );
        fOk->Associate( this );
        fFButton->AddFrame( fOk, 0 );
        fOk->MoveResize( 147, 245, 65, 24 );
        fOk->SetState( kButtonDisabled ); // Disable the OK button since
                                          // nothing's been read yet.

        // Cancel button
        fCancel = new TGTextButton( fFButton, "Cancel", kCancelId );
        fCancel->Associate( this );
        fFButton->AddFrame( fCancel, 0 );
        fCancel->MoveResize( 228, 245, 65, 24 );

        fG->Resize( fG->GetDefaultWidth( ), fG->GetDefaultHeight( ) );

        MapSubwindows( );
        UInt_t width = GetDefaultWidth( );
        UInt_t height = GetDefaultHeight( );
        Resize( width, height );

        // make the message box non-resizable
        SetWMSize( width, height );
        SetWMSizeHints( width, height, width, height, 0, 0 );

        // set dialog box title
        SetWindowName( "Import Matlab Designs" );
        SetIconName( "Import" );
        SetClassHints( "ImportMLDlg`", "ImportMLDlg`" );
        SetMWMHints( kMWMDecorAll | kMWMDecorResizeH | kMWMDecorMaximize |
                         kMWMDecorMinimize | kMWMDecorMenu,
                     kMWMFuncAll | kMWMFuncResize | kMWMFuncMaximize |
                         kMWMFuncMinimize,
                     kMWMInputModeless );

        MapWindow( );
        fClient->WaitFor( this );
    }

    //_____________________________________________________________________________
    TLGImportMLDialog::~TLGImportMLDialog( )
    {
        for ( int i = 0; i < 9; i++ )
            delete fL[ i ];
        delete fLabel[ 0 ];
        delete fLabel[ 1 ];
        delete fPath;
        delete fCdup;
        delete fFv;
        delete fFc;
        delete fFile;
        delete fOk;
        delete fCancel;
#if 0
        delete fText ;
#endif
        delete fG;
    }

    //_____________________________________________________________________________
    void
    TLGImportMLDialog::CloseWindow( )
    {
        // Erase any return value to cancel action.
        if ( fRet )
            fRet[ 0 ] = '\0';
        DeleteWindow( );
    }

    //_____________________________________________________________________________
    Bool_t
    TLGImportMLDialog::ProcessMessage( Long_t msg, Long_t parm1, Long_t parm2 )
    {
        // Buttons
        if ( ( GET_MSG( msg ) == kC_COMMAND ) &&
             ( GET_SUBMSG( msg ) == kCM_BUTTON ) )
        {
            switch ( parm1 )
            {
            // ok
            case kOkId: {
                // Process the file.
                // Open the file, read the filter section, and return.  Just
                // return the raw text, put off parsing it until later.

                // Delete the window and return.
                DeleteWindow( );
                break;
            }
            // cancel
            case kCancelId: {
                // Erase any return value to cancel action.
                if ( fRet )
                    fRet[ 0 ] = '\0';
                DeleteWindow( );
                break;
            }
            // Directory up
            case kDirUpId: {
                // Since the directory is changing, disable the OK button until
                // somthing is actually read.
                fOk->SetState( kButtonDisabled );
#if 0
                    // Clear the contents of the text box
                    fText->Clear() ;
#endif

                fFc->ChangeDirectory( ".." );
                // Update the file list box with the contents of the new
                // directory.
                fPath->Update( fFc->GetDirectory( ) );
                // Populate the file combo box with files from the new
                // directory.
                AddFiles( fFc->GetDirectory( ) );
                break;
            }
            }
        }
        else if ( ( GET_MSG( msg ) == kC_COMMAND ) &&
                  ( GET_SUBMSG( msg ) == kCM_COMBOBOX ) )
        {
            switch ( parm1 )
            {
            // Path
            case kPathId: {
                // Since the directory is changing, disable the OK button until
                // something is actually read.
                fOk->SetState( kButtonDisabled );
#if 0
                    // Clear the contents of the text box
                    fText->Clear() ;
#endif

                // Get the selection from the combo box.
                TGTreeLBEntry* e = (TGTreeLBEntry*)fPath->GetSelectedEntry( );

                if ( e )
                {
                    // Change the directory in the List Box.
                    fFc->ChangeDirectory( e->GetPath( )->GetString( ) );
                    // Update the combo box entries with the new directory
                    // contents.
                    fPath->Update( fFc->GetDirectory( ) );
                    // Update the File combo box entries with files from the new
                    // directory
                    AddFiles( fFc->GetDirectory( ) );
                }
                break;
            }
            // File combo box
            case kFileId: {
                cerr << "ProcessCommand - kFileId" << endl;
                // Get the selection from the combo box.
                TGTextLBEntry* e = (TGTextLBEntry*)fFile->GetSelectedEntry( );
                // We need the path as well.
                TGTreeLBEntry* p = (TGTreeLBEntry*)fPath->GetSelectedEntry( );
                // ?? fFc->GetDirectory() might be used instead, this gives a
                // const char *.

                if ( e && p )
                {
                    // Concatenate the path and selection strings for a return
                    // value.
                    if ( fRet )
                    {
                        sprintf( fRet,
                                 "%s/%s",
                                 p->GetPath( )->GetString( ),
                                 e->GetText( )->GetString( ) );
                        cerr << "ProcessCommand: returning " << fRet << endl;
                        ;
                    }

                    // If the file is acceptable, allow OK to be used.
                    fOk->SetState( kButtonUp );
                }
                break;
            }
            }
        }
        else if ( ( GET_MSG( msg ) == kC_CONTAINER ) &&
                  ( GET_SUBMSG( msg ) == kCT_ITEMDBLCLICK ) )
        {
            if ( parm1 == kButton1 ) // Left button clicked.
            {
                if ( fFc->NumSelected( ) == 1 )
                {
                    void* p = 0;

                    // Since the directory is changing, disable the OK button
                    // until somthing is actually read.

                    fOk->SetState( kButtonDisabled );
#if 0
                    // Clear the contents of the text box
                    fText->Clear() ;
#endif

                    TGFileItem* f = (TGFileItem*)fFc->GetNextSelected( &p );
                    // Update the file list box with the contents of the new
                    // directory.
                    fFc->ChangeDirectory( f->GetItemName( )->GetString( ) );
                    fPath->Update( fFc->GetDirectory( ) );
                    // Populate the file combo box with files from the new
                    // directory.
                    AddFiles( fFc->GetDirectory( ) );
                }
            }
        }
        return kTRUE;
    }

    //_____________________________________________________________________________
    Bool_t
    TLGImportMLDialog::AddFiles( const char* dir )
    {
        typedef std::vector< std::string > stringlist;

        // Remove the current entries from the File combo box.
        fFile->RemoveEntries( 0, 10000 );
        // loop over files
        void* dirp;
        if ( ( dirp = gSystem->OpenDirectory( dir ) ) == 0 )
        {
            fFile->SetTopEntry(
                new TGTextLBEntry( fFile, new TGString( "" ), 0 ),
                new TGLayoutHints( kLHintsLeft | kLHintsExpandY |
                                   kLHintsExpandX ) );
            fFile->MapSubwindows( );
            fFile->Layout( );
            return kFALSE;
        }
        const char* file;
        stringlist  nlist;
        while ( ( file = gSystem->GetDirEntry( dirp ) ) != 0 )
        {
            if ( strcmp( file, "." ) && strcmp( file, ".." ) )
            {
                string fullname = string( dir ) + "/" + string( file );
                // found a file: open it and check magic token
                ifstream inp( fullname.c_str( ) );
                char     line[ 128 ];
                // The first line should be some recognizable line, otherwise
                // don't use it.
                inp.read( line, strlen( kMatlabMagic ) );
                if ( inp &&
                     ( strncmp( line, kMatlabMagic, strlen( kMatlabMagic ) ) ==
                       0 ) )
                {
                    // Don't list directories in the file list.
                    // Do a *nix stat() to figure out what the file is.
                    struct stat fstat;
                    stat( fullname.c_str( ), &fstat );
                    // If the file is a regular file, put it on the list.
                    if ( S_ISREG( fstat.st_mode ) )
                    {
                        nlist.push_back( file );
                    }
                }
            }
        }
        gSystem->FreeDirectory( dirp );
        sort( nlist.begin( ), nlist.end( ) );
        int id = 0;
        // Add the entries to the menu.
        for ( stringlist::iterator i = nlist.begin( ); i != nlist.end( ); ++i )
        {
            fFile->AddEntry( i->c_str( ), id++ );
        }
        // select file
        if ( id > 0 )
        {
            // Select the top item in the list.
            fFile->MapSubwindows( );
            fFile->Select( 0 );
        }
        else
        {
            // The list was empty, so create an empty item for the menu.
            fFile->SetTopEntry(
                new TGTextLBEntry( fFile, new TGString( "" ), 0 ),
                new TGLayoutHints( kLHintsLeft | kLHintsExpandY |
                                   kLHintsExpandX ) );
            fFile->MapSubwindows( );
            fFile->Layout( );
        }
        return kTRUE;
    }

    //////////////////////////////////////////////////////////////////////////
    //                                                                      //
    // TLGInputDialog                                                      //
    //                                                                      //
    //////////////////////////////////////////////////////////////////////////
    TLGInputDialog::TLGInputDialog( const TGWindow* p,
                                    const TGWindow* main,
                                    const char*     dialog_name,
                                    const char*     label,
                                    char*           result )
        : TGTransientFrame( p, main, 10, 10 ), fTitleTxt( dialog_name ),
          fLabelTxt( label ), fRet( result )
    {
        fL[ 0 ] = new TGLayoutHints(
            kLHintsLeft | kLHintsExpandX | kLHintsTop, 2, 2, 2, 2 );
        fL[ 1 ] = new TGLayoutHints( kLHintsRight | kLHintsTop, 6, 6, 12, 4 );
        fL[ 2 ] = new TGTableLayoutHints(
            0, 1, 0, 1, kLHintsLeft | kLHintsCenterY, 2, 2, 25, 2 );
        fL[ 3 ] =
            new TGTableLayoutHints( 1,
                                    3,
                                    0,
                                    1,
                                    kLHintsLeft | kLHintsCenterY | kLHintsFillX,
                                    2,
                                    2,
                                    25,
                                    2 );
        fL[ 4 ] = new TGTableLayoutHints(
            0, 1, 1, 2, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 0 );
        fL[ 5 ] = new TGTableLayoutHints(
            1, 2, 1, 2, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 0 );
        fL[ 6 ] = new TGTableLayoutHints(
            2, 3, 1, 2, kLHintsLeft | kLHintsCenterY, 2, 2, 2, 0 );
        fF = new TGGroupFrame( this, fTitleTxt.c_str( ) );
        AddFrame( fF, fL[ 0 ] );

        // Text entry
        fFText = new TGHorizontalFrame( this, 10, 10 );
        fF->AddFrame( fFText, fL[ 0 ] );
        fName = new TGTextEntry( fFText, "", 0 );
        fFText->AddFrame( fName, fL[ 1 ] );
        fLabel = new TGLabel( fFText, fLabelTxt.c_str( ) );
        fFText->AddFrame( fLabel, fL[ 1 ] );

        // Buttons
        fFButton = new TGHorizontalFrame( this, 10, 10 );
        AddFrame( fFButton, fL[ 0 ] );
        fButton[ 1 ] = new TGTextButton(
            fFButton, new TGHotString( "     &Cancel     " ), kCancelId );
        fButton[ 1 ]->Associate( this );
        fFButton->AddFrame( fButton[ 1 ], fL[ 1 ] );
        fButton[ 0 ] = new TGTextButton(
            fFButton, new TGHotString( "        &Ok        " ), kOkId );
        fButton[ 0 ]->Associate( this );
        fFButton->AddFrame( fButton[ 0 ], fL[ 1 ] );

        // resize & move to center
        MapSubwindows( );
        UInt_t width = GetDefaultWidth( );
        UInt_t height = GetDefaultHeight( );
        Resize( width, height );
        Int_t ax;
        Int_t ay;
        if ( main )
        {
            Window_t wdum;
            gVirtualX->TranslateCoordinates(
                main->GetId( ),
                GetParent( )->GetId( ),
                ( ( (TGFrame*)main )->GetWidth( ) - fWidth ) >> 1,
                ( ( (TGFrame*)main )->GetHeight( ) - fHeight ) >> 1,
                ax,
                ay,
                wdum );
        }
        else
        {
            UInt_t root_w, root_h;
            gVirtualX->GetWindowSize(
                fClient->GetRoot( )->GetId( ), ax, ay, root_w, root_h );
            ax = ( root_w - fWidth ) >> 1;
            ay = ( root_h - fHeight ) >> 1;
        }
        Move( ax, ay );
        SetWMPosition( ax, ay );

        // make the message box non-resizable
        SetWMSize( width, height );
        SetWMSizeHints( width, height, width, height, 0, 0 );

        // set dialog box title
        SetWindowName( fTitleTxt.c_str( ) );
        SetIconName( fTitleTxt.c_str( ) );
        SetClassHints( "NewInputDlg", "NewInputDlg" );
        SetMWMHints( kMWMDecorAll | kMWMDecorResizeH | kMWMDecorMaximize |
                         kMWMDecorMinimize | kMWMDecorMenu,
                     kMWMFuncAll | kMWMFuncResize | kMWMFuncMaximize |
                         kMWMFuncMinimize,
                     kMWMInputModeless );

        MapWindow( );
        fClient->WaitFor( this );
    }

    TLGInputDialog::~TLGInputDialog( )
    {
        delete fButton[ 0 ];
        delete fButton[ 1 ];
        delete fFButton;
        delete fLabel;
        delete fName;
        delete fFText;
        delete fF;

        for ( int i = 0; i < 7; ++i )
            delete fL[ i ];
    }

    void
    TLGInputDialog::CloseWindow( )
    {
        if ( fRet )
            fRet[ 0 ] = '\0';
        DeleteWindow( );
    }

    Bool_t
    TLGInputDialog::ProcessMessage( Long_t msg, Long_t parm1, Long_t parm2 )
    {
        // Buttons
        if ( ( GET_MSG( msg ) == kC_COMMAND ) &&
             ( GET_SUBMSG( msg ) == kCM_BUTTON ) )
        {
            switch ( parm1 )
            {
            case kOkId: {
                strncpy( fRet, fName->GetText( ), 255 );
                // Make sure there's at least one null terminator.
                fRet[ 255 ] = '\0';
                DeleteWindow( );
                break;
            }
            case kCancelId: {
                if ( fRet )
                    fRet[ 0 ] = '\0';
                DeleteWindow( );
                break;
            }
            }
        }
        return kTRUE;
    }

} // namespace filterwiz
