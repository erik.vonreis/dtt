/* version $Id: foton.cc 7856 2017-02-22 20:34:46Z james.batch@LIGO.ORG $ */
/* -*- mode: c++; c-basic-offset: 4; -*- */
#include <cstdio>
#include <unistd.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <cstdlib>
#include <iostream>
#include <fstream>
#include <TROOT.h>
#include <TApplication.h>
#include <TSystem.h>
#include "foton.hh"
#include "TLGFilterWizard.hh"
#include "goption.hh"

// Allow output of debug info to a file, /tmp/fotonDebug.txt JCB
std::fstream debugLog;

// ROOT object
TROOT root( "GUI", "Foton" );

using namespace std;
using namespace filterwiz;

const char* const help_text =
    "Usage: foton [options] [filename]\n"
    " where options may be the following:\n"
    "       -w             read-write mode (readonly default)\n"
#ifdef LEGACY_WRITE
    "       -l             legacy write mode\n"
#endif
    "       -a             Allow any sample rate\n"
    "       -p 'path'      path for filter files\n"
    "       -d 'display'   X windows display\n"
    "       -c             Read and write the file without opening the GUI. "
    "Implies -z.\n"
    "       -o 'outfile'   Used with the -c option, specify a filename to "
    "write the filters\n"
    "       -m 'mergefile' Merge matlab designs into existing filter file\n"
    "       -s 'filt:sec'  filter module/section selection\n"
    "       -r 'datarate'  Rebuild a filter file with a new sample rate, batch "
    "mode, no GUI.\n"
    "       -x             Experiment mode, allow creation of new files and "
    "new modules.\n"
    "       -h             this help\n"
    "       -z             Correct coefficients on load for certain stability "
    "issues.\n"
    "       -Z t | f       If true, warn when there are zeroes in the right-hand plane."
    "       -n             Do not correct coefficients on load (default).\n"
    "\nFoton version " VERSION
    "\n$Id: foton.cc 7856 2017-02-22 20:34:46Z james.batch@LIGO.ORG $\n";

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// main                                                                 //
//                                                                      //
// Main Program                                                         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
extern "C" int
main( int argc, char** argv )
{
    // JCB - for non-GUI processing of matlab merge file.
    int merge_mode = 0;
    int debug_enable = 1;
    int batch_mode = 0;
    int outfile_used = 0;
    int rate_change = 0;
    int correct_default = 0;
    int correct_on_load = 0;


    gdsbase::option_string opt( argc, argv, "LaxcvwZ:znm:o:p:d:s:r:h" );
    // help
    if ( opt.help( ) || ( opt.argnum( ) > 1 ) )
    {
        cout << help_text << endl;
        exit( 0 );
    }
    // This sends deep debugging stuff to a log file. JCB
    if ( opt.opt( 'L' ) )
    {
        debugLog.open( "/tmp/fotonDebug.txt", fstream::out | fstream::trunc );
    }
    // If the file didn't open or the option wasn't selected, send the output to
    // /dev/null.
    if ( !debugLog.is_open( ) )
    {
        debugLog.open( "/dev/null" );
    }

    // Set the batch mode flag to read, write, and exit.  No GUI option,
    // this will allow a filter file to be generated as if it had been
    // saved without altering from the GUI.  Useful when the filter file
    // contains errors such as orphaned filter modules or no design strings.
    if ( opt.opt( 'c' ) )
    {
        batch_mode = 1;
        correct_default = 1;
    }

    // set whether to warn on

    // Set the coefficient "correct on load" state
    if ( opt.opt( 'z' ) )
    {
        correct_on_load = 1;
    }
    else if ( opt.opt( 'n' ) )
    {
        correct_on_load = 0;
    }
    else
    {
        correct_on_load = correct_default;
    }

    // silence output if not verbose
    if ( !opt.opt( 'v' ) )
    {
        int i = ::open( "/dev/null", 2 );
        (void)dup2( i, 2 ); // stderr, cerr
        debug_enable = 0;
    }

    // Filename rules:
    // The file that is opened depends on the -p <path> option as well as the
    // filename given on the command line.
    //
    // If an absolute path to the file is given as part of the filename, as in
    // "foton -p /ligo/home/controls /opt/rtcds/lho/h1/chans/H1PEMMX.txt"
    // the file is opened from the directory given.  If no -o <outpath> is used,
    // the file will be written to the same file and path.  A -p <path> option
    // will be ignored in this case.
    //
    // If a relative path to the file is given as part of the filename, as in
    // "foton -p /ligo/home/controls jcb/H1PEMMX.txt
    // the path to the file will be created by prepending the path given in the
    // -p option (or "./" if the -p option isn't used) with the path part of the
    // filename (if any) and the filename.  In the example above, the full
    // pathname is /ligo/home/controls/jcb/H1PEMMX.txt.
    //
    // If no relative path is given as part of the filename, if the -p <path>
    // option is used the file will be found in <path>/filename.

    // filename
    string filename;
    string shortname;
    string filepath;
    if ( opt.argnum( ) == 1 )
    {
        filename = *opt.argbegin( );
        if ( filename.find( "/" ) != string::npos )
        {
            filepath = gSystem->DirName( filename.c_str( ) );
        }
        shortname = gSystem->BaseName( filename.c_str( ) );
    }

    // Output file
    string outfile;
    string outfileshort;
    string outfilepath;
    if ( opt.getOpt( 'o', outfile ) )
    {
        if ( outfile.empty( ) )
        {
            // Error, need output file name.
        }
        else
        {
            outfile_used = 1;
            if ( outfile.find( "/" ) != string::npos )
            {
                outfilepath = gSystem->DirName( outfile.c_str( ) );
            }
            outfileshort = gSystem->BaseName( outfile.c_str( ) );
        }
    }

    // Allow any sample rate option.
    bool any_sample = opt.opt( 'a' );

    // Sample rate change
    string newRate;
    if ( opt.getOpt( 'r', newRate ) )
    {
        if ( !any_sample )
        {
            // Validate the newRate
            int rate = atoi( newRate.c_str( ) );
            if ( rate < 0 )
            {
                cerr << "Specified rate must be greater than zero." << endl;
                return 1;
            }
            //	    switch (rate) {
            //	       case 65536:
            //	       case 32768:
            //	       case 16384:
            //	       case 8192:
            //	       case 4096:
            //	       case 2048:
            //		  break ;
            //	       default:
            //		  cerr << "Specified data rate invalid, cannot convert file"
            //<< endl ; 		  return 1 ;
            //	    }
        }
        rate_change = 1;
    }

    // read-write mode
    bool rw_mode = opt.opt( 'w' );

    bool experiment_mode = opt.opt( 'x' );

    if ( experiment_mode )
    {
        // Allow any sample rate and make sure write is enabled.
        rw_mode = any_sample = 1;
    }

    // path
    string path;
    string outpath;
    opt.getOpt( 'p', path );
    outpath = path;
    if ( ( !filepath.empty( ) && ( filepath[ 0 ] == '/' ) ) || path.empty( ) )
    {
        path = filepath;
    }
    else if ( !filepath.empty( ) )
    {
        path += "/" + filepath;
    }
    if ( outfile_used )
    {
        if ( ( !outfilepath.empty( ) && ( outfilepath[ 0 ] == '/' ) ) ||
             outpath.empty( ) )
        {
            outpath = outfilepath;
        }
        else if ( !outfilepath.empty( ) )
        {
            outpath += '/' + outfilepath;
        }
    }

    // display
    string display;
    int    rargc = 1;
    char*  rargv[ 10 ];
    rargv[ 0 ] = argv[ 0 ];
    if ( opt.getOpt( 'd', display ) )
    {
        rargv[ 1 ] = new char[ 12 ];
        strcpy( rargv[ 1 ], "-display" );
        rargv[ 2 ] = new char[ display.size( ) + 2 ];
        strcpy( rargv[ 2 ], display.c_str( ) );
        rargc = 3;
    }

    bool                   legacy_mode = 0;

    // select whether to warn if there are zeroes in the right hand plane
    string zero_rhp_setting;
    if (opt.getOpt('Z', zero_rhp_setting)) {
        if (nullptr != strchr("Tt1", zero_rhp_setting.c_str()[0])) {
            setenv("FOTON_WARN_RHP_ZEROS", "T", 1);
        }
        else if (nullptr != strchr("Ff0", zero_rhp_setting.c_str()[0])) {
            setenv("FOTON_WARN_RHP_ZEROS", "F", 1);
        }
        else {
            cerr << "Invalid argument '" << zero_rhp_setting << "' for -Z option. Should be an 'f' or a 't'.";
            return 1;
        }
    }

    // Batch Matlab merge option. Requires a matlab file
    // and a filter file, output file is optional and if
    // not specifies the filter file will be used.
    string matlab_file;
    if ( opt.getOpt( 'm', matlab_file ) )
    {
        if ( matlab_file.empty( ) || shortname.empty( ) )
        {
            cerr << "foton error: -m requires a ";
            if ( matlab_file.empty( ) )
            {
                cerr << "matlab design file ";
                if ( shortname.empty( ) )
                {
                    cerr << "and a";
                }
            }
            if ( shortname.empty( ) )
            {
                cerr << "filter file ";
            }
            cerr << "to be specified on the command line.\n";
            return 1;
        }
        merge_mode = 1;
    }

    // For the batch mode, just create a new FilterFile, read the
    // input file, and write the file.  Return 0 when done if successful.
    if ( batch_mode )
    {
        // realname is the name of the actual input file, in case it's
        // a symbolic link.
        string realname;
        string realoutname;

        // Create a new FilterFile object.
        FilterFile filter_file;

        // Make sure there's an input file.
        if ( shortname.empty( ) )
        {
            cout << "Error: no filter file specified." << endl;
            return -1;
        }

        // Find the real file name in case it's a link. That way the write
        // function won't overwrite the symbolic link.
        // If no path was specified, give getRealFilename a "." to work with.
        if ( path.empty( ) )
            path = ".";
        realname = filter_file.getRealFilename( shortname, path );
        if ( realname.empty( ) )
        {
            cout << "Error: Could not determine path to file " << shortname
                 << endl;
            return -1;
        }

        // Find the real file name of the output file in case it's a link.
        if ( outfile_used )
        {
            if ( outpath.empty( ) )
                outpath = ".";
            realoutname = filter_file.getRealFilename( outfileshort, outpath );
            if ( realoutname.empty( ) )
            {
                cout << "Error: Could not determine path to file "
                     << outfileshort << endl;
                return -1;
            }
        }

        filter_file.set_correct_on_load( correct_on_load );

        // Read the filter file.
        // The act of reading the filter file will correct most errors.  The
        // only error that might occur is if a filter can't be created from the
        // coefficients present in the filter file.
        if ( !filter_file.read( realname.c_str( ) ) )
        {
            cout << "Error: File " << realname << " could not be read." << endl;
            return -1;
        }

        // If there were errors, print them.
        if ( !filter_file.emptyFileMessages( ) )
        {
            cout << "Notifications(s) from reading file:" << endl;
            print_message_vec( filter_file.getFileMessages( ) );
        }

        if ( outfile_used )
        {
            if ( !filter_file.write( realoutname.c_str( ) ) )
            {
                cerr << "Write failed." << endl;
                return -1;
            }
        }
        else
        {
            // Write the filter file.
            if ( !filter_file.write( realname.c_str( ) ) )
            {
                cerr << "Write failed." << endl;
                return -1;
            }
        }
        // Writing the file should have cleared the errors from reading.
        // If there's new errors, they are from writing.  Print them.
        if ( !filter_file.emptyFileMessages( ) )
        {
            cout << "Notifications(s) from writing file:" << endl;
            print_message_vec( filter_file.getFileMessages( ) );
        }

        return 0;
    }

    // For the rate change mode, just create a new FilterFile, read the
    // input file, and write the file.  Return 0 when done if successful.
    if ( rate_change )
    {
        // realname is the name of the actual input file, in case it's
        // a symbolic link.
        string realname;
        string realoutname;

        // Create a new FilterFile object.
        FilterFile filter_file;

        // Make sure there's an input file.
        if ( shortname.empty( ) )
        {
            cout << "Error: no filter file specified." << endl;
            return -1;
        }

        // Find the real file name in case it's a link. That way the write
        // function won't overwrite the symbolic link.
        // If no path was specified, give getRealFilename a "." to work with.
        if ( path.empty( ) )
            path = ".";
        realname = filter_file.getRealFilename( shortname, path );
        if ( realname.empty( ) )
        {
            cout << "Error: Could not determine path to file " << shortname
                 << endl;
            return -1;
        }

        // Find the real file name of the output file in case it's a link.
        if ( outfile_used )
        {
            if ( outpath.empty( ) )
                outpath = ".";
            realoutname = filter_file.getRealFilename( outfileshort, outpath );
            if ( realoutname.empty( ) )
            {
                cout << "Error: Could not determine path to file "
                     << outfileshort << endl;
                return -1;
            }
        }

        filter_file.set_correct_on_load( correct_on_load );
        // Read the filter file.
        // The act of reading the filter file will correct most errors.  The
        // only error that might occur is if a filter can't be created from the
        // coefficients present in the filter file.
        if ( !filter_file.read( realname.c_str( ) ) )
        {
            cout << "Error: File " << realname << " could not be read." << endl;
            return -1;
        }

        // If there were errors, print them.
        if ( !filter_file.emptyFileMessages( ) )
        {
            print_message_vec( filter_file.getFileMessages( ) );
        }

        // Convert the filters to a new rate.  The rate was validated above.
        filter_file.convertFilters( atof( newRate.c_str( ) ) );
        filter_file.setFSample( newRate.c_str( ) );

        if ( outfile_used )
        {
            if ( !filter_file.write( realoutname.c_str( ) ) )
            {
                cerr << "Write failed." << endl;
                return -1;
            }
        }
        else
        {
            // Write the filter file.
            if ( !filter_file.write( realname.c_str( ) ) )
            {
                cerr << "Write failed." << endl;
                return -1;
            }
        }
        // Writing the file should have cleared the errors from reading.
        // If there's new errors, they are from writing.  Print them.
        if ( !filter_file.emptyFileMessages( ) )
        {
            cout << "Error(s) from writing file:" << endl;
            print_message_vec( filter_file.getFileMessages( ) );
        }

        return 0;
    }

    // For the matlab merge, just create a new FilterFile,
    // read the input file, merge the matlab file, and write the results.
    // Return 0 when done if successful.
    if ( merge_mode )
    {
        // realname is the name of the actual input file, in case it's
        // a symbolic link.
        string realname;

        // Create a new FilterFile object.
        FilterFile filter_file;

        // Sanity checks. Make sure there's input files to work with.
        if ( filename.empty( ) )
        {
            cout << "Error: no filter file specified." << endl;
            return -1;
        }
        if ( matlab_file.empty( ) )
        {
            cout << "Error: no matlab merge file specified." << endl;
            return -1;
        }

        // Find the real file name in case it's a link. That way the write
        // function won't overwrite the symbolic link.
        // If no path was spcified, give getRealFilename a "." to work with.
        if ( filepath.empty( ) )
            filepath = ".";
        realname = filter_file.getRealFilename( filename, filepath );
        if ( realname.empty( ) )
        {
            cout << "Error: Could not determine path to file " << filename
                 << endl;
            return -1;
        }

        // Set the debug level for the merge functions, this allows errors to
        // be printed if it fails.
        filter_file.setMergeDebug( 1 );

        // Read the filter file.
        filter_file.set_correct_on_load( correct_on_load );
        (void)filter_file.read( realname.c_str( ) );
        if ( ( filter_file.emptyFileMessages( ) ) )
        {
            cerr << "Read of " << realname << " succeeded!" << endl;
            // Merge the matlab file. (0 return for success)
            if ( filter_file.merge( matlab_file.c_str( ) ) == 0 )
            {
                cerr << "Merge of " << matlab_file << " successful!" << endl;
                // Write the merged filter file.
                if ( !filter_file.write( realname.c_str( ) ) )
                {
                    cerr << "Write failed." << endl;
                    return -1;
                }
                else
                    cerr << "Write succeeded!" << endl;
            }
            else
            {
                // Error, print the merge errors.
                cerr << "Merge failed!" << endl;
                print_message_vec( filter_file.getMergeMessages( ) );
                return -1;
            }
        }
        else
        {
            // Couldn't read the filter file, print the errors.
            cerr << "Read Failed!" << endl;
            print_message_vec( filter_file.getFileMessages( ) );
            return -1;
        }
        // Successful, so return a 0.
        return 0;
    }

    // silence output if not verbose. cerr done above.
    if ( !opt.opt( 'v' ) )
    {
        int i = ::open( "/dev/null", 2 );
        (void)dup2( i, 1 ); // stdout, cout
        debug_enable = 0;
    }

    // Options for preselecting a filter module and section
    string filter_select;
    string fmodule = "";
    int    fsection = 0;
    if ( opt.getOpt( 's', filter_select ) )
    {
        string::size_type pos;
        pos = filter_select.find( ":" );
        fmodule = filter_select.substr( 0, pos );
        if ( pos != string::npos )
        {
            fsection = atoi( filter_select.substr( pos + 1 ).c_str( ) );
        }
        // cerr << "-s option: fmodule = " << fmodule << ", fsection = " <<
        // fsection << endl ;
    }

    // If this wasn't merge mode, continue with the interactive setup.

    // initializing root environment
    TApplication theApp( "Foton", &rargc, rargv );

    // create main window
    bool            modal = false;
    std::string     fdesign;
    TLGFilterWizard mainWindow( gClient->GetRoot( ),
                                modal,
                                "Foton",
                                fdesign,
                                path.empty( ) ? 0 : path.c_str( ),
                                shortname.empty( ) ? 0 : shortname.c_str( ),
                                fmodule.empty( ) ? 0 : fmodule.c_str( ),
                                fsection,
                                any_sample,
                                experiment_mode,
                                correct_on_load );

    // mainWindow.Setup() always needs to be called regardless
    // of mode.  It causes the filter file to be read (default or
    // the one specified on the command line).
    mainWindow.Setup( ); // TLGMainWindow::Setup() gets called.

    // Set the merge debug if verbose was specified.
    if ( debug_enable )
        mainWindow.setMergeDebug( 1 );

    mainWindow.SetReadOnly( !rw_mode );
    mainWindow.SetExperimentMode( experiment_mode );
    mainWindow.SetLegacyWrite( legacy_mode ); // JCB

    // run the GUI
    theApp.Run( );

    debugLog << "Foton exit." << endl;
    debugLog.close( );

    return 0;
}
