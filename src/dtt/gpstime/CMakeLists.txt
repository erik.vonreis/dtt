SET(GPSTIME_SRC
    caldate_mjd.c
    gpst2gpss.c
)

add_library(gpstime_lib OBJECT
    ${GPSTIME_SRC}
)
