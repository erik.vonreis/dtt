
if (NOT ONLY_FOTON)
    #### awggui ####
    add_executable(awggui
            awggui.cc
            )

    target_link_libraries(awggui

            awg
            dttgui
            )

    target_include_directories(awggui PRIVATE
            ${DTT_INCLUDES}
            ${ROOT_INCLUDES}
            dttgui
            )

    #### diaggui ####
    add_executable(diaggui
            diaggui.cc
            diagmain.cc
            diagplot.cc
            diagctrl.cc
            )

    target_link_libraries(diaggui
            dtt
            dttgui
            dfmgui
            )

    target_include_directories(diaggui PRIVATE
            ${DTT_INCLUDES}
            ${ROOT_INCLUDES}
            dttgui
            )

    install(TARGETS diaggui awggui
    		DESTINATION bin)
endif()

add_subdirectory(dttgui)