#include <TVirtualX.h>
#include "TLGComboEditBox.hh"
#include "TLGEntry.hh"
#include <iostream>

namespace dttgui {
   using namespace std;



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGTextLBEntry	                                                //
//                                                                      //
// List box entry with better spacing          		                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGTextLBEntry::TLGTextLBEntry (const TGWindow *p, TGString *s, Int_t id,
                     GContext_t norm, FontStruct_t font, UInt_t options, 
                     ULong_t back) :
   TGTextLBEntry (p, s, id, norm, font, options, back)
   {
   // Create a text listbox entry. The TGString is adopted.
      int max_ascent, max_descent;
   
      fTWidth = gVirtualX->TextWidth(fFontStruct, fText->GetString(), 
                           fText->GetLength()) + 3;
      gVirtualX->GetFontProperties(fFontStruct, max_ascent, max_descent);
      fTHeight = max_ascent + max_descent;
      Resize(fTWidth, fTHeight + 1);
   }

//______________________________________________________________________________

   void TLGTextLBEntry::SetText (TGString *new_text)
   {
   // Set or change text in text entry.
   
      if (fText) delete fText;
      fText = new_text;
      fTextChanged = kTRUE;
   
      int max_ascent, max_descent;
      fTWidth = gVirtualX->TextWidth(fFontStruct, fText->GetString(), 
                           fText->GetLength()) + 3;
      gVirtualX->GetFontProperties(fFontStruct, max_ascent, max_descent);
      fTHeight = max_ascent + max_descent;
   
      Resize(fTWidth, fTHeight + 1);
   
      fClient->NeedRedraw(this);
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGComboBox		                                                //
//                                                                      //
// Combobox with inactive state          		                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGComboBox::TLGComboBox (const TGWindow* p, Int_t id, 
                     UInt_t options, ULong_t back)
   : TGComboBox (p, id, options, back), fState (kTRUE)
   {
   }

//______________________________________________________________________________
   Bool_t TLGComboBox::HandleButton (Event_t* event)
   {
      if (!fState) {
         return kTRUE;
      }
      else {
         return TGComboBox::HandleButton (event);
      }
   }

//______________________________________________________________________________
   void TLGComboBox::SetState (Bool_t state)
   {
      if (!state) fComboFrame->EndPopup(); 
      fState = state;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGComboEditBox	                                                //
//                                                                      //
// Combobox with editable top field          		                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class ComboEditScrollBarElement : public TGScrollBarElement {
   public:
      ComboEditScrollBarElement (const TGWindow *p, const TGPicture* pic,
                        UInt_t w, UInt_t h, UInt_t options)
      : TGScrollBarElement (p, pic, w, h, options) {
      }
      virtual Bool_t HandleButton(Event_t *event) {         
         return ((TLGComboEditBox*)fParent)->HandleButton(event); }
   };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGComboEditBox	                                                //
//                                                                      //
// Combobox with editable top field          		                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGComboEditBox::TLGComboEditBox (const TGWindow *p, Int_t id, 
                     UInt_t options, ULong_t back) 
   : TGCompositeFrame (p, 10, 10, options, back), fState (kTRUE)
   {
   // Create a combo box widget.
   
      fWidgetId  = id;
      fMsgWindow = p;
      fBpic = fClient->GetPicture("arrow_down.xpm");
   
      if (!fBpic)
         Error("TGComboBox", "arrow_down.xpm not found");
   
      fSelEntry = new TGTextEntry (this, "", 0);
      fSelEntry->ChangeOptions (0);
      fDDButton = new ComboEditScrollBarElement(this, fBpic, kDefaultScrollBarWidth,
                           kDefaultScrollBarWidth, kRaisedFrame);
   
      fSelEntry->ChangeOptions(fSelEntry->GetOptions() | kOwnBackground);
   
      AddFrame(fSelEntry, fLhs = new TGLayoutHints(kLHintsLeft |
                                kLHintsExpandY | kLHintsExpandX));
                                                //0, 0, 1, 0));
      AddFrame(fDDButton, fLhb = new TGLayoutHints(kLHintsRight |
                                kLHintsExpandY));
   
      fComboFrame = new TGComboBoxPopup(fClient->GetRoot(), 100, 100, kVerticalFrame);
   
      fListBox = new TGListBox(fComboFrame, fWidgetId, kChildFrame);
      fListBox->Resize(100, 100);
      fListBox->Associate(this);
      fListBox->GetScrollBar()->GrabPointer(kFALSE); // combobox will do a pointergrab
   
      fComboFrame->AddFrame(fListBox, fLhdd = new TGLayoutHints(kLHintsExpandX |
                                             kLHintsExpandY));
      fComboFrame->MapSubwindows();
      fComboFrame->Resize(fComboFrame->GetDefaultSize());
   
      gVirtualX->GrabButton(fDDButton->GetId(), kButton1, kAnyModifier, 
                           kButtonPressMask | kButtonReleaseMask, 
                           kNone, kNone);
   
   // Drop down listbox of combo box should react to pointer motion
   // so it will be able to Activate() (i.e. highlight) the different
   // items when the mouse crosses.
      fListBox->GetContainer()->AddInput(kButtonPressMask | kButtonReleaseMask |
                           kPointerMotionMask);
      // gVirtualX->SelectInput(fListBox->GetContainer()->GetId(), kButtonPressMask |
                           // kButtonReleaseMask | kPointerMotionMask);
   }

//______________________________________________________________________________
   TLGComboEditBox::~TLGComboEditBox()
   {
   // Delete a combo box widget.
      delete fDDButton;
      delete fSelEntry;
      delete fListBox;
      delete fComboFrame;
      delete fLhs;
      delete fLhb;
      delete fLhdd;
   }

//______________________________________________________________________________
   void TLGComboEditBox::DrawBorder()
   {
   // Draw border of combo box widget.
   
#if ROOT_VERSION_CODE > ROOT_VERSION(3,5,5)
      GContext_t shadow = GetShadowGC()();
      GContext_t black = GetBlackGC()();
      GContext_t hilight = GetHilightGC()();
      GContext_t bckgnd = GetBckgndGC()();
#else
      GContext_t shadow = fgShadowGC();
      GContext_t black = fgBlackGC();
      GContext_t hilight = fgHilightGC();
      GContext_t bckgnd = fgBckgndGC();
#endif
      switch (fOptions & (kSunkenFrame | kRaisedFrame | kDoubleBorder)) {
         case kSunkenFrame | kDoubleBorder:
            gVirtualX->DrawLine(fId, shadow, 0, 0, fWidth-2, 0);
            gVirtualX->DrawLine(fId, shadow, 0, 0, 0, fHeight-2);
            gVirtualX->DrawLine(fId, black, 1, 1, fWidth-3, 1);
            gVirtualX->DrawLine(fId, black, 1, 1, 1, fHeight-3);
         
            gVirtualX->DrawLine(fId, hilight, 0, fHeight-1, fWidth-1, fHeight-1);
            gVirtualX->DrawLine(fId, hilight, fWidth-1, fHeight-1, fWidth-1, 0);
            gVirtualX->DrawLine(fId, bckgnd,  1, fHeight-2, fWidth-2, fHeight-2);
            gVirtualX->DrawLine(fId, bckgnd,  fWidth-2, 1, fWidth-2, fHeight-2);
            break;
      
         default:
            TGCompositeFrame::DrawBorder();
            break;
      }
   }

//______________________________________________________________________________
   Bool_t TLGComboEditBox::HandleButton(Event_t *event)
   {
      if (!fState) {
         return kTRUE;
      }
   // Handle mouse button events in the combo box.
      if (event->fType == kButtonPress) {
         if ((Window_t)event->fUser[0] == fDDButton->GetId())   // fUser[0] = child window
            fDDButton->SetState(kButtonDown);
      } 
      else {
         int      ax, ay;
         Window_t wdummy;
      
         fDDButton->SetState(kButtonUp);
         gVirtualX->TranslateCoordinates(fId, (fComboFrame->GetParent())->GetId(),
                              0, fHeight, ax, ay, wdummy);
      
         fComboFrame->PlacePopup(ax, ay, fWidth-2, fComboFrame->GetDefaultHeight());
      }
      return kTRUE;
   }

//______________________________________________________________________________
   void TLGComboEditBox::Select (Int_t id)
   {
      TGLBEntry *e;
   
      e = fListBox->Select(id);
      if (e) {
         TGTextLBEntry* te = dynamic_cast<TGTextLBEntry*>(e);
         if (te) {
            fSelEntry->SetText (te->GetText()->GetString());
         }
         else {
            fSelEntry->SetText ("");
         }
      }
   }

//______________________________________________________________________________
   Bool_t TLGComboEditBox::ProcessMessage(Long_t msg, Long_t, 
                     Long_t parm2)
   {
   // Process messages generated by the listbox and forward
   // messages to the combobox message handling window. Parm2 contains
   // the id of the selected listbox entry.
   
      TGLBEntry *e;
   
      switch (GET_MSG(msg)) {
         case kC_COMMAND:
            switch (GET_SUBMSG(msg)) {
               case kCM_LISTBOX:
                  e = fListBox->GetSelectedEntry();
                  TGTextLBEntry* te = dynamic_cast<TGTextLBEntry*>(e);
                  if (te) {
                     fSelEntry->SetText (te->GetText()->GetString());
                  }
                  else {
                     fSelEntry->SetText ("");
                  }
                  fComboFrame->EndPopup();
                  SendMessage(fMsgWindow, MK_MSG(kC_COMMAND, kCM_COMBOBOX),
                             fWidgetId, parm2);
                  if (e->InheritsFrom(TGTextLBEntry::Class())) {
                     const char *text;
                     text = ((TGTextLBEntry*)e)->GetText()->GetString();
                     Selected(text);
                  }
                  Selected(fWidgetId, (Int_t)parm2);
                  Selected((Int_t)parm2);
                  break;
            }
            break;
      
         default:
            break;
      }
      return kTRUE;
   }

//______________________________________________________________________________
   void TLGComboEditBox::Selected(Int_t widgetId, Int_t id)
   {
      Long_t args[2];
   
      args[0] = widgetId;
      args[1] = id;
   
      Emit("Selected(Int_t,Int_t)", args);
   }

//______________________________________________________________________________
   const char* TLGComboEditBox::GetText () const
   {
      return fSelEntry->GetText();
   }

//______________________________________________________________________________
   void TLGComboEditBox::SetText (const char* txt)
   {
      fSelEntry->SetText (txt);
   }

//______________________________________________________________________________
   void TLGComboEditBox::SetState (Bool_t state)
   {
      if (!state) fComboFrame->EndPopup(); 
      fSelEntry->SetState (state);
      fState = state;
   }

}
