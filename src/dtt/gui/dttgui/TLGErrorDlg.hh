/* version $Id$ */
#ifndef _LIGOTLGERRORDLG_H
#define _LIGOTLGERRORDLG_H
/*----------------------------------------------------------------------*/
/*                                                                      */
/* Module Name: TLGErrorDlg                                             */
/*                                                                      */
/* Module Description: Popup Dialog box that displays general text      */
/*                                                                      */
/* Revision History:                                                    */
/* Rel   Date     Programmer    Comments                                */
/* 0.1   15Nov12  J. Batch      First release                           */
/*                                                                      */
/* Documentation References:                                            */
/*      Man Pages: none                                                 */
/*      References: none                                                */
/*                                                                      */
/*                                                                      */
/*                      -------------------                             */
/*                                                                      */
/*                             LIGO                                     */
/*                                                                      */
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.      */
/*                                                                      */
/*                     (C) The LIGO Project, 2012.                      */
/*                                                                      */
/*                                                                      */
/* Caltech                              MIT                             */
/* LIGO Project MS 51-33                LIGO Project NW-17 161          */
/* Pasadena CA 91125                    Cambridge MA 01239              */
/*                                                                      */
/* LIGO Hanford Observatory             LIGO Livingston Observatory     */
/* P.O. Box 1970 S9-02                  19100 LIGO Lane Rd.             */
/* Richland WA 99352                    Livingston, LA 70754            */
/*                                                                      */
/*----------------------------------------------------------------------*/

#include <TGFrame.h>
#include <TGLabel.h>
#include <TGButton.h>
//#include <TGListBox.h>
//#include <TGComboBox.h>
//#include <TGTextEntry.h>
#include <TGTextView.h>
//#include <TRootEmbeddedCanvas.h>
//#include <TLatex.h>
#include <string>
#include <vector>

/** Display errors dialog box
    @memo Dialog box to display a vector of strings, used to show accumulated errors.
    @author October 2012 by James Batch
    @version 1.0
 ************************************************************************/
   class TLGErrorDialog : public TGTransientFrame{
   protected:
      /// Frame for OK button.
      TGHorizontalFrame	*fFButton ;
      /// Layout hints for widgets
      TGLayoutHints	*fL[3] ;
      /// Group frame
      TGGroupFrame	*fG ;
      /// Ok button
      TGTextButton	*fOk ;
      /// Cancel button
      TGTextButton	*fCancel ; // May not be used.
      /// Text box to display the errors.
      TGTextView	*fText ;
      /// return value if used.
      bool		*fRet ;

      /// Process the messages from the widgets.
      Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t parm2) ;

      /**
       * Add an error message to the window
       *
       * error messages will be broken up by newline
       * @param err_msg
       */
      void add_error(std::string err_msg);

   public:
      /// Constructor.  Args are the file name of text to be displayed and
      /// the title of the box.
      TLGErrorDialog(const TGWindow *p, const TGWindow *main, const char *fname, TGString title) ;
      /// constructor. Args are the vector of strings and the title of the box
      TLGErrorDialog(const TGWindow *p, const TGWindow *main, std::vector<std::string> *errors, TGString title) ;
      /// constructor. Args are the vector of strings and the title of the box
      TLGErrorDialog(const TGWindow *p, const TGWindow *main, std::vector<std::string> errors, TGString title) ;
      /// constructor, Args are an array of character pointers and the title of the box.
      TLGErrorDialog(const TGWindow *p, const TGWindow *main, const char *errors[], TGString title) ;
      /// constructor. Args are the vector of strings, labels for 2 buttons, a return value, and the title of the box
      TLGErrorDialog(const TGWindow *p, const TGWindow *main, 
		  std::vector<std::string> *errors, 
		  const std::string button1, const std::string but1_tooltip,
		  const std::string button2, const std::string but2_tooltip,
		  bool *ret, TGString title) ;

      /// Destructor
      ~TLGErrorDialog() ;

      /// Close window - if user closes the dialog or program quits
      void CloseWindow() ;
   private:
      /// Common code for constructor, all but the text to be displayed.
      void CommonSetup(const TGWindow *p, const TGWindow *main, TGString title) ;
   } ;
#endif
