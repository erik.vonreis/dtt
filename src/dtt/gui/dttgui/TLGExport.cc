/* version $Id: TLGExport.cc 8024 2018-04-18 18:04:26Z ed.maros@LIGO.ORG $ */
#include "PConfig.h"
#ifdef P__LINUX
#define _ISOC99_SOURCE
#endif
#include "TLGExport.hh"
#include "DataDesc.hh"
#include "PlotSet.hh"
#include "Xsil.hh"
#include "XsilStd.hh"
#include "TLGEntry.hh"
#include "TLGCalDlg.hh"
#include "TLGSave.hh"
#include <cmath>
#if HAVE_ISFINITE
#define finite isfinite
#elif defined(P__SOLARIS)
#include <ieeefp.h>
#elif defined (P__LINUX)
// #define finite isfinite
#elif defined (P__DARWIN)
//#define finite isfinite
#elif defined (P__WIN32)
// OK
#else
#error "Need finite/isfinite macro"
#endif
#include <RVersion.h>
#include <TMath.h>
#include <TGFileDialog.h>
#include <TGMsgBox.h>
#include <TVirtualX.h>
#include <set>
#include <iostream>
#include <fstream>
#include <iomanip>
#include <cstring>
#include <cstdio>
#include <unistd.h>
#include <strings.h>

namespace dttgui {
   using namespace std;
   using namespace xml;

//______________________________________________________________________________
   static const char* const gExportTypes[] = { 
   "ASCII", "*.txt",
   "Binary", "*.dat",
   "XML", "*.xml",
   "All files", "*",
   0, 0 };


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Set default export options                                           //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   void SetDefaultExportOptions (ExportOption_t& ex)
   {
      ex.fFilename = "";
      ex.fPlotType = "";
      for (int i = 0; i < kMaxExportColumn; i++) {
         ex.fColumn[i].fInclude = kFALSE;
         ex.fColumn[i].fAChn = "";
         ex.fColumn[i].fBChn = "";
         ex.fColumn[i].fTypeConversion = kETypeAsIs;
      }
      ex.fOutputType = kEOutTypeASCII;
      ex.fXY = kTRUE;
      ex.fXComplex = kFALSE;
      ex.fZeroTime = kTRUE;
      ex.fColumnMajor = kFALSE;
      ex.fSeparateFiles = kFALSE ;
      ex.fWriteOptionHeader = kFALSE ;
      ex.fZeroPadding = kFALSE;
      ex.fBinDouble = kFALSE;
      int test = 0;
      *(char*) &test = 1;
      ex.fBinLittleEndian = (test == 1);
      ex.fXMLAll = kFALSE;
      ex.fXMLReplace = kTRUE;
      ex.fXMLIncludeCal = kTRUE;
      ex.fStart = 0;
      ex.fMax = 1000000;
      ex.fBin = 1;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Set default import options                                           //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   void SetDefaultImportOptions (ImportOption_t& im)
   {
      im.fFilename = "";
      im.fPlotType = "";
      for (int i = 0; i < kMaxExportColumn; i++) {
         im.fColumn[i].fInclude = kFALSE;
         im.fColumn[i].fAChn = "";
         im.fColumn[i].fBChn = "";
         im.fColumn[i].fTypeConversion = kETypeAsIs;
      }
      im.fOutputType = kEOutTypeASCII;
      im.fXY = kTRUE;
      im.fXComplex = kFALSE;
      im.fZeroTime = kTRUE;
      im.fColumnMajor = kFALSE;
      im.fSeparateFiles = kFALSE ;
      im.fWriteOptionHeader = kFALSE ;
      im.fZeroPadding = kFALSE;
      im.fBinDouble = kFALSE;
      int test = 0;
      *(char*) &test = 1;
      im.fBinLittleEndian = (test == 1);
      im.fXMLAll = kTRUE;
      im.fXMLReplace = kTRUE;
      im.fXMLIncludeCal = kTRUE;
      im.fStart = 0;
      im.fMax = 1000000;
      im.fBin = 1;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Column information                                                   //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   class colinfo_t {
   public:
      /// valid info?
      Bool_t		fValid;
      /// Pointer to plot descriptor
      const PlotDescriptor*	fPDesc;
      /// Length of original data
      Int_t		fDataN;
      /// Original data array complex ?
      Bool_t		fDataComplex;
      /// Destination array complex ?
      Bool_t		fDestComplex;
      /// Conversion type
      EExportTypeConversion fConv;
      /// Float pointer to first element of source
      Float_t*		fFData;
      /// Double pointer to first element of source
      Double_t*         fDData;
      /// Number of points in dest which originate from source
      Int_t		fLen;
      /// Float pointer to destination
      Float_t*		fFDest;
      /// Double pointer to destination
      Double_t*		fDDest;
      /// Stride value
      Int_t		fStride;
      /// upsample value
      Int_t		fUp;
      /// bin value
      Int_t		fBin;
   
      /// Default constructor
      colinfo_t () 
      : fValid (kTRUE), fPDesc (0), fDataN (0), fDataComplex (kFALSE),
      fDestComplex (kFALSE), fConv (kETypeAsIs), fFData (0), fDData(0), 
      fLen (0), fFDest (0), fDDest (0), fStride (1), fUp (1), fBin (1) {
      }
   };



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Conversion routines                                                  //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
template <class f>
   inline void DoConversion (f* dest, f* src, EExportTypeConversion conv,
                     Bool_t dcmplx, Bool_t scmplx, f& prev) 
   {
      f y1 = 0;
      f y2 = 0;
      f* p = 0;
      f lim = 0;
      // check if valid datum
      if (!finite (src[0]) || (scmplx && !finite (src[1]))) {
         return;
      }
      // convert
      switch (conv) {
         case kETypeAsIs: 
         default:
            {
               y1 = src[0];
               if (scmplx) {
                  y2 = src[1];
               }
               break;
            }
         case kETypeMagnitude: 
            {
               if (scmplx) {
                  y1 = TMath::Sqrt (src[0]*src[0] + src[1]*src[1]);
               }
               else {
                  y1 = src[0];
               }
               break;
            }
         case kETypedBMagnitude: 
            {
               if (scmplx) {
                  f x = TMath::Sqrt (src[0]*src[0] + src[1]*src[1]);
                  if (x > 0) {
                     y1 =  20. * TMath::Log10 (x);
                  }
                  else {
                     y1 =  -1000.;
                  }
               }
               else {
                  f x = fabs (src[0]);
                  if (x > 0) {
                     y1 = 20. * TMath::Log10 (x);
                  }
                  else {
                     y1 = -1000.;
                  }
               }
               break;
            }
         case kETypeReal: 
            {
               y1 = src[0];
               break;
            }
         case kETypeImaginary: 
            {
               y1 = scmplx ? src[1] : 0.;
               break;
            }
         case kETypePhaseDeg: 
         case kETypePhaseDegCont: 
            {
               y1 = scmplx ? 
                  180. / TMath::Pi() * TMath::ATan2 (src[1], src[0]) : 0.;
               p = &y1;
               lim = 180.;
               break;
            }
         case kETypePhaseRad: 
         case kETypePhaseRadCont: 
            {
               y1 = scmplx ? TMath::ATan2 (src[1], src[0]) : 0.;
               p = &y1;
               lim = TMath::Pi();
               break;
            }
         case kETypeComplex: 
            {
               y1 = src[0];
               y2 = scmplx ? src[1] : 0.;
               break;
            }
         case kETypeComplexAlt1: 
         case kETypeComplexAlt3: 
            {
               if (scmplx) {
                  f x = TMath::Sqrt (src[0]*src[0] + src[1]*src[1]);
                  if (x > 0) {
                     y1 =  20. * TMath::Log10 (x);
                  }
                  else {
                     y1 =  -1000.;
                  }
               }
               else {
                  f x = fabs (src[0]);
                  if (x > 0) {
                     y1 = 20. * TMath::Log10 (x);
                  }
                  else {
                     y1 = -1000.;
                  }
               }
               y2 = scmplx ? 
                  180. / TMath::Pi() * TMath::ATan2 (src[1], src[0]) : 
                  (src[0] < 0 ? 180. : 0.);
               p = &y2;
               lim = 180.;
               break;
            }
         case kETypeComplexAlt2:
         case kETypeComplexAlt4: 
            {
               if (scmplx) {
                  y1 = TMath::Sqrt (src[0]*src[0] + src[1]*src[1]);
               }
               else {
                  y1 = fabs (src[0]);
               }
               y2 = scmplx ? TMath::ATan2 (src[1], src[0]) : 
                  (src[0] < 0 ? TMath::Pi() : 0.);
               p = &y2;
               lim = TMath::Pi();
               break;
            }
      }
      // check for continuous phase
      switch (conv) {
         case kETypePhaseDegCont:
         case kETypeComplexAlt3:
         case kETypePhaseRadCont:
         case kETypeComplexAlt4: 
            {
               f dy = prev - *p;
               if ((dy < lim) || (dy > lim)) {
                  f jump = dy / (2. * lim);
                  if (TMath::Abs (jump - TMath::Floor (jump)) < 0.5) {
                     jump = TMath::Floor (jump);
                  }
                  else {
                     jump = TMath::Ceil (jump);
                  }
                  *p += 2. * lim * jump;
               }
               prev = *p;
               break;
            }
         default:
            break;
      }
      // write result back to destination
      dest[0] = y1;
      if (dcmplx) {
         dest[1] = y2;
      }
   }

//______________________________________________________________________________

template <class f>
   int EConvert (colinfo_t& info, f* data, f* y)
   {
      int srcStride = info.fDataComplex ? 2 : 1;
      // upsample
      //      Float_t * x = info.fFData;
      f* x = data;
      if (info.fUp > 1) {
      //         x = new Float_t[info.fUp * info.fLen * srcStride]; 
         x = new f [info.fUp * info.fLen * srcStride]; 
         if (x == 0) {
            return 0;
         }
         int dest = 0;
         int src = 0;
         for (int i = 0; i < info.fLen; i++, src += srcStride) {
            for (int j = 0; j < info.fUp; j++, dest += srcStride) {
            //               x[dest] = info.fFData[src];
               x[dest] = data[src];
               if (info.fDataComplex) {
               //                  x[dest+1] = info.fFData[src+1];
                  x[dest+1] = data[src+1];
               }
            }
         }
      } 
      // loop over data points
      f pt[2];
      f prev = 0;
      int dest = 0;
      int src = 0;
      for (int ofs = 0; ofs < info.fLen; ofs++, dest += info.fStride) {
      // bin
         pt[0] = 0; pt[1] = 0;
         for (int j = 0; j < info.fBin; j++, src += srcStride) {
            pt[0] += x[src];
            if (info.fDataComplex) {
               pt[1] += x[src+1];
            }
         }
         if (info.fBin > 1) {
            pt[0] /= (f)info.fBin;
            pt[1] /= (f)info.fBin;
         }
      // convert and store result
         DoConversion (y + dest, pt, info.fConv,
                      info.fDestComplex, info.fDataComplex, prev);
      }
   
      // cleanup and return
      if (info.fUp > 1) {
         delete [] x;
      }
      return info.fLen;
   }

//______________________________________________________________________________
template <class f>
   int HistEConvert (colinfo_t& info, f* data, f* y, Int_t start, Int_t nbin, Bool_t xedge)
   {
      f pt[2];
      pt[0] = 0; pt[1] = 0;
      f prev = 0;
      int dest = 0;
      if (xedge) { // histogram bin edges
         for (int i = 0; i < info.fLen; i++, dest += info.fStride) {
            if (i == 0) pt[0] = 0;
            else if (i < info.fLen - 1)
               pt[0] = data[start + (i - 1) * info.fBin];
            else pt[0] = data[start + nbin];
            DoConversion (y + dest, pt, info.fConv, kFALSE, kFALSE, prev);      
         }
      }
      else { // histogram contents
         int k = 1;
         int b = 0;
         f tmp = 0;
         f* x = new f[info.fLen];
         if (x == 0) 
            return 0;
         memset(x,0,info.fLen * sizeof(f)); // clear temporary array
      
         for (int i = 0; i < info.fDataN; i++) {
            if (i < start) x[0] += data[i]; // underflow
            else if (i < start + nbin) { // bin contents
               if (b == info.fBin) {
                  x[k] = tmp;
                  b = 0;
                  tmp = 0;
                  k++;
               }
               tmp += data[i];
               b++;
            }
            else if (i == start + nbin) { // last bin & overflow
               x[k] = tmp;
               x[info.fLen - 1] = data[i];
            }
            else x[info.fLen - 1] += data[i]; // overflow
         }
      
         for (int i = 0; i < info.fLen; i++, dest += info.fStride) {
            pt[0] = x[i];
            DoConversion (y + dest, pt, info.fConv, kFALSE, kFALSE, prev);
         }
      
         if (x) delete[] x;
      }
   
      return info.fLen;
   }


//______________________________________________________________________________
   inline void swapDouble (Double_t* ww)
   {
     union c_dbl {
       double d;
       char   c[8];
     } in, out;
     in.d = *ww;
     out.c[0] = in.c[7];
     out.c[1] = in.c[6];
     out.c[2] = in.c[5];
     out.c[3] = in.c[4];
     out.c[4] = in.c[3];
     out.c[5] = in.c[2];
     out.c[6] = in.c[1];
     out.c[7] = in.c[0];
     *ww = out.d;
   }

//______________________________________________________________________________
   inline void swapFloat (Float_t* w)
   {
     union c_flt {
       double f;
       char   c[4];
     } in, out;
     in.f = *w;
     out.c[0] = in.c[3];
     out.c[1] = in.c[2];
     out.c[2] = in.c[1];
     out.c[3] = in.c[0];
     *w = out.f;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Write calibration records                                            //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   typedef set<string> calnamelist;

//______________________________________________________________________________
   int writeCal (ostream& os, const calnamelist& names,
                const calibration::Table* caltable)
   {
      int num = 0;
      if (caltable) {
         string s;
         for (calnamelist::const_iterator i = names.begin(); 
             i != names.end(); ++i) {
            for (int index = caltable->FindFirst (i->c_str());
                (index != -1) && (index < caltable->Len()) &&
                strcasecmp (i->c_str(), (*caltable)[index].GetChannel()) == 0; 
                ++index) {
               if ((*caltable)[index].Xml (CALNORMAL, s, num++)) {
                  os << s;
               }
            }
         }
      }
      return num;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Export to file (XML)                                                 //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   Bool_t ExportToFileXML (ExportOption_t& ex, PlotSet& pl,
                     const calibration::Table* caltable)
   {
     // XML here
      calnamelist names;
      if (ex.fXMLAll && ex.fXMLIncludeCal && caltable) {
         for (PlotSet::const_iterator i = ((const PlotSet&)pl).begin(); 
             i != ((const PlotSet&)pl).end(); i++) {
            const char* n = i->GetAChannel();
            if (n) names.insert (string (n));
            n = i->GetBChannel();
            if (n) names.insert (string (n));
         }
      }
     // save data to file
      Bool_t ret = kTRUE;
      if (ex.fXMLAll && !ex.fSeparateFiles) { // save all in one file (XML)
         std::ofstream	out (ex.fFilename);
         if (!out) {
            return kFALSE;
         }
         out << xsilHeader() << endl;
         // write cal
         if (ex.fXMLIncludeCal && caltable) {
            for (PlotSet::const_iterator i = ((const PlotSet&)pl).begin(); 
                i != ((const PlotSet&)pl).end(); i++) {
               const char* n = i->GetAChannel();
               if (n) names.insert (string (n));
               n = i->GetBChannel();
               if (n) names.insert (string (n));
            }
            writeCal (out, names, caltable);
         }
         // write data
         out << pl << endl;
         out << xsilTrailer() << endl;
         out.close();
      }
      else if (ex.fXMLAll && ex.fSeparateFiles) { // save all in separate files (XML)
         for (PlotSet::const_iterator i = ((const PlotSet&)pl).begin(); 
             i != ((const PlotSet&)pl).end(); i++) {
            i->SetMarked();
         }
         int resnum = 0;
         int refnum = 0;
         int count = 0;
         string fname;
         char ext[5];
         for (PlotSet::const_iterator i = ((const PlotSet&)pl).begin(); 
             i != ((const PlotSet&)pl).end(); i++) {
            if (i->IsCalculated() || !i->IsMarked()) {
               continue;
            }
            fname = (const char*)ex.fFilename;
            sprintf(ext,".%d",count);
            fname += ext;
            std::ofstream	out (fname.c_str());
            if (!out) {
               return kFALSE;
            }
            out << xsilHeader() << endl;
            // write cal
            if (ex.fXMLIncludeCal && caltable) {
               names.clear();
               const char* n = i->GetAChannel();
               if (n) names.insert (string (n));
               n = i->GetBChannel();
               if (n) names.insert (string (n));
               writeCal (out, names, caltable);
            }
            // write data
            writePlotDescriptor(out,*i,resnum,refnum,true);
            out << endl;
            out << xsilTrailer() << endl;
            out.close();
            count++;
         }
      }
      else if (!ex.fXMLAll && !ex.fSeparateFiles) { // save selected data in one file (XML)
      
         std::ofstream	out (ex.fFilename);
         if (!out) {
            return kFALSE;
         }
      
         // mark plot descriptors
         for (PlotSet::const_iterator i = ((const PlotSet&)pl).begin(); 
             i != ((const PlotSet&)pl).end(); i++) {
            i->SetMarked();
         }
      
         out << xsilHeader() << endl;
         // write calibration
         if (ex.fXMLIncludeCal && caltable) {
            for (int i = 0; i < kMaxExportColumn; i++) {
               if (!ex.fColumn[i].fInclude) {
                  continue;
               }
               names.insert (string(ex.fColumn[i].fAChn.Data()));
               if (ex.fColumn[i].fBChn.Length() != 0) {
		 names.insert (string(ex.fColumn[i].fBChn.Data()));
               }
            }
            writeCal (out, names, caltable);
         }
         // write data
         int resnum = 0;
         int refnum = 0;
         for (int i = 0; i < kMaxExportColumn; i++) {
            if (!ex.fColumn[i].fInclude) {
               continue;
            }
            // get plot descriptor
            const PlotDescriptor* pd = 
               pl.Get (ex.fPlotType, ex.fColumn[i].fAChn, 
                      (ex.fColumn[i].fBChn.Length() == 0) ? 
                      0 : (const char*)ex.fColumn[i].fBChn);
            // check dirty to avoid writing ref. objects multiple times
            if ((pd == 0) || !pd->IsMarked()) {
               continue;
            }
            if (!writePlotDescriptor (out, *pd, resnum, refnum)) {
               ret = kFALSE;
            }
            out << endl;
            pd->SetMarked (kFALSE);
            if (!out) {
               ret = kFALSE;
               break;
            }
         }
      
         out << xsilTrailer() << endl;
         out.close();
      
      }
      else if (!ex.fXMLAll && ex.fSeparateFiles) { // save selected data in separete files (XML)
      
       // mark plot descriptors
         for (PlotSet::const_iterator i = ((const PlotSet&)pl).begin(); 
             i != ((const PlotSet&)pl).end(); i++) {
            i->SetMarked();
         }
      
         int resnum = 0;
         int refnum = 0;
         string fname;
         char ext[5];
         for (int i = 0; i < kMaxExportColumn; i++) {
            if (!ex.fColumn[i].fInclude) {
               continue;
            }
            // get plot descriptor
            const PlotDescriptor* pd = 
               pl.Get (ex.fPlotType, ex.fColumn[i].fAChn, 
                      (ex.fColumn[i].fBChn.Length() == 0) ? 
                      0 : (const char*)ex.fColumn[i].fBChn);
            // check dirty to avoid writing ref. objects multiple times
            if ((pd == 0) || !pd->IsMarked()) {
               continue;
            }
            fname = (const char*)ex.fFilename;
            sprintf(ext,".%d",i);
            fname += ext;
            std::ofstream	out (fname.c_str());
            if (!out) {
               return kFALSE;
            }
            out << xsilHeader() << endl;
            // write cal
            if (ex.fXMLIncludeCal && caltable) {
               names.clear();
               const char* n = pd->GetAChannel();
               if (n) names.insert (string (n));
               n = pd->GetBChannel();
               if (n) names.insert (string (n));
               writeCal (out, names, caltable);
            }
            // write data
            if (!writePlotDescriptor (out, *pd, resnum, refnum)) {
               ret = kFALSE;
            }
            out << endl;
            out << xsilTrailer() << endl;
            out.close();
         
            pd->SetMarked (kFALSE);
            if (!out) {
               ret = kFALSE;
               break;
            }
         }
      
      }
   
      return ret;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Write Options to ASCII file                                          //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
// JCB
// The lines written to the top of the ASCII file are all preceded with
// a "# " to make it easy to strip the header off using grep.
   void WriteFileHeader(ostream &out, ExportOption_t *ex)
   {
      int nchans = 0 ;
      int maxAnamelen = 0 ;
      int maxBnamelen = 0 ;
      int bChansUsed = 0 ;
      int i,j ;

      for(i = 0; i < kMaxExportColumn; i++)
      {
	 if (ex->fColumn[i].fInclude)
	 {
	    nchans++ ; 
	    maxAnamelen = max(maxAnamelen, ex->fColumn[i].fAChn.Length()) ;
	    if (!ex->fColumn[i].fBChn.IsNull())
	    {
	       bChansUsed = 1 ;
	       maxBnamelen = max(maxBnamelen, ex->fColumn[i].fBChn.Length()) ;
	    }
	 }
      }
      out << "# Filename:      " << ex->fFilename.Data() << endl ;
      out << "# Plot type:     " << ex->fPlotType.Data() << endl ;
      out << "# Output Format: " ;
      if (ex->fXY)
	 out << "XY  " ;
      if (ex->fXComplex)
	 out << "X complex  " ;
      if (ex->fZeroTime)
	 out << "Zero time  " ;
      if (ex->fColumnMajor)
	 out << "Column major  " ;
      if (ex->fSeparateFiles)
	 out << "Save separate  " ;
      out << endl ;

      // Write the channel names
      out << "# Channels:" << setw(maxAnamelen/2 + 2) << "A" ;
      if (bChansUsed)
      {
	 out << setw(maxAnamelen/2 + maxBnamelen/2 + 2) << "B" ;
	 out << setw(maxBnamelen/2 + 14) << "Conversion" << endl ;
      }
      else
      {
	 out << setw(maxAnamelen/2 + 14) << "Conversion" << endl ;
      }
      out << setiosflags(ios::left) ; // Left justify the channel names.
      for(i = 0, j = 0; i < kMaxExportColumn; i++)
      {
	 if (ex->fColumn[i].fInclude)
	 {
	    out << "# Column " << setw(2) << j+1 << "  " << setw(maxAnamelen+2) << ex->fColumn[i].fAChn.Data() ;
	    if (bChansUsed)
	       out << "  " << setw(maxBnamelen) << ex->fColumn[i].fBChn.Data() ;
	    switch (ex->fColumn[i].fTypeConversion)
	    {
	       case kETypeMagnitude:
		     out << "  Magnitude" ;
		     break ;
	       case kETypedBMagnitude:
		     out << "  dB Magnitude" ;
		     break ;
	       case kETypeReal:
		     out << "  Real" ;
		     break ;
	       case kETypeImaginary:
		     out << "  Imaginary" ;
		     break ;
	       default:
	       case kETypeAsIs:
		     out << "  As is" ;
		     break ;
	       case kETypePhaseDeg:
		     out << "  Phase (degree)" ;
		     break ;
	       case kETypePhaseRad:
		     out << "  Phase (rad)" ;
		     break ;
	       case kETypePhaseDegCont:
		     out << "  Continuous phase (degree)" ;
		     break ;
	       case kETypePhaseRadCont:
		     out << "  Continuous phase (rad)" ;
		     break ;
	       case kETypeComplex:
		     out << "  Complex (re/im)" ;
		     break ;
	       case kETypeComplexAlt1:
		     out << "  Complex (dB/degree)" ;
		     break ;
	       case kETypeComplexAlt2:
		     out << "  Complex (abs/rad)" ;
		     break ;
	       case kETypeComplexAlt3:
		     out << "  Complex (dB/cont. degree)" ;
		     break ;
	       case kETypeComplexAlt4:
		     out << "  Complex (abs/cont. rad)" ;
		     break ;
	    }

	    out << endl ;
	    j++ ;
	 }
      }
      out << "# Data:" << endl ;
   }

//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Export to file (ASCII & Binary)                                      //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   Bool_t ExportToFileASCII (ExportOption_t& ex, PlotSet& pl)
   {
      colinfo_t*	 cinfo;
      int		 len;
      Float_t*     fdatatmp = 0;
      Double_t*    ddatatmp = 0;
    // ASCII and binary here
    // determine # of columns
      len = 0;
      for (int i = 0; i < kMaxExportColumn; i++) {
         if (ex.fColumn[i].fInclude) len++;
      }
    // allocate memory for column info (index 0 for X)
      cinfo = new colinfo_t[len+1];
      if (cinfo == 0) {
         return kFALSE;
      }
    // gather info
      int index = 0;
      int maxlength = 0;
      int xindex = 0;
      for (int i = 0; i < kMaxExportColumn; i++) {
         if (ex.fColumn[i].fInclude) {
            index++;
         // get plot descriptor
            cinfo[index].fPDesc = 
               pl.Get (ex.fPlotType, ex.fColumn[i].fAChn, 
                      (ex.fColumn[i].fBChn.Length() == 0) ? 
                      0 : (const char*)ex.fColumn[i].fBChn);
            if (cinfo[index].fPDesc == 0) {
               cinfo[index].fValid = kFALSE;
               continue;
            }
         // get length, conversion and complex
         
            if (ex.fPlotType == kPTHistogram1) { // histogram
               cinfo[index].fDataN = cinfo[index].fPDesc->GetN()+2;
               cinfo[index].fDataComplex = cinfo[index].fPDesc->IsComplex();
            }
            else {
               cinfo[index].fDataN = cinfo[index].fPDesc->GetN();
               cinfo[index].fDataComplex = cinfo[index].fPDesc->IsComplex();
            }
            cinfo[index].fConv = ex.fColumn[i].fTypeConversion;
            if (cinfo[index].fConv == kETypeAsIs) {
               cinfo[index].fDestComplex = cinfo[index].fDataComplex;
            }
            else if (cinfo[index].fConv >= kETypeComplex) {
               cinfo[index].fDestComplex = kTRUE;
            }
            else {
               cinfo[index].fDestComplex = kFALSE;
            }
         
         
         
            if (cinfo[index].fDataN <= 0) {
               cinfo[index].fValid = kFALSE;
               continue;
            }
            if (cinfo[index].fDataN > maxlength) {
               maxlength = cinfo[index].fDataN;
               xindex = index;
            }
         }
      }
   
    // set x info
      if ((xindex == 0) || (maxlength == 0)) {
         delete [] cinfo;
         return kFALSE;
      }
      cinfo[0].fPDesc = cinfo[xindex].fPDesc;
      cinfo[0].fDataN = cinfo[xindex].fDataN;
      cinfo[0].fDataComplex = kFALSE;
      cinfo[0].fDestComplex = ex.fXComplex;
      cinfo[0].fConv = ex.fXComplex ? kETypeComplex : kETypeReal;
    // calculate number of points
      if (ex.fStart < 0) ex.fStart = 0;
      if (ex.fBin < 1) ex.fBin = 1;
      if (ex.fMax < 0) ex.fMax = 0;
   
      int ptData;
      int ptDest;
      int startbin = ex.fStart;  // hist mito
      int nbin = 0;  // hist mito
      if (ex.fPlotType == kPTHistogram1) {
         if (startbin < 1) startbin = 1;
         ptData = maxlength - startbin + 1;
         if (ptData > ex.fMax + 2) {
            ptData = ex.fMax + 2;
            nbin = ex.fMax;
         }
         else {
            nbin = ptData - 2;
         }
         ptDest = (ptData - 2) / ex.fBin + 
            (((ptData - 2) % ex.fBin) > 0) + 2;
      }
      else {
         ptData = maxlength - ex.fStart;
         if (ptData > ex.fMax) {
            ptData = ex.fMax;
         }
         ptDest = ptData / ex.fBin;
      }
      if (ptDest <= 0) {
         delete [] cinfo;
         return kFALSE;
      }
   
    // calculate number of columns
      int ptCol = 0;
      for (int i = (ex.fXY ? 0 : 1); i < len + 1; i++) {
         ptCol += cinfo[i].fDestComplex ? 2 : 1;
      }
    // allocate memory for exported data
      int pts = ptCol * ptDest;
      Bool_t dbl = ex.fBinDouble;
      Double_t* ddata = 0;
      Float_t* fdata = 0;
      if (dbl) {
         ddata = new Double_t [pts];
         if (ddata == 0) {
            delete [] cinfo;
            return kFALSE;
         }
      }
      else {
         fdata = new Float_t [pts];
         if (fdata == 0) {
            delete [] cinfo;
            return kFALSE;
         }
      }
    // setup column pointers
      Bool_t colmaj = ex.fColumnMajor;
    // Bool_t zeropad = ex.fZeroPadding;
      int col = 0;
   
      if (ex.fPlotType == kPTHistogram1) {
         for (int i = (ex.fXY ? 0 : 1); i < len + 1; i++) {
            if (cinfo[i].fValid) {
               cinfo[i].fUp = 1;
               cinfo[i].fBin = ex.fBin;
               int first = startbin;
               if (first < cinfo[i].fDataN) {
                  cinfo[i].fLen = ptDest;
               }
               else {
                  first = 0;
                  cinfo[i].fLen = 0;
               }
            
            // fill temp array with histogram data
               if (dbl) {
                  ddatatmp = new Double_t[cinfo[i].fDataN];
                  if (i > 0) memcpy(ddatatmp,cinfo[i].fPDesc->GetData()->GetBinContents(),
                                   cinfo[i].fDataN * sizeof(Double_t));
                  else {
                     ddatatmp[0] = 0;
                     for (Int_t j = 1; j < cinfo[i].fDataN; j++) {
                        ddatatmp[j] = cinfo[i].fPDesc->GetData()->GetXBinEdges()[j-1];
                     }
                  }
               }
               else {
                  fdatatmp = new Float_t[cinfo[i].fDataN];
                  if (i > 0) {
                     for (Int_t j = 0; j < cinfo[i].fDataN; j++) {
                        fdatatmp[j] = (Float_t)cinfo[i].fPDesc->GetData()->GetBinContents()[j];
                     }
                  }
                  else {
                     fdatatmp[0] = 0;
                     for (Int_t j = 1; j < cinfo[i].fDataN; j++) {
                        fdatatmp[j] = (Float_t)cinfo[i].fPDesc->GetData()->GetXBinEdges()[j-1];
                     }
                  }
               }
            
            // set destination pointer
               int ofs = colmaj ? col * ptDest : col;
               if (dbl) {
                  cinfo[i].fDDest = ddata + ofs;
               }
               else {
                  cinfo[i].fFDest = fdata + ofs;
               }
               cinfo[i].fStride = colmaj ?  1 : ptCol;
               col++;
            
            // convert and copy histogram data to buffer
               if (dbl) {
                  HistEConvert (cinfo[i], ddatatmp, cinfo[i].fDDest, 
                               startbin, nbin, (i == 0));
               }
               else {
                  HistEConvert (cinfo[i], fdatatmp, cinfo[i].fFDest, 
                               startbin, nbin, (i == 0));
               }
            
               if (ddatatmp) delete[] ddatatmp;
               ddatatmp = 0;
               if (fdatatmp) delete[] fdatatmp;
               fdatatmp = 0;
            
            }
         }
      
      
      // print info for debugging
         cout << "len = " << len << endl;
         cout << "ptCol = " << ptCol << endl;
         cout << "ptDest = " << ptDest << endl;
         cout << "prec = " << (dbl ? "double" : "single") << endl;
         cout << "format = " << (colmaj ? "column" : "row") << " major" << endl;
         cout << "max length = " << maxlength << endl;
         cout << "x index = "  << xindex << endl;
         for (int i = 0; i < len + 1; i++) {
            cout << "************ info record " << i <<
               " (" << (cinfo[i].fValid ? "valid)" : "invalid)") << endl;
            cout << "  DataN = " << cinfo[i].fDataN << endl;
            cout << "  DataComplex = " << (cinfo[i].fDataComplex ? "yes" : "no") << endl;
            cout << "  DestComplex = " << (cinfo[i].fDestComplex ? "yes" : "no") << endl;
            cout << "  Conversion = " << (int)cinfo[i].fConv << endl;
            cout << "  Len = " << cinfo[i].fLen << endl;
            cout << "  Stride = " << cinfo[i].fStride << endl;
            cout << "  Up = " << cinfo[i].fUp << endl;
            cout << "  Bin = " << cinfo[i].fBin << endl;
         }
      
      
      }
      else {
         for (int i = (ex.fXY ? 0 : 1); i < len + 1; i++) {
            if (cinfo[i].fValid) {
            //  	// set upsample & bin
            //  	if ((cinfo[i].fDataN == maxlength) || zeropad) {
            //  	  cinfo[i].fUp = 1;
            //  	  cinfo[i].fBin = ex.fBin;
            //  	} 
            //  	else {
            //  	  cinfo[i].fUp = (Int_t)((double)maxlength / cinfo[i].fDataN + 0.5);
            //  	  cinfo[i].fBin = ex.fBin;
            //  	}
            //  	// set first data point & length
            //  	int first = ex.fStart / cinfo[i].fUp;
            //  	if (first < cinfo[i].fDataN) {
            //  	  cinfo[i].fLen = 
            //  	    (cinfo[i].fDataN - first) * cinfo[i].fUp / cinfo[i].fBin;
            //  	  if (cinfo[i].fLen > ptDest) {
            //  	    cinfo[i].fLen = ptDest;
            //  	  }
            //  	}
            //  	else {
            //  	  first = 0;
            //  	  cinfo[i].fLen = 0;
            //  	}
            //  	cinfo[i].fFData = (i > 0) ? 
            //  	  cinfo[i].fPDesc->GetY() : cinfo[i].fPDesc->GetX();
            //  	if (cinfo[i].fFData == 0) {
            //  	  cinfo[i].fValid = kFALSE;
            //  	}
            //  	else {
            //  	  cinfo[i].fFData += first;
            //  	}
            //  	// check if upsample & bin are multiples of each other 
            //  	if ((cinfo[i].fUp > 1) && (cinfo[i].fBin > 1)) {
            //  	  if ((cinfo[i].fUp >= cinfo[i].fBin) &&
            //  	      (cinfo[i].fUp % cinfo[i].fBin == 0)) {
            //  	    cinfo[i].fUp /= cinfo[i].fBin;
            //  	    cinfo[i].fBin = 1;
            //  	  }
            //  	  else if ((cinfo[i].fUp < cinfo[i].fBin) &&
            //  		   (cinfo[i].fBin % cinfo[i].fUp == 0)) {
            //  	    cinfo[i].fBin /= cinfo[i].fUp;
            //  	    cinfo[i].fUp = 1;
            //  	  }
            //  	}
            
               cinfo[i].fUp = 1;
               cinfo[i].fBin = ex.fBin;
               int first = ex.fStart;
               if (first < cinfo[i].fDataN) {
                  cinfo[i].fLen = (cinfo[i].fDataN - first) / cinfo[i].fBin;
                  if (cinfo[i].fLen > ptDest) {
                     cinfo[i].fLen = ptDest;
                  }
               }
               else {
                  first = 0;
                  cinfo[i].fLen = 0;
               }
            
               cinfo[i].fFData = (i > 0) ? 
                  cinfo[i].fPDesc->GetY() : cinfo[i].fPDesc->GetX();
               if (cinfo[i].fFData == 0) {
                  cinfo[i].fValid = kFALSE;
               }
               else {
                  cinfo[i].fFData += first;
               }
            }
         
         // set destination pointer
            int ofs = colmaj ? col * ptDest : col;
            if (dbl) {
               cinfo[i].fDDest = ddata + ofs;
            }
            else {
               cinfo[i].fFDest = fdata + ofs;
            }
            cinfo[i].fStride = 
               colmaj ? (cinfo[i].fDestComplex ? 2 : 1) : ptCol;
            col += cinfo[i].fDestComplex ? 2 : 1;
         }
      
      // print info for debugging
      // cout << "len = " << len << endl;
      // cout << "ptCol = " << ptCol << endl;
      // cout << "ptDest = " << ptDest << endl;
      // cout << "prec = " << (dbl ? "double" : "single") << endl;
      // cout << "format = " << (colmaj ? "column" : "row") << " major" << endl;
      // cout << "max length = " << maxlength << endl;
      // cout << "x index = "  << xindex << endl;
      // for (int i = 0; i < len + 1; i++) {
      // cout << "************ info record " << i <<
      // " (" << (cinfo[i].fValid ? "valid)" : "invalid)") << endl;
      // cout << "  DataN = " << cinfo[i].fDataN << endl;
      // cout << "  DataComplex = " << (cinfo[i].fDataComplex ? "yes" : "no") << endl;
      // cout << "  DestComplex = " << (cinfo[i].fDestComplex ? "yes" : "no") << endl;
      // cout << "  Conversion = " << cinfo[i].fConv << endl;
      // cout << "  Len = " << cinfo[i].fLen << endl;
      // cout << "  Stride = " << cinfo[i].fStride << endl;
      // cout << "  Up = " << cinfo[i].fUp << endl;
      // cout << "  Bin = " << cinfo[i].fBin << endl;
      // }
      
      // convert data and fill destination array
         for (int i = (ex.fXY ? 0 : 1); i < len + 1; i++) {
            int ofs = 0;
            if (cinfo[i].fValid) {
               if (dbl) {
                  ofs = EConvert (cinfo[i], cinfo[i].fDData, cinfo[i].fDDest);
               }
               else {
                  ofs = EConvert (cinfo[i], cinfo[i].fFData, cinfo[i].fFDest);
               }
            }
         // pad zeros if necessary
            int addr = ofs * cinfo[i].fStride;
            for (; ofs < ptDest; ofs++, addr += cinfo[i].fStride) {
               if (dbl) {
                  cinfo[i].fDDest[addr] = 0.;
                  if (cinfo[i].fDestComplex) {
                     cinfo[i].fDDest[addr+1] = 0.;
                  }
               }
               else {
                  cinfo[i].fFDest[addr] = 0.;
                  if (cinfo[i].fDestComplex) {
                     cinfo[i].fFDest[addr+1] = 0.;
                  }
               }   
            }
         }
      
      }
   
    // save data to file
      Bool_t ret = kTRUE;
      ofstream	out (ex.fFilename);
      if (!out) {
         ret = kFALSE;
      }
      else {
         switch (ex.fOutputType) {
         // write ASCII file
            case kEOutTypeASCII:
            default:
               {   
	       // Write header if the user wanted it.  - JCB
	         if (ex.fWriteOptionHeader)
		 {
		    WriteFileHeader(out, &ex) ;
		 }
               // set precision
                  Int_t wid = dbl ? 16 : 12;
                  Int_t prec = dbl ? 12 : 8;
#if 0
// JCB - What is this???
                  if (dbl) {
                     out << setw (wid) << setprecision (prec);
                  }
                  else {
                     out << setw (12) << setprecision (8);
                  }
#else
		  out << setw (wid) << setprecision (prec);
#endif

               // column major
                  if (colmaj) {
                     for (int j = (ex.fXY ? 0 : 1); j < len + 1; j++) {
                        for (int i = 0; i < ptDest; i++) {
                           if (dbl) {
                              out << setw (wid) << setprecision (prec) << 
                                 cinfo[j].fDDest[i * cinfo[j].fStride];
                              if (cinfo[j].fDestComplex) {
                                 out << " " << setw (wid) << 
                                    setprecision (prec) << 
                                    cinfo[j].fDDest[i * cinfo[j].fStride + 1];
                              }
                           }
                           else {
                              out << setw (wid) << setprecision (prec) << 
                                 cinfo[j].fFDest[i * cinfo[j].fStride];
                              if (cinfo[j].fDestComplex) {
                                 out << " " << setw (wid) << 
                                    setprecision (prec) << 
                                    cinfo[j].fFDest[i * cinfo[j].fStride + 1];
                              }
                           }
                        }
                     }
                  }
                  // row major
                  else {
                     int addr = 0;
                     for (int i = 0; i < ptDest; i++) {
                        for (int j = 0; j < ptCol; j++, addr++) {
                           if (dbl) {
                              out << setw (wid) << setprecision (prec) << 
                                 ddata[addr];
                           }
                           else {
                              out << setw (wid) << setprecision (prec) << 
                                 fdata[addr];
                           }
                           if (j < ptCol - 1) {
                              out << " ";
                           }
                        }
                        if (i < ptDest - 1) {
                           out << endl;
                        }
                     }
                  }
                  ret = !!out;
                  break;
               }
            // Write binary file
            case kEOutTypeBinary:
               {
               // check if we have to swap byte-order
                  int test = 0;
                  *(char*) &test = 1;
                  Bool_t LittleEndian = (test == 1);
                  if ((LittleEndian != 0) != (ex.fBinLittleEndian != 0)) {
                     for (int i = 0; i < pts; i++) {
                        if (dbl) {
                           swapDouble (ddata + i);
                        }
                        else {
                           swapFloat (fdata + i);
                        }
                     }
                  }
               // dump to file
                  if (dbl) {
                     out.write ((char*) ddata, pts * sizeof (Double_t));
                  }
                  else {
                     out.write ((char*) fdata, pts * sizeof (Float_t));
                  }
                  ret = !!out;
                  break;
               }
            // Write XML file
            case kEOutTypeXML:
               {
                  ret = kFALSE;
                  break;
               }
         }
         out.close();
      }
   
      if (ddatatmp) delete[] ddatatmp;
      delete [] ddata;
      delete [] fdata;
      delete [] cinfo;
      return ret;
   }


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Conversion for reading values                                        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   void ReadNextValue (bool dbl, ifstream& inp, Bool_t cmplx, 
                     Bool_t swap, EExportTypeConversion conv,
                     double* res)
   {
      double tmp[2];
      if (dbl) {
         double r, i;
         inp.read ((char*) &r, sizeof (double));
         if (swap) swapDouble (&r);
         if (cmplx) {
            inp.read ((char*) &i, sizeof (double));
            if (swap) swapDouble (&i);
         }
         else {
            i = 0.;
         }
         tmp[0] = r;
         tmp[1] = i;
      }
      else {
         float r, i;
         inp.read ((char*) &r, sizeof (float));
         if (swap) swapFloat (&r);
         if (cmplx) {
            inp.read ((char*) &i, sizeof (float));
            if (swap) swapFloat (&i);
         }
         else {
            i = 0.;
         }
         tmp[0] = r;
         tmp[1] = i;
      }
      switch (conv) {
         case kETypeMagnitude:
         case kETypeReal:
         case kETypeAsIs:
         case kETypeComplex:
         default:
            {
               res[0] = tmp[0];
               res[1] = tmp[1];
               break;
            }
         case kETypedBMagnitude:
            {
               double x = TMath::Abs (tmp[0]);
               if (x > 0) {
                  x = TMath::Exp (TMath::Log(10.) * x / 20.);
               }
               res[0] = tmp[0];
               break;
            }
         case kETypeImaginary:
            {
               res[0] = tmp[1];
               res[1] = 0;
               break;
            }
         case kETypePhaseDeg:
         case kETypePhaseDegCont:
            {
               res[0] = tmp[0];
               res[1] = tmp[1];
               break;
            }
         case kETypePhaseRad:
         case kETypePhaseRadCont:
            {
               res[0] = tmp[0];
               res[1] = tmp[1];
               break;
            }
         case kETypeComplexAlt1:
         case kETypeComplexAlt3:
            {
               double x = TMath::Exp (TMath::Log(10.) * tmp[0] / 20.);
               double y = tmp[1] / 180. * TMath::Pi();
               res[0] = x * TMath::Cos(y);
               res[1] = x * TMath::Sin(y);
               break;
            }
         case kETypeComplexAlt2:
         case kETypeComplexAlt4:
            {
               res[0] = TMath::Abs (tmp[0]) * TMath::Cos(tmp[1]);
               res[1] = TMath::Abs (tmp[0]) * TMath::Sin(tmp[1]);
               break;
            }
      }
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// filterImport                                                         //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   void demangle (const char* name, string& stem, int& num, 
                 int& index1, int& index2, string& extension)
   {
      string n (name);
      string::size_type pos;
      if ((pos = n.rfind ('(')) == string::npos) {
         extension = "";
      }
      else {
         extension = n.substr (pos, string::npos);
         n.erase (pos);
      }
      xsilStd::analyzeName (n, stem, index1, index2);
      num = 0;
      if ((pos = stem.rfind ("_!_")) != string::npos) {
         bool numeric = true;
         const char* p = stem.c_str() + pos + 1;
         while (*p) {
            if (!isdigit (*p)) {
               numeric = false;
               break;
            }
            p++;
         }
         if (numeric) {
            num = atoi (stem.c_str() + pos + 1);
            stem.erase (pos);
         }
      }
      cout << "Demangled name " << num << " = " << stem << "[" << 
         index1 << "," << index2 << "]" << extension << endl;
   }

//______________________________________________________________________________
   void mangle (string& name, const string& stem, int num, 
               int index1, int index2, const string& extension)
   {
      cout << "mangel begin" << endl;
      name = stem;
      if (num >= 0) {
         char buf[256];
         sprintf (buf, "_!_%i", num);
         name += buf;
      }
      if (index1 >= 0) {
         char buf[256];
         sprintf (buf, "[%i]", index1);
         name += buf;
      }
      if (index2 >= 0) {
         char buf[256];
         sprintf (buf, "[%i]", index2);
         name += buf;
      }
      name += extension;
      cout << "Mangled name = " << name << endl;
   }

//______________________________________________________________________________
   bool nameMangling (const PlotSet& pset, const char* g,
                     string& aa, string&  bb)
   {
      const char* a = aa.c_str();
      const char* b = bb.empty() ? 0 : bb.c_str();
      // first check if duplicate
      if (pset.Get (g, a, b) == 0) {
         return false;
      }
      // demangle input names
      string aName;
      int ai1;
      int ai2;
      string aExt;
      string bName;
      int bi1;
      int bi2;
      string bExt;
      int num;
      cout << "demangel name 1" << endl;
      demangle (a, aName, num, ai1, ai2, aExt);
      if (b) {
         cout << "demangel name 2" << endl;
         demangle (b, bName, num, bi1, bi2, bExt);
      }
      int max = 0;
      cout << "demangel name 3" << endl;
      // check plot pool for largest mangled name
      for (PlotSet::const_iterator i = pset.begin(); 
          i != pset.end(); i++) {
         if (strcasecmp (g, i->GetGraphType()) != 0) {
            continue;
         }
         string paName;
         int pai1;
         int pai2;
         string paExt;
         string pbName;
         int pbi1;
         int pbi2;
         string pbExt;
         int pnum;
         demangle (i->GetAChannel(), paName, pnum, pai1, pai2, paExt);
         if (i->GetBChannel()) {
            demangle (i->GetBChannel(), pbName, pnum, pbi1, pbi2, pbExt);
         }
         // compare
         if ((strcasecmp (aName.c_str(), paName.c_str()) == 0) && 
            (ai1 == pai1) && (ai2 == pai2) && 
            (strcasecmp (aExt.c_str(), paExt.c_str()) == 0) &&
            ((b == 0) || (i->GetBChannel() == 0) ||
            ((strcasecmp (bName.c_str(), pbName.c_str()) == 0) && 
            (bi1 == pbi1) && (bi2 == pbi2) && 
            (strcasecmp (bExt.c_str(), pbExt.c_str()) == 0)))) {
            if (pnum > max) max = pnum;
         }
      }
      // mangle
      max++;
      mangle (aa, aName, max, ai1, ai2, aExt);
      if (b) {
         mangle (bb, bName, max, bi1, bi2, bExt);
      }
      return true;
   }

//______________________________________________________________________________
   class filterImport : public PlotSet::PlotFilter {
   protected:
      PlotSet*		fPlotSet;
      ExportOption_t*	fIm;
   public:
      filterImport (PlotSet& pset, ExportOption_t& im) 
      : fPlotSet (&pset), fIm (&im) {
      }
      virtual bool operator() (const PlotDescriptor& pd, string& g,
                        string& a, string& b) const
      {
         cout << "Include? " << g << " " << a << " " << b << endl;
         // include all
         if (fIm->fXMLAll) {
            // if not replace, apply name mangling
            if (!fIm->fXMLReplace) {
               nameMangling (*fPlotSet, g.c_str(), a, b);
            }
            return true;
         }
         // test if in import list
         else {
            for (int i = 0; i < kMaxExportColumn; i++) {
               if (!fIm->fColumn[i].fInclude) {
                  continue;
               }
               if ((strcasecmp (fIm->fPlotType, g.c_str()) == 0) &&
                  (strcasecmp (fIm->fColumn[i].fAChn, a.c_str()) == 0) &&
                  ((b.size() == 0) || 
                  (strcasecmp (fIm->fColumn[i].fBChn, b.c_str()) == 0))) {
                  // if not replace, apply name mangling
                  if (!fIm->fXMLReplace) {
                     nameMangling (*fPlotSet, g.c_str(), a, b);
                  }
                  return true;
               }
            }
            return false;
         }
      }
   };


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Import from file                                                     //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   Bool_t ImportFromFile (ExportOption_t& im, PlotSet& pl,
                     calibration::Table* caltable)
   {
      colinfo_t*	 cinfo;
      int		 len;
   
      // mark existing plot descriptors
      for (PlotSet::const_iterator i = ((const PlotSet&)pl).begin(); 
          i != ((const PlotSet&)pl).end(); i++) {
         i->SetMarked();
      }
   
      // XML here
      if (im.fOutputType == kEOutTypeXML) {
         int calsize = caltable ? caltable->Len() : 0;
         xsilParser xml;
         PlotSet ptemp;
         xsilHandlerQueryCalibration calQ (caltable, false);
         if (caltable && im.fXMLIncludeCal) {
            xml.AddHandler (calQ);
         }
         xsilHandlerQueryData dataQ (&ptemp, true, caltable);
         xml.AddHandler (dataQ);
         Bool_t ret = xml.ParseFile (im.fFilename);
         // transfer interesting plot desctriptors
         filterImport fi (pl, im);
         pl.Merge (ptemp, fi);
         // add units and compute aux for all unmarked plot descriptors
         vector<PlotDescriptor*> pds;
         for (PlotSet::iterator i = pl.begin(); i != pl.end(); i++) {
            if (i->IsMarked()) {
               // update units if caltable has changed!
               if (caltable && (calsize != caltable->Len())) {
                  caltable->AddUnits (i->Cal());
               }
               continue;
            }
            // this is a newly imported trace
            i->SetDirty();
            const DataCopy* dd = 
               dynamic_cast<const DataCopy*> (i->GetData());
            if (dd) ((DataCopy*)dd)->UnsetXY();
            TLGXMLRestorer::AddComputedTrace (pl, pds, *i, caltable);
            if (caltable) caltable->AddUnits (i->Cal());
         }
         // add computed traces to pool
         for (vector<PlotDescriptor*>::iterator i = pds.begin();
             i != pds.end(); i++) {
            (*i)->SetDirty();
            if (caltable) caltable->AddUnits ((*i)->Cal());
            pl.Add (*i);
         }
         return ret;
      }
   
   
      // ASCII and binary here
      // determine # of columns
      len = 0;
      for (int i = 0; i < kMaxExportColumn; i++) {
         if (im.fColumn[i].fInclude) len++;
      }
      // allocate memory for column info (index 0 for X)
      cinfo = new colinfo_t[len+1];
      if (cinfo == 0) {
         return kFALSE;
      }
      // gather info
      int index = 0;
      for (int i = 0; i < kMaxExportColumn; i++) {
         if (im.fColumn[i].fInclude) {
            index++;
            // make new plot descriptor (no data yet)
            cinfo[index].fValid = kTRUE;
            cinfo[index].fPDesc = 0;
            if ((im.fPlotType.Length() <= 0) || 
               (im.fColumn[i].fAChn.Length() <= 0)) {
               cinfo[index].fValid = kFALSE;
               continue;
            }
            BasicDataDescriptor* dd = 0;
            if (im.fPlotType == kPTHistogram1) 
               dd = new HistDataCopy(); // hist mito
            else dd = new DataCopy();
         
            if (dd == 0) {
               cinfo[index].fValid = kFALSE;
               continue;
            }
            ParameterDescriptor prm;
            cout << "i = " << i << " caltable? " << (caltable ? "yes" : "no") <<
               " plottype = " << im.fPlotType << " a = " << im.fColumn[i].fAChn << endl;
            calibration::Descriptor cal (Time (0,0), 
                                 caltable ? (const char*)im.fPlotType : 0, 
                                 im.fColumn[i].fAChn, 
                                 (im.fColumn[i].fBChn.Length() == 0) ? 
                                 0 : (const char*)im.fColumn[i].fBChn);
         
            if (im.fPlotType == kPTHistogram1) {
               cinfo[index].fPDesc = new PlotDescriptor (dd,
                                    im.fPlotType, im.fColumn[i].fAChn, 
                                    (im.fColumn[i].fBChn.Length() == 0) ? 
                                    0 : (const char*)im.fColumn[i].fBChn, &prm, 0);
            }
            else {
               cinfo[index].fPDesc = new PlotDescriptor (dd,
                                    im.fPlotType, im.fColumn[i].fAChn, 
                                    (im.fColumn[i].fBChn.Length() == 0) ? 
                                    0 : (const char*)im.fColumn[i].fBChn, &prm, &cal);
            }
         
            if (cinfo[index].fPDesc == 0) {
               cinfo[index].fValid = kFALSE;
               continue;
            }
            // set conversion and complex
            cinfo[index].fConv = im.fColumn[i].fTypeConversion;
            if (cinfo[index].fConv >= kETypeAsIs) {
               if ((im.fPlotType == kPTCrossCorrelation) || 
                  (im.fPlotType == kPTTransferFunction) ||
                  (im.fPlotType == kPTTransferCoefficients) ||
                  (im.fPlotType == kPTHarmonicCoefficients) ||
                  (im.fPlotType == kPTIntermodulationCoefficients)) {
                  cinfo[index].fDataComplex = kTRUE;
               }
               else {
                  cinfo[index].fDataComplex = kFALSE;
               }
            }
            else if (cinfo[index].fConv >= kETypeComplex) {
               cinfo[index].fDataComplex = kTRUE;
            }
            else {
               cinfo[index].fDataComplex = kFALSE;
            }
            cinfo[index].fDestComplex =  cinfo[index].fDataComplex;
         }
      }
      // set x info
      cinfo[0].fValid = im.fXY;
      cinfo[0].fPDesc = 0;
      cinfo[0].fDataComplex = im.fXComplex;
      cinfo[0].fDestComplex = kFALSE;
      cinfo[0].fConv = kETypeReal;
      // set length (ascii and binary)
      int maxlength = 0;
      Bool_t dbl = im.fBinDouble;
      TString filename = im.fFilename;
      Bool_t ret = kTRUE;
      switch (im.fOutputType) {
         case kEOutTypeASCII:
         default:
            {
               // convert ascii into binary
               ifstream	inp (im.fFilename);
               if (!inp) {
                  ret = kFALSE;
                  break;
               }

	       //-----------------------  Replace tmpnam 
	       char tmpfile[32];
	       strcpy(tmpfile, "/tmp/TLGExport-XXXXXX");
	       int tmpfd = mkstemp(tmpfile);
               filename = tmpfile;
	       //---------------------------------------

               ofstream	out (filename);
	       close(tmpfd); // close temp file fd.
               if (!out) {
                  ret = kFALSE;
                  break;
               }

               double x;
               while (inp >> x) {
                  out.write ((char*) &x, sizeof (double));
                  maxlength++;
               }
               dbl = kTRUE;
               if (maxlength <= 0) {
                  ret = kFALSE;
                  break;
               }
               cout << "new tmp file @ " << filename << endl;
               break;
            }

         case kEOutTypeBinary:
            {
               // get length from file size
               ifstream	inp (im.fFilename);
               if (!inp) {
                  ret = kFALSE;
                  break;
               }
               inp.seekg (0, ios::end);
               maxlength = inp.tellg() / 
                  (dbl ? sizeof (double) : sizeof (float));
               if (maxlength <= 0) {
                  ret = kFALSE;
                  break;
               }
               break;
            }
         case kEOutTypeXML:
            {
               maxlength = -1;
               break;
            }
      }
      // calculate number of columns
      int ptCol = 0;
      for (int i = (im.fXY ? 0 : 1); i < len + 1; i++) {
         ptCol += cinfo[i].fDataComplex ? 2 : 1;
      }
      if (ptCol == 0) {
         ret = kFALSE;
      }
      // calculate number of points
      if (im.fStart < 0) im.fStart = 0;
      if (im.fBin < 1) im.fBin = 1;
      if (im.fMax < 0) im.fMax = 0;
      int ptData = 0;
      int ptDest = 0;
      int startbin = im.fStart;
      int nbin = im.fMax;
      if (ret && (maxlength > 0)) {
         ptData = maxlength / ptCol; // source data length
         if (im.fPlotType == kPTHistogram1) {
            if (startbin < 1) startbin = 1;
            if (nbin > (ptData - 2) - (startbin - 1)) 
               nbin = (ptData - 2) - (startbin - 1);
            ptDest = nbin / im.fBin +
               ((nbin % im.fBin) > 0) + 2; // destination data length
         }
         else {
            ptDest = (im.fMax - im.fStart) / im.fBin;
            if (ptDest > (ptData - im.fStart) / im.fBin) {
               ptDest = (ptData - im.fStart) / im.fBin;
            }
         }
         if (ptDest <= 0) {
            ret = kFALSE;
         }
      }
      // allocate destination arrays
      if (ret) {
         for (int i = (im.fXY ? 0 : 1); i < len + 1; i++) {
            if (cinfo[i].fValid) {
               cinfo[i].fDataN = ptDest;
               if (i > 0) {
                  if (im.fXY) {
                     if (im.fPlotType == kPTHistogram1) { // hist mito
                        Int_t nbindest = ptDest - 2;
                        Int_t nent = 0;
                        ((HistDataCopy*)(cinfo[i].fPDesc->GetData()))->
                           SetData (0, 0, nbindest,"","",nent,0,kTRUE);
                     }
                     else {
                        ((DataCopy*)(cinfo[i].fPDesc->GetData()))->
                           SetData (0, 0, 
                                   cinfo[i].fDataN, cinfo[i].fDestComplex);
                     }
                  }
                  else {
                     if (im.fPlotType == kPTHistogram1) { // hist mito
                        Int_t nbindest = ptDest - 2;
                        Int_t nent = 0;
                        Double_t* edges = new Double_t[cinfo[i].fDataN - 1];
                        for (Int_t j = 0; j < cinfo[i].fDataN - 1; j++) edges[j] = (Double_t)j;
                        ((HistDataCopy*)(cinfo[i].fPDesc->GetData()))->
                           SetData (edges, 0, nbindest,"","",nent,0,kFALSE);
                        if (edges) delete[] edges;
                     }
                     else {
                        ((DataCopy*)(cinfo[i].fPDesc->GetData()))->
                           SetData (0., 1., 0, 
                                   cinfo[i].fDataN, cinfo[i].fDestComplex);
                     }
                  }	 
               }
               cinfo[i].fUp = 1;
               cinfo[i].fBin = im.fBin;
               cinfo[i].fLen = ptDest;
               if (im.fPlotType == kPTHistogram1) { // hist mito
                  cinfo[i].fDDest = (i > 0) ? 
                     ((HistDataCopy*)(cinfo[i].fPDesc->GetData()))->GetBinContents() : 0;
                  cinfo[i].fDData = (i > 0) ? 
                     ((HistDataCopy*)(cinfo[i].fPDesc->GetData()))->GetXBinEdges() : 0;
                  if ((i > 0) && ((cinfo[i].fDDest == 0) || 
                                 (cinfo[i].fDData == 0))) {
                     cinfo[i].fValid = kFALSE;
                  }
               }
               else {
                  cinfo[i].fFDest = (i > 0) ? cinfo[i].fPDesc->GetY() : 0;
                  cinfo[i].fFData = (i > 0) ? cinfo[i].fPDesc->GetX() : 0;
                  if ((i > 0) && ((cinfo[i].fFDest == 0) || 
                                 (cinfo[i].fFData == 0))) {
                     cinfo[i].fValid = kFALSE;
                  }
               }
            }
         }
      }
   
      // setup column pointers
      Bool_t colmaj = im.fColumnMajor;
      // determine if swap
      int test = 0;
      *(char*) &test = 1;
      Bool_t swap = ((im.fOutputType != kEOutTypeBinary) && 
                    (((test == 1) != 0) != (im.fBinLittleEndian != 0)));
   
      // print info for debugging
         // cout << "len = " << len << endl;
      // cout << "ptCol = " << ptCol << endl;
      // cout << "ptData = " << ptData << endl;
      // cout << "ptDest = " << ptDest << endl;
      // cout << "prec = " << (dbl ? "double" : "single") << endl;
      // cout << "format = " << (colmaj ? "column" : "row") << " major" << endl;
      // cout << "max length = " << maxlength << endl;
      // for (int i = 0; i < len + 1; i++) {
         // cout << "************ info record " << i <<
            // " (" << (cinfo[i].fValid ? "valid)" : "invalid)") << endl;
         // cout << "  DataN = " << cinfo[i].fDataN << endl;
         // cout << "  DataComplex = " << (cinfo[i].fDataComplex ? "yes" : "no") << endl;
         // cout << "  DestComplex = " << (cinfo[i].fDestComplex ? "yes" : "no") << endl;
         // cout << "  Conversion = " << cinfo[i].fConv << endl;
         // cout << "  Len = " << cinfo[i].fLen << endl;
         // cout << "  Stride = " << cinfo[i].fStride << endl;
         // cout << "  Up = " << cinfo[i].fUp << endl;
         // cout << "  Bin = " << cinfo[i].fBin << endl;
      // }
   
      // read in data (ascii and binary)
      if (ret && (im.fOutputType != kEOutTypeXML)) {
         ifstream	inp (filename);
         // column major
         if (colmaj) {
            // read x
            if (im.fXY) {
               double y = 0;
               double val[2];
               int k = 0;
               int b = 0;
               if (im.fPlotType == kPTHistogram1) { // hist mito
                  for (int j = 0; j < ptData; j++) {
                     ReadNextValue (dbl, inp, im.fXComplex, swap,
                                   cinfo[0].fConv, val);
                     if (j < startbin) {
                        continue;
                     }
                     if (b == 0) {
                        for (int i = 1; i < len + 1; i++) {
                           if (cinfo[i].fValid && (k < ptDest - 1))
                              cinfo[i].fDData[k] = val[0];
                        }
                     }
                     b++;
                     if (b == im.fBin || j == (startbin - 1) + nbin) {
                        k++;
                        b = 0;
                     }
                  }
               }
               else {
                  for (int j = 0; j < ptData; j++) {
                     ReadNextValue (dbl, inp, im.fXComplex, swap,
                                   cinfo[0].fConv, val);
                     if (j < im.fStart) {
                        continue;
                     }
                     y += val[0];
                     b++;
                     if (b == im.fBin) {
                        y /= im.fBin;
                        for (int i = 1; i < len + 1; i++) {
                           if (cinfo[i].fValid && (k < ptDest))
                              cinfo[i].fFData[k] = y;
                        }
                        k++;
                        b = 0;
                        y = 0.;
                     }
                  }
               }
               // read y
               for (int i = 1; i < len + 1; i++) {
                  double y[2];
                  double val[2];
                  int k = 0;
                  int b = 0;
                  y[0]=0; y[1]=0;
               
                  if (im.fPlotType == kPTHistogram1) { // hist mito
                  
                     k = 1;
                     cinfo[i].fDDest[0] = 0;
                     cinfo[i].fDDest[ptDest - 1] = 0;
                  
                     for (int j = 0; j < ptData; j++) {
                        ReadNextValue (dbl, inp, cinfo[i].fDataComplex, swap,
                                      cinfo[i].fConv, val);
                     
                        if (cinfo[i].fValid) {
                        
                           if (j < startbin) { // underflow
                              cinfo[i].fDDest[0] += val[0];
                           }
                           else if (j > (startbin - 1) + nbin){ // overflow
                              cinfo[i].fDDest[ptDest - 1] += val[0];
                           }
                           else if (b == im.fBin || j == (startbin - 1) + nbin) {
                              cinfo[i].fDDest[k] = y[0];
                              k++; b = 0;
                              y[0]=0;
                           }
                        
                           if (j >= startbin && j <= (startbin - 1) + nbin) {
                              y[0] += val[0];
                              b++;
                           }
                        
                        }
                     
                     }
                  }
                  else {
                     for (int j = 0; j < ptData; j++) {
                        ReadNextValue (dbl, inp, cinfo[i].fDataComplex, swap,
                                      cinfo[i].fConv, val);
                        if ((j < im.fStart) || !cinfo[i].fValid) {
                           continue;
                        }
                        y[0] += val[0];
                        y[1] += val[1];
                        b++;
                        if (b == im.fBin) {
                           y[0] /= im.fBin;
                           y[1] /= im.fBin;
                           if (k < ptDest) {
                              if (cinfo[i].fDestComplex) {
                                 cinfo[i].fFDest[2*k] = y[0];
                                 cinfo[i].fFDest[2*k+1] = y[1];
                              }
                              else {
                                 cinfo[i].fFDest[k] = y[0];
                              }
                           }
                           k++; b = 0;
                           y[0]=0; y[1]=0;
                        }
                     } 
                  }
               
               }
            }
         }
         // row major
         else {
            double val[2];
            int k = 0;
            int b = 0;
            double x;
            double* y = new double[2*(len+1)];
            if (y == 0) {
               ret = kFALSE;
            }
            else {
               x = 0;
               memset (y, 0, 2*(len+1)*sizeof(double));
               // read rows
               if (im.fPlotType == kPTHistogram1) { // hist mito
                  k = 1;
                  for (int j = 0; j < ptData; j++) {
                     // read x
                     if (im.fXY) {
                        ReadNextValue (dbl, inp, im.fXComplex, swap,
                                      cinfo[0].fConv, val);
                        if (j >= startbin && b == 0 &&  j <= (startbin - 1) + nbin + 1) {
                           for (int i = 1; i < len + 1; i++) {
                              cinfo[i].fDData[k - 1] = val[0]; // copy bin edge
                           }
                        }
                     }
                     // read y values
                     for (int i = 1; i < len + 1; i++) {
                        ReadNextValue (dbl, inp, cinfo[i].fDataComplex, swap,
                                      cinfo[i].fConv, val);
                        if (cinfo[i].fValid) {		  
                           if (j < startbin) { // underflow
                              cinfo[i].fDDest[0] += val[0];
                           }
                           else if (j > (startbin - 1) + nbin) { // overflow
                              cinfo[i].fDDest[ptDest - 1] += val[0];
                           }
                           else {
                              cinfo[i].fDDest[k] += val[0];
                           }
                        
                           if (i == len && j >= startbin) {
                              b++;
                              if (b == im.fBin || j == (startbin - 1) + nbin) {
                                 k++;
                                 b = 0;
                              }
                           }
                        
                        }
                     }
                  }
               }
               else { //===== non hist start
                  for (int j = 0; j < ptData; j++) {
                     // read x
                     if (im.fXY) {
                        ReadNextValue (dbl, inp, im.fXComplex, swap,
                                      cinfo[0].fConv, val);
                        x += val[0];
                     }
                     // read y values
                     for (int i = 1; i < len + 1; i++) {
                        ReadNextValue (dbl, inp, cinfo[i].fDataComplex, swap,
                                      cinfo[i].fConv, val);
                        y[2*i] += val[0];
                        y[2*i+1] += val[1];
                     }
                     if (j < im.fStart) {
                        x = 0;
                        memset (y, 0, 2*(len+1)*sizeof(double));
                        continue;
                     }
                     b++;
                     if (b == im.fBin) {
                        x /= im.fBin;
                        for (int i = 1; i < len + 1; i++) {
                           if (cinfo[i].fValid && (k < ptDest)) {
                              if (im.fXY) {
                                 cinfo[i].fFData[k] = x;
                              }
                              y[2*i] /= im.fBin;
                              y[2*i+1] /= im.fBin;
                              if (cinfo[i].fDestComplex) {
                                 cinfo[i].fFDest[2*k] = y[2*i];
                                 cinfo[i].fFDest[2*k+1] = y[2*i+1];
                              }
                              else {
                                 cinfo[i].fFDest[k] = y[2*i];
                              }
                           }
                        }
                        k++; b = 0;
                        x = 0;
                        memset (y, 0, 2*(len+1)*sizeof(double));
                     }
                  }
               
               } //===== non hist end
            
            }
            delete [] y;
         }
      }
      
      // read in data (xml)
      else if (ret && (im.fOutputType != kEOutTypeXML)) {
         // DONE above!
         ret = kFALSE;
      }
   
   
      // delete temporary file
      if (filename != im.fFilename) {
         remove (filename);
      }
   
      // on error delete descriptors
      if (!ret) {
         for (int i = 1; i < len + 1; i++) {
            if (cinfo[i].fValid && (cinfo[i].fPDesc != 0)) {
               delete (PlotDescriptor*)cinfo[i].fPDesc;
            }
         }
      }
      // on success add to plot set
      else {
         vector<PlotDescriptor*> pds;
         PlotDescriptor* pd = 0;
         for (int i = 1; i < len + 1; i++) {
            if (cinfo[i].fValid && (cinfo[i].fPDesc != 0)) {
               pd = (PlotDescriptor*)cinfo[i].fPDesc;
               pd->SetDirty();
               if (im.fPlotType != kPTHistogram1) { 
                  // try to unset XY if possible
                  ((DataCopy*)(pd->GetData()))->UnsetXY();
               }
               if (caltable) caltable->AddUnits (pd->Cal());
               pl.Add (pd);
               pds.push_back (pd);
            }
         }
         // add computed traces to pool
         vector<PlotDescriptor*> pds2;
         for (vector<PlotDescriptor*>::iterator i = pds.begin();
             i != pds.end(); i++) {
            TLGXMLRestorer::AddComputedTrace (pl, pds2, *(*i), caltable);
         }
         for (vector<PlotDescriptor*>::iterator i = pds2.begin();
             i != pds2.end(); i++) {
            (*i)->SetDirty();
            if (caltable) caltable->AddUnits ((*i)->Cal());
            pl.Add (*i);
         }
      }
   
      delete [] cinfo;
      return ret;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Export to file dialog                                                //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   Bool_t ExportToFileDlg (const TGWindow* p, const TGWindow* main,
                     PlotSet& pl, ExportOption_t* ex, 
                     const calibration::Table* caltable)
   {
      Bool_t 		ret = kFALSE;
      ExportOption_t	exdef;
      ExportOption_t*	pex;
   
      // Fill in defaults
      if (!ex) {
         SetDefaultExportOptions (exdef);
         pex = &exdef;
      }
      else {
         pex = ex;
      }
      // Show export dialog box
      new TLGExportDialog (p, main, *pex, pl.GetPlotMap(), ret);
      if (!ret) {
         return kFALSE;
      }
   
   
      // Write data to file
      if (pex->fSeparateFiles && pex->fOutputType != kEOutTypeXML) { // save separately (ascii & bin)
         Bool_t* inc = new Bool_t[kMaxExportColumn];
         for (Int_t i = 0; i < kMaxExportColumn; i++) {
            inc[i] = pex->fColumn[i].fInclude;
            pex->fColumn[i].fInclude = kFALSE;
         }
         for (Int_t i = 0; i < kMaxExportColumn; i++) {
            if (inc[i]) {
               pex->fColumn[i].fInclude = kTRUE;
               string fname = (const char*)pex->fFilename;
               char ext[5];
               sprintf(ext,".%d",i);
               pex->fFilename += ext;
               ret = ExportToFileASCII (*pex, pl);
               if (!ret) 
                  break;
               pex->fColumn[i].fInclude = kFALSE;
               pex->fFilename = fname.c_str();
            }
         }
      
         if (inc) delete[] inc;
      
      }
      else if (!pex->fSeparateFiles && pex->fOutputType != kEOutTypeXML) { // save in one file (ascii & bin)
      
         ret = ExportToFileASCII (*pex, pl);
      
      }
      else { // write XML data
         ret = ExportToFileXML (*pex, pl, caltable);
      }
   
      // Error message if failed
      if (!ret) {
         TString msg;
         msg = TString ("Unable to complete export.");
         new TGMsgBox (gClient->GetRoot(), p, "Error", 
                      msg, kMBIconStop, kMBOk);
      }
      return ret;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Import from file dialog                                              //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   Bool_t ImportFromFileDlg (const TGWindow* p, const TGWindow* main,
                     PlotSet& pl, ImportOption_t* im,
                     calibration::Table* caltable)
   {
      Bool_t 		ret = kFALSE;
      ImportOption_t	imdef;
      ImportOption_t*	pim;
   
      // Fill in defaults
      if (!im) {
         SetDefaultImportOptions (imdef);
         pim = &imdef;
      }
      else {
         pim = im;
      }
      // Show import dialog box
      new TLGExportDialog (p, main, *pim, pl.GetPlotMap(), ret, kTRUE);
      if (!ret) {
         return kFALSE;
      }
      // Read data from file
      ret = ImportFromFile (*pim, pl, caltable);
      // Error message if failed
      if (!ret) {
         TString msg = TString ("Unable to complete import.");
         new TGMsgBox (gClient->GetRoot(), p, "Error", 
                      msg, kMBIconStop, kMBOk);
      }
      else {
         pl.Update();
      }
      return ret;
   }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// Export/Import option dialog                                          //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGExportDialog::TLGExportDialog (const TGWindow *p, 
                     const TGWindow *main, ExportOption_t& ex, 
                     const PlotMap& plotlist, Bool_t& ret, Bool_t import)
   : TLGTransientFrame (p, main, 10, 10), fPlotList (&plotlist), 
   fExport (!import), fExportValues (&ex), fOk (&ret)
   {
      fExportTemp = ex;
   
      // layout hints
      fL[0] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop, 4, 4, 4, 4);
      fL[7] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX | 
                           kLHintsTop | kLHintsExpandY, 4, 4, 4, 4);
      fL[9] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 0, 2, -6);
      fL[1] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsTop, 0, 0, 0, 0);
      fL[6] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 0, 0, 0);
      fL[5] = new TGLayoutHints (kLHintsLeft | kLHintsExpandX |
                           kLHintsTop, 0, 0, 0, -10);      
      fL[2] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 0, 4, 2, 2);
      fL[8] = new TGLayoutHints (kLHintsLeft | kLHintsTop, 0, 4, 2, 2);
      fL[3] = new TGLayoutHints (kLHintsRight | kLHintsCenterY, 6, 4, 2, 2);
      fL[4] = new TGLayoutHints (kLHintsLeft | kLHintsCenterY, 8, 0, 4, -6);
      // group frames
      fGroupOut = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fGroupOut, fL[1]);
      fGroupType = new TGGroupFrame (fGroupOut, 
                           fExport ? "Output type" : "Input type");
      fGroupOut->AddFrame (fGroupType, fL[7]);
      fGroupFormat = new TGGroupFrame (fGroupOut, 
                           fExport ? "Output format" : "Input format");
      fGroupOut->AddFrame (fGroupFormat, fL[7]);
      fGroupBinOpt = new TGGroupFrame (fGroupOut, "Binary option");
      fGroupOut->AddFrame (fGroupBinOpt, fL[7]);
      fGroupXMLOpt = new TGGroupFrame (fGroupOut, "XML option");
      fGroupOut->AddFrame (fGroupXMLOpt, fL[7]);
         // data & bin
      fGroupDataBin = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fGroupDataBin, fL[1]);
      fGroupDataType = new TGGroupFrame (fGroupDataBin, "Data type");
      fGroupDataBin->AddFrame (fGroupDataType, fL[0]);
      fGroupBin = new TGGroupFrame (fGroupDataBin, "Data range");
      fGroupDataBin->AddFrame (fGroupBin, fL[0]);
      fBinFrame = new TGHorizontalFrame (fGroupBin, 10, 10);
      fGroupBin->AddFrame (fBinFrame, fL[9]);
         // column selection
      fGroupColumnSel = new TGGroupFrame (this, "Column selection");
      AddFrame (fGroupColumnSel, fL[0]);
      fButtonFrame = new TGHorizontalFrame (this, 10, 10);
      AddFrame (fButtonFrame, fL[0]);
      // Data type
      fDataType = new TGComboBox (fGroupDataType, kExportDataType);
      fDataType->Associate (this);
      fDataType->Resize (260, 23);
      fGroupDataType->AddFrame (fDataType, fL[4]);
      // Data range
      fRangeLabel[0] = new TGLabel (fBinFrame, "Start index:");
      fBinFrame->AddFrame (fRangeLabel[0], fL[2]);
      fStart = new TLGNumericControlBox (fBinFrame, 1.0, 7, kExportStart,
                           kNESInteger, kNEANonNegative);
      fStart->Associate (this);
      fBinFrame->AddFrame (fStart, fL[2]);
      fRangeLabel[1] = new TGLabel (fBinFrame, "   Max. length:");
      fBinFrame->AddFrame (fRangeLabel[1], fL[2]);
      fMax = new TLGNumericControlBox (fBinFrame, 1.0, 8, kExportMax,
                           kNESInteger, kNEAPositive);
      fMax->Associate (this);
      fBinFrame->AddFrame (fMax, fL[2]);
      fRangeLabel[2] = new TGLabel (fBinFrame, "   Bin:");
      fBinFrame->AddFrame (fRangeLabel[2], fL[2]);
      fBin = new TLGNumericControlBox (fBinFrame, 1.0, 5, kExportBin,
                           kNESInteger, kNEAPositive);
      fBin->Associate (this);
      fBinFrame->AddFrame (fBin, fL[2]);
      // Column selection
      fGroupColumnShow[0] = new TGHorizontalFrame (fGroupColumnSel, 10, 10);
      fGroupColumnSel->AddFrame (fGroupColumnShow[0], fL[0]);
      for (int i = 0; i < kMaxExportColumn/kShowExportColumn/2; i++) {
         fGroupColumnShow[i+1] = new TGVerticalFrame (
                              fGroupColumnShow[0], 10, 10);
         fGroupColumnShow[0]->AddFrame (fGroupColumnShow[i+1], fL[6]);
      }
      for (int i = 0; i < kMaxExportColumn/kShowExportColumn; i++) {
         char text[32];
         sprintf (text, "%i to %i    ", i * kShowExportColumn, 
                 (i + 1) * kShowExportColumn - 1);
         int j = 0; //i / 2 + 1;
         fColSel[i] = new TGRadioButton (fGroupColumnShow[j], text, 
                              kExportColSel + i);
         fColSel[i]->Associate (this);
         fGroupColumnShow[j]->AddFrame (fColSel[i], fL[2]);
      }
      for (int i = 0; i < kShowExportColumn; i++) {
         // column group frame
         char text[16];
         sprintf (text, "Column %i", i);
         fGroupColumnName[i] = new TGString (text);
         fGroupColumn[i] = new TGGroupFrame (fGroupColumnSel, 
                              fGroupColumnName[i]);
         fGroupColumnSel->AddFrame (fGroupColumn[i], fL[0]);
         fColFrame[i][0] = new TGHorizontalFrame (fGroupColumn[i], 10, 10);
         fGroupColumn[i]->AddFrame (fColFrame[i][0], fL[1]);
         fColFrame[i][1] = new TGHorizontalFrame (fGroupColumn[i], 10, 10);
         fGroupColumn[i]->AddFrame (fColFrame[i][1], fL[5]);
         // active button
         fColActive[i] = new TGCheckButton (fColFrame[i][0], "A:", 
                              kExportColActive + i);
         fColActive[i]->Associate (this);
         fColFrame[i][0]->AddFrame (fColActive[i], fL[2]);
         // A channel
         if (fExport) {
            fColAi[i] = 0;
            fColAe[i] = new TGComboBox (fColFrame[i][0], 
                                 kExportColA + i);
            fColAe[i]->Associate (this);
            fColAe[i]->Resize (360, 23);
            // fColAe[i]->Resize (260, 23); // JCB
            fColFrame[i][0]->AddFrame (fColAe[i], fL[2]);
         }
         else {
            fColAe[i] = 0;
            fColAi[i] = new TLGTextEntry (fColFrame[i][0], "",
                                 kExportColA + i);
            fColAi[i]->Associate (this);
            fColAi[i]->Resize (360, 23);
            // fColAe[i]->Resize (260, 23); // JCB
            fColFrame[i][0]->AddFrame (fColAi[i], fL[2]);
         }
         // B channel
         fColBLabel[i] = new TGLabel (fColFrame[i][0], "   B:");
         fColFrame[i][0]->AddFrame (fColBLabel[i], fL[2]);
         if (fExport) {
            fColBi[i] = 0;
            fColBe[i] = new TGComboBox (fColFrame[i][0], 
                                 kExportColB + i);
            fColBe[i]->Associate (this);
            fColBe[i]->Resize (360, 23);
            // fColBe[i]->Resize (260, 23);  // JCB
            fColFrame[i][0]->AddFrame (fColBe[i], fL[2]);
         }
         else {
            fColBe[i] = 0;
            fColBi[i] = new TLGTextEntry (fColFrame[i][0], "",
                                 kExportColB + i);
            fColBi[i]->Associate (this);
            fColBi[i]->Resize (360, 23);
            // fColBi[i]->Resize (300, 23); // JCB
            fColFrame[i][0]->AddFrame (fColBi[i], fL[2]);
         }
         // conversion type
         fColTypeLabel[i] = new TGLabel (fColFrame[i][0], "   Conv.:");
         fColFrame[i][0]->AddFrame (fColTypeLabel[i], fL[2]);
         fColType[i] = new TGComboBox (fColFrame[i][0], 
                              kExportColType + i);
         fColType[i]->Associate (this);
         fColType[i]->Resize (180, 23);
         // fColType[i]->Resize (200, 23); // JCB
         fColFrame[i][0]->AddFrame (fColType[i], fL[2]);
         if (fExport) {
            fColType[i]->AddEntry ("As is", kETypeAsIs);
         }
         else {
            fColType[i]->AddEntry ("Default", kETypeAsIs);
         }
         fColType[i]->AddEntry ("Magnitude", kETypeMagnitude);
         fColType[i]->AddEntry ("dB Magnitude", kETypedBMagnitude);
         fColType[i]->AddEntry ("Real part", kETypeReal);
         fColType[i]->AddEntry ("Imaginary part", kETypeImaginary);
         fColType[i]->AddEntry ("Phase (degree)", kETypePhaseDeg);
         fColType[i]->AddEntry ("Phase (rad)", kETypePhaseRad);
         if (fExport) {
            fColType[i]->AddEntry ("Continuous phase (degree)", kETypePhaseDegCont);
            fColType[i]->AddEntry ("Continuous phase (rad)", kETypePhaseRadCont);
         }
         fColType[i]->AddEntry ("Complex", kETypeComplex);
         fColType[i]->AddEntry ("Complex (dB/degree)", kETypeComplexAlt1);
         fColType[i]->AddEntry ("Complex (abs/rad)", kETypeComplexAlt2);
         fColType[i]->AddEntry ("Complex (dB/cont. degree)", kETypeComplexAlt3);
         fColType[i]->AddEntry ("Complex (abs/cont. rad)", kETypeComplexAlt4);
      }
      // Output type
      for (int i = 0; i < 3; i++) {
         const char* text[] = {"ASCII", "Binary", "XML"};
         fOutType[i] = new TGRadioButton (fGroupType, text[i], 
                              kExportASCII + i);
         fOutType[i]->Associate (this);
         fGroupType->AddFrame (fOutType[i], fL[8]);
      }
      // Output format
      fXY = new TGCheckButton (fGroupFormat, "XY", kExportXY);
      fXY->Associate (this);
      fGroupFormat->AddFrame (fXY, fL[8]);
      fXComplex = new TGCheckButton (fGroupFormat, "X complex", kExportXComplex);
      fXComplex->Associate (this);
      fGroupFormat->AddFrame (fXComplex, fL[8]);
      fZeroTime = new TGCheckButton (fGroupFormat, "Zero time", 
                           kExportZeroTime);
      fZeroTime->Associate (this);
      fGroupFormat->AddFrame (fZeroTime, fL[8]);
      fColumnMajor = new TGCheckButton (fGroupFormat, "Column major", 
                           kExportColumnMajor);
      fColumnMajor->Associate (this);
      fGroupFormat->AddFrame (fColumnMajor, fL[8]);
   
      if (fExport) {
         fSeparateFiles = new TGCheckButton (fGroupFormat, "Save separate", 
                              kExportSeparateFiles);
         fSeparateFiles->Associate (this);
         fGroupFormat->AddFrame (fSeparateFiles, fL[8]);
	 // Write header that contains channels and options? - JCB
	 fWriteOptionHeader = new TGCheckButton(fGroupFormat, "Include option header", kWriteOptions) ;
	 fWriteOptionHeader->Associate (this) ;
	 fGroupFormat->AddFrame(fWriteOptionHeader, fL[8]) ;
	 // JCB end
      }
      else {
         fSeparateFiles = 0;
	 fWriteOptionHeader = 0 ; // JCB
      }
   
      // Binary options
      fBinDouble = new TGCheckButton (fGroupBinOpt, "Double precision", 
                           kExportBinDouble);
      fBinDouble->Associate (this);
      fGroupBinOpt->AddFrame (fBinDouble, fL[8]);
      fBinLE = new TGCheckButton (fGroupBinOpt, "Little endian", 
                           kExportBinLE);
      fBinLE->Associate (this);
      fGroupBinOpt->AddFrame (fBinLE, fL[8]);
      // XML options
      fXMLAll = new TGCheckButton (fGroupXMLOpt, 
                           fExport ?"Write all" : "Read all", 
                           kExportXMLAll);
      fXMLAll->Associate (this);
      fGroupXMLOpt->AddFrame (fXMLAll, fL[8]);
      if (fExport) {
         fXMLReplace = 0;
      }
      else {
         fXMLReplace = new TGCheckButton (fGroupXMLOpt, 
                              "Replace existing", 
                              kExportXMLReplace);
         fXMLReplace->Associate (this);
         fGroupXMLOpt->AddFrame (fXMLReplace, fL[8]);
      }
      fXMLIncludeCal = new TGCheckButton (fGroupXMLOpt, 
                           "Include calibration", 
                           kExportXMLIncludeCal);
      fXMLIncludeCal->Associate (this);
      fGroupXMLOpt->AddFrame (fXMLIncludeCal, fL[8]);
      // setup buttons
      fCancelButton = new TGTextButton (fButtonFrame, 
                           new TGHotString ("     &Cancel     "), 
                           kExportCancel);
      fCancelButton->Associate (this);
      fButtonFrame->AddFrame (fCancelButton, fL[3]);
      fExportButton = new TGTextButton (fButtonFrame, 
                           new TGHotString (fExport ? "     &Export     " : 
                                           "     &Import     "), 
                           kExportExport);
      fExportButton->Associate (this);
      fButtonFrame->AddFrame (fExportButton, fL[3]);
   
      // initialize parameters
      fStart->SetIntNumber (fExportValues->fStart);
      fMax->SetIntNumber (fExportValues->fMax);
      fBin->SetIntNumber (fExportValues->fBin);
      switch (fExportValues->fOutputType) {
         case kEOutTypeASCII:
         default:
            fOutType[0]->SetState (kButtonDown);
            break;
         case kEOutTypeBinary:
            fOutType[1]->SetState (kButtonDown);
            break;
         case kEOutTypeXML:
            fOutType[2]->SetState (kButtonDown);
            break;
      }
      if (fExportValues->fXY) {
         fXY->SetState (kButtonDown);
      }
      if (fExportValues->fXComplex) {
         fXComplex->SetState (kButtonDown);
      }
      if (fExportValues->fColumnMajor) {
         fColumnMajor->SetState (kButtonDown);
      }
      if (fExportValues->fZeroTime) {
         fZeroTime->SetState (kButtonDown);
      }
      if (fExportValues->fBinDouble) {
         fBinDouble->SetState (kButtonDown);
      }
      if (fExportValues->fBinLittleEndian) {
         fBinLE->SetState (kButtonDown);
      }
      if (fExportValues->fSeparateFiles && fSeparateFiles) {
         fSeparateFiles->SetState (kButtonDown);
      }
      if (fExportValues->fWriteOptionHeader) {
	 fWriteOptionHeader->SetState (kButtonDown) ;
      }
      if (fExportValues->fXMLAll) {
         fXMLAll->SetState (kButtonDown);
      }
      if (fExportValues->fXMLReplace && fXMLReplace) {
         fXMLReplace->SetState (kButtonDown);
      }
      if (fExportValues->fXMLIncludeCal) {
         fXMLIncludeCal->SetState (kButtonDown);
      }
      // setup data type and columns
      if (!fExport) {
         // build a new graph selection list (import only)
         Int_t i = 0;
         const char* const* p = kPTAll;
         while (*p != 0) {
            fDataType->AddEntry (*p++, i++);
         }
         fExportTemp.fPlotType = kPTAll[0];
         fDataType->Select (0);
      }
      fColCur = -1;
      SetColumns (0);
   
      // resize & move to center
      MapSubwindows ();
      UInt_t width  = GetDefaultWidth();
      UInt_t height = GetDefaultHeight();
      Resize (width, height);
   
      Int_t ax;
      Int_t ay;
      if (main) {
         Window_t wdum;
         gVirtualX->TranslateCoordinates (main->GetId(), GetParent()->GetId(),
                              (((TGFrame*)main)->GetWidth() - fWidth) >> 1, 
                              (((TGFrame*)main)->GetHeight() - fHeight) >> 1,
                              ax, ay, wdum);
      }
      else {
         UInt_t root_w, root_h;
         gVirtualX->GetWindowSize (fClient->GetRoot()->GetId(), ax, ay, 
                              root_w, root_h);
         ax = (root_w - fWidth) >> 1;
         ay = (root_h - fHeight) >> 1;
      }
      Move (ax, ay);
      SetWMPosition (ax, ay);
   
      // make the message box non-resizable
      SetWMSize(width, height);
      SetWMSizeHints(width, height, width, height, 0, 0);
   
      // set dialog box title
      TString name = fExport ? "Export" : "Import";
      SetWindowName (name);
      SetIconName (name);
      SetClassHints ("ExportDlg", "ExportDlg");
   
      SetMWMHints(kMWMDecorAll | kMWMDecorResizeH  | kMWMDecorMaximize |
                 kMWMDecorMinimize | kMWMDecorMenu,
                 kMWMFuncAll  | kMWMFuncResize    | kMWMFuncMaximize |
                 kMWMFuncMinimize,
                 kMWMInputModeless);
   
      MapWindow();
      fClient->WaitFor (this);
   }

//______________________________________________________________________________
   TLGExportDialog::~TLGExportDialog ()
   {
      delete fExportButton;
      delete fCancelButton;
      delete fXMLAll;
      if (fXMLReplace) delete fXMLReplace;
      delete fXMLIncludeCal;
      delete fBinLE;
      delete fBinDouble;
      delete fSeparateFiles;
      delete fWriteOptionHeader; // JCB
      delete fColumnMajor;
      delete fZeroTime;
      delete fXComplex;
      delete fXY;
      for (int i = 0; i < 3; i++) {
         delete fOutType[i];
      }
      delete fBin;
      delete fStart;
      delete fMax;
      for (int i = 0; i < 3; i++) {
         delete fRangeLabel[i];
      }
      delete fDataType;
      for (int i = 0; i < kShowExportColumn; i++) {
         delete fColType[i];
         delete fColTypeLabel[i];
         delete fColBLabel[i];
         if (fExport) {
            delete fColBe[i];
            delete fColAe[i];
         }
         else {
            delete fColBi[i];
            delete fColAi[i];
         }
         delete fColActive[i];
         delete fColFrame[i][0];
         delete fColFrame[i][1];
         delete fGroupColumn[i];
      }
      for (int i = 0; i < kMaxExportColumn/kShowExportColumn; i++) {
         delete fColSel[i];
      }
      for (int i = 0; i <= kMaxExportColumn/kShowExportColumn/2; i++) {
         delete fGroupColumnShow[i];
      }
      delete fButtonFrame;
      delete fGroupXMLOpt;
      delete fGroupBinOpt;
      delete fGroupFormat;
      delete fGroupOut;
      delete fGroupType;
      delete fGroupColumnSel;
      delete fBinFrame;
      delete fGroupBin;
      delete fGroupDataType;
      delete fGroupDataBin;
      for (int i = 0; i < 10; i++) {
         delete fL[i];
      }
   }

//______________________________________________________________________________
   void TLGExportDialog::CloseWindow()
   {
      if (fOk) *fOk = kFALSE;
      DeleteWindow();
   }

//______________________________________________________________________________
   void TLGExportDialog::BuildPlotType (Int_t level, Int_t id)
   {
      if (!fExport) {
         return;
      }
   
      // rebuild graph selection
      if (level <= 0) {
         // build a new graph selection list
         fDataType->RemoveEntries (0, 1000);
         const PlotListLink* g = fPlotList->Get();
         if (g != 0) {
            g = g->Child();
         }
         const PlotListLink* gsave = g;
         if (g != 0) {
            while (g != 0) {
               fDataType->AddEntry (g->GetName(), 
                                   fPlotList->GetId (g->GetName()));
               g = g->Next();
            }
            // set selected graph
            Int_t id = fPlotList->GetId (fExportTemp.fPlotType);
            if (id >= 0) {
               fDataType->Select (id);
            }
            else {
               fDataType->Select (0);
               fExportTemp.fPlotType = gsave->GetName();
            }
         }
         else {
            // disable graph selection combobox
            fDataType->SetTopEntry 
               (new TGTextLBEntry (fDataType, new TGString (""), 0), 
               new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                 kLHintsExpandX));
            fDataType->MapSubwindows();
            fExportTemp.fPlotType = "";
         }
      
      }
      // rebuild A channel selections
      if (level <= 1) {
         // loop over columns
         for (int i = 0; i < kShowExportColumn; i++) {
            // build a new A channel selection list
            fColAe[i]->RemoveEntries (0, 1000);
            const PlotListLink* a = fPlotList->Get (fExportTemp.fPlotType);
            if (a != 0) {
               a = a->Child();
            }
            const PlotListLink* asave = a;
            if (a != 0) {
               while (a != 0) {
                  fColAe[i]->AddEntry 
                     (a->GetName(), fPlotList->GetId 
                     (fExportTemp.fPlotType, a->GetName()));
                  a = a->Next();
               }
               // set selected graph
               Int_t id = fPlotList->GetId 
                  (fExportTemp.fPlotType,
                  fExportTemp.fColumn[fColCur + i].fAChn);
               if (id >= 0) {
                  fColAe[i]->Select (id);
               }
               else {
                  fColAe[i]->Select (0);
                  fExportTemp.fColumn[fColCur + i].fAChn = asave->GetName();
               }
            }
            else {
               // disable A channel selection combobox
               fColAe[i]->SetTopEntry 
                  (new TGTextLBEntry (fColAe[i], new TGString (""), 0), 
                  new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                    kLHintsExpandX));
               fColAe[i]->MapSubwindows();
               fExportTemp.fColumn[fColCur + i].fAChn = "";
            }
         }
      }
      // rebuild B channel selection
      if (level <= 2) {
         // loop over columns
         for (int i = 0; i < kShowExportColumn; i++) {
            if ((id >= 0) && (id != i)) {
               continue;
            }
            // build a new B channel selection list
            fColBe[i]->RemoveEntries (0, 1000);
            const PlotListLink* b = 
               fPlotList->Get (fExportTemp.fPlotType, 
                              fExportTemp.fColumn[fColCur + i].fAChn);
            if (b != 0) {
               b = b->Child();
            }
            const PlotListLink* bsave = b;
            if (b == 0) {
               // disable B selection combobox
               fColBe[i]->SetTopEntry 
                  (new TGTextLBEntry (fColBe[i], new TGString (""), 0), 
                  new TGLayoutHints (kLHintsLeft | kLHintsExpandY | 
                                    kLHintsExpandX));
               fColBe[i]->MapSubwindows();
               fExportTemp.fColumn[fColCur + i].fBChn = "";
            }
            else {
               // setup B channel list
               while (b != 0) {
                  fColBe[i]->AddEntry 
                     (b->GetName(), fPlotList->GetId 
                     (fExportTemp.fPlotType, 
                     fExportTemp.fColumn[fColCur + i].fAChn, 
                     b->GetName()));
                  b = b->Next();
               }
               // set selected graph
               Int_t id = fPlotList->GetId 
                  (fExportTemp.fPlotType, 
                  fExportTemp.fColumn[fColCur + i].fAChn, 
                  fExportTemp.fColumn[fColCur + i].fBChn);
               if (id >= 0) {
                  fColBe[i]->Select (id);
               }
               else {
                  fColBe[i]->Select (0);
                  fExportTemp.fColumn[fColCur + i].fBChn = 
                     bsave->GetName();
               }
            }
         }
      }
   }

//______________________________________________________________________________
   void TLGExportDialog::SetColumns (Int_t range)
   {
      if ((fColCur != range) && 
         (range <= kMaxExportColumn - kShowExportColumn)) {
         // read text fields for import
         if ((fColCur >= 0) && !fExport) {
            for (int i = 0; i < kShowExportColumn; i++) {
               fExportTemp.fColumn[fColCur + i].fAChn = fColAi[i]->GetText ();
               fExportTemp.fColumn[fColCur + i].fBChn = fColBi[i]->GetText ();
            }
         }
         if (range >= 0) {
            fColCur = (range / kShowExportColumn) * kShowExportColumn;
            for (int i = 0; i < kShowExportColumn; i++) {
               char buf[100];
               sprintf (buf, "Column %i", fColCur + i);
               *fGroupColumnName[i] = TGString (buf);
               // trick to request redraw of background
               fGroupColumn[i]->Resize (fGroupColumn[i]->GetWidth() - 1, 
                                    fGroupColumn[i]->GetHeight());
               fGroupColumn[i]->Resize (fGroupColumn[i]->GetWidth() + 1, 
                                    fGroupColumn[i]->GetHeight());
               fColActive[i]->SetState 
                  (fExportTemp.fColumn[fColCur+i].fInclude ? 
                  kButtonDown : kButtonUp);
               fColType[i]->Select 
                  (fExportTemp.fColumn[fColCur+i].fTypeConversion);
            }
            // set column selection radio button
            for (int i = 0; i <  kMaxExportColumn / kShowExportColumn; i++) {
               fColSel[i]->SetState (i == fColCur / kShowExportColumn ? 
                                    kButtonDown : kButtonUp);
            }
            // set column values
            if (fExport) {
               BuildPlotType (0);
            }
            else {
               // set channels
               for (int i = 0; i < kShowExportColumn; i++) {
                  fColAi[i]->SetText (fExportTemp.fColumn[fColCur + i].fAChn);
                  fColBi[i]->SetText (fExportTemp.fColumn[fColCur + i].fBChn);
               }
            }
         }
      }
   }

//______________________________________________________________________________
   Bool_t TLGExportDialog::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      // Buttons
      if ((GET_MSG (msg) == kC_COMMAND) &&
         (GET_SUBMSG (msg) == kCM_BUTTON)) {
      
         switch (parm1) {
            // ok
            case kExportExport:
               {
               
                  // read parameters
                  fExportValues->fStart = fStart->GetIntNumber();
                  fExportValues->fMax = fMax->GetIntNumber();
                  fExportValues->fBin = fBin->GetIntNumber();
                  if (fOutType[0]->GetState() == kButtonDown) {
                     fExportValues->fOutputType = kEOutTypeASCII;
                  }
                  else if (fOutType[1]->GetState() == kButtonDown) {
                     fExportValues->fOutputType = kEOutTypeBinary;
                  }
                  else {
                     fExportValues->fOutputType = kEOutTypeXML;
                  }
                  fExportValues->fXY = 
                     fXY->GetState () == kButtonDown;
                  fExportValues->fXComplex = 
                     fXComplex->GetState () == kButtonDown;
                  fExportValues->fZeroTime =
                     fZeroTime->GetState() == kButtonDown;
                  fExportValues->fColumnMajor =
                     fColumnMajor->GetState() == kButtonDown;
                  if (fSeparateFiles) {
                     fExportValues->fSeparateFiles =
                        fSeparateFiles->GetState() == kButtonDown; // hist mito
                  }
		  if (fWriteOptionHeader) {
		     fExportValues->fWriteOptionHeader =
			fWriteOptionHeader->GetState() == kButtonDown; // JCB
		  }
                  fExportValues->fBinDouble =
                     fBinDouble->GetState() == kButtonDown;
                  fExportValues->fBinLittleEndian =
                     fBinLE->GetState() == kButtonDown;
               
                  fExportValues->fXMLAll =
                     fXMLAll->GetState() == kButtonDown;
               
                  if (fXMLReplace) {
                     fExportValues->fXMLReplace =
                        fXMLReplace->GetState() == kButtonDown;
                  }
                  fExportValues->fXMLIncludeCal =
                     fXMLIncludeCal->GetState() == kButtonDown;
                  // read text current column fields for import
                  if ((fColCur >= 0) && !fExport) {
                     for (int i = 0; i < kShowExportColumn; i++) {
                        fExportTemp.fColumn[fColCur + i].fAChn =
                           fColAi[i]->GetText ();
                        fExportTemp.fColumn[fColCur + i].fBChn = 
                           fColBi[i]->GetText ();
                     }
                  }
                  for (int i = 0; i < kMaxExportColumn; i++) {
                     fExportValues->fColumn[i] = fExportTemp.fColumn[i];
                  }
                  fExportValues->fPlotType = fExportTemp.fPlotType;
               
                  Bool_t valid = kTRUE;
                  if (fSeparateFiles &&
                     !fExportValues->fSeparateFiles && 
                     fExportValues->fOutputType != kEOutTypeXML) {
                     valid = IsDataValid();
                  }
               
                  if (valid) {
                  // ask for filename: file save/open as dialog
                     TGFileInfo	info;
                     info.fFilename = 0;
                     info.fIniDir = 0;
                  #if ROOT_VERSION_CODE >= ROOT_VERSION(3,1,6)
                  info.fFileTypes = const_cast<const char**>(gExportTypes);
                  #else
                     info.fFileTypes = const_cast<char**>(gExportTypes);
                  #endif
                     TString defext;
                     if (fOutType[0]->GetState() == kButtonDown) {
                        defext = ".txt";
			info.fFileTypeIdx = 0 ; // First item in the gExportTypes set. JCB
                     }
                     else if (fOutType[1]->GetState() == kButtonDown) {
                        defext = ".dat";
			info.fFileTypeIdx = 2 ; // Second item in the gExportTypes set. JCB
                     }
                     else {
                        defext = ".xml";
			info.fFileTypeIdx = 4 ; // Third item in the gExportTypes set. JCB
                     }
		  #if 1
		     {
			new TLGFileDialog(this, &info, fExport ? kFDSave : kFDOpen) ;
		     }
		     if (!info.fFilename)
		  #else
                     if (!TLGFileDialog (this, info, fExport ? kFDSave : kFDOpen, 
                                        defext))
		  #endif
		     {
                        return kTRUE;
                     }
                     fExportValues->fFilename = info.fFilename;
                  #if ROOT_VERSION_CODE < ROOT_VERSION(3,1,6)
                     delete [] info.fFilename;
                  #endif
                  
                  // set return value and quit
                     if (fOk) *fOk = kTRUE;
                     DeleteWindow();
                     break;
                  }
                  else { // hist mito
                     TString msg = TString("Unequal bin spacing and/or data length.");
                     new TGMsgBox(gClient->GetRoot(), this, "Error", 
                                 msg, kMBIconStop, kMBOk);
                     break;
                  }
               }
            // cancel
            case kExportCancel:
               {
                  if (fOk) *fOk = kFALSE;
                  DeleteWindow();
                  break;
               }
         }
      }
      // Radio buttons
      else if ((GET_MSG (msg) == kC_COMMAND) &&
              (GET_SUBMSG (msg) == kCM_RADIOBUTTON)) {
         switch (parm1) {
            case kExportASCII:
            case kExportBinary:
            case kExportXML:
               {
                  for (int i = 0; i < 3; i++) {
                     fOutType[i]->SetState (i == parm1 - kExportASCII ? 
                                          kButtonDown : kButtonUp);
                  }
                  break;
               }
         }
         if ((parm1 >= kExportColSel) && 
            (parm1 < kExportColSel + kMaxExportColumn / kShowExportColumn)) {
            SetColumns ((parm1 - kExportColSel) * kShowExportColumn);
         }
      }
      // check buttons
      else if ((GET_MSG (msg) == kC_COMMAND) &&
              (GET_SUBMSG (msg) == kCM_CHECKBUTTON)) {
         // column active
         if ((parm1 >= kExportColActive) && 
            (parm1 < kExportColActive + kShowExportColumn)) {
            int id = parm1 - kExportColActive;
            fExportTemp.fColumn[fColCur+id].fInclude =
               (fColActive[id]->GetState() == kButtonDown);
         }
      }
      // combo boxes
      else if ((GET_MSG (msg) == kC_COMMAND) &&
              (GET_SUBMSG (msg) == kCM_COMBOBOX)) {
         // new data/plot type
         if (parm1 == kExportDataType) {
            TGTextLBEntry* entry = 
               (TGTextLBEntry*) fDataType->GetSelectedEntry();
            if (entry) {
               const char* sel = entry->GetText()->GetString();
               if (fExportTemp.fPlotType == sel) {
                  return kTRUE;
               }
               fExportTemp.fPlotType = sel;
            }
            if (fExport) {
               BuildPlotType (1);
            }
         }
         // new A channel
         else if ((parm1 >= kExportColA) && 
                 (parm1 < kExportColA + kShowExportColumn)) {
            int id = parm1 - kExportColA;
            TGTextLBEntry* entry = 
               (TGTextLBEntry*) fColAe[id]->GetSelectedEntry();
            if (entry) {
               const char* sel = entry->GetText()->GetString();
               if (fExportTemp.fColumn[fColCur + id].fAChn == sel) {
                  return kTRUE;
               }
               fExportTemp.fColumn[fColCur + id].fAChn = sel;
            }
            BuildPlotType (2, id);
         }
         // new B channel
         else if ((parm1 >= kExportColB) && 
                 (parm1 < kExportColB + kShowExportColumn)) {
            int id = parm1 - kExportColB;
            TGTextLBEntry* entry = 
               (TGTextLBEntry*) fColBe[id]->GetSelectedEntry();
            if (entry) {
               const char* sel = entry->GetText()->GetString();
               if (fExportTemp.fColumn[fColCur + id].fBChn == sel) {
                  return kTRUE;
               }
               fExportTemp.fColumn[fColCur + id].fBChn = sel;
            }
         }
         // new conversion type
         else if ((parm1 >= kExportColType) && 
                 (parm1 < kExportColType + kShowExportColumn)) {
            int id = parm1 - kExportColType;
            fExportTemp.fColumn[fColCur+id].fTypeConversion = 
               (EExportTypeConversion)fColType[id]->GetSelected();
         }
      }
      return kTRUE;
   }


//______________________________________________________________________________
   Bool_t TLGExportDialog::IsDataValid()
   { // hist mito
   
      Bool_t valid = kTRUE;
   
      const PlotDescriptor* pd = 0;
      for (Int_t i = 0; i < kMaxExportColumn; i++) {
         if (pd == 0) {
            if (fExportValues->fColumn[i].fInclude) {
               if (fExportValues->fColumn[i].fBChn.Length()) {
                  pd = fPlotList->Get(fExportValues->fPlotType,
                                     fExportValues->fColumn[i].fAChn, 
                                     fExportValues->fColumn[i].fBChn)->GetPlot();
               }
               else 
                  pd = fPlotList->Get(fExportValues->fPlotType, 
                                     fExportValues->fColumn[i].fAChn)->GetPlot(); 
            }
         
         }
         else {
         
            if (fExportValues->fColumn[i].fInclude) {
               const PlotDescriptor* pdTmp = 0;
               if (fExportValues->fColumn[i].fBChn.Length()) {
                  pdTmp = fPlotList->Get (fExportValues->fPlotType,
                                       fExportValues->fColumn[i].fAChn,
                                       fExportValues->fColumn[i].fBChn)->GetPlot();
               }
               else 
                  pdTmp = fPlotList->Get (fExportValues->fPlotType,
                                       fExportValues->fColumn[i].fAChn)->GetPlot();
            
               if (fExportValues->fPlotType == kPTHistogram1) {
                  if (pd->GetData()->GetN() ==  pdTmp->GetData()->GetN()) {
                     Int_t i = 0;
                     while (pd->GetData()->GetXBinEdges()[i] == pdTmp->GetData()->GetXBinEdges()[i] && 
                           i < pd->GetData()->GetN()+1)  i++;
                     if ( i < pd->GetData()->GetN()+1 ) {
                        valid = kFALSE;
                        break;
                     }
                  }
                  else {
                     valid = kFALSE;
                     break;
                  }
               }
               else {
                  Int_t min = (pd->GetData()->GetN() < pdTmp->GetData()->GetN()) ? 
                     pd->GetData()->GetN() : pdTmp->GetData()->GetN();
                  while (pd->GetData()->GetX()[i] == pdTmp->GetData()->GetX()[i] && 
                        i < min)  i++;
                  if ( i < min ) {
                     valid = kFALSE;
                     break;
                  }
               }
            }
         }
      }
      return valid;
   
   }

#ifndef __NO_NAMESPACE
}
#endif
