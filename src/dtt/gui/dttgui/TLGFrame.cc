
#include <THashList.h>
#include "TLGFrame.hh"

namespace dttgui {
   using namespace std;


//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGTransientFrame                                                    //
//                                                                      //
// transient frame 	                          		        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGTransientFrame::TLGTransientFrame (const TGWindow* p, 
                     const TGWindow* main,
                     UInt_t w, UInt_t h, UInt_t options)
   : TGTransientFrame (p, main, w, h, options)
   {
   }

//______________________________________________________________________________
   void TLGTransientFrame::DeleteWindow()
   {
   #if ROOT_VERSION_CODE >= ROOT_VERSION(3,4,2)
      //delete this; // not ready
      TGTransientFrame::DeleteWindow(); 
   #else
      delete this;
   #endif
   }

// //______________________________________________________________________________
   // void FindFrames (TGCompositeFrame* fr, THashList& frames, 
                   // THashList& layouts)
   // {
      // TGFrameElement *el;
      // TIter next(fr->GetList());
   // 
      // while ((el = (TGFrameElement *) next())) {
         // TGCompositeFrame* comp =
            // dynamic_cast<TGCompositeFrame*> (el->fFrame);
         // if (comp) {
            // FindFrames (comp, frames, layouts);
         // }
         // if (!frames.FindObject (el->fFrame)) {
            // frames.Add (el->fFrame);
         // }
         // if (!layouts.FindObject (el->fLayout)) {
            // layouts.Add (el->fLayout);
         // }
      // }
   // }
// 
// //______________________________________________________________________________
   // void TLGTransientFrame::Cleanup()
   // {
   // // Cleanup and delete all objects contained in this composite frame.
   // // This will delete all objects added via AddFrame() as well as
   // // frames located recursivly within composite frames therein.
   // // CAUTION: Each frame/layout will only be deleted once even if
   // // it occures multiple times.
   // 
      // if (!fList) 
         // return;
   // 
      // THashList frames (17, 2);
      // THashList layouts (17, 2);
      // FindFrames (this, frames, layouts);
   // 
      // frames.Delete();
      // layouts.Delete();
      // fList->Delete();
   // }



//////////////////////////////////////////////////////////////////////////
//                                                                      //
// TLGMainFrame                                                         //
//                                                                      //
// main frame		                          		        //
//                                                                      //
//////////////////////////////////////////////////////////////////////////
   TLGMainFrame::TLGMainFrame (const TGWindow* p, 
                     UInt_t w, UInt_t h, UInt_t options)
   : TGMainFrame (p, w, h, options)
   {
   }

//______________________________________________________________________________
   void TLGMainFrame::DeleteWindow()
   {
   #if ROOT_VERSION_CODE >= ROOT_VERSION(3,4,2)
      //delete this; // not ready
      TGMainFrame::DeleteWindow(); 
   #else
      delete this;
   #endif
   }

}
