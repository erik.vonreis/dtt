#ifdef __CINT__

#pragma link off all globals;
#pragma link off all classes;
#pragma link off all functions;
#pragma link C++ nestedclasses;
#pragma link C++ nestedtypedefs;

#pragma link C++ namespace dttgui;

#pragma link C++ class dttgui::TLGCalibrationDialog;

#pragma link C++ enum dttgui::EExportTypeConversion;
#pragma link C++ enum dttgui::EExportOutputType;
#pragma link C++ struct dttgui::ExportColumn_t;
#pragma link C++ struct dttgui::ExportOption_t;
#pragma link C++ class dttgui::TLGExportDialog;

#pragma link C++ enum dttgui::ERefTraceMod;
#pragma link C++ struct dttgui::ReferenceTrace_t;
#pragma link C++ struct dttgui::ReferenceTraceList_t;
#pragma link C++ struct dttgui::MathTable_t;
#pragma link C++ class dttgui::TLGReferenceDialog;

#pragma link C++ enum dttgui::EPlotStyle;
#pragma link C++ enum dttgui::EXRangeType;
#pragma link C++ enum dttgui::EYRangeType;
#pragma link C++ enum dttgui::EUnitType;
#pragma link C++ enum dttgui::EAxisScale;
#pragma link C++ enum dttgui::ERange;
#pragma link C++ enum dttgui::ECursorStyle;
#pragma link C++ enum dttgui::ECursorType;
#pragma link C++ enum dttgui::ELegendPlacement;
#pragma link C++ enum dttgui::ELegendStyle;
#pragma link C++ enum dttgui::ELegendText;
#pragma link C++ struct dttgui::OptionValues_t;
#pragma link C++ struct dttgui::OptionTraces_t;
#pragma link C++ struct dttgui::OptionRange_t;
#pragma link C++ struct dttgui::OptionUnits_t;
#pragma link C++ struct dttgui::OptionCursor_t;
#pragma link C++ struct dttgui::OptionConfig_t;
#pragma link C++ struct dttgui::OptionStyle_t;
#pragma link C++ struct dttgui::OptionAxis_t;
#pragma link C++ struct dttgui::OptionLegend_t;
#pragma link C++ struct dttgui::OptionParam_t;
#pragma link C++ struct dttgui::OptionAll_t;
#pragma link C++ class dttgui::OptionArray;
#pragma link C++ class dttgui::TLGOptions;
#pragma link C++ class dttgui::TLGOptionTraces;
#pragma link C++ class dttgui::TLGOptionRange;
#pragma link C++ class dttgui::TLGOptionUnits;
#pragma link C++ class dttgui::TLGOptionCursor;
#pragma link C++ class dttgui::TLGOptionConfig;
#pragma link C++ class dttgui::TLGOptionStyle;
#pragma link C++ class dttgui::TLGOptionAxis;
#pragma link C++ class dttgui::TLGOptionLegend;
#pragma link C++ class dttgui::TLGOptionParam;
#pragma link C++ class dttgui::TLGOptionTab;
#pragma link C++ class dttgui::TLGOptionDialog;

#pragma link C++ class dttgui::TLGPostScript;
#pragma link C++ class dttgui::TLGPrintParam;
#pragma link C++ class dttgui::TLGPrintDialog;

// #pragma link C++ class dttgui::xsilHandlerUnknown;
// #pragma link C++ class dttgui::xsilHandlerData;
// #pragma link C++ class dttgui::xsilHandlerOptions;
// #pragma link C++ class dttgui::xsilHandlerCalibration;
// #pragma link C++ class dttgui::xsilHandlerQueryData;
// #pragma link C++ class dttgui::xsilHandlerQueryOptions;
// #pragma link C++ class dttgui::xsilHandlerQueryReferenence;
// #pragma link C++ class dttgui::xsilHandlerQueryCalibration;
// #pragma link C++ class dttgui::xsilHandlerQueryMath;
// #pragma link C++ class dttgui::xsilHandlerQueryUnknown;
#pragma link C++ class dttgui::TLGSaver;
#pragma link C++ class dttgui::TLGRestorer;
#pragma link C++ class dttgui::TLGXMLSaver;
#pragma link C++ class dttgui::TLGXMLRestorer;

#pragma link C++ class dttgui::TLGraphExtensions;
// #pragma link C++ class dttgui::TLGraph;
#pragma link C++ class dttgui::TLGPadLayout;
#pragma link C++ class dttgui::TLGPad;
#pragma link C++ class dttgui::TLGMultiPadLayout;
#pragma link C++ class dttgui::TLGMultiPadLayoutGrid;
#pragma link C++ class dttgui::TLGMultiPad;
#pragma link C++ class dttgui::TLGLayoutDialog;
#pragma link C++ class dttgui::TLGPadMain;

#pragma link C++ enum dttgui::ESaveRestoreFlag;
#pragma link C++ enum dttgui::EButtonCommandIdentifiers;
#pragma link C++ enum dttgui::EMenuCommandIdentifiers;
#pragma link C++ class dttgui::NotificationMessage;
#pragma link C++ class dttgui::TLGMainWindow;

#pragma link C++ class dttgui::PlotList;

#pragma link C++ class dttgui::BugReportDlg;

#endif

