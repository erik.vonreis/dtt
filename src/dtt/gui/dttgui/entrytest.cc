VCSID("$(#)$Id$");

#include <time.h>
#include <iostream>
#include <TROOT.h>
#include <TApplication.h>
#include <TSystem.h>
#include <TEnv.h>
#include <TGraph.h>
#include <TGLabel.h>
#include <TVirtualX.h>
#include "TLGEntry.hh"

   using namespace dttgui;
   using namespace std;


   const char* const numlabel[13] = {
   "Integer", 
   "One digit real", 
   "Two digit real", 
   "Three digit real", 
   "Four digit real", 
   "Real", 
   "Degree.min.sec", 
   "Min:sec", 
   "Hour:min", 
   "Hour:min:sec", 
   "Day/month/year", 
   "Month/day/year", 
   "Hex"};

   const Double_t numinit[13] = {12345, 1.0, 1.00, 1.000, 1.0000, 1.2E-12, 
   90 * 3600, 120*60, 12 * 60, 12 * 3600 + 15 * 60, 
   19991121, 19991121, (Double_t) 0xDEADFACE};

   class MainMainFrame : public TGMainFrame {
   private:
      TGVerticalFrame*	 fF1;
      TGVerticalFrame*	 fF2;
      TGLayoutHints*     fL1;
      TLGNumericControlBox* fNumericEntries[13];
      TGCheckButton*	 fLowerLimit;
      TGCheckButton*	 fUpperLimit;
      TLGNumericControlBox* fLimits[2];
      TGCheckButton*	 fPositive;
      TGCheckButton*	 fNonNegative;
      TGButton*		 fSetButton;
      TGButton*		 fExitButton;
   
   public:
      MainMainFrame (const TGWindow *p, UInt_t w, UInt_t h);
      virtual ~MainMainFrame();
   
      void SetLimits();
      virtual void CloseWindow();
      virtual Bool_t ProcessMessage(Long_t msg, Long_t parm1, Long_t);
   };


   MainMainFrame::MainMainFrame (const TGWindow *p, UInt_t w, UInt_t h)
   : TGMainFrame (p, w, h, kMainFrame | kHorizontalFrame)
   {
   /*------------------------------------------------------------------------*/
   /* Set font for label text.                                               */
   /*------------------------------------------------------------------------*/
   
      FontStruct_t labelfont;
      labelfont = gClient->GetFontByName 
         (gEnv->GetValue
         ("Gui.NormalFont",
         "-adobe-helvetica-medium-r-*-*-12-*-*-*-*-*-iso8859-1"));
   
   /* Define new graphics context. Only the fields specified in
      fMask will be used. */
   
      GCValues_t   gval;
      gval.fMask = kGCForeground | kGCFont;
      gval.fFont = gVirtualX->GetFontHandle(labelfont);
   
      fF1 = new TGVerticalFrame (this, 200, 300);
      fL1 = new TGLayoutHints (kLHintsTop | kLHintsLeft, 2, 2, 2, 2);
      AddFrame (fF1, fL1);
      fL1 = new TGLayoutHints (kLHintsCenterY | kLHintsRight, 2, 2, 2, 2);
      for (int i = 0; i < 13; i++) {
         TGHorizontalFrame* fF = new TGHorizontalFrame (fF1, 200, 30);
         fNumericEntries[i] = new TLGNumericControlBox (fF, numinit[i], 12, i + 20,
                              (ENumericEntryStyle) i);
         fNumericEntries[i]->Associate (this);
         fF->AddFrame (fNumericEntries[i], fL1);
         fF1->AddFrame (fF, fL1);
         TGLabel* fLabel = new TGLabel (fF, numlabel[i]);
         fF->AddFrame (fLabel, fL1);
      }
   
      fL1 = new TGLayoutHints (kLHintsTop | kLHintsLeft, 2, 2, 2, 2);
      fF2 = new TGVerticalFrame (this, 200, 500);
      AddFrame (fF2, fL1);
      fLowerLimit = new TGCheckButton (fF2, "lower limit:", 4);
      fLowerLimit->Associate (this);
      fF2->AddFrame (fLowerLimit, fL1);
      fLimits[0] = new TLGNumericControlBox (fF2, 0, 12, 10);
      fLimits[0]->SetLogStep (kFALSE);
      fLimits[0]->Associate (this);
      fF2->AddFrame (fLimits[0], fL1);
      fUpperLimit = new TGCheckButton (fF2, "upper limit:", 5);
      fUpperLimit->Associate (this);
      fF2->AddFrame (fUpperLimit, fL1);
      fLimits[1] = new TLGNumericControlBox (fF2, 0, 12, 11);
      fLimits[1]->SetLogStep (kFALSE);
      fLimits[1]->Associate (this);
      fF2->AddFrame (fLimits[1], fL1);
      fPositive = new TGCheckButton (fF2, "Positive", 6);
      fPositive->Associate (this);
      fF2->AddFrame (fPositive, fL1);
      fNonNegative = new TGCheckButton (fF2, "Non negative", 7);
      fNonNegative->Associate (this);
      fF2->AddFrame (fNonNegative, fL1);
      fSetButton = new TGTextButton (fF2, " Set ", 2);
      fSetButton->Associate (this);
      fF2->AddFrame (fSetButton, fL1);
      fExitButton = new TGTextButton (fF2, " Exit ", 1);
      fExitButton->Associate (this);
      fF2->AddFrame (fExitButton, fL1);
   
      SetWindowName ("Entry Test");
      SetIconName ("Entry Test");
      SetClassHints ("Entry Test", "Entry Test");
      SetWMPosition (0,0);
      MapSubwindows ();
   
      // we need to use GetDefault...() to initialize the layout algorithm...
      Resize (GetDefaultSize());
      MapWindow ();
   }


   MainMainFrame::~MainMainFrame()
   {
      delete fF1;
      delete fF2;
      delete fExitButton;
      delete fL1;
   }


   void MainMainFrame::SetLimits()
   {
      Double_t min = fLimits[0]->GetNumber();
      Bool_t low = (fLowerLimit->GetState() == kButtonDown);
      Double_t max = fLimits[1]->GetNumber();
      Bool_t high = (fUpperLimit->GetState() == kButtonDown);
      ENumericEntryLimits lim;
      if (low && high) {
         lim = kNELLimitMinMax;
      }
      else if (low) {
         lim = kNELLimitMin; 
      }
      else if (high) {
         lim = kNELLimitMax;
      }
      else {
         lim = kNELNoLimits;
      }
      Bool_t pos = (fPositive->GetState() == kButtonDown);
      Bool_t nneg = (fNonNegative->GetState() == kButtonDown);
      ENumericEntryAttributes attr;
      if (pos) { 
         attr = kNEAPositive;
      }
      else if (nneg) {
         attr = kNEANonNegative;
      }
      else {
         attr = kNEAAnyNumber;
      }
      for (int i = 0; i < 13; i++) {
         fNumericEntries[i]->SetFormat (fNumericEntries[i]->GetNumStyle(), attr);
         fNumericEntries[i]->SetLimits (lim, min, max);
      }
   }


   Bool_t MainMainFrame::ProcessMessage (Long_t msg, Long_t parm1, 
                     Long_t parm2)
   {
      switch (GET_MSG (msg)) {
         case kC_COMMAND:
            {
               switch (GET_SUBMSG (msg)) {
                  case kCM_BUTTON:
                     {
                        switch (parm1) {
                           // exit button
                           case 1:
                              {
                                 CloseWindow();
                                 break;
                              }
                           // set button
                           case 2:
                              {
                                 SetLimits();
                                 break;
                              }
                        }
                        break;
                     }
                  case kCM_CHECKBUTTON:
                     {
                        switch (parm1) {
                           // lower limits check button
                           case 4:
                           // upper limits check button
                           case 5:
                           // positive check button
                           case 6:
                           // non-negative check button
                           case 7:
                              {
                                 SetLimits();
                                 break;
                              }
                        }
                     }
                  case kCM_MENU: 
                     {
                     }
               }
               break;
            }
         case kC_TEXTENTRY:
            {
               switch (GET_SUBMSG (msg)) {
                  case kTE_TEXTUPDATED:
                     {
                        switch (parm1) {
                           // lower limit entry
                           case 10:
                           // upper limit entry
                           case 11:
                              {
                                 SetLimits();
                                 break;
                              }
                        }
                        break;
                     }
               }
               break;
            }
      }
      return kTRUE;
   }


   void MainMainFrame::CloseWindow()
   {
   /* Got close message for this MainFrame. Calls parent CloseWindow()
    (which destroys the window) and terminate the application.
    The close message is generated by the window manager when its close
    window menu item is selected. */
   
      TGMainFrame::CloseWindow();
      gApplication->Terminate(0);
   }


   TROOT root("GUI", "TLG Test");



   int main(int argc, char **argv)
   {
      TApplication theApp ("Diagnostics tests", &argc, argv);
      MainMainFrame mainWindow (gClient->GetRoot(), 600, 240);
      theApp.Run();
   
      return 0;
   }
