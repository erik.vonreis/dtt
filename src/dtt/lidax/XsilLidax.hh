/* version $Id: XsilLidax.hh 6319 2010-09-17 18:06:52Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: dfmxsil							*/
/*                                                         		*/
/* Module Description: XML/XSIL helper routines for save/restore	*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: dfmxml.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_LIDAXXML_H
#define _LIGO_LIDAXXML_H


#include "ldxtype.hh"
#include "Xsil.hh"


   class TGWindow;


namespace lidax {


/** @name Lidax Xsil support 
    This header defines support methods for saveing and restoring 
    settings of the lidax program to and form file. It uses the 
    LIGO-LW format.
   
    @memo Lidax Xsil support
    @author Written April 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** Xsil output operator for lidax parameter record.
    
    @memo Lidax parameter output operator.
 ************************************************************************/
   std::ostream& operator<< (std::ostream& os, const LidaxParam& ldx);

/** Xsil parser for lidax parameter record.
    
    @memo Lidax parameter parser.
 ************************************************************************/
   class xsilHandlerLidax : public xml::xsilHandler {
   protected:
      class LidaxList;
   
      /// lidax parameters
      LidaxParam& 		fLdx;
      /// Temporary list
      LidaxList*		fList;
   
   public:
      /// Constructor
      xsilHandlerLidax (LidaxParam& ldx);
      /// Destructor
      virtual ~xsilHandlerLidax();
   
      /// bool parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const bool& p, int N = 1);
      /// byte parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const char& p, int N = 1) {
         return false; }
      /// short parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const short& p, int N = 1) {
         return false; }
      /// int parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const int& p, int N = 1);
      /// long parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const long long& p, int N = 1) {
         return false; }
      /// float parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const float& p, int N = 1) {
         return false; }
      /// double parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const double& p, int N = 1);
      /// complex float parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const std::complex<float>& p, int N = 1) {
         return true; }
      /// complex double parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr,
                        const std::complex<double>& p, int N = 1) {
         return true; }
      /// string parameter callback (must return true if handled)
      virtual bool HandleParameter (const std::string& name,
                        const attrlist& attr, const std::string& p);
      /// time callback (must return true if handled)
      virtual bool HandleTime (const std::string& name,
                        const attrlist& attr,
                        unsigned long sec, unsigned long nsec);
   };


/** Xsil parser query class for lidax parameter record.
    
    @memo Lidax Xsil query class.
 ************************************************************************/
   class xsilHandlerQueryLidax : public xml::xsilHandlerQuery {
   protected:
      /// lidax parameters
      LidaxParam& 		fLdx;
   
   public:
      /// Constructor
      xsilHandlerQueryLidax (LidaxParam& ldx) : fLdx (ldx) {
      }
      /// returns a handler for the specified object (or 0 if not)
      virtual xml::xsilHandler* GetHandler (const attrlist& attr);
   };


/** Store lidax parameters. Shows save as dialog box.
    
    @memo Store function.
 ************************************************************************/
   bool xsilStoreLidax (const TGWindow *p, const LidaxParam& ldx);

/** Restore lidax parameters. Shows open dialog box.
    
    @memo Restore function.
 ************************************************************************/
   bool xsilRestoreLidax (const TGWindow *p, LidaxParam& ldx);

/** Store lidax parameters to file
    
    @memo Store file function.
 ************************************************************************/
   bool xsilStoreLidaxToFile (const char* filename, 
                     const LidaxParam& ldx, std::string& error);

/** Restore lidax parameters from file.
    
    @memo Restore file function.
 ************************************************************************/
   bool xsilRestoreLidaxFromFile (const char* filename, 
                     LidaxParam& ldx, std::string& error);


//@}

}

#endif // _LIGO_LIDAXXML_H
