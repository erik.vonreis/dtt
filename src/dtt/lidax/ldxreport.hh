/* version $Id: ldxreport.hh 7389 2015-06-11 18:51:56Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: dfmreport						*/
/*                                                         		*/
/* Module Description: Lidax report generation				*/
/*                                                         		*/
/* Revision History:					   		*/
/* Rel   Date     Programmer  	Comments				*/
/* 0.1	 9Jan01   D. Sigg    	First release		   		*/
/*                                                         		*/
/* Documentation References:						*/
/*	Man Pages: lidax.html						*/
/*	References: none						*/
/*                                                         		*/
/* Author Information:							*/
/* Name          Telephone       Fax             e-mail 		*/
/* Daniel Sigg   (509) 372-8132  (509) 372-8137  sigg_d@ligo.mit.edu	*/
/*                                                         		*/
/*                                                         		*/
/*                      -------------------                             */
/*                                                         		*/
/*                             LIGO					*/
/*                                                         		*/
/*        THE LASER INTERFEROMETER GRAVITATIONAL WAVE OBSERVATORY.	*/
/*                                                         		*/
/*                     (C) The LIGO Project, 1999.			*/
/*                                                         		*/
/*                                                         		*/
/* Caltech				MIT		   		*/
/* LIGO Project MS 51-33		LIGO Project NW-17 161		*/
/* Pasadena CA 91125			Cambridge MA 01239 		*/
/*                                                         		*/
/* LIGO Hanford Observatory		LIGO Livingston Observatory	*/
/* P.O. Box 1970 S9-02			19100 LIGO Lane Rd.		*/
/* Richland WA 99352			Livingston, LA 70754		*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#ifndef _LIGO_DFMREPORT_H
#define _LIGO_DFMREPORT_H


#include "Time.hh"
#include "Interval.hh"
#include "fmsgq.hh"
#include <iosfwd>
#include <string>


namespace lidax {

   struct LidaxParam;
   class LidaxProgress;


/** @name Lidax report generation 

    @memo Lidax report generation
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/

//@{


/** Lidax report class.
    @memo report class.
    @author Written January 2001 by Daniel Sigg
    @version 1.0
 ************************************************************************/
   class lidax_report {
   public:
      /// Create report header
      lidax_report (LidaxParam& param, LidaxProgress& prog, 
                   int fd_std = -1);
      /// Create report footer
      ~lidax_report();
      /// Update report
      void update ();
   
   protected:
      /// Reference to parameter list
      LidaxParam&	fParam;
      /// Progress data
      LidaxProgress&	fProgress;
      /// Start time
      Time 		fStart;
      /// Stop time
      Time		fStop;
      /// Duration
      Interval		fDuration;
      /// Bytes read
      long long 	fInpbytes;
      /// Bytes written
      long long 	fOutbytes;
      /// Files read
      int	 	fInFiles;
      /// Files written
      int	 	fOutFiles;
      /// File descriptor for log file
      int		flogid;
      /// Log stream
      std::ostream*	log;
      /// Last 10 input files
      fantom::fmsgqueue	fInLast;
      /// Last 10 output files
      fantom::fmsgqueue	fOutLast;
      /// Last update
      Time		fLastUpdate;
   
      /// Write setup information
      void writeSetup (bool inp, std::ostream& os, bool web = false);
      /// Write web summary
      void writeWeb (const char* wtype = 0);
      /// Write e-mail
      void writeEmail ();
      /// Write time/date
      static std::string writeDate (const Time& t);
      /// Write bytes
      static std::string writeBytes (long long b);
   };

//@}
}

#endif // _LIGO_DFMREPORT_H



