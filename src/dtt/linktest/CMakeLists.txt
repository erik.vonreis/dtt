
#test linkage in OSX versus linux

add_library(linktest_shared SHARED
        linktest.h
        linktest.cpp
        )

target_include_directories(linktest_shared PUBLIC
        ${DTT_INCLUDES}
        ${ROOT_INCLUDES}
        )

target_link_libraries(linktest_shared
	${BASE_LIBRARIES}
	z
	)
