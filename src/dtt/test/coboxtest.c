/* version $Id: coboxtest.c 6306 2010-09-17 16:54:02Z james.batch@LIGO.ORG $ */
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/types.h>
#include <unistd.h>

#include "cobox.h"
#include "gdsutil.h"


/* main program */
   void
   #ifdef OS_VXWORKS
   coboxtest(int argc, char *argv[])
   #else
   main(int argc, char *argv[])
   #endif
   {
      char	cobox[256] = "10.1.5.1";
      int	fd;
      char	msg[256] = "Hello\013\012";
      int	i;
   
      fd = openCobox (cobox, 1);
      if (fd < 0) {
         printf ("Unable to open connection to cobox\n");
         exit (EXIT_FAILURE);
      }
      write (fd, msg, strlen (msg)+1);
   
      exit (1);
   #ifndef OS_VXWORKS
      sleep (10);
   #endif
      i = read (fd, msg, 255);
      msg[i] = '\0';
      printf ("replay was: %s\n", msg);
   
      close (fd);
   
      exit (EXIT_SUCCESS);
   }
