/* version $Id: errtest.c 6306 2010-09-17 16:54:02Z james.batch@LIGO.ORG $ */
#include <stdio.h>
#include <errno.h>
#include <stdlib.h>

#include "gdsutil.h"


/* main program */
   void
   #ifdef OS_VXWORKS
   errtest(int argc, char *argv[])
   #else
   main(int argc, char *argv[])
   #endif
   {
      int i;
   
      gdsErrorMessage ("This is an error message - this is a TEST"); 
      gdsConsoleMessage ("This is a console message - this is a TEST"); 
      gdsWarningMessage ("This is a warning message - this is a TEST"); 
      gdsDebugMessage ("This is a debug message - this is a TEST"); 
   
      gdsDebug ("TESSSSST");
   
   /*
   for (i=2; i>-12; i--) {
     gdsError (i, "test nonsense");
   }
   */
   
      printf ("Start\n");
   
      gdsCopyStdToConsole (1, 1);
   
      printf ("Let's see what happens\n");
      printf ("Let's see what happens\n");
   
      fprintf (stderr, "THIS eh kjfeh kjhf kjfn kjflkj flkjhlj nlkjfd lkj;lgk"
              " iuoarhjkfkj hnkjfh ,nkjfdh d kljlj kjfjh dmn kjd lkf dkjd kdsj lfdskj"
              " kjhkj lk hnbfdb vgkjhlkfd jkjfj g klfjlk lfdj lj lkjfdlkfjdgfl dldfs\n");
   
      fprintf (stderr, "THIS eh kjfeh kjhf kjfn kjflkj flkjhlj nlkjfd lkj;lgk"
              " iuoarhjkfkj hnkjfh ,nkjfdh d kljlj kjfjh dmn kjd lkf dkjd kdsj lfdskj"
              " kjhkj lk hnbfdb vgkjhlkfd jkjfj g klfjlk lfdj lj lkjfdlkfjdgfl dldfs\n");
   
      i = 5353;
      printf ("I really like printf, let i = %i\n", i); 
   
      exit (EXIT_SUCCESS);
   }
