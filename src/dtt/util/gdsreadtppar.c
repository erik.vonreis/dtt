/* Version $Id: gdsreadtppar.c 7344 2015-04-22 20:15:00Z james.batch@LIGO.ORG $ */

/* This file provides a function to read the aLIGO testpoint.par file and store the 
 * dcuid, model name, and host name in a structure to allow association of the data when
 * one of the values is known.
 *
 * The modelInfo type is defined in gdsreadtppar.h to be a structure containing an int
 * called dcuid, and 2 character array of 64 characters called hostname and modelname.
 *
 * readTestpointPar() attempts to open the testpoint.par file located at 
 * $CDSROOT/target/gds/param/testpoint.par, where CDSROOT is an environment variable value
 * that normally contains "/opt/rtcds/${loc}/${ifo}". CDSROOT should be set in the standard
 * setup scripts that control room users call to set up a user environment.  If the testpoint.par
 * file is not found or cannot be opened, the function returns a negative value.  If the file is
 * opened successfully, up to max entries are read and stored in the array of modelInfo which
 * the caller is responsible for allocating and the return value is 0.  Naturally, there should 
 * be max entries in the array.
 * 
 * The testpoint.par file contains entries of the form 
 * [H-node24]
 * hostname=h1oaf0
 * system=h1pemcs
 *
 * where the system is actually the model name, and the dcuid number follows the node in
 * the top line.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "gdsreadtppar.h"

static int my_debug = 0 ;

int
readTestpointPar(modelInfo *node, int *nread, int max)
{
   modelInfo 	*nptr = node ;
   char 	tp_path[1024] ;
   char 	*cds_path = getenv("CDSROOT") ;
   char 	buf[128] ;
   int		i = 0 ;
   FILE 	*fp ;

   if (node == (modelInfo *) NULL || max <= 0 || nread == (int *) NULL)
   {
      /* Incorrect parameters, return. */
      return -2 ;
   }

   if (!cds_path)
   {
      return -1 ;
   }


   memset(tp_path, '\0', 1024) ;

   /* Form the path to the testpoint.par file. */
   sprintf(tp_path, "%s/target/gds/param/testpoint.par", cds_path ) ;

   /* Attempt to open the testpoint.par file. */
   fp = fopen(tp_path,"r") ;
   if (!fp)
   {
      fprintf(stderr, "Cannot open testpoint.par file at %s\n",tp_path) ;
      return -1 ;
   }

   /* Initialize the node info. */
   for (i=0; i<max; ++i)
   {
      nptr->dcuid = 0;
      memset(nptr->hostname, '\0', 64) ;
      memset(nptr->modelname, '\0', 64) ;
      nptr++ ;
   }

   /* Read the file. */
   i = 0 ;
   nptr = node ;
   while (!feof(fp) && i < max)
   {
      fgets(buf, 127, fp) ;
      if (buf[strlen(buf)-1] == '\n') buf[strlen(buf)-1] = '\0' ;
      if (buf[0] == '[')
      {
	 /* Get the number embedded in the string of the form [H-node26] */
	 nptr->dcuid = atoi(buf+7) ;
	 if (my_debug) fprintf(stderr, "Found node %d in string %s\n", nptr->dcuid, buf) ;

	 /* The next line should be hostname=host or system=modelname */
	 if (!feof(fp))
	 {
	    int paramlen = strlen("hostname=") ;
	    fgets(buf, 127, fp) ;
	    if (buf[strlen(buf)-1] == '\n') buf[strlen(buf)-1] = '\0' ;
	    if (strncmp(buf, "hostname=", paramlen) == 0)
	    {
	       /* This is the hostname. Copy the part after the '=' to the struct. */
	       strncpy(nptr->hostname, buf+paramlen, (strlen(buf+paramlen) < 63 ? strlen(buf+paramlen) : 63)) ;
	       if (my_debug) fprintf(stderr, "  Node %d, copied %s to hostname.\n", i, nptr->hostname) ;
	    }
	 }
	 if (!feof(fp))
	 {
	    int paramlen = strlen("system=") ;
	    fgets(buf, 127, fp) ;
	    if (buf[strlen(buf)-1] == '\n') buf[strlen(buf)-1] = '\0' ;
	    if (strncmp(buf, "system=", paramlen) == 0)
	    {
	       /* This is the modelname. Copy the part after the '=' to the struct. */
	       strncpy(nptr->modelname, buf+paramlen, (strlen(buf+paramlen) < 63 ? strlen(buf+paramlen) : 63)) ;
	       if (my_debug) fprintf(stderr, "  Node %d, copied %s to modelname.\n", i, nptr->modelname) ;
	    }
	 }
	 nptr++ ;
	 ++i ;
	 if (i >= max)
	    break ;
      }
   }
   *nread = i ;

   return 0 ;

}

