/* version $Id: gdsstringcc.cc 6313 2010-09-17 17:25:05Z james.batch@LIGO.ORG $ */
/*----------------------------------------------------------------------*/
/*                                                         		*/
/* Module Name: gdsstring						*/
/*                                                         		*/
/* Module Description: implements functions for dealing with strings	*/
/* and the LIGO channel naming convention				*/
/*                                                         		*/
/*----------------------------------------------------------------------*/

#include "gdsstringcc.hh"
#include <cstdio>

namespace diag {
   using std::string;

   tempFilename::tempFilename () : string ("") {
      char		filename [L_tmpnam];
      this->assign (tmpnam (filename));
   }

}


