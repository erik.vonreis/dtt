/// Create python binding for libfilterfile

#include <pybind11/pybind11.h>
#include <stdexcept>
#include "FilterMessage.hh"
#include "FilterFile.hh"
#include "FilterModule.hh"
#include "FilterSection.hh"

using namespace filterwiz;

namespace py = pybind11;



PYBIND11_MODULE(pyfilterfile, m) {

  m.import("pydmtsigp");


  py::enum_<message_level_t>(m, "message_level_t")
    .value("WARNING", message_level_t::WARNING)
    .value("ERROR", message_level_t::ERROR)
    .export_values();

  py::class_<message_t>(m, "FilterMessage")
    .def("get_level", &message_t::get_level)
    .def("get_fmt_str", &message_t::get_fmt_str);


  py::class_<FilterFile>(m, "FilterFile")
      .def(py::init())
      .def("clear", &FilterFile::clear)
      .def("add", &FilterFile::add, py::arg("name"), py::arg("fsample")=1.0)
      .def("remove", &FilterFile::remove)
      .def("find", static_cast<FilterModule *(FilterFile::*)(const char *)>(&FilterFile::find),
           py::return_value_policy::reference)
      //.def("find", static_cast<const FilterModule *(FilterFile::*)(const char *)const>(&FilterFile::find))
      .def("modules", [](FilterFile &ff){
        FilterModuleList &mod_list = ff.modules();
        py::list pymod_list;
        for (auto mod=mod_list.begin(); mod != mod_list.end(); mod++) {
          filterwiz::FilterModule &modptr = *mod;
          auto pythis = py::cast(ff);
          pymod_list.append(py::cast(modptr, py::return_value_policy::reference_internal, pythis));
        }
        return pymod_list;
      })
      //.def("modules", static_cast<const FilterModuleList &(FilterFile::*)() const>(&FilterFile::modules))
      .def("getFilename", &FilterFile::getFilename)
      .def("setFilename", &FilterFile::setFilename)
      .def("valid", [](FilterFile &ff){
        std::string errmsg;
        bool is_valid = ff.valid(errmsg);
        return py::make_tuple(is_valid, errmsg);
      })
      .def("update", &FilterFile::update)
      .def("read_file", static_cast<bool (FilterFile::*)(const char *)>(&FilterFile::read))
      .def("read_string",[](FilterFile &ff, std::string buffer) {
        return ff.read(buffer.c_str(), buffer.size(), false);
      })
      .def("write_file", static_cast<bool (FilterFile::*)(const char *)>(&FilterFile::write))
      .def("write_string", [](FilterFile &ff){
        const size_t upper_limit = 1l<<32;
        size_t bufsize = (1<<10);
        char *buffer = new char[bufsize];
        if (NULL == buffer) {
          throw std::bad_alloc();
        }
        int bytes = 0;
        while(bufsize <= upper_limit) {
          bytes = ff.write(buffer, bufsize - 1);
          if (bytes < bufsize) {
            buffer[bufsize - 1] = 0;
            break;

          }
          bufsize = bytes+2;
          delete[](buffer);
          buffer = new char[bufsize];
          if (NULL == buffer) {
            throw std::bad_alloc();
          }
        }

        if (bytes >= bufsize) {
          throw std::out_of_range("output exceeded maximum of 4GB");
        }
        while(!buffer[bytes] && bytes > 0) {
          bytes--;
        }
        std::string filter_text(buffer, bytes);
        delete[] buffer;
        return filter_text;
      })
      //.def("write", static_cast<bool (FilterFile::*)(const char *, char *)>(&FilterFile::write))
      .def("convertFilters", &FilterFile::convertFilters)
      .def("checkFileStat", &FilterFile::checkFileStat)
      .def("updateFileStat", &FilterFile::updateFileStat)
      .def("getRealFilename", &FilterFile::getRealFilename)
      .def("LegacyWrite", &FilterFile::LegacyWrite)
      .def("setLegacyWrite", &FilterFile::setLegacyWrite)
      .def("SetAnySampleRate", &FilterFile::SetAnySampleRate)
      .def("AnySampleRate", &FilterFile::AnySampleRate)
      .def("set0SosAllowed", &FilterFile::set0SosAllowed)
      .def("sos0Allowed", &FilterFile::sos0Allowed)
      .def("getFSampleStr", &FilterFile::getFSampleStr)
      .def("getFSample", &FilterFile::getFSample)

      .def("setFSample", static_cast<void (FilterFile::*)(const char *)>(&FilterFile::setFSample))
      .def("setFSample", static_cast<void (FilterFile::*)(unsigned int)>(&FilterFile::setFSample))
      .def("setFSample", static_cast<void (FilterFile::*)(double)>(&FilterFile::setFSample))

      .def("merge", static_cast<int (FilterFile::*)(const char *)>(&FilterFile::merge))
      .def("merge", static_cast<int (FilterFile::*)(const char *, int)>(&FilterFile::merge))

      .def("setMergeDebug", &FilterFile::setMergeDebug)
      .def("printMergeErrors", []() { throw std::runtime_error("The printMergeErrors function has been removed. Printing should be moved to the calling code."); })
      .def("clearMergeErrors", []() { throw std::runtime_error("The clearMergeErrors function has been removed. Use clearMergeMessages instead."); })
      .def("getMergeErrors", [](FilterFile& ff) { throw std::runtime_error("The getMergeErrors function has been removed. Use getMergeMessages instead."); })
      .def("printFileErrors", []() { throw std::runtime_error("The printFileErrors function has been removed. Printing should be moved to the calling code."); })
      .def("clearErrors", []() { throw std::runtime_error("The clearErrors function has been removed. Use clearFileMessages instead."); })
      .def("errorsEmpty", []() { throw std::runtime_error("The errorsEmpty function has been removed. Use emptyFileMessages instead."); })
      .def("getErrors", [](FilterFile& ff) { throw std::runtime_error("The getErrors function has been removed. Use getFileMessages instead."); })

      .def("getMergeMessages", [](FilterFile& ff) {
        auto mm = ff.getMergeMessages();
        py::list pymm;
        for (auto m: mm) {
          pymm.append(m);
        }
        return pymm;
      })

      .def("getFileMessages", [](FilterFile& ff) {
        auto fm = ff.getFileMessages();
        py::list pyfm;
        for (auto m: fm) {
          pyfm.append(m);
        }
        return pyfm;
      })
      .def_property("correct_on_load", &FilterFile::get_correct_on_load,
                    &FilterFile::set_correct_on_load);


  py::class_<FilterModule>(m, "FilterModule")
      .def(py::init())
      .def(py::init<const char *, double>())
      .def("checkDesign", [](FilterModule &fm) {
        std::vector<message_t> message_vec;
        fm.checkDesign(&message_vec);
        py::list pyml;
        for (auto m: message_vec) {
          pyml.append(m);
        }
        return pyml;
      })
      .def("getName", &FilterModule::getName)
      .def("setName", &FilterModule::setName)
      .def("getFSample", &FilterModule::getFSample)
      .def("setFSample", &FilterModule::setFSample)
      .def("defaultFSample", &FilterModule::defaultFSample)
      .def("__getitem__", static_cast<const FilterSection &(FilterModule::*)(int) const>(&FilterModule::operator[]),
           py::return_value_policy::reference)
      .def("__setitem__", [](FilterModule &fm, int i, FilterSection &fs){
        fm[i] = fs;
      })
      .def("__lt__", static_cast<bool (FilterModule::*)(const FilterModule &) const>(&FilterModule::operator<))
      .def("__eq__", static_cast<bool (FilterModule::*)(const FilterModule &) const>(&FilterModule::operator==))
      .def("SaveSections", &FilterModule::SaveSections)
      .def("RestoreSections", &FilterModule::RestoreSections)
      .def("RestoreSectionsEmpty", &FilterModule::RestoreSectionsEmpty)
      .def("errorMessage", [](FilterModule &fm, const char * msg) { throw std::runtime_error("The errorMessage function has been removed. Creating errors is done internally."); })
      .def("clearErrors", []() { throw std::runtime_error("The clearErrors function has been removed. checkDesign and changeSampleRate have been modified to return any errors."); })
      .def("errorsEmpty", []() { throw std::runtime_error("The errorsEmpty function has been removed. checkDesign and changeSampleRate have been modified to return any errors."); })
      .def("changeSampleRate", [](FilterModule &fm, double newSample) {
        std::vector<message_t> message_vec;
        fm.changeSampleRate(newSample, &message_vec);
        py::list pyml;
        for (auto m: message_vec) {
          pyml.append(m);
        }
        return pyml;
      })
      .def("getErrors", [](FilterModule &fm) { throw std::runtime_error("The getErrors function has been removed. checkDesign and changeSampleRate have been modified to return any errors."); });

  py::class_<FilterSection>(m, "FilterSection")
      .def(py::init())
      .def(py::init<double, int>())
      .def("empty", &FilterSection::empty)
      .def("designEmpty", &FilterSection::designEmpty)
      .def("check", &FilterSection::check)
      .def("valid", &FilterSection::valid)
      .def("update", &FilterSection::update)
      .def("add", &FilterSection::add)
      .def("getIndex", &FilterSection::getIndex)
      .def("setIndex", &FilterSection::setIndex)
      .def("getName", &FilterSection::getName)
      .def("setName", &FilterSection::setName)
      .def("getDesign", &FilterSection::getDesign)
      .def("refDesign", &FilterSection::refDesign)
      .def("setDesign", static_cast<void (FilterSection::*)(const std::string)>(&FilterSection::setDesign))
      //.def("setDesign", static_cast<void (FilterSection::*)(const char *, bool, int)>(&FilterSection::setDesign))
      //.def("filter", static_cast<const FilterDesign& (FilterSection::*)() const> (&FilterSection::filter))
      .def("filter", static_cast< FilterDesign& (FilterSection::*)() > (&FilterSection::filter),
           py::return_value_policy::reference)
      .def("getInputSwitch", &FilterSection::getInputSwitch)
      .def("setInputSwitch", &FilterSection::setInputSwitch)
      .def("getOutputSwitch", &FilterSection::getOutputSwitch)
      .def("setOutputSwitch", &FilterSection::setOutputSwitch)
      .def("getRamp", &FilterSection::getRamp)
      .def("setRamp", &FilterSection::setRamp)
      .def("getTolerance", &FilterSection::getTolerance)
      .def("setTolerance", &FilterSection::setTolerance)
      .def("getTimeout", &FilterSection::getTimeout)
      .def("setTimeout", &FilterSection::setTimeout)
      .def("getHeader", &FilterSection::getHeader)
      .def("setHeader", &FilterSection::setHeader)
      //.def("splitCmd", &FilterSection::splitCmd)
      .def("setGainOnly", &FilterSection::setGainOnly)
      .def("getGainOnly", &FilterSection::getGainOnly)
      .def("setGainOnlyGain", &FilterSection::setGainOnlyGain)
      .def("getGainOnlyGain", &FilterSection::getGainOnlyGain);

  m.attr("kMaxFilterSections") = kMaxFilterSections;
  m.attr("kMaxFilterSOS") = kMaxFilterSOS;
  m.attr("kModuleDef") = kModuleDef;
  m.attr("kModuleSamplingDef") = kModuleSamplingDef;
  m.attr("kSectionDesignDef") = kSectionDesignDef;
  m.attr("kMagic") = kMagic;
  m.attr("kMatlabMagic") = kMatlabMagic;


  py::enum_<input_switching>(m, "input_switching")
      .value("kAlwaysOn", input_switching::kAlwaysOn)
      .value("kZeroHistory", input_switching::kZeroHistory)
      .export_values();
  py::enum_<output_switching>(m, "output_switching")
    .value("kImmediately", output_switching::kImmediately)
    .value("kRamp", output_switching::kRamp)
    .value("kInputCrossing", output_switching::kInputCrossing)
    .value("kZeroCrossing", output_switching::kZeroCrossing)
    .export_values();


}