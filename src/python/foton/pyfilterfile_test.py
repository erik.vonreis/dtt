import pydmtsigp as sigp
import pyfilterfile as filterfile
import unittest

class FilterFileTest(unittest.TestCase):
    def setUp(self):
        self.fname = "test_filter.txt"
        self.ff = filterfile.FilterFile()

    def test_filename(self):
        self.ff = filterfile.FilterFile()
        self.ff.setFilename(self.fname)
        self.assertEqual(self.ff.getFilename(), self.fname)

    def test_valid(self):
        self.assertTrue(self.ff.valid())

    def test_read_file(self):
        success = self.ff.read_file(self.fname)
        self.assertTrue(success)
        self.assertEqual(self.ff.getFilename(), self.fname)
        self.assertTrue(self.ff.valid())
        success = self.ff.read_file("not_a_file")
        self.assertFalse(success)
        mods = self.ff.modules()
        self.assertEqual(len(mods), 1)
        self.assertEqual(mods[0].getName(), "BOUNCE_ALL")

    def test_clear(self):
        self.ff.read_file(self.fname)
        self.assertEqual(len(self.ff.modules()), 1)
        self.ff.clear()
        self.assertEqual(len(self.ff.modules()), 0)

    def test_add(self):
        self.ff.add("newmod")
        mod = self.ff.modules()
        self.assertEqual(len(mod), 1)
        self.assertEqual(mod[0].getName(), "newmod")
        self.assertEqual(mod[0].getFSample(), 1.0)

    def test_remove(self):
        self.ff.read_file(self.fname)
        self.ff.remove("BOUNCE_ALL")
        self.assertEqual(len(self.ff.modules()), 0)


    def test_find(self):
        self.ff.read_file(self.fname)
        mod = self.ff.find("BOUNCE_ALL")
        self.assertEqual(mod.getName(), "BOUNCE_ALL")
        mod = self.ff.find("NOTAMOD")
        self.assertIsNone(mod)

    def test_update(self):
        self.ff.read_file(self.fname)
        self.assertTrue(self.ff.update())

    def test_read_string(self):
        with open(self.fname, "rt") as f:
            buf = f.read()
        success = self.ff.read_string(buf)
        self.assertTrue(success)
        mod = self.ff.find("BOUNCE_ALL")
        self.assertIsNotNone(mod)

    def test_write_file(self):
        self.ff.read_file(self.fname)
        temp_file = "/tmp/test_filter.txt"
        self.ff.write_file(temp_file)
        fftmp = filterfile.FilterFile()
        fftmp.read_file(temp_file)
        mod = fftmp.find("BOUNCE_ALL")
        self.assertIsNotNone(mod)


    def test_write_string(self):
        with open(self.fname, "rt") as f:
            buf = f.read()
        self.ff.read_file(self.fname)
        buf2 = self.ff.write_string()
        tmp_file = "/tmp/test_filter.txt"
        with open(tmp_file, "wt") as f:
            f.write(buf2)
        ff2 = filterfile.FilterFile()
        ff2.read_file(tmp_file)
        # mod1 = self.ff.find("BOUNCE_ALL")
        # mod2 = ff2.find("BOUNCE_ALL")
        mod1 = self.ff.modules()[0]
        mod2 = ff2.modules()[0]
        print(f"pymod1={mod1.getName()}")
        print(f"pymod2={mod2.getName()}")
        sec1 = mod1[0]
        sec2 = mod2[0]
        self.assertIsInstance(sec1, filterfile.FilterSection)
        self.assertIsInstance(sec2, filterfile.FilterSection)
        print(f"name={sec1.getName()}")
        print(f"name={sec2.getName()}")
        self.assertTrue(sigp.iircmp(sec1.filter().get(), sec1.filter().get()))

    def test_get_sections_from_modules(self):
        with open(self.fname, "rt") as f:
            buf = f.read()
        self.ff.read_file(self.fname)
        modules = self.ff.modules()
        print(len(modules))
        mod1 = modules[0]
        sec1 = mod1[0]
        self.assertIsInstance(sec1, filterfile.FilterSection)
        print(f"name={sec1.getName()}")

    def test_convertFilters(self):
        with open(self.fname, "rt") as f:
            buf = f.read()
        self.ff.read_file(self.fname)
        self.ff.convertFilters(self.ff.getFSample()*2)
        buf2 = self.ff.write_string()
        self.assertNotEqual(buf, buf2)

    def test_checkFileStat(self):
        self.ff.read_file(self.fname)
        self.assertTrue(self.ff.checkFileStat())

    def test_updateFileStat(self):
        """
        Just check we don't throw any exceptions
        :return:
        """
        self.ff.updateFileStat(self.fname)

    def test_getRealFilename(self):
        real_name = self.ff.getRealFilename(self.fname, ".")
        self.assertEqual(real_name, f"./{self.fname}")

    def test_legacy_write(self):
        self.assertFalse(self.ff.LegacyWrite())
        self.ff.setLegacyWrite(True)
        self.assertTrue(self.ff.LegacyWrite())

    def test_AnySampleRate(self):
        self.assertTrue(self.ff.AnySampleRate())
        self.ff.SetAnySampleRate(False)
        self.assertFalse(self.ff.AnySampleRate())

    def test_sos0Allowed(self):
        self.assertTrue(self.ff.sos0Allowed())
        self.ff.set0SosAllowed(False)
        self.assertFalse(self.ff.sos0Allowed())

    def test_getFSampleStr(self):
        self.ff.read_file(self.fname)
        self.assertEqual(self.ff.getFSampleStr(), "16384")

    def test_FSample(self):
        self.ff.read_file(self.fname)
        self.assertEqual(self.ff.getFSample(), 16384)
        self.ff.setFSample(2048)
        self.assertEqual(self.ff.getFSample(), 2048)
        self.ff.setFSample(1024.0)
        self.assertEqual(self.ff.getFSample(), 1024)
        self.ff.setFSample("4096")
        self.assertEqual(self.ff.getFSample(), 4096)

    def test_merge(self):
        """
        Test merge from matlab file
        Since we don't have a matlab file, we test for failure
        when trying to merge another type of filter file.

        Will also print out ("Not a Matlab Design file")
        :return:
        """
        self.ff.read_file(self.fname)
        ret_code = self.ff.merge(self.fname)
        self.assertEqual(ret_code, -1)

        warnings = list(filter(lambda m: m.get_level() == filterfile.message_level_t.WARNING, self.ff.getMergeMessages()))
        warnings = list(map(lambda m: m.get_fmt_str(), warnings))
        errors = list(filter(lambda m: m.get_level() == filterfile.message_level_t.ERROR, self.ff.getMergeMessages()))
        errors = list(map(lambda m: m.get_fmt_str(), errors))
        print(f"merge warnings={warnings}")
        print(f"merge errors={errors}")
        self.assertFalse(warnings)
        self.assertFalse(errors)

    def test_errors(self):
        self.ff.read_file(self.fname)

        warnings = list(filter(lambda m: m.get_level() == filterfile.message_level_t.WARNING, self.ff.getMergeMessages()))
        warnings = list(map(lambda m: m.get_fmt_str(), warnings))
        errors = list(filter(lambda m: m.get_level() == filterfile.message_level_t.ERROR, self.ff.getMergeMessages()))
        errors = list(map(lambda m: m.get_fmt_str(), errors))
        print(f"merge warnings={warnings}")
        print(f"merge errors={errors}")
        self.assertFalse(warnings)
        self.assertFalse(errors)

    def test_correct_on_load(self):
        ffx = filterfile.FilterFile()
        mods = ffx.modules()
        self.assertEqual(len(mods), 0)

        ffx.setFSample(2**14)
        f=0.00175
        design = f'butter("LowPass",4,{f:g})'
        modname = "butter"
        ffx.add(modname, 2**14)

        mods = ffx.modules()
        self.assertEqual(len(mods), 1)

        fm = ffx.find(modname)
        print(f"fm={fm}")
        sec = fm[1]
        sec.setDesign(design)

        ff_text2 = ffx.write_string()

        ff2 = filterfile.FilterFile()
        ff2.correct_on_load = True
        ff2.read_string(ff_text2)

        errors = [m.get_fmt_str() for m in ff2.getFileMessages() if m.get_level() == filterfile.message_level_t.ERROR]
        print(f"col errors={errors}")
        self.assertEqual(len(errors), 1)


        ff3 = filterfile.FilterFile()
        ff3.correct_on_load = False
        ff3.read_string(ff_text2)

        errors = [m.get_fmt_str() for m in ff3.getFileMessages() if m.get_level() == filterfile.message_level_t.ERROR]
        print(f"col errors={errors}")
        self.assertEqual(len(errors), 0)




class FilterModuleTest(unittest.TestCase):
    def setUp(self):
        self.ff=filterfile.FilterFile()
        self.ff.read_file("test_filter.txt")
        self.mod = self.ff.modules()[0]
        self.mod2 = filterfile.FilterModule()
        self.mod2.setName("MOD2")

    def test_checkDesign(self):
        errors = self.mod.checkDesign()
        self.assertFalse(errors)

    def test_name(self):
        self.assertEqual(self.mod.getName(), "BOUNCE_ALL")
        self.mod.setName("BOUNCE_NONE")
        self.assertEqual(self.mod.getName(), "BOUNCE_NONE")

    def test_FSample(self):
        self.mod.setFSample(2048)
        self.assertEqual(self.mod.getFSample(), 2048)
        self.mod.defaultFSample()
        self.assertEqual(self.mod.getFSample(), 16384)

    def test_accessor(self):
        fs = self.mod[0]
        self.assertIsInstance(fs, filterfile.FilterSection)
        fs.setName("NOTASECTION")
        self.assertEqual(self.mod[0].getName(), "NOTASECTION")

        fs2 = filterfile.FilterSection()
        fs2.setName("NEWSECTION")
        self.mod[2] = fs2
        self.assertEqual(self.mod[2].getName(), "NEWSECTION")

    def test_less_than(self):
        self.assertTrue(self.mod < self.mod2)

    def test_equal(self):
        self.assertTrue(self.mod == self.mod)

    def test_save_restore(self):
        self.mod.SaveSections()
        self.mod[0].setName("NOTASECTION")
        self.assertEqual(self.mod[0].getName(), "NOTASECTION")
        self.assertFalse(self.mod.RestoreSectionsEmpty())
        self.mod.RestoreSections()
        self.assertEqual(self.mod[0].getName(), "bp9-11")
        self.assertTrue(self.mod.RestoreSectionsEmpty())

#    def test_errors(self):
#        te = "test error"
#        self.mod.errorMessage(te)
#        errors = self.mod.getErrors()
#        self.assertEqual(errors[-1], te)
#        self.assertFalse(self.mod.errorsEmpty())
#        self.mod.clearErrors()
#        self.assertTrue(self.mod.errorsEmpty())

    def test_changeSampleRate(self):
        """
        Note this changes the filter coefficients themselves to match the
        design string at the new rate but does *not* change
        the fSample value, so filters and fSample can get out of sync.
        :return:
        """
        old_rate = self.mod.getFSample()
        errors = self.mod.changeSampleRate(old_rate * 2)
        self.assertEqual(old_rate, self.mod.getFSample())
        self.assertFalse(errors)


class FilterSectionTest(unittest.TestCase):
    def setUp(self):
        self.ff = filterfile.FilterFile()
        self.ff.read_file("test_filter.txt")
        self.mod = self.ff.modules()[0]
        self.sec = self.mod[0]

    def test_empty(self):
        self.assertFalse(self.sec.empty())
        self.assertTrue(self.mod[9].empty())

    def test_designEmpty(self):
        self.assertFalse(self.sec.designEmpty())
        self.assertTrue(self.mod[9].designEmpty())

    def test_check(self):
        self.assertTrue(self.sec.check())
        before_sos = sigp.iir2z_sos(self.mod[0].filter().get())
        before_rate = self.mod[0].filter().getFSample()
        self.mod.changeSampleRate(self.mod.getFSample()*2)
        after_sos = sigp.iir2z_sos(self.mod[0].filter().get())
        after_rate = self.mod[0].filter().getFSample()

        self.assertNotEqual(after_sos, before_sos)
        self.assertNotEqual(before_rate, after_rate)
        self.assertEqual(after_rate, self.sec.filter().getFSample())
        self.assertTrue(self.sec.check())

        self.sec.filter().setFSample(before_rate)
        self.assertFalse(self.sec.check())
        self.sec.update()
        self.assertTrue(self.sec.check())

    def test_filter(self):
        """
        Check that filter returns a reference to a FilterDesign object
        :return:
        """
        f = self.sec.filter()
        self.assertIsInstance(f, sigp.FilterDesign)
        orig_design = f.getFilterSpec()
        f.gain(2)
        target_design = orig_design+"gain(2)"
        f2 = self.sec.filter()
        self.assertEqual(f2.getFilterSpec(), target_design)

    def test_valid(self):
        self.assertTrue(self.sec.valid())

    def test_add(self):
        old_design = self.sec.getDesign()
        self.sec.add("gain(2)")
        self.assertEqual(self.sec.getDesign(), old_design+"gain(2)")
        self.assertTrue(self.sec.check())

    def test_index(self):
        self.sec.setIndex(1)
        self.assertEqual(self.sec.getIndex(), 1)
        self.sec.setIndex(2)
        self.assertEqual(self.sec.getIndex(), 2)

    def test_name(self):
        self.assertEqual(self.sec.getName(), "bp9-11")
        self.sec.setName("notasection")
        self.assertEqual(self.sec.getName(), "notasection")

    def test_design(self):
        design = "gain(2)"
        self.sec.setDesign(design)
        self.assertEqual(self.sec.getDesign(), design)
        self.assertEqual(self.sec.refDesign(), design)

    def test_inputSwitch(self):
        inp_switch = self.sec.getInputSwitch()
        self.assertEqual(inp_switch, filterfile.input_switching.kZeroHistory)
        self.sec.setInputSwitch(filterfile.input_switching.kAlwaysOn)
        self.assertEqual(self.sec.getInputSwitch(), filterfile.input_switching.kAlwaysOn)

    def test_outputSwitch(self):
        self.assertEqual(self.sec.getOutputSwitch(), filterfile.output_switching.kImmediately)
        self.sec.setOutputSwitch(filterfile.output_switching.kZeroCrossing)
        self.assertEqual(self.sec.getOutputSwitch(), filterfile.output_switching.kZeroCrossing)

    def test_ramp(self):
        new_ramp = self.sec.getRamp() + 1
        self.assertEqual(new_ramp, 1)
        self.sec.setRamp(new_ramp)
        self.assertEqual(self.sec.getRamp(), new_ramp)

    def test_tolerance(self):
        new_tol = self.sec.getTolerance() + 0.1
        self.assertAlmostEqual(new_tol, 0.1)
        self.sec.setTolerance(new_tol)
        self.assertEqual(new_tol, self.sec.getTolerance())

    def test_timeout(self):
        new_to = self.sec.getTimeout() + 1
        self.assertEqual(new_to, 1)
        self.sec.setTimeout(new_to)
        self.assertEqual(new_to, self.sec.getTimeout())

    def test_header(self):
        old_header = self.sec.getHeader()
        new_header = "a header"
        self.assertNotEqual(old_header, new_header)
        self.sec.setHeader(new_header)
        self.assertEqual(self.sec.getHeader(), new_header)

    def test_gain_only(self):
        self.assertFalse(self.sec.getGainOnly())
        self.sec.setGainOnly(True)
        self.assertTrue(self.sec.getGainOnly())
        old_gain = self.sec.getGainOnlyGain()
        new_gain = "3.14159"
        self.assertNotEqual(new_gain, old_gain)
        self.sec.setGainOnlyGain(new_gain)
        self.assertEqual(self.sec.getGainOnlyGain(), new_gain)

if __name__ == '__main__':
    unittest.main()
