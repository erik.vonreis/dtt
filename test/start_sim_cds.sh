#!/bin/bash

set -x
set -e

# setup and run local simulated frontend, local DC, daqd, awgtpman.
# this assumes there's a fresh build of advligorts in
# ../../advligorts/.build
# also assumes the current user is a member of the advligorts group


export TARGET=/tmp
mkdir -p "${TARGET}/target/gds/param"
workdir="../../advligorts/.build"

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd "${SCRIPT_DIR}/${workdir}"
# start models
src/fe_stream_test/fe_simulation/fe_simulated_streams -i /tmp/target/gds/param -s shm:// -R 1 -M /tmp/target/master -a &
model_pid=$!
sleep 1

# start local DC
src/local_dc/local_dc -s mod5 -d '/tmp/target/gds/param' &
sleep 2

# start daqd
src/daqd/daqd -c "${SCRIPT_DIR}/sim_daqdrc" >> /dev/null &
daqd_pid=$!

# start awg
src/gds/awgtpman/awgtpman -s mod5 -m 'shm' -g &
awg_pid=$!

finish="echo closing; kill ${awg_pid}; kill ${daqd_pid}; kill ${model_pid}"

trap "${finish}; exit 0;" SIGINT
trap "${finish}" EXIT

while true ; do
  echo .
  sleep 1
done
